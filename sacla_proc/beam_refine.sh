for x in -9  -6  -3   0   3   6   9
do
 for y in -9  -6  -3   0   3   6   9
 do
  test -e ${x}_${y}.geom && continue
  python ../shift_beam.py ../kuntaro_ini.geom $x $y
  qsub -v xy=${x}_${y} -N i_${x}_${y} index_dirax.sh
 done
done

