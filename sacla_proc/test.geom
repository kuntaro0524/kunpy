; CrystFEL geometry file produced by prepare_cheetah_api2.py
;   Takanori Nakane (tnakane@mrc-lmb.cam.ac.uk)
; Detector ID: MPCCD-8-2-002-1
; for tiled but NOT reassembled images (512x8192 pixels)

clen = 0.1000               ; 100.0 mm camera length. You SHOULD optimize this!
res = 20000                 ; = 1 m /50 micron
;badrow_direction = x
;max_adu = 250000           ; should NOT be used. see best practice on CrystFEL's Web site
data = /%/data
;mask = /metadata/pixelmask ; this does not work in CrystFEL 0.6.2 (reported bug)
mask_good = 0x00            ; instead, we can specify bad regions below if necessary
mask_bad = 0xFF
photon_energy = /%/photon_energy_ev ; roughly 8951.0 eV

; Definitions for geoptimiser
rigid_group_q1 = q1
rigid_group_q2 = q2
rigid_group_q3 = q3
rigid_group_q4 = q4
rigid_group_q5 = q5
rigid_group_q6 = q6
rigid_group_q7 = q7
rigid_group_q8 = q8

rigid_group_collection_connected = q1,q2,q3,q4,q5,q6,q7,q8
rigid_group_collection_independent = q1,q2,q3,q4,q5,q6,q7,q8

; Panel definitions
; sensor MPCCD-8-2-002-1
q1/adu_per_eV = 0.001117
q1/min_fs = 0
q1/min_ss = 0
q1/max_fs = 511
q1/max_ss = 1023
q1/fs = -0.001309x -0.999999y
q1/ss = 0.999999x -0.001309y
q1/corner_x = 31.984600
q1/corner_y = 1033.576016

; sensor MPCCD-8-2-002-2
q2/adu_per_eV = 0.001117
q2/min_fs = 0
q2/min_ss = 1024
q2/max_fs = 511
q2/max_ss = 2047
q2/fs = -0.000593x -1.000000y
q2/ss = 1.000000x -0.000593y
q2/corner_x = 33.113000
q2/corner_y = 500.320000

; sensor MPCCD-8-2-002-3
q3/adu_per_eV = 0.001117
q3/min_fs = 0
q3/min_ss = 2048
q3/max_fs = 511
q3/max_ss = 3071
q3/fs = -0.000541x -1.000000y
q3/ss = 1.000000x -0.000541y
q3/corner_x = -15.983740
q3/corner_y = -34.702000

; sensor MPCCD-8-2-002-4
q4/adu_per_eV = 0.001117
q4/min_fs = 0
q4/min_ss = 3072
q4/max_fs = 511
q4/max_ss = 4095
q4/fs = -0.002531x -0.999997y
q4/ss = 0.999997x -0.002531y
q4/corner_x = -15.655699
q4/corner_y = -569.976016

; sensor MPCCD-8-2-002-5
q5/adu_per_eV = 0.001117
q5/min_fs = 0
q5/min_ss = 4096
q5/max_fs = 511
q5/max_ss = 5119
q5/fs = -0.000817x +1.000000y
q5/ss = -1.000000x -0.000817y
q5/corner_x = 15.840100
q5/corner_y = 569.550000

; sensor MPCCD-8-2-002-6
q6/adu_per_eV = 0.001117
q6/min_fs = 0
q6/min_ss = 5120
q6/max_fs = 511
q6/max_ss = 6143
q6/fs = 0.000611x +1.000000y
q6/ss = -1.000000x +0.000611y
q6/corner_x = 16.770400
q6/corner_y = 36.363999

; sensor MPCCD-8-2-002-7
q7/adu_per_eV = 0.001117
q7/min_fs = 0
q7/min_ss = 6144
q7/max_fs = 511
q7/max_ss = 7167
q7/fs = 0.000000x +1.000000y
q7/ss = -1.000000x +0.000000y
q7/corner_x = -33.000000
q7/corner_y = -498.000000

; sensor MPCCD-8-2-002-8
q8/adu_per_eV = 0.001117
q8/min_fs = 0
q8/min_ss = 7168
q8/max_fs = 511
q8/max_ss = 8191
q8/fs = -0.000314x +1.000000y
q8/ss = -1.000000x -0.000314y
q8/corner_x = -32.367400
q8/corner_y = -1032.876016

