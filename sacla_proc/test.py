import os,sys,numpy,scipy
import pandas as pd
import glob

stream_files=glob.glob("./*stream")

def read_cell(stream_file):
	for l in open(stream_file):
		if "Cell" in l:
			cols=l.split()
			return stream_file,cols[2],cols[3],cols[4],cols[6],cols[7],cols[8]
	return cell_parm

cell_array=[]
for stream_file in stream_files:
	array_data=read_cell(stream_file)
	print(array_data)
	cell_array.append(array_data)

ppp=pd.DataFrame(cell_array)

ppp.to_csv("cell_param.csv")
