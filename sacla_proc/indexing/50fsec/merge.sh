#PBS -l nodes=1:ppn=6
#PBS -j oe

#refmtz=/work/hirata/200312_eh5/2oh6.mtz

#export PATH=~/program/crystfel/150910_0.6.1+6aeb5402_mod/build/bin/:$PATH
export PATH=~hirata/program/prime_150529/build/bin:$PATH
#export PATH=/home/sacla_sfx_app/local/bin:$PATH
source /home/sacla_sfx_app/setup.sh
export PATH=~sacla_sfx_app/packages/crystfel-0.9.0/build:$PATH

dmin=2.5

cd $PBS_O_WORKDIR/

if test "$st" == ""
then
 st=$PWD/all.stream
 while read l
 do
  cat $l >> $st
 done < files.lst

fi

yamtbx.python ~hirata/program/yam_scripts/get_cells.py $st > cells.dat &

process_hkl -y 4/mmm -i $st -o processed.hkl &
process_hkl -y 4/mmm -i $st --scale --stat=scale_cc.dat -o scaled.hkl &

mkdir halfstat
cd halfstat
process_hkl -y 4/mmm -i $st --even-only -o 1.hkl &
process_hkl -y 4/mmm -i $st --odd-only -o 2.hkl &
process_hkl -y 4/mmm -i $st --even-only --scale -o s1.hkl &
process_hkl -y 4/mmm -i $st --odd-only --scale -o s2.hkl &
cd ..

wait

R --vanilla --slave <<+
d <- read.table("cells.dat", h=T)
a <- median(c(d\$a,d\$b))
s <- sprintf("CRYST1%9.3f%9.3f%9.3f%7.2f%7.2f%7.2f P 43 21 2      ", a,a,median(d\$c), 90, 90,90)
cat(s, file="median_cell.pdb")
+

cellpdb=$PWD/median_cell.pdb
cd halfstat

compare_hkl  1.hkl  2.hkl -y 4/mmm -p $cellpdb --lowres=20 --highres=$dmin --nshells=40 --fom=CC --shell-file=shells_CC.dat > stat.log 2>&1
compare_hkl s1.hkl s2.hkl -y 4/mmm -p $cellpdb --lowres=20 --highres=$dmin --nshells=40 --fom=CC --shell-file=shells_scaled_CC.dat > stat_scaled.log 2>&1

cd ..

check_hkl -y 4/mmm -p $cellpdb --lowres=20 --highres=$dmin --nshells=40 processed.hkl --shell-file=shells.dat
check_hkl -y 4/mmm -p $cellpdb --lowres=20 --highres=$dmin --nshells=40 scaled.hkl --shell-file=shells_scaled.dat

yamtbx.python ~hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/hkl2various.py $cellpdb processed.hkl dmin=$dmin dmax=20
yamtbx.python ~hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/hkl2various.py $cellpdb scaled.hkl dmin=$dmin dmax=20

ctruncate  -mtzin scaled.mtz -mtzout scaled_ctruncate.mtz -colin "/*/*/[I,SIGI]" > ctruncate_scaled.log &
ctruncate  -mtzin processed.mtz -mtzout processed_ctruncate.mtz -colin "/*/*/[I,SIGI]" > ctruncate_processed.log &
wait

~hirata/program/yam_scripts/copy_free_R_flag.py -r ~/6Q8T.mtz scaled_ctruncate.mtz

#qsub -q smp -v 
#mtz=../scaled_ctruncate_copy_free.mtz sh $refscr

mkdir refine0
cd refine0
phenix.refine ../scaled_ctruncate_copy_free.mtz /home/hirata/6Q8T.pdb input.xray_data.high_resolution=2.0 strategy=rigid_body+individual_sites+individual_adp ordered_solvent=true /home/hirata/program/ccp4/ccp4-7.0/lib/data/monomers/p/PEG.cif

