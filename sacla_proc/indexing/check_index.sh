for f in *.stream
do
 awk '/Cell/{c+=1}/filename/{f+=1}END{printf("%s %.1f%% %d %d\n", "'$f'", f>0?c*100/f:0, c, f)}' $f
done

