#!/bin/bash
#PBS -l nodes=10:ppn=28
#PBS -q psmall
##PBS -l nodes=20:ppn=28
##PBS -q plarge

if [ -n "$PBS_O_WORKDIR" -a "$PBS_ENVIRONMENT" != "PBS_INTERACTIVE" ]; then
        cd $PBS_O_WORKDIR
fi

source /home/nureki/.bashrc

TARGET=all.lst
PDB=/work/hirata/201211_eh5/tst-index/06_peakparam/sfx_thr20_snr6.cell
GEOM=/work/hirata/201211_eh5/tst-index/03_xy/2_28.geom

uniq $PBS_NODEFILE | awk '{print "2/"$1}' > nodefile.$PBS_JOBID
parallel --sshloginfile nodefile.$PBS_JOBID --workdir . sh ../run-index-cmd.sh {} $PDB $GEOM $GEOM < $TARGET
#parallel -P1  qsub -v TARGET={},METHOD=$METHOD run-index-cmd.sh < $TARGET


#while read f
#do
# qsub -v TARGET=$f,METHOD="dirax" run-index-cmd.sh
#done < $TARGET
