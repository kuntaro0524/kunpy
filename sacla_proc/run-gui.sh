#!/bin/bash
source ~sacla_sfx_app/setup.sh
#export PATH="/home/sacla_sfx_app/packages/crystfel-0.8.0/build:$PATH"
#PHENIX_GUI_ENVIRONMENT=1 cctbx.python ~sacla_sfx_app/packages/cheetah_window/cheetah_window-0.8.0.py

PHENIX_GUI_ENVIRONMENT=1 cctbx.python ~sacla_sfx_app/packages/cheetah_window/cheetah_window.py \
 --bl=3 \
 --clen=100.0 \
 --queue=bl3-occupancy \
