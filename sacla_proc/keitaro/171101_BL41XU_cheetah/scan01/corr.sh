source ~/program/dials/dials-v1-6-0/dials_env.sh

cd $PBS_O_WORKDIR

for f in *_out.stream
do
 geom=${f/_out.stream/.geom}
 yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/correct_stream_nonempirical.py geom=$geom correction=L rotation_axis=-1,0,0 $f
done

