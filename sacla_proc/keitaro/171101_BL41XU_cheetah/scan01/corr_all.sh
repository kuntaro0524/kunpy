scr=$PWD/corr.sh

for f in $PWD/*/*/*_out.stream
do
 of=${f/.stream/_corrected.stream}
 test -e $of && continue
 d=`dirname $f`
 cd $d
 pwd
 qsub $scr
done
