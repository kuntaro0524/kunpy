root=/home/hirata/work/_ssrox/171101_BL41XU_cheetah
scr=$PWD/check_merge.sh

while true
do

for k in `find $root -name \*corrected.stream | sed -e "s,.*/,,"|sort|uniq|sed -e "s,_out_corrected.stream,,"`
do
 echo "kind=$k"
 d1=merge_$k
 mkdir -pv $d1
 test -e $d1/files.lst && rm $d1/files.lst

 for f in `find $root -name ${k}_out_corrected.stream`
 do
  test -e ${f/_out_corrected.stream/_cheetah.stdout} || continue
  echo $f >> $d1/files.lst
 done

 # check last run
 lastd=`ls $d1/merge_*/ -d 2> /dev/null | tail -n1`
 if [ "$lastd" != "" ]
 then
  diff $lastd/files.lst $d1/files.lst > /dev/null && continue # if no diff
 fi

 # start run
 d2=$d1/merge_`date "+%y%m%d_%H%M%S"`
 mkdir -pv $d2
 mv $d1/files.lst $d2/
 cd $d2
 qsub $scr
 cd -
done

LANG=C date
sleep 1800

done
