dst=~/UserData/_ssrox/171101_BL41XU_cheetah
src=~/work/_ssrox/171101_BL41XU_cheetah

cd $src/merge_all
mkdir -pv $dst/merge_all
for d in *
do
 test -d $d || continue
 pwd
 tar cvf $dst/merge_all/${d}.tar $d
done

cd $src/merge_tmp
mkdir -pv $dst/merge_tmp
for d in *
do
 test -d $d || continue
 pwd
 tar cvf $dst/merge_tmp/${d}.tar $d
done

cd $src/scan01
mkdir -pv $dst/scan01
for d in */
do
 cd $src/scan01/$d
 mkdir -pv $dst/scan01/$d
 for d2 in *
 do
  test -d $d2 || continue
  pwd
  tar cvf $dst/scan01/$d/${d2}.tar $d2
 done
done

cd $src/scan02
mkdir -pv $dst/scan02
for d in */
do
 cd $src/scan02/$d
 mkdir -pv $dst/scan02/$d
 for d2 in *
 do
  test -d $d2 || continue
  pwd
  tar cvf $dst/scan02/$d/${d2}.tar $d2
 done
done

