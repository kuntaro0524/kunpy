#PBS -l nodes=1:ppn=4
#PBS -q smp
#PBS -j oe

export PATH=/home/hirata/program/crystfel/crystfel-0.6.3/build/bin:$PATH

dmin=2
dmax=50

cd $PBS_O_WORKDIR

st=all.stream
#cat ../../$name/*/run_${n}/$stname > $st

while read l
do
 bzcat $l >> $st
done < files.lst

yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/get_cells.py $st > cells.dat &
process_hkl -y mmm -i $st -o processed.hkl &
process_hkl -y mmm -i $st --scale --stat=scale_cc.dat -o scaled.hkl &
process_hkl -y 222 -i $st -o processed_ano.hkl &
process_hkl -y 222 -i $st --scale --stat=scale_cc_ano.dat -o scaled_ano.hkl &

mkdir halfstat
cd halfstat
process_hkl -y mmm -i ../$st --even-only -o 1.hkl &
process_hkl -y mmm -i ../$st --odd-only -o 2.hkl &
process_hkl -y mmm -i ../$st --even-only --scale -o s1.hkl &
process_hkl -y mmm -i ../$st --odd-only --scale -o s2.hkl &
cd ..

wait

R --vanilla --slave <<+
d <- read.table("cells.dat", h=T)
s <- sprintf("CRYST1%9.3f%9.3f%9.3f%7.2f%7.2f%7.2f P 21 21 21     ", median(d\$a), median(d\$b), median(d\$c),90, 90,90)
cat(s, file="median_cell.pdb")
+

cellpdb=$PWD/median_cell.pdb
cd halfstat

compare_hkl 1.hkl 2.hkl -y mmm -p $cellpdb --lowres=$dmax --highres=$dmin --nshells=40 --fom=CC --shell-file=shells_CC.dat > stat.log 2>&1
compare_hkl s1.hkl s2.hkl -y mmm -p $cellpdb --lowres=$dmax --highres=$dmin --nshells=40 --fom=CC --shell-file=shells_scaled_CC.dat > stat_scaled.log 2>&1

cd ..

check_hkl -y mmm -p $cellpdb --lowres=$dmax --highres=$dmin --nshells=40 processed.hkl --shell-file=shells.dat
check_hkl -y mmm -p $cellpdb --lowres=$dmax --highres=$dmin --nshells=40 scaled.hkl --shell-file=shells_scaled.dat

yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/hkl2various.py median_cell.pdb processed.hkl dmin=$dmin dmax=$dmax &
yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/hkl2various.py median_cell.pdb scaled.hkl dmin=$dmin dmax=$dmax &
yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/hkl2various.py median_cell.pdb processed_ano.hkl dmin=$dmin dmax=$dmax &
yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/hkl2various.py median_cell.pdb scaled_ano.hkl dmin=$dmin dmax=$dmax &
wait

#mkdir refine0_scaled
#cd refine0_scaled
#qsub -v mtz=../scaled.mtz ~/bin/refine0.sh
#cd ..

ctruncate  -mtzin scaled.mtz -mtzout scaled_ctruncate.mtz -colin "/*/*/[I,SIGI]" > ctruncate_scaled.log &
ctruncate  -mtzin processed.mtz -mtzout processed_ctruncate.mtz -colin "/*/*/[I,SIGI]" > ctruncate_processed.log &
wait

