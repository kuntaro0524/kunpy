root=/isilon/users/umena_7822/umena_7822/171019_BL41XU/1F
shika_cutoff=500

for d in $root/???/*
do
 test -d $d/_spotfinder || continue
 name=`echo $d | sed -e "s,$root/,,; s,/,-,g"`
 mkdir -pv $name
 awk '/cbf/&&$2>=200{print $1}' $d/_spotfinder/cheetah.dat > $name/hits.lst
 /oys/xtal/cctbx/snapshots/upstream/build/bin/yamtbx.python /oys/xtal/cctbx/snapshots/upstream/modules/yamtbx/dataproc/crystfel/command_line/prep_geom_for_pilatus.py `head -n1 $name/hits.lst`
 mv -v pilatus.geom $name/
done
