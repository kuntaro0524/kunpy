#$ -j y
#$ -V
#$ -S /bin/bash
#$ -cwd
#$ -pe par 28

for slog in ~/171019_BL41XU/1F/*/*/diffscan.log
do
 d=`dirname $slog`
 wd=$d/_spotfinder
 test -e $wd || mkdir $wd
 cd $wd
 /oys/xtal/cheetah/eiger-zmq/bin/cheetah.local_singles $d/*cbf --nproc=14
done
