#for slog in `find ~/171019_BL41XU/1F -type f -name diffscan.log`
for slog in ~/171019_BL41XU/1F/*/*/diffscan.log
do
 d=`dirname $slog`
 wd=$d/_spotfinder
 test -e $wd || mkdir $wd
 cat <<+ > $wd/cheetah.sh
#$ -j y
#$ -wd $wd
#$ -pe par 28
/oys/xtal/cheetah/eiger-zmq/bin/cheetah.local_singles ../*cbf --nproc=14
+
 qsub $wd/cheetah.sh
done
