root=/home/hirata/work/_ssrox/171105_BL41XU/cheetah
scr=$PWD/check_merge.sh

for k in 00 45 00+45
do
 echo "kind=$k"
 d1=merge_$k
 mkdir -pv $d1
 test -e $d1/files.lst && rm $d1/files.lst

 for f in `test "$k" == "00+45" && find $root/38* -name \*_out_corrected.stream || find $root/38* -name \*${k}_out_corrected.stream`
 do
  test -e ${f/_out_corrected.stream/_cheetah.stdout} || continue
  echo $f >> $d1/files.lst
 done

 # start run
 cd $d1
 qsub $scr
 cd -
done

