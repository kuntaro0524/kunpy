dst=~/UserData/_ssrox/171105_BL41XU/cheetah
src=~/work/_ssrox/171105_BL41XU/cheetah

mkdir -pv $dst
cd $src
pwd
cp -pv * $dst/
pwd
tar cvf $dst/predrefine_cheetah.tar predrefine_cheetah

for d0 in 383 384 385 merge_1 merge_2 merge_2_exwrong merge_2_exwrong.org merge_2_exwrong_bystep
do
 cd $src/$d0
 mkdir -pv $dst/$d0
 for d in *
 do
  test -d $d || continue
  pwd
  tar cvf $dst/$d0/${d}.tar $d
 done
done

for d0 in crystfel_2
do
 cd $src/$d0
 for d in */
 do
  cd $src/$d0/$d
  mkdir -pv $dst/$d0/$d
  for d2 in *
  do
   test -d $d2 || continue
   pwd
   tar cvf $dst/$d0/$d/${d2}.tar $d2
  done
 done
done
