n=2
root=/home/hirata/work/_ssrox/171105_BL41XU/cheetah
geom=$root/predrefine_cheetah/01_00-predrefine.geom

for d in $root/38*/*
do
 test -d $d || continue
 rd=`echo $d | sed -e "s,$root/,,"`
 test -e $rd && continue
 mkdir -pv $rd
 ls  $d/*h5 > $rd/files.lst
 test -s $rd/files.lst || continue
 cat <<+ > $rd/run_${n}.sh
#PBS -l nodes=1:ppn=14
#PBS -o $rd/crystfel.out
#PBS -j oe

cd $PWD/$rd
/home/sacla_sfx_app/local/bin/indexamajig -g $geom  --peaks=peakfinder8 --threshold=100 -o out.stream -j \$NCPUS -i files.lst -p ~/3wu2_cell.pdb --indexing=dirax,mosflm --int-radius=4,5,7
rm -fr indexamajig.*
grep -c Cell out.stream > indexed.cnt

source ~/program/dials/dials-v1-6-0/dials_env.sh
yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/correct_stream_nonempirical.py geom=$geom correction=L rotation_axis=-1,0,0 out.stream
+
 qsub $rd/run_${n}.sh
done

