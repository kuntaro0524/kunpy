#!/bin/sh
. ~/program/dials/dials-v1-6-0/dials_env.sh
PHENIX_GUI_ENVIRONMENT=1 yamtbx.python /home/hirata/program/cheetah-biochem-fan/source/cheetah-diffscan-41xu/cheetah_dispatcher.py --data-root="../0Flash" \
 --queue=serial --crystfel_args=" -p ~/3wu2_cell.pdb --indexing=dirax,mosflm --int-radius=4,5,7"
