root=/home/hirata/work/_ssrox/171105_BL41XU/cheetah
scr=$PWD/check_merge.sh

for k in 00 45 00+45
do
 echo "kind=$k"
 d1=merge_$k
 mkdir -pv $d1
 test -e $d1/files.lst && rm $d1/files.lst

 if [ "$k" == "00+45" ]
 then
  ls $root/crystfel_2/38*/*/out_corrected.stream.bz2 > $d1/files.lst
 else
  ls $root/crystfel_2/38*/*_${k}/out_corrected.stream.bz2 > $d1/files.lst
 fi

 # start run
 cd $d1
 qsub $scr
 cd -
done

