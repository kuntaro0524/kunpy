n=4
geom=/work/hirata/_ssrox/170717_BL41XU/yamproc2/Dark/pilatus-predrefine.geom

#for d in $PWD/*/*
#for d in $PWD/?F/*
for d in $PWD/Dark/*
do
 test -s $d/hits.lst || continue
 mkdir $d/run_${n}
 cat <<+ > $d/run_${n}/run_${n}.sh
#PBS -l nodes=1:ppn=14
#PBS -o $d/run_${n}/crystfel.out
#PBS -j oe

cd $d/run_${n}
/home/hirata/program/crystfel/crystfel-0.6.3/build/bin/indexamajig \\
 -i ../hits.lst -g $geom --peaks=peakfinder8 -j \$NCPUS \\
 --indexing=dirax,mosflm -o test_${n}.stream -p $PWD/psii_cell.pdb --int-radius=4,5,7 --integration=rings-grad
yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/correct_stream_nonempirical.py geom=$geom correction=L rotation_axis=-1,0,0 test_${n}.stream
rm -rfv indexamajig.*
+
 qsub $d/run_${n}/run_${n}.sh
done

