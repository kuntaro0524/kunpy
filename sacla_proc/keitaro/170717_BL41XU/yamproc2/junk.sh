n=4
geom=/work/hirata/_ssrox/170717_BL41XU/yamproc2/Dark/pilatus-predrefine.geom

#for d in $PWD/*/*
#for d in $PWD/?F/*
for d in $PWD/Dark/*
do
 test -s $d/hits.lst || continue
 mkdir $d/run_${n}
 cat <<+ > $d/run_${n}/tmp.sh
#PBS -l nodes=1:ppn=1
#PBS -o $d/run_${n}/corr.out
#PBS -j oe

. ~/program/dials/dials-v1-6-0/dials_env.sh 
cd $d/run_${n}
yamtbx.python /home/hirata/program/yam_scripts/yamtbx/dataproc/crystfel/command_line/correct_stream_nonempirical.py geom=$geom correction=L rotation_axis=-1,0,0 test_${n}.stream
+
 qsub $d/run_${n}/tmp.sh
done

