root="/home/hirata/work/_ssrox/17110?_BL41XU_cheetah"
scr=$PWD/check_merge.sh

for k in `find $root/scan* -name \*corrected.stream.bz2 | sed -e "s,.*/,,"|awk '{print tolower($0)}'|sort|uniq|sed -e "s,_out_corrected.stream.bz2,,"`
do
 echo "kind=$k"
 d1=merge_$k
 mkdir -pv $d1
 test -e $d1/files.lst && rm $d1/files.lst

 for f in `find $root/scan* -iname ${k}_out_corrected.stream.bz2`
 do
  test -e ${f/_out_corrected.stream.bz2/_cheetah.stdout} || continue
  echo $f >> $d1/files.lst
 done


 nuniq=`sed -e "s,.*ssrox/,,; s,/scan.*,," $d1/files.lst|sort|uniq|wc -l`
 test "$nuniq" != "2" && continue

 # start run
 cd $d1
 pwd
 qsub $scr
 cd -
done

