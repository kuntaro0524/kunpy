root=/home/hirata/work/_ssrox/171103_BL41XU_cheetah
scr=$PWD/check_merge.sh

for k in `find $root/scan* -name \*corrected.stream.bz2 | sed -e "s,.*/,,"|sort|uniq|sed -e "s,_out_corrected.stream.bz2,,"`
do
 echo "kind=$k"
 d1=merge_$k
 mkdir -pv $d1
 test -e $d1/files.lst && rm $d1/files.lst

 for f in `find $root/scan* -name ${k}_out_corrected.stream.bz2`
 do
  test -e ${f/_out_corrected.stream.bz2/_cheetah.stdout} || continue
  echo $f >> $d1/files.lst
 done

 # start run
 cd $d1
 qsub $scr
 cd -
done

