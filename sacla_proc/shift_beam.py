#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Determine mean detector shift based on prediction refinement results
#
# Copyright © 2015-2018 Deutsches Elektronen-Synchrotron DESY,
#                       a research centre of the Helmholtz Association.
#
# Author:
#    2015-2018 Thomas White <taw@physics.org>
#    2016      Mamoru Suzuki <mamoru.suzuki@protein.osaka-u.ac.jp>
#    2018      Chun Hong Yoon
#

import sys
import os
import re

geom = sys.argv[1]
dx, dy = sys.argv[2:]

out = "%s_%s.geom" % (dx,dy)
dx, dy = float(dx), float(dy)

ofs = open(out, 'w')
for l in open(geom):
  if "corner_x" in l:
    v = float(l[l.index("=")+1:])
    l = l[:l.index("=")] + "= %f\n" % (v+dx)
  if "corner_y" in l:
    v = float(l[l.index("=")+1:])
    l = l[:l.index("=")] + "= %f\n" % (v+dy)
  ofs.write(l)

