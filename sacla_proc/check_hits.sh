echo "run all hits hit.rate"
for r in $@
do
 echo -n "$r "
 awk '/Frames processed/{a+=$3} /Number of hits/{h+=$4} END{printf("%4d %4d %5.1f%%\n", a, h, h/a*100)}' $r-*/status.txt
done 
