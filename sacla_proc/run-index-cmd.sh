#!/bin/bash
#PBS -l nodes=1:ppn=14
####PBS -q occupancy

if [ -n "$PBS_O_WORKDIR" -a "$PBS_ENVIRONMENT" != "PBS_INTERACTIVE" ]; then
	cd $PBS_O_WORKDIR
fi

source ~sacla_sfx_app/setup.sh
source /home/hirata/program/ccp4/ccp4-7.0/bin/ccp4.setup-sh

TARGET=$1
PDB=$2
GEOM=$3
METHOD=xgandalf,asdf,mosflm

if [ -z "$TARGET" -o -z "$METHOD" ]; then
	echo "Option: TARGET.h5 METHOD "
	exit 1
fi

echo `hostname` $TARGET $METHOD 

OUTPUT=`dirname $TARGET | sed -e "s,/work/hirata/200720_eh5/,,"`
mkdir -p `dirname $OUTPUT`

~sacla_sfx_app/packages/crystfel-0.9.1/build/indexamajig -i - -o $OUTPUT.stream -j 14 -g $GEOM \
 -p $PDB \
 --indexing=$METHOD \
 --int-radius=3,4,7 --min-snr=4 --threshold=100 --min-pix-count=2 --local-bg-radius=3 --peaks=peakfinder8 <<EOF
$TARGET
EOF

 #--peaks=peakfinder8 --threshold=0 --min-snr=4.3 --min-pix-count=2 --local-bg-radius=3 --int-radius=3,4,7 <<EOF
