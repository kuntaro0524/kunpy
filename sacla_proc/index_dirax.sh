#!/bin/bash
#PBS -l nodes=1:ppn=14
#PBS -q serial

pwd
cd $PBS_O_WORKDIR
pwd
source ~sacla_sfx_app/setup.sh

echo ../run958655-1.h5 | indexamajig -i - -o ${xy}.stream -j 14 -g ${xy}.geom  \
 --indexing=dirax --int-radius=3,4,7 --min-snr=4 --threshold=100 --min-pix-count=2 --local-bg-radius=3 --peaks=peakfinder8
 #-p ../sfx.cell

# -p ../sfx.cell --indexing=dirax --peaks=zaef --threshold=400 --min-gradient=40000 --min-snr=5 --int-radius=3,4,7

