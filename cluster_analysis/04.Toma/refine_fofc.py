import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis")

import glob,numpy
import DirectoryProc
import ComRefine

# SPACE GROUP
symm="C2221"
ref_dmin=3.2
prefix = "ref_fofc"

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
mtz_list,path_list=dp.findTarget("free_common.mtz")

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(mtz_list)
cnt=0
proc_mtz_list=[]
proc_path_list=[]

for proc_mtz,path in zip(mtz_list,path_list):
    proc_mtz_list.append(proc_mtz)
    proc_path_list.append(path)
    cnt+=1

print "LEN=",len(proc_path_list)

# Model PDB
model="/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis/Asada/nolig.pdb"

for proc_mtz,proc_path in zip(proc_mtz_list,proc_path_list):
    comrefine=ComRefine.ComRefine(proc_path)
    #comname = comrefine.ridx_freer_phx_low(refmtz,proc_mtz,symm,ref_dmin,model,"refine",hkl_sort="")
    # Give paths as 'absolute' ones.
    comname = comrefine.refine_only(proc_mtz,model,ref_dmin,prefix,pdbin_abspath=True,mtzin_abspath=True)
    os.system("qsub %s"%comname)
