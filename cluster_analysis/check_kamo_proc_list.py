import os,sys,math

# Raw image root directory
image_root=sys.argv[1]

# KAMO root directory
kamo_root=sys.argv[2]

#print image_root,kamo_root

imgpaths=os.listdir(image_root)
kamopaths=os.listdir(kamo_root)

imgdirs=[]
for compo in imgpaths:
	if os.path.isdir(compo):
		imgdirs.append(compo)

kamodirs=[]
for compo in kamopaths:
	if os.path.isdir(compo):
		kamodirs.append(compo)

#print imgdirs,kamodirs

n_imgdirs=len(imgdirs)
not_found_dirs=[]
n_match=0
for imgdir in imgdirs:
	found_flag=False
	# Finding the target directory
	for kamodir in kamodirs:
		if imgdir==kamodir:
			#print "Match!"
			found_flag=True
			n_match+=1
	if found_flag==False:
		not_found_dirs.append(imgdir)
		print "'%s' cannot be found in KAMO proc dire."%imgdir

print "Image directories: %5d Found KAMO: %5d"%(n_imgdirs,n_match)
print "not found:",not_found_dirs

#print imgdirs
#print kamodirs
