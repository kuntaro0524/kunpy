#!/bin/csh
# 2017/11/20 K.Hirata
# For cluster analysis 2500, 1000, 500, 200, 100, 50 datasets
#/isilon/users/target/target/Staff/kuntaro/171101-PH/_kamoproc/merge_blend_PH1deg/blend_1.20A_final/cluster_all/run_03

set TOPDIR=$PWD
foreach PROCDIR(`ls| grep proc_`)

set LOWER_RES=1.8
set HIGHER_RES=1.2

if (-e $PROCDIR/ccp4/) then
cd $PROCDIR/ccp4/
yamtbx.python ~/PPPP/11.ClusterAnalysis/make_paired_refine_xscale.py /isilon/users/khirata/khirata/170822-Conso/2oh6_prot.pdb I23 $LOWER_RES $HIGHER_RES F ../xscale.mtz
csh all.csh
else
endif

cd $TOPDIR
end
