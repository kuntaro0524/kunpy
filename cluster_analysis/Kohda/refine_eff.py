import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis")

import glob,numpy
import DirectoryProc
import ComRefine

# SPACE GROUP
symm="P21221"
ref_dmin=2.6

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(xscale_list)
cnt=0
proc_xscale_list=[]
proc_path_list=[]

for xscale_mtz,path in zip(xscale_list,path_list):
    proc_xscale_list.append(xscale_mtz)
    proc_path_list.append(path)
    cnt+=1

print "LEN=",len(proc_path_list)

# .eff file for phenix.refine
eff_file = "/isilon/users/khirata/khirata/190308-kohda/merge_group1_P222_all/TaguchiData/taguchi.eff"

# Free-R flags common
refmtz="/isilon/users/khirata/khirata/190308-kohda/merge_group1_P222_all/TaguchiData/free_common.mtz"

for mtzin, proc_path in zip(proc_xscale_list,proc_path_list):
    comrefine=ComRefine.ComRefine(proc_path)
    # Reindex -> common Free-R flag -> 
    #comname = comrefine.ridx_freer_phx_low(refmtz,"xscale.mtz",symm,ref_dmin,model,"refine",hkl_sort="")
    comname = comrefine.ridx_freer_phx_eff(refmtz,mtzin,symm,ref_dmin,eff_file,hkl_sort="",mtzin_abspath=True)

    os.system("qsub %s"%comname)
