#!/bin/csh

set MODEL=/isilon/users/target/target/Staff/kuntaro/171118-PH/_kamoproc/model.pdb
set SYMM=I23
set LOWRES=2.0
set HIGHRES=1.6
set COL="F"

set PWD=`pwd`

foreach PROCDIR (`ls | grep proc_`)
cd $PROCDIR/ccp4/
yamtbx.python ~/PPPP/11.ClusterAnalysis/make_paired_refine_xscale.py $MODEL $SYMM $LOWRES $HIGHRES $COL
chmod 744 all.csh
csh ./all.csh &
cd $PWD
end


