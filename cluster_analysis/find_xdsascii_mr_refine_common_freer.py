import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis/")
import glob,numpy
import DirectoryProc
import ComRefine

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

# MTZ file list for refinement
symm=sys.argv[1]
dmin=float(sys.argv[2])

# Symm
hkl_sort="h,k,l"

# Path selection keywords
keys=["run_02","final"]

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(xscale_list)
cnt=0
proc_xscale_list=[]
proc_path_list=[]

for xscale_mtz,path in zip(xscale_list,path_list):
        good_count=0
        for key in keys:
            if xscale_mtz.rfind(key)!=-1:
                good_count+=1
        if good_count==len(keys):
            proc_xscale_list.append(xscale_mtz)
            proc_path_list.append(path)
            cnt+=1

# Model PDB
model="/isilon/users/target/target/AutoUsers/180621/abe/_kamoproc/prot_mr.pdb"

# Free-R flags common
refmtz="/isilon/users/target/target/AutoUsers/180621/abe/_kamoproc/blend_A3-2/blend_1.50A_final/cluster_0190/run_03/ccp4/free.mtz"

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path in proc_path_list:
    comrefine=ComRefine.ComRefine(proc_path)
    # Make mtzfile
    # Extract Free-R flag columns
    free_column=comrefine.extractFreeR(refmtz)
    #print free_column
    #comname=comrefine.refine_common_free(refmtz,"xscale.mtz",symm,dmin,model,"refine",free_column=free_column)
    comname=comrefine.mr_refine_common_free(refmtz,"xscale.mtz",symm,dmin,model,"refine",free_column=free_column,nmon=1)
    os.system("chmod 744 %s"%(comname))
    os.system("qsub %s"%comname)
