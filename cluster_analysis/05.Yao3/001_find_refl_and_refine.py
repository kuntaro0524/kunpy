import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/Libs")

import ResolutionFromXscaleHKL
import glob,numpy
import DirectoryProc
import ComRefine
import Subprocess
import AnaXSCALE
import LibSPG

# Settings
proj_name = "yao3"
# SYMM
symm = "P21221"
# Model PDB
model_pdb="/isilon/users/xu_fei_39323/xu_fei_39323/191106_BL45XU_Yali/_kamoproc/merge_blend_A/blend_3.27A_final/yao3_nolig.pdb"

# Free-R flags common
ref_mtz="/isilon/users/xu_fei_39323/xu_fei_39323/191106_BL45XU_Yali/_kamoproc/merge_blend_A/blend_3.27A_final/cluster_0326/run_03/ccp4/free.mtz"

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.hkl")

subproc=Subprocess.Subprocess()

# Bad reflection file
bad_files = []

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for xscale_path, proc_path in zip(xscale_list,path_list):
    print "Processing %s " % proc_path

    resol_calculator = ResolutionFromXscaleHKL.ResolutionFromXscaleHKL(xscale_path)
    dmin = resol_calculator.get_resolution()

    print "resolution limit = ", dmin

    xscalelp_path = os.path.join(proc_path, "XSCALE.LP")
    ac=AnaXSCALE.AnaXSCALE(xscalelp_path)
    cells = ac.getCellParm()
    spg_num = ac.getFinalSPG()
    libspg = LibSPG.LibSPG()
    spg_xscale = libspg.search_spgnum(spg_num)

    # CCP4 directory
    ccp4_dir = os.path.join(proc_path, "ccp4")

    if os.path.exists(ccp4_dir) != True:
        logstr = "CCP4 directory does not exit: %s" % proc_path
        bad_files.append(logstr)
    else:
        mtz_file = os.path.join(ccp4_dir, "xscale.mtz")

    if spg_xscale.lower() == symm.lower():
        print "this is under consideration"
    else:
        print "this is not so good"
        print "MTZ SPG=", spg_xscale.lower()
        print "PDB SPG=", symm.lower()
        bad_files.append(xscale_path)
        #continue

    # Extract Free-R flag columns
    comf = ComRefine.ComRefine(ccp4_dir)
    free_column=comf.extractFreeR(ref_mtz)
    comname=comf.refine_common_free(ref_mtz,"xscale.mtz",symm,dmin,model_pdb,proj_name,free_column=free_column)
    #comname = comf.dimple_common_free(ref_mtz, "xscale.mtz",spg_xscale,dmin,model_pdb, proj_name)
    os.system("qsub %s" % comname)
