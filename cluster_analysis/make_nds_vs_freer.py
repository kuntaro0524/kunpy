import os,sys,math,glob
import time
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import DirectoryProc

if __name__ == "__main__":
	dp=DirectoryProc.DirectoryProc("./")
	dlist=dp.getDirList()

	ofile=open("nds_vs_freer.dat","w")

	resol_range=[1.6,1.7,1.8,1.9]

	for resol in resol_range:
		refine_log="refine01_water_%3.1fA_001.log"%resol

		for dire in dlist:
			abs_path=os.path.abspath(dire)
			targetlist=dp.findTarget(refine_log)

		# Sort
		targetlist.sort()
		curr_dir=os.path.abspath(".")
	
		comindex=0
		for tfile in targetlist:
			tpath=tfile.replace(refine_log,"")
			#print tfile
			cols0=tfile.split('/')
			nds=int(cols0[1].replace("proc_",""))
			lines=open(tfile,"r").readlines()
			for line in lines:
				if line.rfind("Final R-work")!=-1:
					cols=line.split()
					final_free=float(cols[6])
				if line.rfind(" end:")!=-1:
					cols=line.split()
					#print cols
					bfac=float(cols[7])
			ofile.write("%6.3f %5d %8.4f %8.4f\n"%(resol,nds,final_free,bfac))
		ofile.write("\n\n")
	ofile.close()
