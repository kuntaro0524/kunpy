import sys,os

class PhenixRefineLog:
    def __init__(self,logfile):
        self.logfile=logfile
        self.lines=open(logfile,"r").readlines()

    def isComplete(self):
        final_line=self.lines[len(self.lines)-1]
        if final_line.rfind("R-work")==-1:
            return False
        else:
            return True

    def getRfactors(self):
        cols=self.lines[len(self.lines)-1].split(' ')
        if self.isComplete()==False:
            print "Skipping this data because refine was not finished."
            return -999,-999
        else:
            self.r_work=float(cols[3].replace(",",""))
            self.free_r=float(cols[6].replace(",",""))
        return self.r_work,self.free_r

    def getBfactor(self):
        if self.isComplete()==False:
            print "Skipping this data because refine was not finished."
            return -999
        for line in self.lines:
            if line.rfind(" end:")!=-1:
                return float(line.split()[7])

    def getResolution(self):
        for line in self.lines:
            if line.rfind("refinement.input.xray_data.high_resolution") != -1:
                cols = line.split()
                resol = float(cols[2])
                return resol

        # resolution was not found
        return -999.9999
        
