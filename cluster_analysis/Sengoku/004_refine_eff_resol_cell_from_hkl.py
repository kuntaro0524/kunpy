import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis")

import glob,numpy
import DirectoryProc
import ComRefine
import ResolutionFromXscaleHKL
import AnaCORRECT

# SPACE GROUP
symm="P22121"

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,ccp4path_list=dp.findTarget("xscale.mtz")

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(xscale_list)
cnt=0
proc_xscale_list=[]
proc_path_list=[]
xscale_hkl_path_list=[]

xscale_cell_list = []

for xscale_mtz,ccp4path in zip(xscale_list,ccp4path_list):
    proc_xscale_list.append(xscale_mtz)
    proc_path_list.append(ccp4path)
    xscale_lp_path = "%s/../XSCALE.LP"%ccp4path
    ac = AnaCORRECT.AnaCORRECT(xscale_lp_path)
    cr = ac.getCellParm()
    cell_str = "%8.3f %8.3f %8.3f %8.3f %8.3f %8.3f" % (cr[0],cr[1],cr[2],cr[3],cr[4],cr[5])
    xscale_cell_list.append(cell_str)
    cnt+=1

# .eff file for phenix.refine
base_path = "/isilon/users/khirata/khirata/190713-sengoku/_kamoproc_10deg/merge_blend_sushi/blend_2.78A_final_ver2/BaseFiles/"
eff_file = "%s/beta_refine_91.eff" % base_path

# Free-R flags common
refmtz="%s/beta_refine_91.mtz" % base_path

for mtzin, proc_path, cell_str in zip(proc_xscale_list,proc_path_list,xscale_cell_list):
    comrefine=ComRefine.ComRefine(proc_path)
    # xscale.mtz is included in ccp4/ for normal cases
    # so this line removes 'ccp4'
    xscalehkl_path = proc_path.replace("ccp4","")
    xscalehkl_path = xscalehkl_path + "/xscale.hkl"
    print "XSCALE.HKL path = %s "%xscalehkl_path,
    rfx = ResolutionFromXscaleHKL.ResolutionFromXscaleHKL(xscalehkl_path)
    ref_dmin = rfx.get_resolution()
    print "resolution limit = ", ref_dmin
    # Setting warning message for setting cells
    comrefine.setThroughCellDiff()
    comrefine.setCellParams(cell_str, symm)

    # Reindex -> common Free-R flag ->
    comname = comrefine.ridx_freer_phx_eff(refmtz,mtzin,symm,ref_dmin,eff_file,hkl_sort="",mtzin_abspath=True)

    os.system("qsub %s"%comname)
