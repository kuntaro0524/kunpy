import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis/")
import glob,numpy
import DirectoryProc
import ComRefine
import Subprocess

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

subproc=Subprocess.Subprocess()

# Symm
symm="P22121"
dmin=3.0
hkl_sort="h,k,l"

# Model PDB
model="/isilon/users/target/target/AutoUsers/190713/sengoku/_kamoproc_10deg/merge_blend_sushi/blend_2.78A_final/beta_refine_47.pdb"

# Free-R flags common
refmtz="/isilon/users/target/target/AutoUsers/190713/sengoku/_kamoproc_10deg/merge_blend_sushi/blend_2.78A_final/beta_refine_47.mtz"

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path in path_list:
    comrefine=ComRefine.ComRefine(proc_path)
    # Extract Free-R flag columns
    free_column=comrefine.extractFreeR(refmtz)
    comname=comrefine.refine_common_free(refmtz,"xscale.mtz",symm,dmin,model,"refine",free_column=free_column)
    os.system("chmod 744 %s"%(comname))
    #print comname
    os.system("qsub %s"%comname)
