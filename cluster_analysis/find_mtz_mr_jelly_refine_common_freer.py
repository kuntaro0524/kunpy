import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis/")
import glob,numpy
import DirectoryProc
import ComRefine
import Subprocess

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

subproc=Subprocess.Subprocess()

# Symm
symm="I23"
dmin=1.5
hkl_sort="h,k,l"

# Model PDB
model="/isilon/users/target/target/AutoUsers/180621/abe/_kamoproc/prot_mr.pdb"

# Free-R flags common
refmtz="/isilon/users/target/target/AutoUsers/180621/abe/_kamoproc/blend_A3-2/blend_1.50A_final/cluster_0190/run_03/ccp4/free.mtz"

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path in path_list:
    comrefine=ComRefine.ComRefine(proc_path)
    # Extract Free-R flag columns
    free_column=comrefine.extractFreeR(refmtz)
    #print free_column
    #comname=comrefine.refine_common_free(refmtz,"xscale.mtz",symm,dmin,model,"refine",free_column=free_column)
    comname=comrefine.mr_refine_common_free(refmtz,"xscale.mtz",symm,dmin,model,"refine",free_column=free_column,nmon=1)
    os.system("chmod 744 %s"%(comname))
    os.system("qsub %s"%comname)
