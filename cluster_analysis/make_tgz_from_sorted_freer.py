import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import glob,numpy
import DirectoryProc
import PhenixRefineLog

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
logname=sys.argv[1]
phenilog_list,path_list=dp.findTarget(logname)

# LOG LIST
loglist=[]
# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
ofile=open("phenix_rfac.dat","w")
for phenilog,pathh in zip(phenilog_list,path_list):
    pl=PhenixRefineLog.PhenixRefineLog(phenilog)
    r,freer=pl.getRfactors()
    bfac=pl.getBfactor()
    cols=pathh.split('/')
    cluster="none"
    for col in cols:
        if col.rfind("cluster")!=-1:
            cluster=col
    loglist.append((pathh,r,freer,bfac))
    print "%s %8.4f %8.4f %8.4f"%(pathh,r,freer,bfac)
    ofile.write("%s R= %8.4f Free= %8.4f B= %8.4f\n"%(pathh,r,freer,bfac))

print loglist
loglist.sort(key=lambda x:float(x[2]))

for l in loglist:
    print l
