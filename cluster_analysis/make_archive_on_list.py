import numpy,os,sys

proc_paths=open(sys.argv[1],"r").readlines()
arc_name=sys.argv[2]

required_files=["XSCALE.INP","XSCALE.LP","ccp4","aniso.log"]

command="tar cvfz %s "%arc_name
for proc_path in proc_paths:
    proc_path=proc_path.strip()
    for required_file in required_files:
        arcfile=os.path.join(proc_path,required_file)
        command+=arcfile+" "

os.system(command)
