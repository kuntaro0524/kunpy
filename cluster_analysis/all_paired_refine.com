#!/bin/csh 

set MODEL_PDB=/isilon/users/khirata/khirata/170729-final-exp/_kamoproc/merge_tln/jelly01.pdb

foreach HRES(1.00 1.10 1.15 1.20)

if (-e "free.mtz") then
goto JELLY
else

reindex hklin xscale.mtz hklout reindex.mtz <<EOF > reindex.log
symm P6122
end
EOF

uniqueify -p 0.05 reindex.mtz free.mtz 

endif #REINDEX

JELLY:
if (-e "jelly01.pdb") then
goto PHENIX
else

refmac5 \
hklin free.mtz \
hklout jelly01.mtz \
xyzin $MODEL_PDB \
xyzout ./jelly01.pdb << eof | tee jelly.log
refi     type REST     resi MLKF     meth CGMAT     bref ISOT
ncyc 20
scal     type SIMP     LSSC     ANISO     EXPE
solvent YES
weight     AUTO
RIDG DIST SIGM 0.02
END
eof

PHENIX:

phenix.refine free.mtz jelly01.pdb ordered_solvent=true \
  xray_data.low_resolution=25.0 xray_data.high_resolution=$HRES \
  ordered_solvent.mode=every_macro_cycle output.prefix=refine01_water_$HRES

phenix.refine free.mtz refine01_water_${HRES}_001.pdb strategy=individual_adp \
  xray_data.low_resolution=25.0 xray_data.high_resolution=$HRES \
  adp.individual.anisotropic="not element H" output.prefix=anis_$HRES

phenix.reduce anis_${HRES}_001.pdb > anis_reduce.pdb

phenix.refine free.mtz anis_reduce.pdb strategy=individual_adp \
  xray_data.low_resolution=25.0 xray_data.high_resolution=$HRES \
  adp.individual.anisotropic="not element H"

end
