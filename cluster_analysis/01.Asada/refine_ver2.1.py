import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis")

import glob,numpy
import DirectoryProc
import ComRefine

# SPACE GROUP
symm="C2221"
ref_dmin=3.2

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(xscale_list)
cnt=0
proc_xscale_list=[]
proc_path_list=[]

for xscale_mtz,path in zip(xscale_list,path_list):
    proc_xscale_list.append(xscale_mtz)
    proc_path_list.append(path)
    cnt+=1

print "LEN=",len(proc_path_list)

# Model PDB
model="/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis/Asada/asada.pdb"

# Free-R flags common
refmtz="/isilon/users/khirata/khirata/180921-Asada-Helix/_kamoproc_180627/merge_180628-AT2_zennbunose/blend_3.0A_framecc_b+B/cluster_3699/run_03/ccp4-3.0A/free_common.mtz"

for proc_path in proc_path_list:
    comrefine=ComRefine.ComRefine(proc_path)
    # Reindex -> common Free-R flag -> 
    comname = comrefine.ridx_freer_phx_low(refmtz,"xscale.mtz",symm,ref_dmin,model,"refine",hkl_sort="")
    os.system("qsub %s"%comname)
