import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import glob,numpy
import DirectoryProc
import ComRefine

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

# Model PDB
model="/old_isilon_sub/target_backup/User_Data/UserData_2015B_4/iwata/_kamo_asada_revise/merge_180323-112420/blend_3.2A_framecc_b+B/AT2R_4A03Fab_refine_140.pdb"

# Free-R flags common
refmtz="/old_isilon_sub/target_backup/User_Data/UserData_2015B_4/iwata/_kamo_asada_revise/AT2R_4A03Fab_refine_140.mtz"

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path in path_list:
	comrefine=ComRefine.ComRefine(proc_path)

        comname=comrefine.refine_phenix_low_only(refmtz,"xscale.mtz","P21212",3.2,model,"refine_low",hkl_sort="l,h,k")
	#print comname
	os.system("qsub %s"%comname)
