import sys,os
import glob,numpy

# Absolute path of the current directory
abs_path=os.path.abspath("./")
dlist=glob.glob("cluster*")

logname_include="anis_"
logname_include="refine01_water_"

#print dlist
#print abs_path

def analyze_phenix_refine(logfile):
	lines=open(logfile,"r").readlines()
	lindex=0
	for line in lines:
		if line.rfind("Resolution    Compl Nwork Nfree R_work    <Fobs>  <Fmodel> kiso   kani kmask")!=-1:
			saveindex=lindex
		lindex+=1

	cols=lines[lindex-1].split()
	path_cols=logfile.split('/')
	#print path_cols
	lll=len(path_cols)
	#print path_cols[lll-2],path_cols[lll-3], path_cols[lll-4]
	refine_resol=float(path_cols[lll-2].replace("refine","").replace("A",""))
	freer=float(cols[6].replace(",",""))
	rfact=float(cols[3].replace(",",""))
	return refine_resol,freer,rfact

all_list=[]
for dire in dlist:
	subpath="run_02/ccp4"
	anapath="%s/%s"%(dire,subpath)
	refdir_list=glob.glob("%s/refine*"%anapath)
	dataa=[]
	if len(refdir_list)==0:
		print "%s : not include refinement"%anapath
		continue
	freer_min=1.0
	for each_refdir in refdir_list:
		refabs="%s/%s"%(abs_path,each_refdir)
		#print refabs
		logfile=glob.glob("%s/%s*.log"%(refabs,logname_include))
		if len(logfile)==0:
			print "refinement log cannot be found in %s"%(refabs)
			continue
		resol,freer,rfact=analyze_phenix_refine(logfile[0])
		dataa.append((resol,freer,rfact))
		# LOGGING
	all_list.append((anapath,dataa))

for each_dataset in all_list:
	anapath,dataa=each_dataset
	#print anapath,dataa
	min_freer=1.0
	min_resol=0.0
	for d in dataa:
		resol,freer,rfact=d
		if freer < min_freer:
			min_freer=freer
			min_resol=resol
	print anapath,min_resol,min_freer
	#resol,freer,rfact=dataa
"""
model_pdb=sys.argv[1]
symm=sys.argv[2]
reslimit_low=float(sys.argv[3])
reslimit_high=float(sys.argv[4])

# resolution
reslimit_list=numpy.arange(reslimit_high,reslimit_low,0.1)
print reslimit_list

mtzfile=glob.glob("./xscale.mtz")
print "%s/%s"%(abs_path,mtzfile[0])
print abs_path

def make_com(comname,res_limit,outdir):
	comf=open(comname,"w")
	comf.write("#!/bin/csh\n")
	comf.write("#$ -cwd\n")
	comf.write("set MODEL_PDB=%s\n"%model_pdb)
	comf.write("reindex hklin ../xscale.mtz hklout reindex.mtz <<EOF > reindex.log\n")
	comf.write("symm %s\n"%symm)
	comf.write("end\n")
	comf.write("EOF\n\n")
	comf.write("uniqueify -p 0.05 reindex.mtz free.mtz\n\n")
	comf.write("refmac5 \\\n")
	comf.write("hklin free.mtz \\\n")
	comf.write("hklout jelly01.mtz \\\n")
	comf.write("xyzin $MODEL_PDB \\\n")
	comf.write("xyzout ./jelly01.pdb << eof | tee jelly.log \n")
	comf.write("refi     type REST     resi MLKF     meth CGMAT     bref ISOT \n")
	comf.write("ncyc 20\n")
	comf.write("scal     type SIMP     LSSC     ANISO     EXPE\n")
	comf.write("solvent YES\n")
	comf.write("weight     AUTO\n")
	comf.write("LABIN FP=F SIGFP=SIGF FREE=FreeR_flag\n")
	comf.write("RIDG DIST SIGM 0.02\n")
	comf.write("END\n")
	comf.write("eof\n\n")
	comf.write("phenix.refine free.mtz jelly01.pdb ordered_solvent=true \\\n")
  	comf.write("xray_data.low_resolution=25.0 xray_data.high_resolution=%f \\\n"%res_limit)
  	comf.write("ordered_solvent.mode=every_macro_cycle output.prefix=refine01_water_%03.1fA\\\n\n"%res_limit)
	comf.write("phenix.refine free.mtz refine01_water_%03.1fA_001.pdb strategy=individual_adp \\\n"%res_limit)
  	comf.write("xray_data.low_resolution=25.0 xray_data.high_resolution=%f \\\n"%res_limit)
  	comf.write("adp.individual.anisotropic=\"not element H\" output.prefix=anis_%03.1fA \\\n\n"%res_limit)
	comf.write("phenix.reduce anis_%03.1fA_001.pdb > anis_reduce.pdb\n"%res_limit)
	comf.write("phenix.refine free.mtz anis_reduce.pdb strategy=individual_adp \\\n")
  	comf.write("xray_data.low_resolution=25.0 xray_data.high_resolution=%f \\\n"%res_limit)
  	comf.write("adp.individual.anisotropic=\"not element H\"\n")
	comf.write("end\n")
	comf.close()

all_csh=open("all.csh","w")
all_csh.write("#!/bin/csh\n")
for res_limit in reslimit_list:
	outdir="refine%03.1fA"%res_limit
	if os.path.exists(outdir)==False:
		os.makedirs(outdir)
	comname="refine_%03.1fA.com"%res_limit
	compath="%s/%s"%(outdir,comname)
	all_csh.write("cd %s\n"%outdir)
	all_csh.write("qsub ./%s\n"%comname)
	all_csh.write("cd ../\n")
	make_com(compath,res_limit,outdir)
	os.system("chmod 744 %s"%compath)
"""
