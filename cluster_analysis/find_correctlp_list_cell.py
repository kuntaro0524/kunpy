import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import glob,numpy
import DirectoryProc
import AnaCORRECT

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
corrlp_list,path_list=dp.findTarget("CORRECT.LP")

# Log file
logf=open("cell_list.txt","w")

corrlp_list.sort()

print "LEN=",len(corrlp_list)

# find CORRECT.LP and analyze
data_and_cell=[]
for corrlp in corrlp_list:
    ac=AnaCORRECT.AnaCORRECT(corrlp)
    a,b,c,alpha,beta,gamma = ac.getCellParm()
    data_and_cell.append((corrlp,a,b,c,alpha,beta,gamma))

data_and_cell.sort(key=lambda x:float(x[1]))

for data_name,a,b,c,alpha,beta,gamma in data_and_cell:
    #print data_name,a,b,c,alpha,beta,gamma
    logf.write("%30s %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n"%(data_name,a,b,c,alpha,beta,gamma))
