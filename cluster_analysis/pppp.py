import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import glob,numpy
import DirectoryProc
import ComRefine
import pylab,numpy,sys
import matplotlib.pyplot as plt

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
log_list,logpath_list=dp.findTarget("jelly.log")

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path,logname in zip(logpath_list,log_list):
	mtzfile="%s/refine_001.mtz"%proc_path
	plotfile="%s/rfact.dat"%proc_path
	# R-factor and Free-R factor evaluation
	command="phenix.python /misc/oys/xtal/phenix/phenix-1.11.1-2575/modules/cctbx_project/mmtbx/command_line/show_r_factors_by_shell.py %s > %s"%(mtzfile,plotfile)
	os.system(command)

	# Plotting
	fig = plt.figure()
	fig.subplots_adjust(bottom=0.2)
	ax = fig.add_subplot(111)
	
	lines=open(plotfile,"r").readlines()
	da=[]
	ra=[]
	fra=[]
	n_blank_line=0
	for line in lines:
    		cols=line.split()
    		if len(cols)==0:
        		n_blank_line+=1
        		if n_blank_line==2:
                		break
        		else:
                		continue
    		d=float(cols[2])
    		ds2=1/d/d
    		da.append(ds2)
    		ra.append(float(cols[3])*100.0)
    		fra.append(float(cols[4])*100.0)
	
	nda=numpy.array(da)
	nra=numpy.array(ra)
	nfa=numpy.array(fra)
	
	plt.plot(nda,nra,'-o',label="R-factor")
	plt.plot(nda,nfa,'-o',label="Free-R")
	plt.legend(loc='upper left',fontsize=18)
	
	plt.ylim([0,40])
	plt.xlabel(r'$d^{*2}$ [$\AA^{-2}$]',fontsize=18)
	plt.tick_params(labelsize=18)
	plt.ylabel("Crystallographic R-factor [%]",fontsize=18)
	plt.savefig("%s/revised.png"%proc_path,dpi=300)
