import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import DirectoryProc
import AnaCORRECT

if __name__=="__main__":
    # Path selection keywords
    keys=["run_03"]
    # PREFIX
    if len(sys.argv) <= 1:
        print "No arguments!"
    else:
        keys.append(sys.argv[1])

    # Finding CORRECT.LP
    dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
    correct_list,path_list=dp.findTarget("CORRECT.LP")
    
    # Keyword selection
    newlist=[]
    #print "Original datasets: %5d"%len(correct_list)
    cnt=0
    proc_correct_list=[]
    proc_path_list=[]

    ofile=open("correct_rmeas_list.dat","w")
    
    for correct_lp,path in zip(correct_list,path_list):
            good_count=0
            for key in keys:
                if correct_lp.rfind(key)!=-1:
                    good_count+=1
            if good_count==len(keys):
                proc_correct_list.append(correct_lp)
                proc_path_list.append(path)
                cnt+=1

            ac=AnaCORRECT.AnaCORRECT(correct_lp)

            inner_rmeas = ac.getInnerShellRmeas()
            total_rmeas = ac.getTotalRmeas()
            d_min = ac.getDmin()

            ofile.write("%s %5.1fA %5.2f %5.2f\n"%(correct_lp,d_min,inner_rmeas,total_rmeas))

            #print ac.getOuterShellRmeas()
            #print ac.countDatasets()
            #print ac.getCellParm()
            #print ac.getFinalSPG()
