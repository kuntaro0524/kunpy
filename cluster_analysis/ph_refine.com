#!/bin/csh
#$ -cwd
set MODEL_PDB=~/170725-PH-resolution/a2model1.pdb

reindex hklin ./xscale.mtz hklout reindex.mtz <<EOF > reindex.log
symm I23
end
EOF

uniqueify -p 0.05 reindex.mtz free.mtz

molrep HKLIN free.mtz \
       MODEL $MODEL_PDB \
       PATH_OUT ./ <<stop > molrep.log
NMON   1
NP     3
NPT    10
_END
stop
#

refmac5 \
hklin free.mtz \
hklout jelly01.mtz \
xyzin molrep.pdb \
xyzout ./jelly01.pdb << eof | tee jelly.log 
refi     type REST     resi MLKF     meth CGMAT     bref ISOT 
ncyc 20
scal     type SIMP     LSSC     ANISO     EXPE
solvent YES
weight     AUTO
LABIN FP=F SIGFP=SIGF FREE=FreeR_flag
RIDG DIST SIGM 0.02
END
eof

phenix.refine free.mtz jelly01.pdb 

end
