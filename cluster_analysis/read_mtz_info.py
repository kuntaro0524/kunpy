import sys
import iotbx.mtz
from iotbx.reflection_file_reader import any_reflection_file
from cctbx import crystal

if __name__ == "__main__":
    hkl_in = any_reflection_file(sys.argv[1])
    miller_arrays = hkl_in.as_miller_arrays()
    i_obs = miller_arrays[0]

    #print i_obs.show_summary()
    #mtz_dataset = i_obs.as_mtz_dataset(column_root_label="I")
    #mtz_dataset.add_miller_array(f_obs, column_root_label="F")
    #mtz_dataset.add_miller_array(r_free_flags, column_root_label="FreeR_flag")
    #mtz_dataset.mtz_object().write("data.mtz")

    def get_label(m):
        l = m.info().labels[0]
        if l.endswith(("(+)", "(-)")):
            l = l[0:-3]
        return l

    for arr in miller_arrays[1:]:
        print get_label(arr)

    unit_cell=miller_arrays[0].unit_cell()
    print unit_cell

    space_group_info=miller_arrays[0].space_group().info()
    print space_group_info

"""
        input_symm = crystal.symmetry(
            unit_cell=miller_arrays[0].unit_cell(),
            space_group_info=miller_arrays[0].space_group().info(),
            assert_is_compatible_unit_cell=False,
            force_compatible_unit_cell=False)
"""

