import sys,math,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import DirectoryProc
import AnaXSCALE

dp=DirectoryProc.DirectoryProc("./")
dlist=dp.getDirList()

dsl_list=dp.findTargetFile("XSCALE.LP")

#print dsl_list
#print dlist


plist=[]
for lp_file in dsl_list:
	if lp_file.rfind("proc_")!=-1:
		cols=lp_file.split('/')
		nds=int(cols[1].replace("proc_",""))
		print "processing %s"%lp_file
		ac=AnaXSCALE.AnaXSCALE(lp_file)
		outfile=lp_file.replace(".LP",".plt")
		ofile=open(outfile,"w")
		for logstr in ac.readLog():
        		ofile.write("%s"%logstr)
		ds2_limit=ac.determineResolution()
		plist.append((nds,ds2_limit))

plist.sort()
logfile="numds_ds2_new.dat"
matomefile=open(logfile,"w")
for n,ds2 in plist:
	matomefile.write("%5d %8.4f\n"%(n,ds2))
matomefile.close()
