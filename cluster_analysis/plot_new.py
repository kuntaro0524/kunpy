import pylab 
import numpy
import glob

files=["1deg_new.csv","5deg_new.csv","10deg_new.csv"]

plot_list=[]
for f in files:
	nda=[]
	ds2a=[]

	dataname=f.replace(".csv","")
	lines=open(f,"r").readlines()
	for line in lines:
		if line.rfind("#")!=-1:
			continue
		cols=line.split(",")
		ndata=int(cols[0])
		ds2=float(cols[1])
		nda.append(ndata)
		ds2a.append(ds2)

	x=numpy.array(nda)
	y=numpy.array(ds2a)
	pylab.xscale("log")
	pylab.legend(fontsize=10)
	pylab.plot(x,y,linestyle="dashed",marker="o",label=dataname) 

pylab.legend(['1deg','5deg','10deg'],loc='upper left')
pylab.xlabel("log(# of datasets)")
pylab.ylabel("d*^2")
pylab.savefig("nds2_new.png")
