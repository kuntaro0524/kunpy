import sys,os
import glob,numpy

# Absolute path of the current directory
abs_path=os.path.abspath("./")
model_pdb=sys.argv[1]
symm=sys.argv[2]
reslimit_low=float(sys.argv[3])
reslimit_high=float(sys.argv[4])

fcol=sys.argv[5]
sigfcol="SIG%s"%fcol

# resolution
reslimit_list=numpy.arange(reslimit_high,reslimit_low,0.1)
print reslimit_list

mtzfile=glob.glob("./xscale.mtz")
print "%s/%s"%(abs_path,mtzfile[0])
print abs_path

def make_com(comname,res_limit,outdir):
	comf=open(comname,"w")
	comf.write("#!/bin/csh\n")
	comf.write("#$ -cwd\n")
	comf.write("set MODEL_PDB=%s\n"%model_pdb)
	comf.write("reindex hklin ../xscale.mtz hklout reindex.mtz <<EOF > reindex.log\n")
	comf.write("symm %s\n"%symm)
	comf.write("end\n")
	comf.write("EOF\n\n")
	comf.write("uniqueify -p 0.05 reindex.mtz free.mtz\n\n")
	comf.write("refmac5 \\\n")
	comf.write("hklin free.mtz \\\n")
	comf.write("hklout jelly01.mtz \\\n")
	comf.write("xyzin $MODEL_PDB \\\n")
	comf.write("xyzout ./jelly01.pdb << eof | tee jelly.log \n")
	comf.write("refi     type REST     resi MLKF     meth CGMAT     bref ISOT \n")
	comf.write("ncyc 20\n")
	comf.write("scal     type SIMP     LSSC     ANISO     EXPE\n")
	comf.write("solvent YES\n")
	comf.write("weight     AUTO\n")
	comf.write("LABIN FP=%s SIGFP=%s FREE=FreeR_flag\n"%(fcol,sigfcol))
	comf.write("RIDG DIST SIGM 0.02\n")
	comf.write("END\n")
	comf.write("eof\n\n")
	comf.write("phenix.refine free.mtz jelly01.pdb ordered_solvent=true \\\n")
  	comf.write("xray_data.low_resolution=25.0 xray_data.high_resolution=%f \\\n"%res_limit)
  	comf.write("ordered_solvent.mode=every_macro_cycle output.prefix=refine01_water_%03.1fA\\\n\n"%res_limit)
	comf.write("end\n")
	comf.close()

all_csh=open("all.csh","w")
all_csh.write("#!/bin/csh\n")
for res_limit in reslimit_list:
	outdir="refine%03.1fA"%res_limit
	if os.path.exists(outdir)==False:
		os.makedirs(outdir)
	comname="refine_%03.1fA.com"%res_limit
	compath="%s/%s"%(outdir,comname)
	all_csh.write("cd %s\n"%outdir)
	all_csh.write("qsub ./%s\n"%comname)
	all_csh.write("cd ../\n")
	make_com(compath,res_limit,outdir)
	os.system("chmod 744 %s"%compath)
