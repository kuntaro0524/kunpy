import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis/")
import glob,numpy
import DirectoryProc
import ComRefine

# MTZ file
mtzname="xscale.mtz"

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget(mtzname)

# MTZ file list for refinement
symm="C2"
dmin=3.2

# Symm
hkl_sort="h,k,l"

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(xscale_list)
cnt=0
proc_xscale_list=[]
proc_path_list=[]

for xscale_mtz,path in zip(xscale_list,path_list):
    good_count=0
    proc_xscale_list.append(xscale_mtz)
    proc_path_list.append(path)
    cnt+=1



# Model PDB
model="/isilon/users/khirata/khirata/180921-RevisionShiimura-forIPRseminer/170601/_kamoproc/merge_180921/ghsr_refine_123_deloop_model.pdb"

# Free-R flags common
refmtz="/isilon/users/khirata/khirata/180921-RevisionShiimura-forIPRseminer/170601/_kamoproc/merge_180921/ghsr_refine_123.mtz"

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path in proc_path_list:
    comrefine=ComRefine.ComRefine(proc_path)
    # Extract Free-R flag columns
    free_col=comrefine.extractFreeR(refmtz)
    #comname=comrefine.mr_refine_common_free(refmtz,mtzname,symm,dmin,model,"refine",hkl_sort=hkl_sort,free_column=free_column,nmon=1)
    comname=comrefine.refine_phenix_low_only(refmtz,mtzname,symm,3.2,model,"refine_low",hkl_sort=hkl_sort,free_column=free_col)
    os.system("chmod 744 %s"%(comname))
    os.system("qsub %s"%comname)
