#!/bin/csh
gnuplot << eof
set terminal png
set output "nds_vs_freer.png"
plot "nds_vs_freer.dat" i 0 u 2:3 w lp  ti "1.6A", \
 "" i 1 u 2:3 w lp  ti "1.7A", \
 "" i 2 u 2:3 w lp  ti "1.8A", \
 "" i 3 u 2:3 w lp  ti "1.9A"
eof

gnuplot << eof
set terminal png
set output "nds_vs_bave.png"
plot "nds_vs_freer.dat" i 0 u 2:4 w lp  ti "1.6A", \
 "" i 1 u 2:4 w lp  ti "1.7A", \
 "" i 2 u 2:4 w lp  ti "1.8A", \
 "" i 3 u 2:4 w lp  ti "1.9A"
eof
