import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import glob,numpy
import DirectoryProc
import AnaCORRECT

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_lp_list,path_list=dp.findTarget("XSCALE.LP")

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for pathh in path_list:
    ac=AnaCORRECT.AnaCORRECT("%s/XSCALE.LP"%pathh)
    outfile="%s/nds.dat"%pathh
    ofile=open(outfile,"w")
    total_rmeas=ac.getTotalRmeas()
    compl,redun,outer_rmeas,outer_isigi=ac.getOuterShellInfo()
    nds= ac.countDatasets()
    cols=pathh.split('/')
    cluster="none"
    # Cluster name
    for col in cols:
        if col.rfind("cluster")!=-1:
            cluster=col
	ofile.write("%s %5d %8.4f %8.4f %8.4f %8.4f %8.4f\n"%(cluster,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi))
	ofile.close()
	print "%s %5d %8.4f %8.4f %8.4f %8.4f %8.4f"%(cluster,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi)
