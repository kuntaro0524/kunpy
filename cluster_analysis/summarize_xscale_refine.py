import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import glob,numpy
import DirectoryProc
import PhenixRefineLog
import DirectoryProc
import AnaCORRECT

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
logname=sys.argv[1]
phenilog_list,path_list=dp.findTarget(logname)

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
loglines=[]
for phenilog,pathh in zip(phenilog_list,path_list):
    outfile="%s/cluster_result.dat"%pathh
    pl=PhenixRefineLog.PhenixRefineLog(phenilog)
    r,freer=pl.getRfactors()
    bfac=pl.getBfactor()

    # XSCALE.LP normally locates at "../" from the phenix refine directory
    ac=AnaCORRECT.AnaCORRECT("%s/../XSCALE.LP"%pathh)
    #print outfile
    ofile=open(outfile,"w")
    total_rmeas=ac.getTotalRmeas()
    compl,redun,outer_rmeas,outer_isigi,cchalf=ac.getOuterShellInfo()
    nds= ac.countDatasets()
    cols=pathh.split('/')

    loglines.append("path,%s,nds,%5d,compl,%8.4f,redun,%8.4f,total_rmeas,%8.4f,outer_rmeas,%8.4f,outer_isigi,%8.4f,cc_half,%8.4f" \
        %(pathh,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi,cchalf))
    loglines.append(",rwork,%8.5f,rfree,%8.5f,bfac,%8.5f\n"%(r,freer,bfac))

    ofile.write("path,%s,nds,%5d,compl,%8.4f,redun,%8.4f,total_rmeas,%8.4f,outer_rmeas,%8.4f,outer_isigi,%8.4f,cc_half,%8.4f" \
        %(pathh,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi,cchalf))
    ofile.write(",rwork,%8.5f,rfree,%8.5f,bfac,%8.5f\n"%(r,freer,bfac))
    ofile.close()


for line in loglines:
    print line,
