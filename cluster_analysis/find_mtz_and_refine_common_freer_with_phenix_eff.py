import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis/")
import glob,numpy
import DirectoryProc
import ComRefine
import Subprocess

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

subproc=Subprocess.Subprocess()

# Symm
symm="C2"
dmin=2.7
hkl_sort="h,k,l"

# Model PDB
model="/isilon/users/target/target/AutoUsers/180621/abe/_kamoproc/prot_mr.pdb"

# Free-R flags common
refmtz="/isilon/users/target/target/AutoUsers/180621/abe/_kamoproc/blend_A3-2/blend_1.50A_final/cluster_0190/run_03/ccp4/free.mtz"

# .eff file for phenix.refine
efffile="thal.eff"

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path in path_list:
    comrefine=ComRefine.ComRefine(proc_path)
    # Extract Free-R flag columns
    free_column=comrefine.extractFreeR(refmtz)
    #comname=comrefine.refine_common_free(refmtz,"xscale.mtz",symm,dmin,model,"refine",free_column=free_column)
    comname=comrefine.refine_common_free_with_eff(refmtz,"xscale.mtz",symm,dmin,model,"refine",efffile,free_column=free_column)
    
    os.system("chmod 744 %s"%(comname))
    #print comname
    os.system("qsub %s"%comname)
