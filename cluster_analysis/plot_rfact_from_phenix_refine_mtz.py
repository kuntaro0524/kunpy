import pylab,numpy,sys
import matplotlib.pyplot as plt

fig = plt.figure()
fig.subplots_adjust(bottom=0.2)
ax = fig.add_subplot(111)

lines=open(sys.argv[1],"r").readlines()

da=[]
ra=[]
fra=[]
n_blank_line=0
for line in lines:
    cols=line.split()
    if len(cols)==0:
	n_blank_line+=1
	if n_blank_line==2:
		break
	else:
		continue
    d=float(cols[2])
    ds2=1/d/d
    da.append(ds2)
    ra.append(float(cols[3])*100.0)
    fra.append(float(cols[4])*100.0)

nda=numpy.array(da)
nra=numpy.array(ra)
nfa=numpy.array(fra)

plt.plot(nda,nra,'-o',label="R-factor")
plt.plot(nda,nfa,'-o',label="Free-R")
plt.legend(loc='upper left',fontsize=18)

#plt.title("R-factor plot")
plt.ylim([0,40])
plt.xlabel(r'$d^{*2}$ [$\AA^{-2}$]',fontsize=18)
plt.tick_params(labelsize=18)
#plt.xlabel(r'$\Sigma_{d}={0.5M_{d}/\pi R_{e,d}^2}$[M$_{\odot}$/kpc$^2$]',fontsize=13)
plt.ylabel("Crystallographic R-factor [%]",fontsize=18)
plt.savefig("revised.png",dpi=300)
#plt.savefig("sample.png",format = 'png', dpi=300)

