import os,sys,math
import numpy as np
from scipy import signal, interpolate
from matplotlib import pylab as plt

lines=open(sys.argv[1],"r").readlines()

txa=[]
tya=[]
for line in lines:
	cols=line.split()
	nfiles=int(cols[0])
	ds2=float(cols[1])

	txa.append(math.log(nfiles))
	tya.append(ds2)

t=np.array(txa)
y=np.array(tya)

minx=t.min()
maxx=t.max()

print minx,maxx

A = np.array([t,np.ones(len(t))])
A = A.T
a,b = np.linalg.lstsq(A,y)[0]

newx=np.linspace(minx,maxx,100)
newy=(a*newx+b)

x=math.log(10000.0)
dstar2_this=a*x+b
print "  10000 datasets d=%8.5f"%math.sqrt(1/dstar2_this)

x=math.log(1000000.0)
dstar2_this=a*x+b
print "1000000 datasets d=%8.5f"%math.sqrt(1/dstar2_this)

#f1 = interpolate.interp1d(t, y)
#f4 = interpolate.interp1d(t, y, kind="slinear")
#y4 = f1(newx)

for x,y in zip(t,y):
	print x,y

print "\n\n"

for x,y in zip(newx,newy):
	print x,y
