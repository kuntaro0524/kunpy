import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis")

import glob,numpy
import DirectoryProc
import ComRefine
import ResolutionFromXscaleHKL

# SPACE GROUP
symm="C2"

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,ccp4path_list=dp.findTarget("xscale.mtz")

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(xscale_list)
cnt=0
proc_xscale_list=[]
proc_path_list=[]
xscale_hkl_path_list=[]

for xscale_mtz,ccp4path in zip(xscale_list,ccp4path_list):
    proc_xscale_list.append(xscale_mtz)
    proc_path_list.append(ccp4path)
    cnt+=1

# .eff file for phenix.refine
eff_file = "/isilon/users/khirata/khirata/180921-RevisionShiimura-forIPRseminer/170601/_kamoproc/merge_180921/blend_3.0A_framecc_b+B_1deg/cluster_0529/run_01/Models/ghsr_final.eff"

# Free-R flags common
refmtz="/isilon/users/khirata/khirata/180921-RevisionShiimura-forIPRseminer/170601/_kamoproc/merge_180921/blend_3.0A_framecc_b+B_1deg/cluster_0529/run_01/Models/ghsr_refine_126.mtz"


for mtzin, proc_path in zip(proc_xscale_list,proc_path_list):
    comrefine=ComRefine.ComRefine(proc_path)
    # xscale.mtz is included in ccp4/ for normal cases
    # so this line removes 'ccp4'
    xscalehkl_path = proc_path.replace("ccp4","")
    xscalehkl_path = xscalehkl_path + "/xscale.hkl"
    print "XSCALE.HKL path = %s "%xscalehkl_path,
    rfx = ResolutionFromXscaleHKL.ResolutionFromXscaleHKL(xscalehkl_path)
    ref_dmin = rfx.get_resolution()
    print "resolution limit = ", ref_dmin
    # Reindex -> common Free-R flag -> 
    comname = comrefine.ridx_freer_phx_eff(refmtz,mtzin,symm,ref_dmin,eff_file,hkl_sort="",mtzin_abspath=True)
    os.system("qsub %s"%comname)
