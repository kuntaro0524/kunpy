import pylab 
import numpy
import glob

files=["./proc_0010/XSCALE.plt", "./proc_0100/XSCALE.plt", "./proc_0500/XSCALE.plt", "./proc_1000/XSCALE.plt", "./proc_2000/XSCALE.plt"]

plot_list=[]
for f in files:
	ds2a=[]
	ccha=[]

	dataname=f.replace(".plt","")
	lines=open(f,"r").readlines()
	for line in lines:
		if line.rfind("#")!=-1:
			continue
		cols=line.split()
		if len(cols)==0:
			continue
		ds2=float(cols[2])
		cchalf=float(cols[5])
		ds2a.append(ds2)
		ccha.append(cchalf)

	x=numpy.array(ds2a)
	y=numpy.array(ccha)
	pylab.legend(fontsize=10)
	pylab.plot(x,y,linestyle="dashed",marker="o",label=dataname) 

pylab.legend(['10sets','100sets','500sets','1000sets','2000sets'],loc='lower left')
pylab.ylabel("CC(1/2)")
pylab.xlabel("d*^2")
pylab.ylim([0,110])
pylab.savefig("cc_half.png")
