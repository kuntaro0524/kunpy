import os,sys,math
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import AroundXSCALE

ax=AroundXSCALE.AroundXSCALE()

dandan="../"

def prepDirectory(dirname):
        if os.path.exists(dirname)==False:
                os.mkdir(dirname)

nthresh=int(sys.argv[1])
flist=ax.getFileListOfEnoughReflections(nthresh=nthresh)

def get_xscale_inp(nfiles):
	file_list=[]
	idx=0
	for f in flist:
		idx+=1
        	file_list.append(f)
		if idx == nfiles:
			break
	#print file_list
	return file_list

otherlines=ax.getOtherLinesInputFiles()
#print otherlines

#infile_num=[2500,2000,1500,1000,500,200,100,50,25,10]
infile_num=[8000]

# delete exceeded infile_num 
dellist = lambda items, indexes: [item for index, item in enumerate(items) if index not in indexes]
n_allfiles=len(flist)

print n_allfiles

idx=0
delete_list=[]
for l in infile_num:
        if l > n_allfiles:
                delete_list.append(idx)
        idx+=1

infile_num=dellist(infile_num,delete_list)

allcom=open("all_xscale.com","w")
allcom.write("#!/bin/csh\n")
# processing directory
for inum in infile_num:
	pdir="proc2_%04d"%inum
	# Make processing directory
	prepDirectory(pdir)
	apath=os.path.abspath(pdir)
	new_xscale_inp="%s/XSCALE.INP"%pdir
	ofile=open(new_xscale_inp,"w")
	for l in otherlines:
		ofile.write("%s"%l)
	inpfiles=get_xscale_inp(inum)

	for l in inpfiles:
		ofile.write("INPUT_FILE=../%s\n"%l)

	cname="%s/run.com"%apath
	comfile=open(cname,"w")
	comfile.write("#!/bin/csh\n#$ -cwd\nyamtbx.run_xscale\n")
	comfile.close()
	os.system("chmod 744 %s"%cname)

	# making all.com
	allcom.write("cd %s\n"%apath)
	allcom.write("qsub run.com\n")
	allcom.write("cd ../\n")
