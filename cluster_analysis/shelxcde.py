import os,sys

patt=sys.argv[1].replace("xscale.hkl","")

abspatt= os.path.abspath(patt)
oname="%s/all.com"%abspatt

ofile=open("%s/all.com"%abspatt,"w")
ofile.write("#!/bin/csh\n")
ofile.write("#$ -cwd\n")
ofile.write("#$ -o %s\n"%abspatt)
ofile.write("#$ -e %s\n"%abspatt)
ofile.write("shelxc sad << eof > shelxc.log\n")
ofile.write("SAD  xscale.hkl\n")
ofile.write("CELL 45.20 143.96 294.47 90 90 90\n")
ofile.write("SPAG C2221\n")
ofile.write("MAXM 25\n")
ofile.write("eof\n")
ofile.write("shelxd sad_fa\n")
ofile.write("shelxe sad_fa -m20 -s0.57 -l2 -d0.1 -e3.2 -h -b -a3 -q -i > shelxe_o.log\n")
ofile.write("shelxe sad_fa -m20 -s0.57 -l2 -d0.1 -e3.2 -h -b -a3 -q  > shelxe_i.log\n")
ofile.close()

os.system("chmod a+x %s"%oname)
os.system("qsub %s"%oname)
