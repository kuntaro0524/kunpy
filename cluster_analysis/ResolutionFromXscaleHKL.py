import os,sys,math,subprocess
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")


class ResolutionFromXscaleHKL:
    def __init__(self, xscale_path):
        self.xscale_path = xscale_path

    def get_resolution(self):
        command = "yamtbx.python /oys/xtal/yamtbx/yamtbx/dataproc/auto/command_line/decide_resolution_cutoff.py %s"%self.xscale_path
        logs = subprocess.Popen(
            command, stdout=subprocess.PIPE,
            shell=True).communicate()[0]

        lines = logs.split('\n')
        
        for line in lines:
            if line.rfind("Suggested cutoff")!=-1:
                resol = float(line.split()[2])
        return resol

if __name__ == "__main__":
    rfx = ResolutionFromXscaleHKL(sys.argv[1])
    print rfx.get_resolution()
