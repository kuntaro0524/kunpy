import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/11.ClusterAnalysis")

import glob,numpy
import DirectoryProc
import ComRefine

# SPACE GROUP
symm="C2"
ref_dmin=2.7

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscale_list,path_list=dp.findTarget("xscale.mtz")

# Path selection keywords
keys=["run_03"]

# Keyword selection
newlist=[]
print "Original datasets: %5d"%len(xscale_list)
cnt=0
proc_xscale_list=[]
proc_path_list=[]

for xscale_mtz,path in zip(xscale_list,path_list):
        print path
        good_count=0
        for key in keys:
            if xscale_mtz.rfind(key)!=-1:
                good_count+=1
        if good_count==len(keys):
            proc_xscale_list.append(xscale_mtz)
            proc_path_list.append(path)
            cnt+=1

print "LEN=",len(proc_path_list)

# .eff file template
eff_file="/isilon/users/target/target/AutoUsers/181012/david/_kamoproc/Models/refine_template.eff"

# Free-R flags common
refmtz="/isilon/users/target/target/AutoUsers/181012/david/_kamoproc/Models/M5_ML375_reflections.mtz"

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
for proc_path in proc_path_list:
    comrefine=ComRefine.ComRefine(proc_path)
    comname=comrefine.refine_with_efffile(refmtz,"xscale.mtz",symm,ref_dmin,eff_file,"refine")
    os.system("qsub %s"%comname)
