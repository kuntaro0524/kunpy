import sys,os
sys.path.append("/isilon/BL32XU/BLsoft/PPPP/")
import glob,numpy
import DirectoryProc
import PhenixRefineLog
import DirectoryProc
import AnaCORRECT
from libtbx import easy_mp

# Finding xscale.mtz
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
logname=sys.argv[1]
phenilog_list,path_list=dp.findTarget(logname)

# All log file
alllog = open("refine_summary.csv","w")
alllog.write("# path,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi,cc_half,dmin,rwork,rfree,bfac\n")

# find xscale.mtz and refine  with REFMAC JellyBody and phenix.refine
loglines=[]
for phenilog,pathh in zip(phenilog_list,path_list):
    outfile="%s/cluster_result.dat"%pathh
    pl=PhenixRefineLog.PhenixRefineLog(phenilog)
    r,freer=pl.getRfactors()
    bfac=pl.getBfactor()
    dmin_ref = pl.getResolution()

    # XSCALE.LP normally locates at "../" from the phenix refine directory
    ac=AnaCORRECT.AnaCORRECT("%s/../XSCALE.LP"%pathh)
    #print outfile
    ofile=open(outfile,"w")
    ofile.write("# path,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi,cc_half,dmin,rwork,rfree,bfac\n")
    total_rmeas=ac.getTotalRmeas()
    compl,redun,outer_rmeas,outer_isigi,cchalf=ac.getOuterShellInfo()
    nds= ac.countDatasets()
    cols=pathh.split('/')

    loglines.append("path,%s,nds,%5d,compl,%8.4f,redun,%8.4f,total_rmeas,%8.4f,outer_rmeas,%8.4f,outer_isigi,%8.4f,cc_half,%8.4f,dmin,%5.2f" \
        %(pathh,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi,cchalf,dmin_ref))
    loglines.append(",rwork,%8.5f,rfree,%8.5f,bfac,%8.5f\n"%(r,freer,bfac))

    ofile.write("%s,%5d,%8.4f,%8.4f,%8.4f,%8.4f,%8.4f,%8.4f,%5.2f"%(pathh,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi,cchalf,dmin_ref))
    ofile.write(",%8.5f,%8.5f,%8.5f\n"%(r,freer,bfac))
    ofile.close()

    alllog.write("%s,%5d,%8.4f,%8.4f,%8.4f,%8.4f,%8.4f,%8.4f,%5.2f"%(pathh,nds,compl,redun,total_rmeas,outer_rmeas,outer_isigi,cchalf,dmin_ref))
    alllog.write(",%8.5f,%8.5f,%8.5f\n"%(r,freer,bfac))

alllog.close()

