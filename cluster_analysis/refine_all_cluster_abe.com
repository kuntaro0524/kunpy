#!/bin/csh

set TOPDIR=$PWD

foreach CLUSDIR(`ls| grep cluster_`)

# if run_02/ exists
if (-e $CLUSDIR/run_02/ccp4/) then
echo "ARUYO $CLUSDIR"
cd $CLUSDIR/run_02/ccp4/
yamtbx.python ~/PPPP/11.ClusterAnalysis/make_paired_refine_isotropic.py /isilon/users/khirata/khirata/170725-PH-resolution/model.pdb I23 2.0 1.5 F
csh all.csh
else
echo "run_02 directory does not exist"
endif

cd $TOPDIR
end
