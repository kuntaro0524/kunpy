import os,sys,subprocess

xscale_file = sys.argv[1]
xscale_file = os.path.abspath(sys.argv[1])

def res_cmd(cmd):
  return subprocess.Popen(
      cmd, stdout=subprocess.PIPE,
      shell=True).communicate()[0]

command = "yamtbx.python /oys/xtal/yamtbx/yamtbx/dataproc/auto/command_line/decide_resolution_cutoff.py %s"%xscale_file

#subprocess.call([command],shell=False)

logs = res_cmd(command)
lines = logs.split('\n')

for line in lines:
    if line.rfind("Suggested cutoff")!=-1:
        print line.split()[2]
#print logs
