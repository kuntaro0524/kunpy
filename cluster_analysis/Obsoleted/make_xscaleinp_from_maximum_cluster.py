import os,sys,math

dandan="../"

def prepDirectory(dirname):
        if os.path.exists(dirname)==False:
                os.mkdir(dirname)

lines=open(sys.argv[1],"r").readlines()

otherlines=[]
inputfiles=[]
for line in lines:
	if line.rfind("INPUT_FILE")!=-1:
		if line.rfind("*")!=-1:
			newpath=line.replace("INPUT_FILE=*","INPUT_FILE=")
		else:
			newpath=line.replace("INPUT_FILE= ","INPUT_FILE= %s"%(dandan))
		inputfiles.append(newpath)
	else:
		otherlines.append(line)

ninput=len(inputfiles)
#infile_num=[2500,1000,500,200,100,50,25,10]
infile_num=[ninput,2500,1000,500,200,100,50,25,10]

allcom=open("all_xscale.com","w")
allcom.write("#!/bin/csh\n")
# processing directory
for inum in infile_num:
	pdir="proc_%04d"%inum
	# Make processing directory
	prepDirectory(pdir)
	apath=os.path.abspath(pdir)
	new_xscale_inp="%s/XSCALE.INP"%pdir
	ofile=open(new_xscale_inp,"w")
	for l in otherlines:
		ofile.write("%s"%l)
	# reference data is always same
	#ofile.write("%s"%ref_data)
	for i in range(0,inum):
		ofile.write("%s"%inputfiles[i])

	cname="%s/run.com"%apath
	comfile=open(cname,"w")
	comfile.write("#!/bin/csh\n#$ -cwd\nyamtbx.run_xscale\n")
	comfile.close()
	os.system("chmod 744 %s"%cname)

	# making all.com
	allcom.write("cd %s\n"%apath)
	allcom.write("qsub run.com\n")
	allcom.write("cd ../\n")
