import os,sys,math

dandan="../"

def prepDirectory(dirname):
        if os.path.exists(dirname)==False:
                os.mkdir(dirname)

lines=open("XSCALE.INP","r").readlines()
lp_lines=open("XSCALE.LP","r").readlines()

line_idx=0
flist=[]
for line in lp_lines:
        if line.rfind("R-FACTORS FOR INTENSITIES OF DATA SET")!=-1:
		#print line,line_idx,lp_lines[line_idx+5]
                cols=line.split()
                line2=lp_lines[line_idx+5]
                cols2=line2.split()
                rfac=float(cols2[1].replace("%",""))
                fname=cols[6]
                flist.append((rfac,fname))
        line_idx+=1

# R-factor sorted file list
flist.sort(key=lambda x:x[0])

def get_xscale_inp(nfiles):
	file_list=[]
	idx=0
	for f in flist:
		idx+=1
        	file_list.append(f[1])
		if idx == nfiles:
			break
	return file_list

otherlines=[]
for line in lines:
	if line.rfind("INPUT_FILE")!=-1:
		continue
	else:
		otherlines.append(line)

infile_num=[2500,2000,1500,1000,500,200,100,50,25,10]

# delete exceeded infile_num 
dellist = lambda items, indexes: [item for index, item in enumerate(items) if index not in indexes]
n_allfiles=len(flist)

idx=0
delete_list=[]
for l in infile_num:
        if l > n_allfiles:
                delete_list.append(idx)
        idx+=1

infile_num=dellist(infile_num,delete_list)

allcom=open("all_xscale.com","w")
allcom.write("#!/bin/csh\n")
# processing directory
for inum in infile_num:
	pdir="proc_%04d"%inum
	# Make processing directory
	prepDirectory(pdir)
	apath=os.path.abspath(pdir)
	new_xscale_inp="%s/XSCALE.INP"%pdir
	ofile=open(new_xscale_inp,"w")
	for l in otherlines:
		ofile.write("%s"%l)
	inpfiles=get_xscale_inp(inum)
	for l in inpfiles:
		ofile.write("INPUT_FILE=../%s\n"%l)

	cname="%s/run.com"%apath
	comfile=open(cname,"w")
	comfile.write("#!/bin/csh\n#$ -cwd\nyamtbx.run_xscale\n")
	comfile.close()
	os.system("chmod 744 %s"%cname)

	# making all.com
	allcom.write("cd %s\n"%apath)
	allcom.write("qsub run.com\n")
	allcom.write("cd ../\n")
