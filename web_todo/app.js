const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose")

const app = express();

app.set('view engine', 'ejs');
// CSS path for html
app.use(express.static("public"));

// body parser encoding
app.use(bodyParser.urlencoded({
  extended: true
}))

// Mongoose initialization
mongoose.connect("mongodb://localhost:27017/todolist_db", {
  useNewUrlParser: true
});

// mongoose.connect("mongodb+srv://kuntaro0524:kunio335@cluster0.pvt85.mongodb.net/todolist_db", {
//   useNewUrlParser: true
// });

// Mongoose schema description
const todo_schema = {
  name: String
};

// making a model of each todo String
const Item = mongoose.model("Item", todo_schema);

// Mongoose list schema (including toto_schema
const list_schema = {
  listname: String,
  contents: [todo_schema]
};
// Making mongoose model
const List = mongoose.model("List", list_schema);

// Default items for the initial template
const the_first = new Item({
  name: "Read this document."
});
const the_second = new Item({
  name: "Add ToDo by putting [+] button."
});
const the_third = new Item({
  name: "Remove by clicking the checkbox!"
});
// Making an array of the default todo listname
const default_todolist = [the_first, the_second, the_third];

app.get("/", function(req, res) {
  Item.find({}, function(err, foundItems) {
    console.log("################# FOUND ITEMS")
    console.log(foundItems);
    if (foundItems.length === 0) {
      Item.insertMany(default_todolist, function(err) {
        if (err) {
          console.log("Something wrong!");
        } else {
          console.log("Okay to be saved.");
        }
      })
    } else {
      res.render("list", {
        list_title: "Today",
        list_contents: foundItems
      });
    }
  })
})

app.get("/:tobemade", function(req, res) {
  listname = req.params.tobemade;
  console.log("app.get tobemade was called.");
  // 取得したリスト名での登録がなかった場合には
  List.findOne({
    listname: listname
  }, function(err, foundItems) {
    if (!err) {
      if (!foundItems) {
        console.log("Not exists");
        const newList = new List({
          listname: listname,
          contents: default_todolist
        })
        newList.save();
        res.redirect("/" + listname);
      } else {
        console.log("Exists!");
        console.log(foundItems);
        res.render("list", {
          list_title: listname,
          list_contents: foundItems.contents
        });
      }
    }
  });
});

app.post("/", function(req, res) {
  const listname = req.body.listname;
  // 新しいtodoを受け取る
  const newItem = req.body.newItem;

  // 新しいToDoを受け取ったlistnameのDBに対して登録する
  List.findOne({
    listname: listname
  }, function(err, foundList) {
    console.log("<<<<<<<post.findOne<<<<<<<<<<");
    console.log(foundList);
    console.log("<<<<<<<<<<<<<<<<<");

    if (!err) {
      if (!foundList) {
        console.log("Not exists");
        const newList = new List({
          listname: listname,
          contents: default_todolist
        })
        newList.save();
        res.redirect("/" + listname);
      } else {
        console.log("Exists EEEEEEEEEEEEEEE!");
        // ToDoをリストのアイテム配列に追加する
        console.log("###########################");
        const item_tobe_added = new Item({
          name: newItem
        });
        console.log(foundList.contents + "," + newItem);
        console.log("###########################");
        foundList.contents.push(item_tobe_added);
        foundList.save();
        res.redirect("/" + listname);
      }
    }
  });

});

app.post("/delete", function(req, res) {
  // 新しいtodoを受け取る
  console.log(req.body);

  const ID_tobe_deleted = req.body.id_todo;
  console.log("To be deleted:" + ID_tobe_deleted);
  List.findOneAndUpdate({
      listname: req.body.listTitle
    }, {
      $pull: {
        contents: {
          _id: ID_tobe_deleted
        }
      }
    }, function(err, foundList) {
      if (!err) {
        console.log("Success!")
      }
    });
  // IDを指定してそれを削除するmongoDBのメソッド
  // List.findByIdAndRemove(ID_tobe_deleted, function(err) {
  //   if(!err) {
  //     console.log("Successfully deleted."+ID_tobe_deleted);
  //   }
  // })
  // 存在するリストの全てに対して網羅的に調査をする
  // List.find({}, function(err, foundList) {
  //   console.log("LISTLIST="+foundList.length);
  //   foundList.forEach(function(each_list) {
  //     each_list.contents.forEach(function(content) {
  //       console.log(content);
  //       if(content._id == ID_tobe_deleted) {
  //         console.log("deleting!")
  //       } else {
  //         console.log("ID="+content._id);
  //       }
  //     });
  //   });
  // });

  // // 受け取ったIDのデータをDBから消去する
  // List.findOneAndDelete({
  //   _id: ID_tobe_deleted
  // }, function(err, foundItem) {
  //   console.log("Deleted!" + foundItem);
  // });
  res.redirect("/" + req.body.listTitle);
});

// heroku server用に色々と準備
let port = process.env.PORT;
if(port==null || port == "") {
  port=9200;
}

app.listen(port, function() {
  console.log("Server started.")
})

