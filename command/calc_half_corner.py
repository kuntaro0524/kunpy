import os,sys,numpy

wavelength = float(sys.argv[1])
user_resol = float(sys.argv[2])
beamline = sys.argv[3]

if beamline.lower() == "bl32xu":
    min_dim = 233.0
elif beamline.lower() == "bl45xu":
    min_dim = 422.0

theta = numpy.arcsin(wavelength / 2.0 / user_resol)
bunbo = 2.0 * numpy.tan(2.0 * theta)
camera_len = min_dim / bunbo

# half corner resolution 
half_corner_radius = numpy.sqrt(5.0)/2.0 * (min_dim/2.0)
#print half_corner_radius
theta_re = 0.5 * numpy.arctan(half_corner_radius/camera_len)

dmin_re = wavelength / 2.0 / numpy.sin(theta_re)

print "half corner resolution is %8.3f A when the edge resolution is %8.3fA" % (dmin_re, user_resol)
