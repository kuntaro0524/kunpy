import os, sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

proc_path=sys.argv[1]
# read SPOTS.XDS
spotxds=os.path.join(proc_path,"COLSPOT.LP")
lines = open(spotxds, "r").readlines()

read_flag=False
#data_dict={}
dict_list=[]
for line in lines:
    if (line.rfind("FRAME #      NBKG     NSTRONG   I/O-FLAG")!=-1):
        read_flag=True
        continue
    if read_flag==True and line.rfind("NUMBER OF")!=-1:
        break
    if read_flag:
        cols=line.split()
        data_dict={}
        if len(cols)>1:
            frame=int(cols[0])
            n_spots=int(cols[2])
            data_dict['frame']=frame
            data_dict['nspots']=n_spots
            dict_list.append(data_dict)

#df = pd.DataFrame.from_dict(dict_list, orient='index')
df = pd.DataFrame(dict_list)

print(df.describe())

# Definition
def make_df_above(df, score_thresh):
    return(df[df['nspots']>mean_spots])

def make_df_in_frame(df, start_num, end_num):
    #print("RANGE = %5d ~ %5d" % (start_num, end_num))
    sel=((df['frame']>=start_num) & (df['frame']<=end_num))
    block_df=df[sel]
    
    return block_df

# Above mean value
mean_spots = df['nspots'].mean()
sel_df=make_df_above(df, mean_spots)

scatter_plot=sel_df.plot.scatter(x='frame',y='nspots',c="Red")
#plt.show()
#print("LENGTH=%5d\n" % len(sel_df))
#print(sel_df)

for block_first in np.arange(1,7200,300):
    block_end=block_first+299
    # Spot okay & in this block
    block_df=make_df_in_frame(sel_df, block_first, block_end)
    if len(block_df)>250:
        print("PROCESS:", block_first, block_end, len(block_df))
    # Relaxed conditions
    else:
        relaxed_df = make_df_above(df, 200)
        block_df = make_df_in_frame(relaxed_df, block_first, block_end)
        #print("Relaxed dataframe LENGTH=%5d" % len(block_df))
        if len(block_df)==0:
            continue

        max_frame = block_df['frame'].max()
        min_frame = block_df['frame'].min()
        #min_frame=block_df.loc[[block_df['frame'].idxmin()]]
        #max_frame=block_df.loc[[block_df['frame'].idxmax()]]
        #print("MIN=%5d"%min_frame)
        #print("MAX=%5d"%max_frame)
        print("RE PROCESS:", min_frame, max_frame, len(block_df))
