import numpy,os,sys

lines = open("XSCALE.INP","r").readlines()
ofile=open("XSCALE.HEAD","w")

nbatch=0
for line in lines:
    if line.rfind("INPUT_FILE")!=-1: continue
    if line.rfind("NBATCH")!=-1:
        tmp_batch=int(line.replace("NBATCH=",""))
        if tmp_batch > nbatch: nbatch=tmp_batch
    ofile.write("%s"%line)

print(nbatch)
