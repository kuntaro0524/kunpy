import os,sys,math, numpy
import re_xds_xscale.ChooseAndMakeXSCALEINP as ChooseAndMakeXSCALEINP
import re_xds_xscale.RunXDSXSCALE as RunXDSXSCALE
import Libs.DirectoryProc as DirectoryProc

if __name__=="__main__":

    if len(sys.argv) != 7:
        print "# sys.argv[1]: high resolution limit to choose datasets"
        print "# sys.argv[2]: low resolution limit to choose datasets"
        print "# sys.argv[3]: number of resolution cuts"
        print "# sys.argv[4]: higher riso values"
        print "# sys.argv[5]: lower riso values"
        print "# sys.argv[6]: number of rfactor cuts"
        sys.exit()

    # Making "XSCALE.HEAD" from XSCALE.INP
    lines = open("XSCALE.INP","r").readlines()
    ofile=open("XSCALE.HEAD","w")

    # Read a current XSCALE.INP and extracts NBATCH
    nbatch=0
    for line in lines:
        if line.rfind("INPUT_FILE")!=-1: continue
        if line.rfind("NBATCH")!=-1:
            tmp_batch=int(line.replace("NBATCH=",""))
            if tmp_batch > nbatch: nbatch=tmp_batch
            continue
        ofile.write("%s"%line)

    # R-factor list
    rfact_list = [50,75,100,125,150,175,200]

    dp = DirectoryProc.DirectoryProc("./")
    #    def __init__(self, xscalelp, input_header, nbin = 20):
    xscalelp = "XSCALE.LP"
    head_xscale = "XSCALE.HEAD"
    high_res = float(sys.argv[1])
    low_res = float(sys.argv[2])
    n_trials = int(sys.argv[3])
    high_riso = float(sys.argv[4])
    low_riso = float(sys.argv[5])
    n_riso = int(sys.argv[6])

    # resolution limit list
    resol_list = numpy.linspace(high_res, low_res, n_trials)
    # riso limit list
    riso_list = numpy.linspace(high_riso, low_riso, n_riso)

    camx = ChooseAndMakeXSCALEINP.ChooseAndMakeXSCALEINP(xscalelp, head_xscale, nbin=10)
    camx.prep()

    for dthresh in resol_list:
        for rthresh in riso_list:
            camx.reset_list()
            # select datasets with higher resolution than 'dthresh'
            nresol = camx.choose_resol(dthresh)
            # select datasets with lower overall Rsymm than 'rthresh'
            noverr = camx.choose_overall_r(rthresh)

            print(nresol, noverr)
        
            dirname = "./%3.1fA_rfac_%d"%(dthresh,rthresh)

            # making directories
            abs_path = os.path.abspath(dirname)
            if os.path.exists(dirname)==False:
                os.makedirs(abs_path)
            # make XSCALE.INP for each scaling
            camx.makeXSCALEINP(abs_path,add_path="../",nbatch=nbatch)
        
            ofile = open("%s/choose.log"%abs_path,"w")
            ofile.write("Resolution cutoff %8.2f A\n"%dthresh)
            ofile.write("Overall R  cutoff %8.1f percent\n"%rthresh)
            ofile.write("N(resol)=%5d N(overall_R)=%5d\n"%(nresol,noverr))

            # qsub file
            #camx.makeXSCALEINP(abs_path,add_path="../")
            rxs = RunXDSXSCALE.RunXDSXSCALE(abs_path)
            # conducting everything for scaling
            rxs.runXSCALE(option = "convenient")
            print abs_path
