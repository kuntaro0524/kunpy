import sys
sys.path.append("/user/target/NewZoo/Libs/")

import Capture
import Device
import sys,os
import time
import socket
import numpy as np

if __name__ == "__main__":
    # ms 
    ms = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ms.connect(("172.24.242.41",10101))

    dev = Device.Device(ms)
    dev.init()

    gonio=dev.gonio

    cap = Capture.Capture()
    cappath = "/user/target/Data/2023B/BL32XU_240214_adachi/Montage/"

    print("START-connect from main")
    # cap.setGain(1500)
    # print "END  -connect from main"
    cap.prep()

    # coordinates
    # The 1st experiment
    # upper_left = (-2.4589, 9.4721, -2.1436)
    # upper_right = (-2.4589, -8.7016, -2.1436)
    # lower_left = (-2.4589, 9.4721, 3.1564)
    # lower_right = (-2.4589, -8.7016, 3.1564)

    # The 2nd experiment
    upper_left=(   -0.2052,   14.1353,   -6.1124 )
    upper_right=(   -0.2052,   -9.8647,   -6.1123 )
    lower_left=(   -0.2052,   14.1353,    4.1562 )
    lower_right=(   -0.2052,   -9.8647,    4.1577 )

    # Starting point is upper_left
    gonio.moveXYZmm(upper_left[0],upper_left[1],upper_left[2])
    initial_y = upper_left[1]
    initial_z = upper_left[2]

    # Y length
    y_whole = upper_left[1] - upper_right[1]
    # Z length
    z_whole = upper_left[2] - lower_left[2]
    print(y_whole, z_whole)

    # y step
    y_step = 1.7
    z_step = 1.4

    # y grids
    ny = int(np.fabs(y_whole / y_step)) + 2
    nz = int(np.fabs(z_whole / z_step)) + 2
    print(ny,nz)

    currdir=os.path.abspath("./")

    logfile=open("cap.log","w")
    for pz in range(0,nz):
        target_z = initial_z + pz * z_step
        if target_z > lower_left[2]:
            target_z = lower_left[2]
        for py in range(0,ny):
            target_y = initial_y - py * y_step
            if target_y < upper_right[1]:
                target_y = upper_right[1]

            print(upper_left[0],target_y, target_z)
            gonio.moveXYZmm(upper_left[0], target_y, target_z)

            filename="%s/%03d_%03d_cap.ppm" % (currdir,py,pz)
            cap.setBright(18000)
            time.sleep(0.5)
            cap.capture(filename)
            logfile.write("%5d %5d %8.4f %8.4f %8.4f %s\n"%(py, pz, upper_left[0],target_y, target_z, filename))
    logfile.close()
