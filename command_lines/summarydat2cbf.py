import sys,math,os

sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/Libs/")
sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/h5/")
import ManH5
import SummaryDat

if __name__ == "__main__":
    prefix="2d"
    summary_dat_path="../_spotfinder/"

    nv=int(sys.argv[2])
    nh=int(sys.argv[3])

    sumdat=SummaryDat.SummaryDat(summary_dat_path,nv,nh)
    # Dummy values to skip completeness check
    nimages_all=3
    completeness=0.95

    #sumdat.readSummary(prefix,nimages_all,completeness,timeout=120)
    sumdat.setMinMax(30, 100)
    #heatmap=sumdat.process("n_spots")

    # Reading Hd5 file
    mh5=ManH5.ManH5()
    masterfile=sys.argv[1]
    mydic=mh5.readHeader(masterfile)

    # Pandas dataframe of 'good' scored images
    df = sumdat.makeDF()

    for filename in df['filename']:
        cols=filename.split("_")
        imgnum=int(cols[1].replace(".img",""))
        # Image number
        outimg = "out_%06d.cbf" % imgnum
        mh5.generateSingle(masterfile, imgnum, outimg)
