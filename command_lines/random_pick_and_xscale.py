import random,sys,os

# input file = "XSCALE.INP"
lines = open(sys.argv[1], "r").readlines()

# store lines
# NBATCH get flag
isNbatch = False
isGetImageDirectory = False

input_files = []
store_lines = []
for line in lines:
    if line.rfind("INPUT_FILE") != -1:
        pppp=line.split("=")
        relative_refl_path = pppp[1].lstrip()
        # print(relative_image_path)
        # image_dire = relative_image_path[:relative_image_path.rfind("/")]
        abs_refl_path = os.path.abspath(relative_refl_path)
        input_files.append(abs_refl_path)

    elif line.rfind("NBATCH=") != -1:
        if isNbatch == False:
            nbatch = line.split("=")[1]
            isNbatch = True
        else:
            continue
    else:
        store_lines.append(line)

# making XDS input file
def make_xscale_out(dir_name, proc_subsets, sge_run = True):
    if os.path.exists(dir_name) == False:
        os.makedirs(dir_name)

    # relative_refl_path = os.path.relpath()
    filename = os.path.join(dir_name, "XSCALE.INP")
    xdsinp = open(filename,"w")

    for line in store_lines:
        xdsinp.write("%s" % line)

    for proc_set in proc_subsets:
        relpath = os.path.relpath(proc_set, dir_name)
        relpath = relpath.replace("*","")
        xdsinp.write("INPUT_FILE=%s" % relpath)
        xdsinp.write("NBATCH=%s" % nbatch)

    xdsinp.close()

    # Com file for qsub of SGE
    if sge_run == True:
        # Making a com file
        abs_dir = os.path.abspath(dir_name)

        comfile = os.path.join(abs_dir, "xds_auto.sh")
        ofile = open(comfile,"w")
        # ofile.write("#!/bin/csh\n\n")
        ofile.write("#$ -wd %s\n" % abs_dir)
        ofile.write("#$ -o %s\n"%abs_dir)
        ofile.write("#$ -e %s\n\n"%abs_dir)
        ofile.write("xscale_par")
        ofile.close()

        # # Change mode of the file
        # os.system("chmod 744 %s" % comfile)
        # os.system("csh %s" % comfile)
        # os.system("xscale_par")

proc_group = {}

# number of datasets in run_03 of the maximum number: cluster
# 199 1MGy
# 293 2MGy
# 251 5MGy
# 306 10MGy
# 279 20MGy

print(len(input_files))

# Directory name
n_merge = int(sys.argv[2])
dir_name = "merge%03d" % (n_merge)
proc_subsets = random.sample(input_files, n_merge)
make_xscale_out(dir_name, proc_subsets)
