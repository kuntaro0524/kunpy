import scipy
import os,sys
import pandas as pd
import numpy as np

sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/h5/")
import ManH5

h5file=ManH5.ManH5()
mydic=h5file.readHeader(sys.argv[1])
print(mydic)

def isIceRing(dvalue):
    range_list=[(3.93,3.87), (3.70,3.64), (3.47,3.41), (2.70,2.64), (2.28,2.22), (2.102,2.042), (1.978,1.918), (1.948,1.888), (1.913,1.853), (1.751,1.691), (1.524,1.519), (1.473,1.470), (1.444,1.440), (1.372,1.368), (1.367,1.363), (1.299,1.296), (1.275,1.274), (1.261,1.259), (1.224,1.222), (1.171,1.168), (1.124,1.122)]

    for rmax,rmin in range_list:
        #print("checking range:%8.5f - %8.5f"% (rmin,rmax))
        if dvalue > rmin and dvalue <= rmax:
            #print("checking range:%8.5f - %8.5f: value=%8.5f"% (rmin,rmax,dvalue))
            return True

    return False

def calc_d(xorg,yorg,xdet,ydet,dist,wavelength,pix_resol):
    # distance from the origin [mm]
    dt = np.sqrt(np.power(xorg-xdet,2.0)+np.power(yorg-ydet,2.0)) * pix_resol
    # theta value (radians)
    theta = np.arctan(dt/dist)/2.0
    # d value for this reflection
    d = wavelength / 2.0 / np.sin(theta)
    return d

# Detector parameters
#'Distance': 0.18, 'BeamX': 1544.7, 'BeamY': 1595.5, 
xorg = mydic['BeamX']
yorg = mydic['BeamY']
dist = mydic['Distance']*1000.0
wl = mydic['Wavelength']
pix_resol=mydic['PixelX']*1000.0

print(pix_resol)

datafile=pd.read_csv(sys.argv[2],delim_whitespace=True)

selfile=open("./selecte.dat","w")
good_list=[]
for x,y,filename in zip(datafile['x'],datafile['y'],datafile['imagename']):
    dvalue=calc_d(xorg,yorg,x,y,dist,wl,pix_resol)
    if isIceRing(dvalue) == False:
        selfile.write("%30s %8.5f %8.5f\n"%(filename,x,y))

grouped = datafile.groupby("imagename").size().reset_index(name='count')
sorted_grouped = grouped.sort_values('count', ascending=False)

condition1 = sorted_grouped['count'] <= 100 
condition2 = sorted_grouped['count'] >= 50
final_condition=condition1 & condition2
filtered_group = sorted_grouped[final_condition]
print(filtered_group)

for filename in filtered_group['imagename']:
    cols=filename.split("/")
    fffff=cols[-1]
    imgnum=int(fffff.split("_")[1].replace(".img",""))
    out_image = "out_%06d.cbf"%imgnum
    h5file.generateSingle(sys.argv[1], imgnum, out_image)
