import pandas as pd
import os,sys
import ZoodbPandas

if __name__ == "__main__":
    zdp = ZoodbPandas.ZoodbPandas()
    dbfiles=sys.argv[1:]

    df=zdp.prep(dbfiles)
    # Data collected
    col_df=zdp.selectCollectedPins(df)
    target_group = zdp.makeGroupBySampleName(col_df)

    for (target_name), tmpdf in target_group:
        # Same groups
        print("###############%s###################"%target_name)
        for column_name, item in tmpdf.iterrows():
            print("%s: sample=%s wl=%s dist=%s"
                         % (item['root_dir'], item['sample_name'], item['wavelength'], item['dist_ds']))

    # sorted=zdp.sortDFbyPuckPin(col_df)


    # Sorting the dataframe
