import pandas as pd
import numpy as np

# DEMO for pandas function
# My dictionary settings
mydict = pd.DataFrame ({
    'pin':['CPS1999-02', 'CPS1999-03','CPS2000-05','CPS2001-05','CPS2002-06','CPS2003-07'],
    'sample_name':['test1', 'test1','test1','test2','test2','test2'],
    'mode':['multi', 'helical','helical','multi','multi','helical'],
    'exp_time':[0.05, 0.04,0.03,0.04,0.02,0.03],
    'dist_ds': [150.0, 200.0, 175.0, 100.0,125.0, 130.0]
})

print("PANDAS DICTIONARY")
print(mydict)

# Columns label
print("COLUMNS ONLY")
print(mydict.columns)

# Rows label
print("Data index")
print(mydict.index)

# Group by
print("Grouping by sample_name")
indices = mydict.groupby('sample_name').groups
print("Grouping indices=", indices)

# Extracting grouping information like this
sample_name_list = []
for sample_name, group_df in mydict.groupby('sample_name'):
    # print( "SAMPLE_NAME, GROUP_DF",sample_name,group_df
    sample_name_list.append(sample_name)

print(sample_name_list)


print("######### SAMPLE NAME GROUPING ############")

new_pan=[]
for s in sample_name_list:
    pan = mydict.groupby('sample_name').get_group(s)

    new_pan.append(pan)

print("@@@@@@@@MODE GROUPING@@@@@@@@")

for n in new_pan:
    for mode, group_df in n.groupby('mode'):
        print( mode, group_df)
