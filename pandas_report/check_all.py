import sys
import pandas as pd
import numpy as np
import Libs.ESA as ESA
import sqlite3

def read_summary_dat(summary_path, score_thresh=10):
    df = pd.read_csv(summary_path, delim_whitespace=True)
    df.describe()
    sel1= (df['kind'].str.contains("n_spots"))
    ch1=df[sel1]
    ngrids= len(ch1)
    #ch1.to_csv("test.csv")
    sel2=ch1['data']>score_thresh
    
    if sel2.sum() !=0:
        ch2=ch1[sel2]
        return ngrids,len(ch2['data']),ch2['data'].mean(),ch2['data'].min(),ch2['data'].max()
    else:
        print("No data")
        return ngrids,None,None

def makeOnMouse(imgindex, picture, height, button):
    string = ""
    string += "<td><a href=%s " % picture
    string += "onmouseover=\"document.getElementById('ph-%ds').style.display='block';\" " % imgindex
    string += "onmouseout=\"document.getElementById('ph-%ds').style.display='none'; \"> %s </a> " % (imgindex, button)
    string += "<div style=\"position:absolute;\"> "
    string += "<img src=\"%s\" height=\"%d\" id=\"ph-%ds\"" % (picture, height, imgindex)
    string += " style=\"zindex: 10; position: absolute; top: 50px; display:none;\" /></td></div>"
    return string

if __name__ == "__main__":
    # File reading 
    file_sqlite3=sys.argv[1]
    conn = sqlite3.connect(file_sqlite3)
    cursor=conn.cursor()
    df=pd.read_sql_query('SELECT * FROM ESA', conn)

    # Output html
    outhtml=open("log.html","w")

    # Extracting grouping information like this
    imgindex=0
    for time_str, group_df in df.groupby('sample_name'):
        # Data selection
        # No severe troubles
        no_troubles_cond = group_df.isDone < 3000
        pd.set_option('display.max_rows', 50)
    
        # Data collected pins
        collected_cond = (group_df.nds_multi > 0) | (group_df.nds_helical>0)
        coldf=group_df[collected_cond]
        print("Data collected pins=",len(coldf))
        
        #Sort by puck/pin IDs
        sorted_df = coldf.sort_values(["puckid","pinid"],ascending=[True,True])
    
        # iterative processing for each 'row'
        for column_name, item in sorted_df.iterrows():
            # Root directory 
            data_root="%s/%s-%02d/"%(item.root_dir, item.puckid, item.pinid)
            # check paths
            ana_scan_path="%s/scan%02d/2d/_spotfinder" % (data_root, item.n_mount)
            summary_path = "%s/summary.dat" % ana_scan_path
            #print(item.sample_name,read_summary_dat(summary_path, score_thresh=10))
            #print(summary_path)

            # Scan png file path
            scan_png="%s/plot_2d_n_spots.png" % ana_scan_path
            #print(scan_png)

            outhtml.write("<h4>%s<h4>\n"%makeOnMouse(imgindex, scan_png, 400, item.sample_name))
            imgindex+=1
