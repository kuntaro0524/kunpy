import pandas as pd
import sqlite3,sys
import logging
import logging.config
import numpy
import Libs.Date as Date
#import Libs.Date

class ProcPandas():
    def __init__(self):
        d = Date.Date()
        time_str = d.getNowMyFormat(option="date")
        logname = "zoodbpandas_%s.log" % time_str
        logging.config.fileConfig('/isilon/BL32XU/BLsoft/PPPP/10.Zoo/Libs/logging.conf', defaults={'logfile_name': logname})
        self.logger = logging.getLogger('zoodb_pandas')
        self.beamline="bl32xu"

    def setDF(self, df):
        self.df = df

    def readZooDB(self, dbfile):
        conn = sqlite3.connect(dbfile)
        cursor=conn.cursor()
        df=pd.read_sql_query('SELECT * FROM ESA', conn)
        return df

    def viewDF(self):
        self.logger.info(self.df)

    def viewColumns(self):
        self.logger.info(self.df.columns)
        
    def makeGroup(self, column_name):
        target_group=self.df.groupby(column_name)
        return target_group

    def getDFwithSamplename(self, target_name):
        groups=self.makeGroup('sample_name')

        for name, group in groups:
            if name == target_name:
                return group

    def makeWavelengthGroup(self, df):
        groups=df.groupby('wavelength')
        for wavelength, group in groups:
            print(wavelength, group)

    def makeGroupSameResolutionLimit(self):
        target_group=df.groupby(['wavelength','dist_ds'])
        if len(target_group) == 1:
            self.logger.info("wavelength/distance group is only one.")
        else:
            return target_group

    def make(self, df):
        groups=df.groupby('wavelength')
        for wavelength, group in groups:
            print(wavelength, group)

    def calcResolHalfCorner(self, wavelength, dist_ds):
        if self.beamline.lower() == "bl32xu":
            min_dim = 233.0
        elif self.beamline.lower() == "bl45xu":
            min_dim = 422.0
        elif self.beamline.lower() == "bl41xu":
            min_dim = 311.2

        # half corner resolution
        half_corner_radius = numpy.sqrt(5.0) / 2.0 * (min_dim / 2.0)
        theta_re = 0.5 * numpy.arctan(half_corner_radius / dist_ds)

        dmin_re = wavelength / 2.0 / numpy.sin(theta_re)
        return dmin_re

    def getCollectedDF(self, df):
        self.logger.info("All data in DB={}".format(len(df)))
        sel_failed = df['isDone']>=4000
        self.logger.info("Failed collection={}".format(len(df[sel_failed])))
        sel_ok = df['isDone']==1
        self.logger.info("Okay collection={}".format(len(df[sel_ok])))

        return df[sel_ok]

    def groupByMode(self, df):
        target_group = df.groupby(['mode'])
        groups=[]
        for mode, df in target_group: 
            groups.append((mode,df))
    
        self.logger.info(groups)

        return groups

    # 2020/11/28 K.Hirata
    # Resolution limit grouping for KAMO processing
    def groupSameResolutionLimit(self, df):
        target_group=df.groupby(['wavelength','dist_ds'])
        n_groups=len(target_group)
        
        self.logger.info("N_groups=%5d"%n_groups)

        loglines=[]
        for (wl,dist), df in target_group: 
            dmin=self.calcResolHalfCorner(wl, dist)
            test_data = df[['root_dir', 'sample_name','puckid', 'pinid']]
            for column_name, item in test_data.iterrows():
                datadir="%s/_kamoproc/%s-%02d" % (item.root_dir, item.puckid, item.pinid)
                loglines.append("%s,%s,%s"%(datadir,item['sample_name'],dmin))
        return loglines
                                
##########################################

if __name__ == "__main__":
    zdp = ProcPandas()
    df = zdp.readZooDB(sys.argv[1])
    zdp.setDF(df)

    target_column="sample_name"
    target_name="YeeEJ"
    newdf=zdp.getDFwithSamplename(target_name)
    print(newdf)
    
    print("<<<<<<<<<<<<<<<<<<<<<<")
    coldf=zdp.getCollectedDF(df)
    groups=zdp.groupByMode(coldf)
    for mode, gdf in groups:
        print("MODE={}".format(mode))
        lines=zdp.groupSameResolutionLimit(gdf)
        for line in lines:
            print(line)


