import sys
import pandas as pd
import numpy as np
#import Libs.ESA as ESA
import sqlite3
import glob

def read_summary_dat(summary_path, score_thresh=10):
    df = pd.read_csv(summary_path, delim_whitespace=True)
    df.describe()
    sel1= (df['kind'].str.contains("n_spots"))
    ch1=df[sel1]
    ngrids= len(ch1)
    #ch1.to_csv("test.csv")
    sel2=ch1['data']>score_thresh
    
    if sel2.sum() !=0:
        ch2=ch1[sel2]
        return ngrids,len(ch2['data']),ch2['data'].mean(),ch2['data'].min(),ch2['data'].max()
    else:
        print("No data")
        return ngrids,None,None

if __name__ == "__main__":
    # File reading 
    
    for file_index,db_files in enumerate(sys.argv[1:]):
        conn = sqlite3.connect(db_files)
        cursor=conn.cursor()
        if file_index==0:
            df=pd.read_sql_query('SELECT * FROM ESA', conn)
            print("First file contains: %5d\n" % len(df))
        else:
            tmpdf=pd.read_sql_query('SELECT * FROM ESA', conn)
            df=pd.concat([df,tmpdf])

    print("All files contains: %5d\n" % len(df))
    print("ALLDF=", df)

    # KAMO directory

    # Extracting grouping information like this
    for time_str, group_df in df.groupby('sample_name'):
        isFound = group_df['sample_name'].str.contains("F10")
        if isFound.iloc[0]==True:
            target_df=group_df
        else:
            target_df=None

    if target_df is None:
        print("Something wrong")
        sys.exit()

    # Data selection
    # No severe troubles
    no_troubles_cond = target_df.isDone < 3000
    pd.set_option('display.max_rows', 50)

    # Data collected pins
    collected_cond = (target_df.nds_multi > 0) | (target_df.nds_helical>0)
    coldf=target_df[collected_cond]
    print("Data collected pins=",len(coldf))
    
    #Sort by puck/pin IDs
    sorted_df = coldf.sort_values(["puckid","pinid"],ascending=[True,True])

    # iterative processing for each 'row'
    for column_name, item in sorted_df.iterrows():
        # Root directory 
        data_root="%s/%s-%02d/"%(item.root_dir, item.puckid, item.pinid)
        kamo_root="%s/_kamoproc/%s-%02d/data%02d/"%(item.root_dir, item.puckid, item.pinid,item.n_mount)
        # check paths
        ana_scan_path="%s/scan%02d/2d/_spotfinder" % (data_root, item.n_mount)
        summary_path = "%s/summary.dat" % ana_scan_path

        correctlp_list = glob.glob("%s/*/CORRECT.LP"%kamo_root)
        if len(correctlp_list) != 0:
            print("%s,%s,yes" % (kamo_root, item.sample_name))
