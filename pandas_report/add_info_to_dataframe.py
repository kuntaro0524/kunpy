import sys
import pandas as pd
import numpy as np
import datetime
import ESA
import logging

if __name__ == "__main__":

    #df = pd.DataFrame(np.arange(12).reshape(3, 4),
    #             columns=['col_0', 'col_1', 'col_2', 'col_3'],
    #             index=['row_0', 'row_1', 'row_2'])
    df = pd.DataFrame(np.arange(12).reshape(3, 4),
                  columns=['col_0', 'col_1', 'col_2', 'col_3'])

    print(df)

    new_data=["pochi","mike","bow"]
    df["new_col1"] = new_data

    print(df)

    # calculation of the array
    df["calulated_value1"] = df['col_3']-df['col_1']
    df["calulated_value2"] = df['col_3']*df['col_1']


    print(df)

    print("###############################")

    # Function
    def function_example(column_value):
        print "COLUMN_VALUE=",column_value
        if column_value == "pochi":
            return datetime.datetime.now()
        elif column_value == "mike":
            return np.pi
        elif column_value == "bow":
            return np.sqrt(2.0)

    add_array = np.arange(12).reshape(3,4)
    print add_array

    ################################
    s1 = pd.Series([100, 200, 300], index=['row_0', 'row_1', 'row_2'], name='NEWNEW')
    print (s1)
    df_concat = pd.concat([df, s1],axis=1)
    print (df_concat)

    ################################
    time1=[]
    time2=[]
    time3=[]
    for i in range(0,3):
        time1.append(i*3)
        time2.append(i*5)
        time3.append(i*10)

    # Making pandas series from calculated array
    time1_ser = pd.Series(time1,  name="time1")
    time2_ser = pd.Series(time2,  name="time2")
    time3_ser = pd.Series(time3,  name="time3")
    print "TIME1 series",time1_ser
    print "TIME2 series",time2_ser
    print "TIME3 series",time3_ser

    df_concat = pd.concat([df, time1_ser],axis=1)
    df_concat = pd.concat([df_concat, time2_ser],axis=1)
    df_concat = pd.concat([df_concat, time3_ser],axis=1)
    print (df_concat)
