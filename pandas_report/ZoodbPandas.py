import pandas as pd
import sqlite3,sys
import logging
import logging.config
import numpy
#sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/Libs/")
#import Date
#import Libs.Date

class ZoodbPandas():
    def __init__(self):
        self.isPrep = False
        self.beamline="bl32xu"
        # kuntaro_log
        #d = Date.Date()
        #time_str = d.getNowMyFormat(option="date")
        logname = "zoodbpandas.log"
        logging.config.fileConfig('logging.conf', defaults={'logfile_name': logname})
        self.lg = logging.getLogger('zoodb_pandas')

        # setting of pandas
        pd.set_option('display.max_rows', 50)

    def readDBfiles(self, dbfiles):
        if len(dbfiles)==0: self.lg.error("TEST")
        for file_index,db_files in enumerate(dbfiles):
            conn = sqlite3.connect(db_files)
            cursor=conn.cursor()
            if file_index==0:
                df=pd.read_sql_query('SELECT * FROM ESA', conn)
                self.lg.info("First file contains: %5d\n" % len(df))
            else:
                tmpdf=pd.read_sql_query('SELECT * FROM ESA', conn)
                df=pd.concat([df,tmpdf])
    
        self.lg.info("All files contains: %5d\n" % len(df))
        # self.lg.info("ALLDF=%s", df)

        return df

    def prep(self, dbfiles):
        df=self.readDBfiles(dbfiles)
        self.isPrep=True
        return df

    def viewDF(self):
        self.lg.info(self.df)

    def viewColumns(self):
        self.lg.info(self.df.columns)
        
    def makeGroupBy(self, target_column, value):
        if self.isPrep==False: self.prep()
        # Group by
        self.lg.info("Grouping by sample_name")
        indices = self.df.groupby('sample_name').groups

        # Extracting grouping information like this
        new_index = self.df.groupby('sample_name')

        sample_name_list = []
        for sample_name, group_df in self.df.groupby('sample_name'):
            sample_name_list.append(sample_name)

    def makeGroupBySampleName(self, df):
        # Group by
        self.lg.info("Grouping by sample_name")
        group_df = df.groupby('sample_name')

        return group_df

    def makeGroup(self, column_name):
        target_group=self.df.groupby(column_name)
        return target_group

    def getSampleName(self, target_name):
        groups=self.makeGroup('sample_name')

        for name, group in groups:
            if name == target_name:
                return group

    def makeWavelengthGroup(self, df):
        groups=df.groupby('wavelength')
        for wavelength, group in groups:
            print(wavelength, group)

    def makeGroupSameResolutionLimit(self):
        target_group=df.groupby(['wavelength','dist_ds'])
        if len(target_group) == 1:
            self.lg.info("wavelength/distance group is only one.")
        else:
            return target_group

    def selectNoTroubles(self, df):
        # No severe troubles (error codes: 4001)
        no_troubles_cond = df.isDone < 3000
        return df[no_troubles_cond]

    def selectCollectedPins(self, df):
        if len(df) == 0:
            self.lg.info("No data is included!")
            return
        # Data collected pins
        collected_cond = (df.nds_multi > 0) | (df.nds_helical > 0)
        # collected &
        coldf = df[collected_cond]
        self.lg.info("Data collected pins=%5d" % len(coldf))
        return coldf

    # select dataframes with 'sample_name'='target_sample_name'
    def selectDFwithSampleName(self, df, target_sample_name):
        # Extracting grouping information like this
        for time_str, group_df in df.groupby('sample_name'):
            isFound = group_df['sample_name'].str.contains(target_sample_name)
            if isFound.iloc[0] == True:
                self.lg.info("Target name %s was found." % target_sample_name)
                target_df = group_df
            else:
                target_df = None

        if target_df is None:
            print("Something wrong")
            return None
        else:
            return target_df

    def make(self, df):
        groups=df.groupby('wavelength')
        for wavelength, group in groups:
            print(wavelength, group)

    def calcResolHalfCorner(self, wavelength, dist_ds):
        if self.beamline.lower() == "bl32xu":
            min_dim = 233.0
        elif self.beamline.lower() == "bl45xu":
            min_dim = 422.0
        elif self.beamline.lower() == "bl41xu":
            min_dim = 311.2

        # half corner resolution
        half_corner_radius = numpy.sqrt(5.0) / 2.0 * (min_dim / 2.0)
        theta_re = 0.5 * numpy.arctan(half_corner_radius / dist_ds)

        dmin_re = wavelength / 2.0 / numpy.sin(theta_re)
        return dmin_re

    def sortDFbyPuckPin(self, df):
        # Sort by puck/pin IDs
        sorted_df = df.sort_values(["puckid", "pinid"], ascending=[True, True])
        return sorted_df

    # 2020/11/28 K.Hirata
    # Resolution limit grouping for KAMO processing
    def groupSameResolLimit(self, df):
        target_group=df.groupby(['wavelength','dist_ds'])
        n_groups=len(target_group)

        # Divide into groups
        self.lg.info(">> %5d groups <<" % n_groups)
        self.lg.info("#####################################")
        self.lg.info("%s"%target_group)
        self.lg.info("#####################################")
        print(target_group)
        group_df=[]
        for (wl,dist), tmpdf in target_group:
            dmin=self.calcResolHalfCorner(wl, dist)
            test_data = tmpdf[['root_dir', 'sample_name','wavelength', 'dist_ds']]
            # Same groups
            self.lg.info("##########################################")
            for column_name, item in test_data.iterrows():
                self.lg.info("%s: sample=%s wl=%s dist=%s dmin=%5.2fA"
                             %(item['root_dir'],item['sample_name'],item['wavelength'],item['dist_ds'], dmin))
            self.lg.info("##########################################")
            group_df.append(test_data)

        return group_df
                                
##########################################

if __name__ == "__main__":
    zdp = ZoodbPandas()
    dbfiles=sys.argv[1:]
    df=zdp.prep(dbfiles)

    ppp=zdp.groupSameResolLimit(df)
    # ppp=zdp.groupSameResolLimit(df)
    for edb in ppp:
        print("#<<<<<<<<<<<<Same group<<<<<<<<<<<<<<")
        print(edb)
        print("#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

    """
    # Put sampe sample name data frame
    target_df=zdp.selectDFwithSampleName(df, "F10")
    # Data collected
    col_df=zdp.selectCollectedPins(target_df)
    # Sorting the dataframe
    sorted=zdp.sortDFbyPuckPin(col_df)

    print(ppp)
    """
    #newdf=zdp.makeGroupBy("sample_name","test")
    #newdf=zdp.makeGroup("sample_name")
    #print(newdf.groups)
    #target_column="sample_name"
    #target_name="BepPepON"
   
    #newdf=zdp.getSampleName(target_name)
    #print(newdf)
    #newdf2=zdp.makeWavelengthGroup(newdf)
    #print(newdf2)
    #newdf3=zdp.makeW
    #print("TESTESTESTESTESTSET")
    #zdp.groupSameResolutionLimit()
