import sys
import pandas as pd
import numpy as np
import Libs.ESA as ESA
import logging

class ZooPandas():
    def __init__(self, zoodb):
        self.zoodb = zoodb
        esa = ESA.ESA(self.zoodb)
        self.zoodicts = esa.getDict()
        self.isPrep = False

        # Logging
        FORMAT = '%(asctime)-15s %(module)s %(levelname)-8s %(lineno)s %(message)s'

        logging.basicConfig(filename="./test.log", level=logging.DEBUG, format=FORMAT)
        self.logger = logging.getLogger('ZooPandas')

    def init(self):
        self.dict_all = {}
        self.panda_dict = pd.DataFrame(self.zoodicts)

        self.logger.info(self.panda_dict.columns)
        self.logger.info(self.panda_dict.index)
        self.logger.info(self.panda_dict)

        self.isPrep = True

    def pickupMode(self, mode):
        if not self.isPrep: self.prep()
        print self.panda_dict["mode"]

    def calcTime(self):
        criteria1 = self.panda_dict.isDS > 0
        criteria2 = self.panda_dict.nds_multi > 1
        final_criteria = criteria1 & criteria2
        selected_items = self.panda_dict[final_criteria]
        self.logger.info("Selected items=%5d"%len(selected_items))
        self.logger.info(self.panda_dict[final_criteria])
        #print self.panda_dict["t_meas_start"]

    """
    # Group by
    print "Grouping by sample_name"
    indices = mydict.groupby('sample_name').groups
    print "Grouping indices=", indices

    # Extracting grouping information like this
    sample_name_list = []
    for sample_name, group_df in mydict.groupby('sample_name'):
        # print "SAMPLE_NAME, GROUP_DF",sample_name,group_df
        sample_name_list.append(sample_name)

    print sample_name_list

    print "######### SAMPLE NAME GROUPING ############"

    new_pan=[]
    for s in sample_name_list:
        pan = mydict.groupby('sample_name').get_group(s)

        new_pan.append(pan)

    print "@@@@@@@@MODE GROUPING@@@@@@@@"

    for n in new_pan:
        for mode, group_df in n.groupby('mode'):
            print mode, group_df
    """

if __name__ == "__main__":
    zoop = ZooPandas(sys.argv[1])
    zoop.init()

    zoop.pickupMode("multi")
    zoop.calcTime()