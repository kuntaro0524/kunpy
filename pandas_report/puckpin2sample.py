import sys
import pandas as pd
import sqlite3

file_sqlite3=sys.argv[1]
conn = sqlite3.connect(file_sqlite3)
cursor=conn.cursor()

df=pd.read_sql_query('SELECT * FROM ESA', conn)

# Selection
sel01 = df['puckid'] == sys.argv[2]
sel02 = df['pinid'] == int(sys.argv[3])

sel_all=sel01&sel02

df_sel=df[sel_all]

print(df_sel['sample_name'])
