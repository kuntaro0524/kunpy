import configparser
 
config = configparser.ConfigParser()
config.read('config.ini')
 
print(config.sections())
 
sample1 = config['SAMPLE1']
 
int_key = sample1.getint('int_key')
print(int_key)
 
int_key = sample1.getint('int_key2','0')
print(int_key)
