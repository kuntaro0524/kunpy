import os,sys
import cv2
import argparse
import glob

def variance_of_laplacian(image):
    # compute the Laplacian of the image and then return the focus
    # measure, which is simply the variance of the Laplacian
    return cv2.Laplacian(image, cv2.CV_64F).var()

def imgname_x_code(imgname):
    rindx = imgname.rfind("_")
    gx=float(imgname[rindx+1:].replace(".ppm",""))
    return gx


# Image list
image_list = glob.glob("./*.ppm")

for imgname in image_list:
    # load the image, convert it to grayscale, and compute the
    # focus measure of the image using the Variance of Laplacian
    # method
    text="test"
    image = cv2.imread(imgname)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    fm = variance_of_laplacian(gray)

    cv2.putText(image, "{}: {:.2f}".format(text, fm), (10, 30),
        cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)

    gx = imgname_x_code(imgname)
    print("%s %s" %(gx, fm))
    #cv2.imshow("Image", image)
    #key = cv2.waitKey(0)
