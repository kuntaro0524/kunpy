// import axios from "axios";
// import { useState } from "react";

export const DisplayConds = (props) => {
    const { conds } = props;
    return (
        <div>
            <table>
                {conds.map((cond) => (
                    <tr key={cond._id}>
                        <td>
                            {cond.puckid}
                        </td>
                        <td>
                            {cond.pinid}
                        </td>
                    </tr>
                ))}
            </table>
        </div>
    );
};
