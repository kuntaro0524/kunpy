import cv2
import numpy as np
import math
import sys

def main():
    raw_img = cv2.imread(sys.argv[1], cv2.IMREAD_GRAYSCALE)
    height, width = raw_img.shape[:2]

    imgproc = cv2.bitwise_not(raw_img)
    # 二値化
    _,src_img = cv2.threshold(imgproc, 0, 255, \
                             cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    # 輪郭を取得する
    contours, _ = cv2.findContours(src_img, \
                             cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # 輪郭までの距離を計算する
    dist_img = np.empty(src_img.shape, dtype=np.float32)
    for i in range(src_img.shape[0]):
        for j in range(src_img.shape[1]):

            # この関数は，点が輪郭の内側にあるか，外側にあるか，
            # 輪郭上に乗っている（あるいは，頂点と一致している）かを判別。
            #
            # 返却値
            # 正値（内側），
            # 負置（外側）
            # 0（辺上）
            dist_img[i,j] = cv2.pointPolygonTest(contours[0], (j,i), True)

    # 最小値, 最大値, 最小値の座標, 最大値の座標
    minVal, maxVal, min_loc, max_loc = cv2.minMaxLoc(dist_img)
    minVal = abs(minVal)    # 外側
    maxVal = abs(maxVal)    # 内側

    # 距離をグラフィカルに描く
    dw_img = np.zeros((height, width, 3), dtype=np.uint8)
    for y in range(src_img.shape[0]):
        for x in range(src_img.shape[1]):
            if dist_img[y,x] < 0:
                # 輪郭の外側
                v = int(255.0 - abs(dist_img[y,x]) * 255.0 / minVal)
                dw_img[y,x]  = (0,v,v)
            elif dist_img[y,x] > 0:
                # 輪郭の内側
                v = 255 - dist_img[y,x] * 255 / maxVal
                dw_img[y,x] = (0,0,v)
            else:
                # 輪郭(白)
                dw_img[y,x] = (255,255,255)
    cv2.imwrite('dw_img.jpg', dw_img)

    # 距離と内接円
    dw_img2 = np.zeros((height, width, 3), dtype=np.uint8)
    dw_img2.fill(255)
    cv2.drawContours(dw_img2,contours,0,(255,0,255),-1)

    # 距離イメージで求めた最大値,最小値の座標
    radius = int(maxVal)
    cv2.circle(dw_img2,max_loc, radius, (100,0,0), 2, cv2.LINE_AA)
    cv2.imwrite('dw_img2.png', dw_img2)

    cv2.imshow('dw_img', dw_img)
    cv2.imshow('dw_img2', dw_img2)
    cv2.imshow('Source', src_img)
    cv2.waitKey()

if __name__ == '__main__':
    main()