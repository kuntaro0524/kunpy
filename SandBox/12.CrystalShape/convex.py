from tkinter import W
import cv2,sys
import numpy as np


im = cv2.imread(sys.argv[1])
# グレイで読む
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
# データを解析するものは白が物体になるので黒で描いてしまったから反転する
imgproc = cv2.bitwise_not(imgray)

# カラー画像を出力用にコピーする
im_con = im.copy()
im_sec = im.copy()
# 二値化している
ret,thresh = cv2.threshold(imgproc,127,255,0)
# コントアを探している→エッジ抽出
contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

# 見つかったコントアを図に描きこんでいる
cv2.drawContours(im_con, contours, -1, (125,255,125), 5)

# 結果を画像ファイルとして保存
cv2.imwrite('result.png', im_con)

for veps in [0.1,0.05,0.01,0.005,0.001]:
    for cont in contours:
        epsilon = veps*cv2.arcLength(cont,True)
        approx = cv2.approxPolyDP(cont,epsilon,True)
        print(veps, len(approx))
        cv2.drawContours(im_sec, approx, -1, (0,0,255), 15)
        judge = cv2.isContourConvex(cont)
        print("Totsuhou?=",judge)
    cv2.imwrite('concon_%s.png' % veps, im_sec)
    # hull = cv2.convexHull(cont)
    # print("######### %s ###############" % k)
    # for [e] in hull:
        # print(e[0],e[1])

