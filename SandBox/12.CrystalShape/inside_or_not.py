import cv2
import numpy as np
import math
import sys

def main():
    raw_img = cv2.imread(sys.argv[1], cv2.IMREAD_GRAYSCALE)
    height, width = raw_img.shape[:2]

    imgproc = cv2.bitwise_not(raw_img)
    # 二値化
    _,src_img = cv2.threshold(imgproc, 0, 255, \
                             cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    # 輪郭を取得する
    contours, _ = cv2.findContours(src_img, \
                             cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # 輪郭までの距離を計算する
    dist_img = np.empty(src_img.shape, dtype=np.float32)
    for i in range(src_img.shape[0]):
        for j in range(src_img.shape[1]):

            # この関数は，点が輪郭の内側にあるか，外側にあるか，
            # 輪郭上に乗っている（あるいは，頂点と一致している）かを判別。
            #
            # 返却値
            # 正値（内側），
            # 負置（外側）
            # 0（辺上）
            return_value = cv2.pointPolygonTest(contours[0], (j,i), True)
            # dist_img[i,j] = cv2.pointPolygonTest(contours[0], (j,i), True)
            if return_value < 0.0:
                return_value = 0.0
            else:
                return_value=1
            print(i,j,return_value)

if __name__ == '__main__':
    main()