# ユーザ情報も管理できるようになっている

### 復習

MongoDB の中の collection をわけて管理 →RESTful の API 機能をもたせてる
<mongodb 中>
DB: zoo
collections: bluser

10.ZOOside/manBLuser.py からこの API を通して新しい情報を更新できるようになっている。
（？）重複した情報を登録しないようにしないといけないような。

2022/01/14：ユーザの情報に実験情報を追加していくことが理想的ではあるが、まずは２月初旬の BT 終了までに ZOO の測定をビームラインでやるということを目標に実装を進める。
