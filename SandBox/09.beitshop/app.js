const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");

const app = express();

app.set("view engine", "ejs");

/* normally required */
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

/* JSONデータを読むために必要 */
app.use(bodyParser.json());

/* Mongoose DB を読み込む */
mongoose.connect("mongodb://localhost:27017/zoo", {
  useNewUrlParser: true,
});

app.use(express.static("public"));

var cors = require("cors");
app.use(cors());

const collection_schema = {
  root_dir: String,
  o_index: Number,
  p_index: Number,
  mode: String,
  puckid: String,
  pinid: Number,
  sample_name: String,
  wavelength: Number,
  raster_vbeam: Number,
  raster_hbeam: Number,
  att_raster: Number,
  hebi_att: Number,
  exp_raster: Number,
  dist_raster: Number,
  loopsize: Number,
  score_min: Number,
  score_max: Number,
  maxhits: Number,
  total_osc: Number,
  osc_width: Number,
  ds_vbeam: Number,
  ds_hbeam: Number,
  exp_ds: Number,
  dist_ds: Number,
  dose_ds: Number,
  offset_angle: Number,
  reduced_fact: Number,
  ntimes: Number,
  meas_name: String,
  cry_min_size_um: Number,
  cry_max_size_um: Number,
  hel_full_osc: Number,
  hel_part_osc: Number,
  raster_roi: Number,
  ln2_flag: Number,
  cover_scan_flag: Number,
  zoomcap_flag: Number,
  warm_time: Number,
  isSkip: Number,
  isMount: Number,
  isLoopCenter: Number,
  isRaster: Number,
  isDS: Number,
  isDone: Number,
  scan_height: Number,
  scan_width: Number,
  n_mount: Number,
  nds_multi: Number,
  nds_helical: Number,
  nds_helpart: Number,
  t_meas_start: String,
  t_mount_end: String,
  t_cent_start: Number,
  t_cent_end: Number,
  t_raster_start: Number,
  t_raster_end: Number,
  t_ds_start: Number,
  t_ds_end: Number,
  t_dismount_start: Number,
  t_dismount_end: Number,
  data_index: Number,
  n_mount_fails: Number,
  log_mount: Number,
  hel_cry_size: Number,
  flux: Number,
  phs_per_deg: Number,
};

/* definitions of data record on the database: 'mesurements'*/
const MeasInfo = mongoose.model("pppp", collection_schema, "measurements");

app.get("/measurements", function (req, res) {
  MeasInfo.find(function (err, foundItems) {
    res.send(foundItems);
  });
});

/* json dataを読むための手続き */
app.post("/measurements", function (req, res) {
  /* Expected 'req.body' contains 'measurement condition' in json format. */
  const newCond = req.body;
  console.log(req.body);
  /* post されたデータを新たにDBへ登録する */
  const newData = new MeasInfo(newCond);
  newData.save(function (err) {
    if (!err) {
      res.send("Successfully added a new condition");
    } else {
      res.send(err);
    }
  });
});

/* delete を実装する */
app.delete("/measurements", function (req, res) {
  MeasInfo.deleteMany(function (err) {
    if (!err) {
      res.send("successfully deleted all conds");
    } else {
      res.send(err);
    }
  });
});

/* IDを指定してその情報に対して　get/post などする*/
/* ID = 'id' assigned by MongoDB at the commit stage of ZOODB. */
/* I noticed that 'id' is required to enter the new database, 
this is sometimes impossible */

app
  .route("/measurements/:measID")
  .get(function (req, res) {
    const mid = req.params.measID;
    MeasInfo.findOne({ _id: mid }, function (err, foundCond) {
      if (foundCond) {
        res.send(foundCond);
      } else {
        res.send(err);
      }
    });
  })
  .put(function (req, res) {
    const mid = req.params.measID;
    console.log("PPPP=" + mid);
    /* JSON data */
    const newCond = req.body;
    console.log(newCond);
    MeasInfo.updateOne(
      /* conditions */
      { _id: mid },
      { $set: newCond },
      { overwrite: true },
      function (err) {
        if (!err) {
          res.send("Successfully updated article.");
        } else {
          res.send("ERRORERROR");
        }
      }
    );
  })

  /* 正直、メカニズムは理解できていないがこれにより、URLで与えられたIDの条件の一つずつを指定して
　　更新できるようになった。 */

  .patch(function (req, res) {
    const mid = req.params.measID;
    console.log("PPPP=" + mid);
    /* JSON data */
    /* 受け取るのはJSONそのもの */
    /* condの中身のパラメータを一つでも保有していればOK */
    const newCond = req.body;
    console.log(newCond);

    MeasInfo.updateOne(
      /* conditions */
      { _id: mid },
      /* この部分にrequestから受け取ったJSONを入れる */
      { $set: newCond },
      { overwrite: true },
      function (err) {
        if (!err) {
          res.send("Successfully updated article.");
        } else {
          res.send("ERRORERROR");
        }
      }
    );
  })

  /* IDを指定してそのエントリを削除するという方法　使うことはなさげ */
  .delete(function (req, res) {
    const mid = req.params.measID;
    MeasInfo.findOneAndDelete(
      /* conditions */
      { _id: mid },
      function (err) {
        if (!err) {
          res.send("Successfully updated article.");
        } else {
          res.send("ERRORERROR");
        }
      }
    );
  });

// ユーザ情報に関するDBについてもこれに実装してみるよ
/* definitions of data record on the database: 'users'*/
const users_schema = {
  name: String,
  sp8id: String,
  type: String,
  affiliation: String,
  assigned: Date,
  expired: Date,
  measIDs: [Object],
};
const UserInfo = mongoose.model("users", users_schema, "blusers");

/* json dataを読むための手続き */
app.post("/blusers", function (req, res) {
  /* Expected 'req.body' contains 'measurement condition' in json format. */
  const newCond = req.body;
  console.log(newCond);
  console.log("Here:" + req.body);
  /* post されたデータを新たにDBへ登録する */
  const newData = new UserInfo(newCond);
  newData.save(function (err) {
    if (!err) {
      res.send("Successfully added a new condition");
    } else {
      res.send(err);
    }
  });
});

// user情報を取得するための方法
app.get("/blusers", function (req, res) {
  UserInfo.find(function (err, foundItems) {
    res.send(foundItems);
  });
});

// // TODO:
app.listen(1234, function () {
  console.log("Server srated on port 1234");
});
