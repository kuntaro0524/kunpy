const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require('cors');

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(express.static("public"));
app.use(cors());

mongoose.connect("mongodb://localhost:27017/zoo", {
  useNewUrlParser: true
});

const collection_schema = {
 "root_dir" : String,
 "o_index" : Number,
 "p_index" : Number,
 "mode" : String,
 "puckid" : String,
 "pinid" : Number,
 "sample_name" : String,
 "wavelength" : Number,
 "raster_vbeam" : Number,
 "raster_hbeam" : Number,
 "att_raster" : Number,
 "hebi_att" : Number,
 "exp_raster" : Number,
 "dist_raster" : Number,
 "loopsize" : Number,
 "score_min" : Number,
 "score_max" : Number,
 "maxhits" : Number,
 "total_osc" : Number,
 "osc_width" : Number,
 "ds_vbeam" : Number,
 "ds_hbeam" : Number,
 "exp_ds" : Number,
 "dist_ds" : Number,
 "dose_ds" : Number,
 "offset_angle" : Number,
 "reduced_fact" : Number,
 "ntimes" : Number,
 "meas_name" : String,
 "cry_min_size_um" : Number,
 "cry_max_size_um" : Number,
 "hel_full_osc" : Number,
 "hel_part_osc" : Number,
 "raster_roi" : Number,
 "ln2_flag" : Number,
 "cover_scan_flag" : Number,
 "zoomcap_flag" : Number,
 "warm_time" : Number,
 "isSkip" : Number,
 "isMount" : Number,
 "isLoopCenter" : Number,
 "isRaster" : Number,
 "isDS" : Number,
 "isDone" : Number,
 "scan_height" : Number,
 "scan_width" : Number,
 "n_mount" : Number,
 "nds_multi" : Number,
 "nds_helical" : Number,
 "nds_helpart" : Number,
 "t_meas_start" : String,
 "t_mount_end" : String,
 "t_cent_start" : Number,
 "t_cent_end" : Number,
 "t_raster_start" : Number,
 "t_raster_end" : Number,
 "t_ds_start" : Number,
 "t_ds_end" : Number,
 "t_dismount_start" : Number,
 "t_dismount_end" : Number,
 "data_index" : Number,
 "n_mount_fails" : Number,
 "log_mount" : Number,
 "hel_cry_size" : Number,
 "flux" : Number,
 "phs_per_deg" : Number }

/*const MeasInfo = mongoose.model("pppp",collection_schema, 'conds');*/
const MeasInfo = mongoose.model("pppp", collection_schema, 'measurements');

// 重要なことはここで puckid はそのまま入力しているということだろう
MeasInfo.find({},function(error,result) {
	console.log("Here is the debug code");
	console.log(result);
});

app.route("/conds")
  .get(function(req, res) {
    MeasInfo.find({},function(err, result) {
      if (!err) {
        res.send(result);
      } else {
        res.send("Error!");
      }
    });
  })

    app.listen(1111, function() {
      console.log("Server started on port 3000");
    });
