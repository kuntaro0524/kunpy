const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require('cors');

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(express.static("public"));
app.use(cors());

mongoose.connect("mongodb://localhost:27017/zooconds", {
  useNewUrlParser: true
});

const collection_schema = {
  puckid: String,
  pinid: Number,
  meas00: {
	wavelength: Number,
	dist: Number
	}
}

const MeasInfo = mongoose.model("pppp",collection_schema, 'conds');

// 重要なことはここで puckid はそのまま入力しているということだろう
MeasInfo.find({},function(error,result) {
	console.log("Here is the debug code");
	console.log(result);
});

app.route("/conds")
  .get(function(req, res) {
    MeasInfo.find({},function(err, result) {
      if (!err) {
        res.send(result);
      } else {
        res.send("Error!");
      }
    });
  })

    app.listen(1111, function() {
      console.log("Server started on port 3000");
    });
