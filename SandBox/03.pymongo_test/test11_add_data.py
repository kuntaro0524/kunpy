import pprint
from pymongo import MongoClient
client = MongoClient('localhost', 27017)

db = client.zooconds
col = db.conds

new_meas = [
{
    "puckid": "CPS1092",
    "pinid": 11,
    "meas00": {
        "wavelength":1.0000,
        "dist": 120.0
        }
},
{
    "puckid":"CPS1010",
    "pinid": 15,
    "meas00": {
        "wavelength":1.2555,
        "dist": 150.0
        }
    }
]

result = col.insert_many(new_meas)

# post_id = "612877bdffcc8a6a42f44b1f"
# pprint.pprint(col.find_one({"name": "Kunio Hirata"}))
