import pprint
from pymongo import MongoClient
client = MongoClient('localhost', 27017)

db = client.zoo
col = db.measurements

new_meas = [
{
    "puckid": "CPS1091",
    "pinid": 16,
    "Date": "2020/08/30",
    "capture_image": "/isilon/kuntaro/raster.jpg",
    "measurement_conditions": {
        "scan_conditions": {
            "loop_size": 300.0,
            "raster_exp": 0.02,
            "dist_raster": 200.0,
            "rotation": "false",
            "start-time": "2020/08/30 01:30:55",
            "end-time": "2020/08/30 01:30:55",
            "finish_flag": "true"
        },
        "datacollection_conditions": {
            "exposure": 0.02,
            "distance": 150.0,
            "total_osc": 360.0,
            "osc_width": 0.02,
            "start-time": "2020/08/30 01:35:55",
            "end-time": "2020/08/30 01:35:55"
        }
    }
},
{
    "puckid": "CPS1091",
    "pinid": 11,
    "Date": "2020/08/30",
    "capture_image": "/isilon/kuntaro/raster.jpg",
    "measurement_conditions": {
        "scan_conditions": {
            "loop_size": 300.0,
            "raster_exp": 0.02,
            "dist_raster": 200.0,
            "rotation": "true",
            "start-time": "2020/08/30 01:40:55",
            "end-time": "2020/08/30 01:40:55",
            "finish_flag": "false"
        },
        "datacollection_conditions": {
            "exposure": 0.02,
            "distance": 150.0,
            "total_osc": 360.0,
            "osc_width": 0.02,
            "start-time": "2020/08/30 01:45:55",
            "end-time": "2020/08/30 01:45:55"
        }
    }
}
]

result = col.insert_many(new_meas)

# post_id = "612877bdffcc8a6a42f44b1f"
# pprint.pprint(col.find_one({"name": "Kunio Hirata"}))
