import re
from pymongo import MongoClient
import pymongo


def test_document_insert_and_find_many():
    with MongoClient("localhost", 27017) as client:
        test_db = client.test_db
        book_collection = test_db.book_collection

        books = [
            {"isbn": "978-4873117386", "title": "入門 Python 3", "price": 4070},
            {"isbn": "978-4048930611", "title": "エキスパートPythonプログラミング改訂2版", "price": 3960},
            {"isbn": "978-4297111113", "title": "Python実践入門", "price": 3278}
        ]

        book_collection.insert_many(books)
        assert book_collection.count_documents({}) == 3

        found_all_books = [book for book in book_collection.find(
            {}, sort=[("price", pymongo.DESCENDING)])]
        assert found_all_books[0]["title"] == "入門 Python 3"
        assert len(found_all_books) == 3

        found_books = [book for book in book_collection.find({"title": re.compile(
            "Python"), "price": {"$gt": 3500}}, sort=[("price", pymongo.ASCENDING)])]
        assert found_books[0]["title"] == "エキスパートPythonプログラミング改訂2版"
        assert len(found_books) == 2

        book_collection.delete_many({})

        assert book_collection.count_documents({}) == 0

test_document_insert_and_find_many()
