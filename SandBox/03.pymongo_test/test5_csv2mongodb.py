import os,sys, json
import pprint
import pandas as pd
from pymongo import MongoClient
client = MongoClient('localhost', 27017)

# CSVファイルを読み込んでJSONにする
df = pd.read_csv(sys.argv[1])
print("PANDAS data frame")
print(df)
print("###############")
json_strings = df.to_json(orient='records')

parsed = json.loads(json_strings)

print(json_strings)
print(parsed)

db = client.zoo
col = db.measurements
#
result = col.insert_many(parsed)

# post_id = "612877bdffcc8a6a42f44b1f"
# pprint.pprint(col.find_one({"name": "Kunio Hirata"}))
