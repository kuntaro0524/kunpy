import os,sys, json
import pprint
import pandas as pd
from pymongo import MongoClient
client = MongoClient('localhost', 27017)

db = client.zoo
col = db.measurements

# CPS3922 6 dist_ds will be updated to 550.0 \

filfil = [{'puckid':"CPS3922"}, {'pinid':6}]
print(filfil)
update = col.find_one_and_update({'$and':filfil},{'$set':{'dist_ds':550.0}})

# print("Updated number="+str(update.matched_count))
