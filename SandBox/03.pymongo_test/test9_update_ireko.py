import os,sys, json
import pprint
import pandas as pd
from pymongo import MongoClient
client = MongoClient('localhost', 27017)

db = client.zoo
col = db.measurements

# CPS3922 6 dist_ds will be updated to 550.0 \
ireko_data = {
    "measure_index": 0,
    "consumed_time": 6000.0,
    "crystal_size": 200.0,
    "scan_failed": "false"
}

process_result = {
    "proc_dir": "/isilon/_kamoproc/puckid/pinid",
    "resolution_limit": 3.2,
    "correct_lp": """
     SUBSET OF INTENSITY DATA WITH SIGNAL/NOISE >= -3.0 AS FUNCTION OF RESOLUTION
 RESOLUTION     NUMBER OF REFLECTIONS    COMPLETENESS R-FACTOR  R-FACTOR COMPARED I/SIGMA   R-meas  CC(1/2)  Anomal  SigAno   Nano
   LIMIT     OBSERVED  UNIQUE  POSSIBLE     OF DATA   observed  expected                                      Corr

    14.89         815     215       255       84.3%      17.6%     14.0%      773   11.82     19.9%    87.1*    -4    0.999      72
    11.61         925     236       254       92.9%      16.3%     15.3%      886   10.09     18.4%    95.8*    -7    0.852      75
     9.84        1260     313       329       95.1%      18.6%     17.3%     1215    8.95     20.9%    94.7*     4    0.930     104
     8.69        1315     337       353       95.5%      20.4%     21.7%     1267    6.58     23.4%    92.9*    -4    0.695     107
     7.87        1331     364       385       94.5%      25.2%     35.4%     1275    4.31     29.0%    89.6*    -8    0.576     100
     7.24        1425     412       442       93.2%      34.5%     62.9%     1348    2.81     39.7%    89.4*     8    0.521      99
     6.75        1357     414       453       91.4%      47.1%     92.5%     1269    1.78     54.2%    80.6*    -1    0.608      87
     6.34        1439     447       496       90.1%      84.1%    191.9%     1359    1.13     99.0%    50.4*    -5    0.403      99
     6.00        1650     474       518       91.5%     114.5%    276.1%     1574    0.82    132.4%    46.2*    -3    0.451     124
    total       11517    3212      3485       92.2%      22.9%     29.5%    10966    4.45     26.1%    92.7*    -1    0.651     867
"""
}

filfil = [{'puckid':"CPS3922"}, {'pinid':6}]
print(filfil)
update = col.find_one_and_update({'$and':filfil},{'$set':{'meas00':ireko_data}})
update = col.find_one_and_update({'$and':filfil},{'$set':{'proc00':process_result}})

# print("Updated number="+str(update.matched_count))
