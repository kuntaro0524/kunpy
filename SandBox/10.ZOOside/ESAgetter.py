# -*- coding: utf-8 -*-
import requests
import sqlite3
import os,sys, json

import pprint
import pandas as pd
#from pandas import json_normalize
# from pymongo import MongoClient


# This is the test program to communicate with 'BeitShop'
# ZOO ESA server

class ESAgetter: 
	def __init__(self):
		self.baseurl = "http://robo02:1234/measurements"
		self.isDebug = False

	# Get measurement information as JSON from the server.
	def get(self):
		r = requests.get(self.baseurl)
		self.jsons = r.json()

		# 単純にPandasのDataFrameとして処理する
		# これであっさりよめるじゃんか
		df =pd.DataFrame(self.jsons)
		# convert DataFrame to Python dictionary
		meas_dic = df.to_dict(orient='records')

		if self.isDebug:
			for p in meas_dic:
				print("#################3")
				print(p['p_index'],p['puckid'])

		return meas_dic

	# Get the measurement information via 'measID' defined in URL.
	def getIDinfo(self, targetID):
		# ID operation
		id_path=self.baseurl+"/%s" % targetID
		r = requests.get(id_path)

		# This 'jsons' has a type 'class dict' in python.
		jsons = r.json()

		return(jsons)

	# Modify designated value on the designated ID
	# ID is defined by 'URL' 
	def updateValue(self, targetID, pname, pvalue):
		print(targetID, pname, pvalue)
		# Patch operation
		patch_path=self.baseurl+"/%s" % targetID

		# BeitShopのPatch機能を用いれば、一つだけでも複数でも情報を与えるだけ変更することが可能
		r = requests.patch(patch_path, {pname: pvalue})

	# Python dictionary is directly used to update JSON object in 'patching' database.
	def updateValuesByDict(self, targetID, replace_dic):
		# tmpdict = {"cry_min_size_um": 50.0, "cry_max_size_um": 50.0, "dist_ds": 180.0, "nds_helical":30}
		# Patch operation
		patch_path=self.baseurl+"/%s" % targetID
		r = requests.patch(patch_path, replace_dic)

	def addCond(self, cond):
		requests.post(self.baseurl, cond)

	def addCondsFromZOODB(self, zoodb_name) :
		conn = sqlite3.connect(zoodb_name)
		cursor=conn.cursor()
		df=pd.read_sql_query('SELECT * FROM ESA', conn)
		print("Data frame size=")
		print(df.shape)
		json_strings = df.to_json(orient='records')
		print("##################JSON_STRING")
		print(json_strings)
		print("########################")
		parsed = json.loads(json_strings)

		print(type(parsed))
		for each_json in parsed:
			print("<<<<<<<")
			print(each_json)
			print(">>>>>>>")
			requests.post(self.baseurl, each_json)

	def patchAtOindex(self, o_index, param, value):
		replace_dic={param: value}
		self.patchAtOindexByDict(o_index,replace_dic)

	def patchAtOindexByDict(self, o_index, user_dict):
		for i,cond in enumerate(meas_dict):
			if cond['o_index'] == o_index:
				tmp_id = cond['_id']
				print("Found ID=", tmp_id)

		self.updateValuesByDict(tmp_id, user_dict)

if __name__ == "__main__":
	eg = ESAgetter()

	# Get entire information from ESAYA
	meas_dict=eg.get()
	print(meas_dict)
	print(type(meas_dict))

	#targetID="619b48bd9aef25a9b76c31c6"

	# light tests
	#print(eg.getIDinfo(targetID))

	# Update value in designated ID
	#eg.updateValue(targetID, 'dist_raster', 120.0)

	# Adding conditions : does this work? -> Yes, it does! 211223 done.
	#cond={"root_dir":"/isilon/users/target/target/AutoUsers/211022/my_new_address/","o_index":0,"p_index":0,"mode":"helical","puckid":"JAM0001","pinid":1,"sample_name":"Arabi-apo","wavelength":1.0002,"raster_vbeam":15,"raster_hbeam":10,"att_raster":27.797,"hebi_att":27.797,"exp_raster":0.01,"dist_raster":123,"loopsize":300,"score_min":10,"score_max":9999,"maxhits":1,"total_osc":360,"osc_width":0.1,"ds_vbeam":25,"ds_hbeam":10,"exp_ds":0.02,"dist_ds":184.6,"dose_ds":10,"offset_angle":0,"reduced_fact":1,"ntimes":1,"meas_name":"normal","cry_min_size_um":100,"cry_max_size_um":100,"hel_full_osc":60,"hel_part_osc":40,"raster_roi":1,"ln2_flag":1,"cover_scan_flag":1,"zoomcap_flag":1,"warm_time":30,"isSkip":0,"isMount":1,"isLoopCenter":1,"isRaster":1,"isDS":1,"isDone":1,"scan_height":198.7258,"scan_width":302.9754,"n_mount":0,"nds_multi":0,"nds_helical":1,"nds_helpart":0,"t_meas_start":"0,{meas_start_00:20211022102923},{mount_start_00:20211022102923},{mount_end_00:20211022103029},{center_start_00:20211022103144},{center_end_00:20211022103204},{raster_start_00:20211022103204},{raster_end_00:20211022103321},{ds_start_00:20211022103321},{ds_end_00:20211022103537},{meas_end_00:20211022103537}","t_mount_end":"","t_cent_start":0,"t_cent_end":0,"t_raster_start":0,"t_raster_end":0,"t_ds_start":0,"t_ds_end":0,"t_dismount_start":0,"t_dismount_end":0,"data_index":0,"n_mount_fails":0,"log_mount":0,"hel_cry_size":0,"flux":13771869695200,"phs_per_deg":0,"__v":0}
	#eg.addCond(cond)

	# Read sqlite3 DB from designated file in the 1st argument
	# zoodb（sqlite3）から登録できるようにしたものっぽいけど今の設計では不要
	# eg.addCondsFromZOODB(sys.argv[1])

	# Replace a bunch of information by a designated dictionary
    # search '_id' value from 'o_index' 
	print("########################")
	print("########################")
	print("########################")
	#print(next(x for x in meas_dict if x["o_index"] ==16))
	#print(next((i for i, x in enumerate(meas_dict) if x["o_index"] == 16), None))
	eg.patchAtOindex(16, "wavelength", 1.99999)
	eg.patchAtOindex(16, "puckid", "TEMPTEMP")
	eg.patchAtOindex(16, "isDone", 1)
	eg.patchAtOindex(0, "isSkip", 1)
	eg.patchAtOindex(1, "isSkip", 1)
	eg.patchAtOindex(2, "isSkip", 1)
	eg.patchAtOindex(3, "isSkip", 1)

	#replace_dic = {"cry_min_size_um": 99.9, "cry_max_size_um": 99.9, "dist_ds": 999.9, "nds_helical":99}
	#eg.updateValuesByDict(targetID, replace_dic)
