import os,sys, json
import sqlite3
import pprint
import pandas as pd
from pymongo import MongoClient
client = MongoClient('localhost', 27017)

# CSVファイルを読み込んで
df = pd.read_csv(sys.argv[1])
json_strings = df.to_json(orient='records')
parsed = json.loads(json_strings)

# ZOO 'data collection' collection -> 'collection'
db = client.zoo
col = db.collections
result = col.insert_many(parsed)

print("JSON_STRING")
print(json_strings)
print("Parsed JSON strings")
print(parsed)

db = client.zoo
col = db.measurements
result = col.insert_many(parsed)
