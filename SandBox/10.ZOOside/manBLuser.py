import requests
import sqlite3
import os,sys, json

import pprint
import pandas as pd
# from pymongo import MongoClient


# This is the test program to communicate with 'BeitShop'
# ZOO ESA server

class MANgetter: 
	def __init__(self):
		# API: beitshop を利用している
		self.baseurl = "http://localhost:1234/blusers"

	# Get measurement information as JSON from the server.
	def get(self):
		r = requests.get(self.baseurl)
		self.jsons = r.json()

		print(self.jsons)

		for json in self.jsons:
			# print("UNKO")
			print(json)

	# Get the measurement information via 'measID' defined in URL.
	def getIDinfo(self, targetID):
		# ID operation
		id_path=self.baseurl+"/%s" % targetID
		r = requests.get(id_path)

		# This 'jsons' has a type 'class dict' in python.
		jsons = r.json()

		return(jsons)

	# Modify designated value on the designated ID
	# ID is defined by 'URL' 
	def updateValue(self, targetID, pname, pvalue):
		print(targetID, pname, pvalue)
		# Patch operation
		patch_path=self.baseurl+"/%s" % targetID

		# BeitShopのPatch機能を用いれば、一つだけでも複数でも情報を与えるだけ変更することが可能
		r = requests.patch(patch_path, {pname: pvalue})

	# Python dictionary is directly used to update JSON object in 'patching' database.
	def updateValuesByDict(self, targetID, replace_dic):
		# tmpdict = {"cry_min_size_um": 50.0, "cry_max_size_um": 50.0, "dist_ds": 180.0, "nds_helical":30}

		# Patch operation
		patch_path=self.baseurl+"/%s" % targetID

		r = requests.patch(patch_path, replace_dic)

	# APIを通してデータベースに新しい情報を登録する
	def addCond(self, cond):
		requests.post(self.baseurl, cond)

	def addCondsFromZOODB(self, zoodb_name) :
		conn = sqlite3.connect(zoodb_name)
		cursor=conn.cursor()
		df=pd.read_sql_query('SELECT * FROM ESA', conn)
		print("Data frame size=")
		print(df.shape)
		json_strings = df.to_json(orient='records')
		print("##################JSON_STRING")
		print(json_strings)
		print("########################")
		parsed = json.loads(json_strings)

		print(type(parsed))
		for each_json in parsed:
			print("<<<<<<<")
			print(each_json)
			print(">>>>>>>")
			requests.post(self.baseurl, each_json)

if __name__ == "__main__":
	eg = MANgetter()
	#new_user = {"name": "Kunio Hirata", "sp8id": "0004294", "measIDs":["65423ED","UNKO","CHINKO"]}
	import datetime, time
	d = datetime.datetime.utcnow()
	nt = datetime.datetime.now()
	print(nt,nt.timestamp())
	for_js = int(time.mktime(d.timetuple())) * 1000
	print(for_js)
	new_user = {"name": "Ibuki Hirata", "sp8id": "0004649", "type":"public", "affiliation":"Kyoto University", "assigned":for_js, "expired": for_js}
	eg.addCond(new_user)