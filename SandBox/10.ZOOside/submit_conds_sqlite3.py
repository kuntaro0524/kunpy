import os,sys, json
import sqlite3
import pprint
import pandas as pd
from pymongo import MongoClient
client = MongoClient('localhost', 27017)

file_sqlite3=sys.argv[1]
conn = sqlite3.connect(file_sqlite3)
cursor=conn.cursor()
df=pd.read_sql_query('SELECT * FROM ESA', conn)
df.columns

# sqlite3ファイルを読み込んでJSONにする
json_strings = df.to_json(orient='records')
parsed = json.loads(json_strings)

print("JSON_STRING")
print(json_strings)
print("Parsed JSON strings")
print(parsed)

db = client.zoo
col = db.measurements
result = col.insert_many(parsed)
