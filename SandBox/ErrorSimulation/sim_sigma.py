import math
import numpy as np
import scipy

class ErrorModel:
    def __init__(self):
        self.test=3.0
        self.gain = 1.0

    def estimateSigma(self, Is, Ibg, m_bg_pix, n_pk_pix, k, a, n_time=1):
        # sigma_total^2 = sigma_Is^2 + m * sigma_Ins^2
        sigma_Is2 = self.gain * (Is +  Ibg + (float(m_bg_pix/n_pk_pix)*Ibg))
        sigma_Ins = m_bg_pix * np.power(k/a,2.0) * np.power(Is,2.0) / float(n_time)

        sigma_total2 = sigma_Is2 + sigma_Ins
        sigma_total = np.sqrt(sigma_total2)

        return sigma_total

if __name__=="__main__":
    em = ErrorModel()

    intensity=100.0
    for intensity in [1.0, 10.0, 100.0, 1000.0]:
        i_bg = np.sqrt(intensity)

        for ntime in [1,5,10,50,60,100,150,200]:
            sigma=em.estimateSigma(intensity, i_bg, 5, 25, 1, 10, n_time=ntime)
            ios=intensity/sigma
            print(intensity, ntime, sigma, ios)