<script>

 $('input#test').change(function() {
          var testfile = document.getElementById("test");
          var This = $(this);
          var files = $(this).prop('files');

          for(var i = 0; i < files.length; i++){
              readfile(files[i]);
          }
 });

function readfile(file){
    var div = document.createElement('div');
    var a_tag = document.createElement('a');
    var reader = new FileReader();
    var testfilename = document.getElementById("testfilename");
    reader.readAsDataURL(file)
    reader.onload = function() {
               a_tag.href = reader.result;
               a_tag.download = file.name;
               a_tag.textContent =  file.name;
               var br = document.createElement('br');
               div.appendChild(a_tag);
               div.appendChild(br);
               testfilename.appendChild(div);
    }
}
 </script>
