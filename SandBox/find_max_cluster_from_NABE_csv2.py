#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np
pd.set_option('display.max_colwidth', 150)
pd.get_option("display.max_colwidth")


# In[3]:


df = pd.read_csv("./oden_prep.csv")
print("Number of data=", len(df))


# In[18]:


n_final=0
n_merge = 0
new_array=[]

# filtering 'final'
df1 = df[df['hkl_file'].str.contains('final')]
df2 = df1[df1['hkl_file'].str.contains('run_03')]

for index, col in df2.iterrows():
    if "run_03" in col['hkl_file']:
        filepath=col['hkl_file']
        dose_path = filepath[:filepath.rfind('blend')]
        col['dose_path']=dose_path
        
        pppp = filepath[:filepath.rfind('run_03')]
        cluster_index = int(pppp[pppp.rfind("_"):].replace("/","").replace("_",""))
        col['cluster_index']=cluster_index
    
        new_array.append(col)
        n_merge+=1

na = pd.DataFrame(new_array)


# In[20]:


print(len(na))
# Grouping 'dose_path'
target_group = na.groupby(['dose_path'])
n_groups = len(target_group)
print(n_groups)

aaa=[]
for dose_path, tmpdf in target_group: 
    max_index=tmpdf['cluster_index'].idxmax()
    aaa.append(tmpdf.loc[max_index])

final_df = pd.DataFrame(aaa)


# In[35]:


overwritten_df=[]
for index, tmpdf in final_df.iterrows():
    chkpath=tmpdf['hkl_file']
    cols=chkpath.split('/')
    for col in cols:
        if "merge_blend_" in col:
            ecols=col.split("_")
            for ecol in ecols:
                if "S" in ecol:
                    final_resol = float(ecol.replace("S",""))
                    print(final_resol)
                    tmpdf['dmin']=final_resol
                    overwritten_df.append(tmpdf)
                    
tttt=pd.DataFrame(overwritten_df)



# In[43]:


overwritten_df=[]
for index, tmpdf in final_df.iterrows():
    chkpath=tmpdf['hkl_file']
    cols=chkpath.split('/')
#     print(cols)
    for col in cols:
        if "_final" in col:
            print(col)
            ecols=col.split("_")
            for ecol in ecols:
                if "A" in ecol:
                    final_resol = float(ecol.replace("A",""))
                    print(final_resol)
                    tmpdf['dmin']=final_resol
                    overwritten_df.append(tmpdf)
                    
tttt=pd.DataFrame(overwritten_df)


# In[44]:


tttt


# In[49]:


print(tttt.columns)
final_final_df = tttt.reindex(columns=['prefix','hkl_file','cell_params','space_group','anom_atom','n_anom','n_try','phase_dmax','solv_cnt','n_dm','build_cycle','dmin'])


# In[51]:


final_final_df.to_csv("oden_mod.csv", index=False)


# In[ ]:




