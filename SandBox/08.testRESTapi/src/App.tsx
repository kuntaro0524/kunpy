import "./App.css";
import axios from "axios";
import { useEffect, useState } from "react";
import { ChakraProvider, Flex } from "@chakra-ui/react";

import { Conds, LogStr } from "./components/types/api/conds";
import { Home } from "./components/atoms/Home";
import { Preps } from "./components/atoms/Preps";
import { CondsSelection } from "./components/atoms/CondsSelection";
import { useConds } from "./components/hooks/useConds";

import {
  AllCondsContext,
  AllCondsProvider,
} from "./components/providers/CondsProvider";
import { Link, Route, Routes } from "react-router-dom";

function App() {
  return (
    <ChakraProvider>
      <AllCondsProvider>
        <div className="App">
          <h1> ENSOKU WEBGUI ver 1.0 </h1>
          <Preps />
        </div>
      </AllCondsProvider>
    </ChakraProvider>
  );
}

export default App;
