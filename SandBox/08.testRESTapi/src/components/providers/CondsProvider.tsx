import React, {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useState,
} from "react";

import { Conds } from "../types/api/conds";

// 保持する変数と設定関数
export type AllCondsType = {
  // 測定条件を保持する配列
  condsArray: Conds[];
  // useState などの更新関数の型は以下のようになるらしい→おぼえげー
  setCondsArray: Dispatch<SetStateAction<Array<Conds>>>;
};

// Type scriptの表現方法として {} を as で受けて型を指定する
export const AllCondsContext = createContext<AllCondsType>({} as AllCondsType);

// childrenすべてに影響があるよーって話だったっけ
export const AllCondsProvider = (props: { children: ReactNode }) => {
  const { children } = props;
  // 再レンダリングする規模によっては変数と関数は別にしたほうが良い場合もある
  const [condsArray, setCondsArray] = useState<Array<Conds>>([]);
  // console.log(typeof setCondsArray);

  return (
    <AllCondsContext.Provider value={{ condsArray, setCondsArray }}>
      {children}
    </AllCondsContext.Provider>
  );
};
