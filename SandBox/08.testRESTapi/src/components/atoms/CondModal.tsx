import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
} from "@chakra-ui/modal";

import {
  Button,
  Stack,
  FormLabel,
  FormControl,
  Input,
  Center,
  Select,
  Grid,
  WrapItem,
  Box,
  VStack,
  Container,
} from "@chakra-ui/react";
import { ChangeEvent, useEffect, useState, VFC } from "react";
import axios from "axios";
import { Conds } from "../types/api/conds";
import { useConds } from "../hooks/useConds";

type Props = {
  targetCond: Conds;
  isOpen: boolean;
  onClose: () => void;
};

export const CondModal: VFC<Props> = (props) => {
  const readonlyFlag = false;
  const { targetCond, isOpen, onClose } = props;

  const current_id = targetCond._id;

  // useCondsを利用してパッチ関数を得る
  const { patchConds, isCorrecting } = useConds();

  //   Modal上で編集するパラメータについてhooksを準備して利用する;
  const [rootDir, setRootDir] = useState("");
  // フォーマットする数値としてフィールドに入力すると非常にややこしい
  // 文字列として扱うことにする（方法が明らかになるまで）
  const [WL, setWL] = useState("1.00000");
  const [distRaster, setDistRaster] = useState(0.0);
  const [distDS, setDistDS] = useState(0.0);
  const [targetParam, setTargetParam] = useState<string>("");
  const [targetValue, setTargetValue] = useState<string>("");

  //   Modal上で編集したパラメータをhooksで管理する
  const onChangeRootDir = (e: ChangeEvent<HTMLInputElement>) => {
    setRootDir(e.target.value);
  };
  const onChangeWL = (e: ChangeEvent<HTMLInputElement>) => {
    console.log("target wavelength = " + e.target.value);

    if (e.target.value != null) {
      setWL(e.target.value);
    }
  };
  const onChangeDistRaster = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    if (e.target.value != null) setDistRaster(parseFloat(e.target.value));
  };
  const onChangeDistDS = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    if (e.target.value != null) setDistDS(parseFloat(e.target.value));
  };

  // 渡されてきた条件のキーから編集したいパラメタを選択する準備
  const keys = Object.keys(targetCond);
  console.log(keys);

  // Modal上のパラメータをデータベースに反映する
  const onClickUpdate = () => {
    const wl_float = parseFloat(WL);
    const replace_db = {
      ...targetCond,
      wavelength: wl_float,
      dist_raster: distRaster,
      dist_ds: distDS,
      [targetParam]: targetValue,
    };
    console.log("Replace DB=", replace_db);
    patchConds({ id: current_id, newConds: replace_db });
    if (!isCorrecting) {
      onClose();
    }
  };

  const onSelectParameter = (e: ChangeEvent<HTMLSelectElement>) => {
    const paramname = e.target.value;
    setTargetParam(paramname);
    const ppp = targetCond[paramname];
    setTargetValue(ppp);
  };

  const onChangeTargetValue = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    setTargetValue(e.target.value);
  };

  //   Modal上で編集できるパラメータについてここでuseStateで管理するので初期値を設定して上げる必要がある
  //   useEffectを最初だけ呼んでおく（というかそういう機能
  useEffect(() => {
    setRootDir(targetCond?.root_dir ?? "");
    setWL(targetCond?.wavelength.toFixed(2) ?? "1.0");
    setDistRaster(targetCond?.dist_raster ?? 125.0);
    setDistDS(targetCond?.dist_raster ?? 125.0);
  }, [targetCond]);

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader
          mx={0}
          bg="yellow"
          w="full"
          boxShadow="xs"
          borderRadius="10px"
        >
          Modify a condition
          <Center color="blackAlpha.400">
            puck:{targetCond.puckid} pin:{targetCond.pinid} sample:
            {targetCond.sample_name}
          </Center>
        </ModalHeader>
        <ModalCloseButton />
        {/* マージンを調整 */}
        <ModalBody mx={4}>
          {/* 項目を並べるためにStackは入れておこう（配列調整がしやすい） これは */}
          <Stack>
            <FormControl>
              <FormLabel>Root directory</FormLabel>
              <Input
                // useState()で管理している値になった
                value={rootDir}
                // value={targetCond.root_dir}
                isReadOnly={readonlyFlag}
                onChange={onChangeRootDir}
              />
            </FormControl>
            <FormControl>
              <FormLabel>Wavelength</FormLabel>
              <Input
                // value={WL.toFixed(6)}
                value={WL}
                isReadOnly={readonlyFlag}
                onChange={onChangeWL}
              />
            </FormControl>
            <FormControl>
              <FormLabel>Distance(raster)</FormLabel>
              <Input
                value={distRaster}
                isReadOnly={readonlyFlag}
                onChange={onChangeDistRaster}
              />
            </FormControl>
            <FormControl>
              <FormLabel>Distance(data)</FormLabel>
              <Input
                value={distDS}
                isReadOnly={readonlyFlag}
                onChange={onChangeDistDS}
              />
            </FormControl>
            <VStack bg="lightblue" w="full" pb="5" pl="5" pr="5">
              <Center color="blackAlpha.500">Advanced setting</Center>
              <Box w="full" h="40px" bg="yellow.200">
                <Select
                  placeholder="Choose target parameter."
                  bg="tomato"
                  onChange={onSelectParameter}
                  value={targetParam}
                  w="full"
                  textAlign={"center"}
                >
                  {keys.map((key) => (
                    <option value={key}>{key}</option>
                  ))}
                </Select>
              </Box>
              <Input
                textAlign="center"
                h="40px"
                bg="white"
                w="full"
                value={targetValue}
                onChange={onChangeTargetValue}
              />
            </VStack>
          </Stack>
          <Button onClick={onClickUpdate}> Submit </Button>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
