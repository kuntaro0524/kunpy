import React from "react";
import { ReactNode, VFC } from "react";
import { Button } from "@chakra-ui/react";
import { useConds } from "../hooks/useConds";
import { DisplayConds } from "../molecules/DisplayConds";

export const Preps = () => {
  const { condsArray, setCondsArray, useDBs, isRead } = useConds();
  useDBs();

  return <>{isRead ? <DisplayConds conds={condsArray} /> : null}</>;
};
