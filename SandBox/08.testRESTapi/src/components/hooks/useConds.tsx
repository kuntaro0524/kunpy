import { filter } from "@chakra-ui/react";
import axios from "axios";
import { useCallback, useContext, useEffect, useState, VFC } from "react";
import { DiagnosticCategory } from "typescript";
import { AllCondsContext } from "../providers/CondsProvider";
import { Conds } from "../types/api/conds";
import { useMessage } from "./useMessage";

// useContextを利用していることをカプセル化するためのクラス（らしい）
// import { NtrialContext, NtrialContextType } from "../providers/QuizProvider";

type Props = {
  subject: string;
  start_page: number;
  end_page: number;
  category: string;
};

type Props2 = {
  id: string;
  newConds: Conds;
};

export const useConds = () => {
  const { condsArray, setCondsArray } = useContext(AllCondsContext);
  const [isRead, setIsRead] = useState<boolean>(false);
  const [isCorrecting, setIsCorrecting] = useState<boolean>(false);

  const { showMessage } = useMessage();

  const server_url = process.env.REACT_APP_SERVER_URL;
  const server_port = process.env.REACT_APP_SERVER_PORT;

  const patchConds = (props2: Props2) => {
    setIsCorrecting(true);
    let { id, newConds } = props2;
    let quiz_url = `http://${server_url}:${server_port}/measurements/${id}`;
    axios
      .patch<Array<Conds>>(quiz_url, newConds, {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      })
      .then((res) => {
        console.log("Success to update :" + id);
        showMessage({ title: "Succesfully updated!", status: "success" });
        setIsCorrecting(false);
      })
      .catch(function (error) {
        showMessage({
          title: "Error in updating database!",
          status: "error",
        });
        console.log(error.config);
        for (let key of Object.keys(error)) {
          console.log(key);
          console.log(error[key]);
        }
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log("Error", error.message);
        }
        console.log(error.config);
      });
  };

  type Props3 = {
    subject: string;
  };

  const updateDB = (props3: Props3) => {
    console.log("updateDB was called.");

    let { subject } = props3;
    condsArray.map((eachconds) => {
      const id = eachconds._id;
      patchConds({ id, newConds: eachconds });
    });
  };

  // const useDBs = (props: Props) => {
  const useDBs = () => {
    // const { start_page, end_page, subject, category } = props;
    const { showMessage } = useMessage();

    useEffect(() => {
      axios
        .get<Array<Conds>>(`http://${server_url}:${server_port}/measurements`, {
          headers: {
            "Access-Control-Allow-Origin": "*",
          },
        })
        .then((res) => {
          console.log("<<<< BEFORE >>>>>");
          console.log(res.data);
          setCondsArray(res.data);
          // setQnum(filtered_quiz.length);
          setIsRead(true);
        })
        .catch(function (error) {
          console.log("ERROR?");
          console.log(error.config);
          console.log(error);
          for (let key of Object.keys(error)) {
            console.log(key);
            console.log(error[key]);
          }
          if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log("Error", error.message);
          }
          console.log(error.config);
        });
    }, []);
  };

  return {
    condsArray,
    setCondsArray,
    useDBs,
    isRead,
    isCorrecting,
    patchConds,
    updateDB,
  };
};
