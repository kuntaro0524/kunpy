const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose")

const app = express();

app.set('view engine', 'ejs');
// CSS path for html
app.use(express.static("public"));

// body parser encoding
app.use(bodyParser.urlencoded({
  extended: true
}))

// Mongoose initialization
mongoose.connect("mongodb://localhost:27017/todolist_db", {
  useNewUrlParser: true
});

// Mongoose schema description
const todo_schema = {
  name: String
};

// making a model of each todo String
const Item = mongoose.model("Item", todo_schema);

// Mongoose list schema (including toto_schema
const list_schema = {
  listname: String,
  contents: [todo_schema]
};
// Making mongoose model
const List = mongoose.model("List", list_schema);

// Default items for the initial template
const the_first = new Item({
  name: "Read this document."
});
const the_second = new Item({
  name: "Add ToDo by putting [+] button."
});
const the_third = new Item({
  name: "Remove by clicking the checkbox!"
});
// Making an array of the default todo listname
const default_todolist = [the_first, the_second, the_third];

app.get("/", function(req, res) {
  console.log("Thank you for your access!");
  Item.find({}, function(err, foundItems) {
    console.log("################# FOUND ITEMS")
    console.log(foundItems);
    if (foundItems.length === 0) {
      Item.insertMany(default_todolist, function(err) {
        if (err) {
          console.log("Something wrong!");
        } else {
          console.log("Okay to be saved.");
        }
      })
    } else {
      res.render("list", {
        list_title: "Today",
        list_contents: foundItems
      });
    }
  })
})

app.get("/:tobemade", function(req, res) {
  listname = req.params.tobemade;
  console.log("app.get tobemade was called.");
  // 取得したリスト名での登録がなかった場合には
  List.findOne({
    listname: listname
  }, function(err, foundItems) {
    if (!err) {
      if (!foundItems) {
        console.log("Not exists");
        const newList = new List({
          listname: listname,
          contents: default_todolist
        })
        newList.save();
        res.redirect("/"+listname);
      } else {
        console.log("Exists!");
        console.log(foundItems);
        res.render("list", {
          list_title: listname,
          list_contents: foundItems.contents
        });
      }
    }
  });
});

app.post("/", function(req, res) {
  // 新しいtodoを受け取る
  const newItem = req.body.newItem;

  // 新しいToDoをDBに追加する
  const addingItem = new Item({
    name: newItem
  });
  addingItem.save();
  res.redirect("/");

});

app.post("/delete", function(req, res) {
  // 新しいtodoを受け取る
  const ID_tobe_deleted = req.body.id_todo;
  // 受け取ったIDのデータをDBから消去する
  Item.findOneAndDelete({
    _id: ID_tobe_deleted
  }, function(err, foundItem) {
    console.log("Deleted!" + foundItem);
  });
  res.redirect("/");
});

// ポートを開放して接続可能にするおまじない
app.listen(3000, function() {
  console.log("Server started on port 3000");
});
