import

yamtbx.dataproc.auto.resolution_cutoff


class ResolLimiter(self):
    def __init__(self):
        self.dmin=-999.999

    def fun_ed_aimless(self, s, d0, r):
        # CC1/2 fitting equation used in Aimless, suggested by Ed Pozharski
        return 0.5 * (1 - numpy.tanh((s-d0)/r))
