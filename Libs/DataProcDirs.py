import sys,os
import MyException
import glob,numpy
import DirectoryProc

# DEBUG flag
isIncludeMTZ = False

class DataProcDirs():
    def __init__(self, root_dir):
        self.root_dir = os.path.abspath(root_dir)
        self.isPrep = False
        self.isDebug = False
        # Dictionary for directories (type and directory name)
        self.dir_dict = {}
        self.log_string = []

    def prep(self):
        # store all of directories in the designated path.
        dp = DirectoryProc.DirectoryProc(self.root_dir)

        # Directories in the 'root_dir' are stored to 'dirs'
        ######################################################
        # Expected directories are like (root_dir = "_kamoproc" and "_kamo_30deg" and so on.)
        # _kamoproc/puck01-01/data00/...
        # _kamo_30deg/puck01-01/data00/...
        # _kamo_30deg/merge_blend_1.29S_sample1/
        # ...
        dirs = dp.findDirs()

        if self.isDebug: print "Directory list in root_dir=", dirs, "in %s" % self.root_dir

        # Merging directories are stored to 'self.merge_dirs'
        # The informatin can be extracted by using self.dir_dict["merge"]
        self.merge_dirs = []
        # Merge-related directories
        for d in dirs:
            if d.startswith("merge") == True:
                abs_path_merge = os.path.join(self.root_dir, d)
                # abs_path_merge = os.path.abspath(d)
                self.merge_dirs.append(abs_path_merge)

        self.dir_dict["merge"] = self.merge_dirs

        if self.isDebug:
            for d in self.dir_dict["merge"]:
                print "DDDDD=%s" % d

        # Finding data processing directory
        self.found_procd = []

        # For each data processing directory  (both large and small wedge will be detected.)
        # _kamoproc/CPS1416-01/ : pin_dir = "CPS1416-01"
        print ("ProcDirs:prep(): data processing directory analysis")
        for pin_dir in dirs:
            # An absolute path for a 'pin' directory
            abs_pin_dir = os.path.join(self.root_dir, pin_dir)
            pp = DirectoryProc.DirectoryProc(abs_pin_dir)
            # Directory search in the directory here
            qq = pp.findDirs()
            # Check root_dir/path1/path2/:(path2 = q)
            # /isilon/users/target/target/AutoUsers/200218/ariyoshi/_kamoproc/CPS1416-01
            # qq = ["data00", "data01"..]
            # Thus 'q' is succeeded data processing directory
            for q in qq:
                max_index = -9999
                # Data directory exists
                if q.rfind("data") != -1:
                    # Extracting 'data index' for this directory
                    # data00 -> index=0
                    # data01 -> index=1...
                    cols = q.split('/')
                    index_str = cols[-1].replace("data","")
                    max_index = int(index_str)
                # Found directory for data collection (even though its empty)
                # is stored to 'self.found_procd'.
                if max_index != -9999:
                    ddir_abs = os.path.abspath(abs_pin_dir)
                    found_d = "%s/%s/" % (ddir_abs, q)
                    self.found_procd.append(found_d)
                else:
                    if self.isDebug == True:
                        print "No data processed: %s" % q

        # Directories for 'each' data processing
        self.dir_dict["each_procd"] = self.found_procd

        # Here we should group found directories
        # TYPE: 'multi' 'helical' 'single'
        self.multi_dirs = []
        self.largewedge_dirs = []

        for procd_abs in self.found_procd:
            dataroot = DirectoryProc.DirectoryProc(procd_abs)
            # directories in "...data00/KOZO0001-09-multi_1101-1200" -> multiple small wedge datasets
            # otherwise: directory can be regarded as 'large wedge' type.
            proc_dirs = dataroot.findDirs()
            
            for procd in proc_dirs:
                if procd.rfind("multi") != -1:
                    final_path = os.path.join(procd_abs, procd)
                    self.multi_dirs.append(final_path)
                else:
                    final_path = os.path.join(procd_abs, procd)
                    self.largewedge_dirs.append(final_path)

        self.dir_dict["small_wedge"] = self.multi_dirs
        self.dir_dict["large_wedge"] = self.largewedge_dirs

        if self.isDebug: print "Analysing data structures has been finished: ",self.dir_dict

        if self.isDebug:
            for proc_type, dir in self.dir_dict.items():
                print "#########################################"
                print proc_type, dir
                print "#########################################"

        self.isPrep = True
        return self.found_procd

    def getTypeDirs(self, designated_key):
        if self.isPrep == False: self.prep()

        if designated_key not in self.dir_dict:
            raise MyException.NoKeysInDict("No designated key in the existing dict.")
        return self.dir_dict[designated_key]

    def listupProcessDirs(self, type_of_data):
        if self.isPrep == False: self.prep()
        proc_dirs = self.getTypeDirs(type_of_data)

        return proc_dirs

    # This is for each path directory
    # Directory path is analyzed to get 'PUCK_PIN' directory name
    def find_pin_datasets(self, proc_dir):
        cols = proc_dir.split('/')
        loop_index = 0
        # Searching "kamo" string in cols.
        for col in cols:
            if col.rfind("kamo") != -1:
                data_dir = os.path.join(cols[loop_index+1], cols[loop_index+2], cols[loop_index+3])
                return cols[loop_index+1], data_dir
            else:
                loop_index += 1

        MyException.NoKAMOpathInString("Designated columns do not include '_kamo*' path")

if __name__ == "__main__":
    arc_files = []
    jw = DataProcDirs(sys.argv[1])
    jw.isDebug = False

    #small_wedge_okay = jw.listupGoodProcessDirs("small_wedge")
    #ng_dirs = jw.listupBadProcessDirs("small_wedge")
    #print small_wedge_okay
    #print "NG=", ng_dirs

    ppp=jw.listupProcessDirs("large_wedge")
    #print "All Large_wedge=", ppp, len(ppp)
    print "Largewedge=",ppp, len(ppp)

    ppp=jw.listupProcessDirs("small_wedge")
    print "All small wedge=",ppp, len(ppp)

    print "merge directories", jw.getTypeDirs("merge")
