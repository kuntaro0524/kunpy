import sys,os
import glob,numpy
import Libs.DirectoryProc as DirectoryProc
import Libs.MyDate as MyDate
import time
import logging
import Libs.DataProcDirs as DataProcDirs
import MultiMergeLogKAMO
import Date

from yamtbx.dataproc import eiger
from yamtbx.dataproc import cbf
from yamtbx.dataproc.XIO.plugins import eiger_hdf5_interpreter
from libtbx import easy_mp

# DEBUG flag
isIncludeMTZ = False

class Unko():

    def __init__(self, root_dir):
        self.root_dir = os.path.abspath(root_dir)
        self.isPrep = False
        self.isDebug = False
        self.required_files = ["CORRECT.LP", "XDS_ASCII.HKL"]
        self.isMTZ = True

        # Logging setting
        d = Date.Date()
        time_str = d.getNowMyFormat(option="date")
        FORMAT = '%(asctime)-15s %(module)s %(levelname)-8s %(lineno)s %(message)s'
        logging.basicConfig(filename="./make_archive.log", level=logging.DEBUG, format=FORMAT)
        self.logger = logging.getLogger('UNKO')

    def addRequiredFile(self, filename):
        self.required_files.append(filename)

    # Good process directories are defined as where "CORRECT.LP" exits in the directory
    def listupGoodProcessDirs(self, paths):
        ok_dirs = []
        for proc_dir in paths:
            correct_path = os.path.join(proc_dir, "CORRECT.LP")

            if os.path.exists(correct_path) == True:
                ok_dirs.append(proc_dir)

        return ok_dirs

    def prep(self):
        dpd = DataProcDirs.DataProcDirs(self.root_dir)
        large_wedge_paths = dpd.listupProcessDirs("large_wedge")

        self.logger.info("all of large_wedge datasets %5d"% len(large_wedge_paths))
        # large wedge paths
        self.good_large_wedge_dirs = self.listupGoodProcessDirs(large_wedge_paths)

        for d in self.good_large_wedge_dirs:
            self.logger.info("SUCCESS= %s" %(d))

        self.merge_dirs = dpd.listupProcessDirs("merge")

        self.isPrep = True

    def makeMTZfile(self, xds_proc_dir, xds_ascii_path):
        mtz_generate_path = os.path.join(xds_proc_dir, "ccp4")
        print "Generating in : %s" % mtz_generate_path

        # Check if 'ccp4' directory exists or not
        if os.path.exists(mtz_generate_path):
            print "ccp4 path already exists %s" % mtz_generate_path
            # Check if MTZ exists in the directory
            dp = DirectoryProc.DirectoryProc(mtz_generate_path)
            target_list, path_list = dp.findTargetWith(".mtz")
            if len(target_list) != 0:
                if len(target_list) > 2:
                    print "Two or more MTZ files were found."
                    print "The routine returns the first MTZ file path"
                    print "MTZ file already exists", target_list[0]
                # Relative path from 'self.root_dir'
                return_path = os.path.relpath(target_list[0], ".")
                return return_path
        # Try to make MTZ file by using xds2mtz.py(yamtbx.python)
        try:
            command = "cd %s\n xds2mtz.py\n cd %s \n " % (xds_proc_dir, self.root_dir)
            os.system(command)
        except Exception as e:
            print e.args

        # Return the ccp4 mtz path
        generated_mtz_path = xds_ascii_path.replace(".HKL", ".mtz")
        return_path = os.path.relpath(generated_mtz_path, ".")
        return return_path

    # What this function do
    # 1. In "large wedge' directories
    # 2. Find 'required files' those can be set by 'class variants'
    # 3. Check existence of a file listed by 'required files' in each data processing directory
    # 4. Automatically this checks an existence of 'XDS_ASCII.HKL'
    # 5. MTZ file is generated from 'XDS_ASCII.HKL' if the flag is True.
    # 6.
    def makeReportLargeWedge(self, mtz_prep = True):
        if self.isPrep == False:
            self.prep()
        arc_large = []

        self.isMTZ = mtz_prep

        # MTZ file list
        to_be_made = []

        # For each XDS process directory with "CORRECT.LP"
        for xds_proc_dir in self.good_large_wedge_dirs:
            # Searching required files for a report archive
            for required_file in self.required_files:
                # Setting the target file path
                file_path = os.path.join(xds_proc_dir, required_file)
                # Check existense of the target file
                if os.path.exists(file_path) == True:
                    # relative file path is generated for the archive file.
                    rel_path = os.path.relpath(file_path, "./")
                    # if XDS_ASCHII.HKL file is found
                    # This tries to generate MTZ file if the flag is true.
                    if rel_path.rfind("XDS_ASCII.HKL")!=-1:
                        xds_ascii_path = os.path.relpath(file_path)
                        if self.isMTZ == True:
                            # Path of an expected generated MTZ file
                            mtz_generate_path = os.path.relpath(os.path.join(xds_proc_dir, "ccp4/XDS_ASCII.mtz"))
                            # Add the generated MTZ file to the list for making the final arvhive for user report
                            arc_large.append(mtz_generate_path)
                            # For real processing
                            to_be_made.append((xds_proc_dir, xds_ascii_path))
                        else:
                            # MTZ option is False.
                            arc_large.append(xds_ascii_path)
                    else:
                        # Add this path to the list for the archive file.
                        arc_large.append(rel_path)

        # Generating MTZ files using multi-processing procedure
        if len(to_be_made) != 0:
            easy_mp.pool_map(fixed_func=lambda n: self.multiGenMTZ(n), args=to_be_made, processes=4)
            print "Waiting for 10 seconds to generate all of CORRECT plots."
            time.sleep(10.0)

        return arc_large

    # Function to achieve parallel processing of generating MTZ files on PC cluster.
    def multiGenMTZ(self, params):
        xds_proc_dir, xds_ascii_path = params
        
        mtz_generate_path = os.path.join(xds_proc_dir, "ccp4")
        print "Generating in : %s" % mtz_generate_path

        # Check if 'ccp4' directory exists or not
        if os.path.exists(mtz_generate_path):
            print "ccp4 path already exists %s" % mtz_generate_path
            # Check if MTZ exists in the directory
            dp = DirectoryProc.DirectoryProc(mtz_generate_path)
            target_list, path_list = dp.findTargetWith(".mtz")
            if len(target_list) != 0:
                if len(target_list) > 2:
                    print "Two or more MTZ files were found."
                    print "The routine returns the first MTZ file path"
                    print "MTZ file already exists", target_list[0]
                return_path = os.path.relpath(target_list[0], ".")
                return return_path
        # Try to make MTZ file by using xds2mtz.py(yamtbx.python)
        try:
            command = "cd %s\n xds2mtz.py\n cd %s \n " % (xds_proc_dir, self.root_dir)
            os.system(command)
        except Exception as e:
            print e.args

        # Return the ccp4 mtz path
        generated_mtz_path = xds_ascii_path.replace(".HKL", ".mtz")
        return_path = os.path.relpath(generated_mtz_path, ".")
        return return_path

    # Objective: large wedge KAMO directories
    # Each directory: XDS data processing of a single wedge data(180deg, 360deg...)
    # large wedge directories are extracted by using DataProcDirs.py
    # and also evaluated in self.prep() here.
    def makeArchiveLargeWedge(self):
        if self.isPrep == False:
            self.prep()

        arc_large = []

        # sucess_paths: a list of directories containing "CORRECT.LP". -> means completion of XDS.
        for ok in self.good_large_wedge_dirs:
            # required_files: they will be included in user report archive files.
            for required_file in self.required_files:
                file_path = os.path.join(ok, required_file)
                if os.path.exists(file_path) == True:
                    rel_path = os.path.relpath(file_path, "./")
                    arc_large.append(rel_path)

        ##print "HEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",arc_large
        return arc_large

    # From merging directories of all samples
    # For each sample ->  this function finds its '_final' merging directory
    def getListOfFinalMergeDirs(self):
        if self.isPrep == False:
            self.prep()

        # Check paths as 'absolute' paths
        merge_paths = []
        for merge_dir in self.merge_dirs:
            self.logger.info("merging directories=%s" % merge_dir)
            joined_path = os.path.join(self.root_dir, merge_dir)
            abs_path = os.path.abspath(joined_path)
            merge_paths.append(abs_path)

        # Finding 'final' directories in designated paths
        final_paths = []
        for check_path in merge_paths:
            dp = DirectoryProc.DirectoryProc(check_path)
            first_layers = dp.findDirs()

            # Paths with 'final'
            for directory in first_layers:
                # print "directory=",directory
                if directory.rfind("final") != -1:
                    connected_path = os.path.join(check_path, directory)
                    abs_path = os.path.abspath(connected_path)
                    # print "Appending %s" % abs_path
                    final_paths.append(abs_path)
                else:
                    continue
        # 'final_paths' is a list of the final merging directories
        # each member of the list -> each sample_name
        self.logger.info("Number of final paths=%5d" % len(final_paths))
        return final_paths

    def getOKmergeDirs(self, paths_to_be_checked):
        okay_merge_paths = []

        for final_dir in paths_to_be_checked:
            kamo_merge_log = "%s/multi_merge.log" % final_dir
            self.logger.info("%s is checking" % kamo_merge_log)
            if os.path.exists(kamo_merge_log) == True:
                MMLK = MultiMergeLogKAMO.MultiMergeLogKAMO(kamo_merge_log)
                status = MMLK.checkStatus()
                isDone = MMLK.isDone()
                if status == 0 and isDone == True:
                    okay_merge_paths.append(final_dir)

        return okay_merge_paths

    # Merging directories of 'small wedge datasets'
    # Check if 'merging' finishes normally from 'multi_merge.log'
    def getListOfFinishedMergeDirs(self):
        if self.isPrep == False:
            self.prep()
        # Find 'merge' directories with '_final' for the final resolution limmit
        final_paths = self.getListOfFinalMergeDirs()
        # Final directories are checked if "KAMO merging process" finishes or not.
        okay_merge_paths = self.getOKmergeDirs(final_paths)

        self.logger.info("OKAY_MERGE_PATH=%5d" % len(okay_merge_paths))

        return  okay_merge_paths

    # find 'cluster_###' directories in the final clustering
    def findClusterDirsIn(self, final_merge_dirs):
        if self.isPrep == False:
            self.prep()

        # Find a cluster with the maximum number of datasets
        cluster_dirs = []
        for final_merge_dir in final_merge_dirs:
            # Directory
            dp = DirectoryProc.DirectoryProc(final_merge_dir)
            dires_in_final_merge_dir = dp.findDirs()

            for curr_dir in dires_in_final_merge_dir:
                if curr_dir.rfind("cluster_") != -1:
                    joined_path = os.path.join(final_merge_dir, curr_dir)
                    self.logger.info("CLUSTER_DIR=%s", joined_path)
                    cluster_dirs.append(joined_path)
        return cluster_dirs

    def findRunDirsIn(self, cluster_dirs):
        # Cluster/run directories to find 'reflection files'
        cluster_run_dirs = []
        # Searching the maximum number of run
        for cluster_dir in cluster_dirs:
            dp = DirectoryProc.DirectoryProc(cluster_dir)
            dires_in_this_dir = dp.findDirs()
            for sec_dir in dires_in_this_dir:
                if sec_dir.rfind("run") != -1:
                    joined_path = os.path.join(cluster_dir, sec_dir)
                    self.logger.info("RUN_DIR=%s", joined_path)
                    cluster_run_dirs.append(joined_path)

        return cluster_run_dirs

    # Input information: 'okay_dirs' : merge_*/*_final/
    # okay_dir: "each merge directory" with 'finally decided resolution limit'
    def findFinalResultsDir(self, okay_dirs):
        # Find a cluster with the maximum number of datasets
        max_cluster_dirs = []
        for okay_dir in okay_dirs:
            self.logger.debug("Searching in %s" % okay_dir)
            # Directory
            dp=DirectoryProc.DirectoryProc(okay_dir)
            first_layers = dp.findDirs()

            max_cluster_no = 0
            for dire_in_okayd in first_layers:
                if dire_in_okayd.rfind("cluster_") != -1:
                    cluster_no=int(dire_in_okayd.split("cluster")[1].replace("_","").split("/")[0])

                    if max_cluster_no < cluster_no:
                        max_cluster_no = cluster_no
                        join_path = os.path.join(okay_dir, dire_in_okayd)

            max_cluster_dirs.append(join_path)

        # HTML file of KAMO merging
        # Selected directories 'okay_dirs' were already selected.
        self.html_reports = []
        for okay_dir in okay_dirs:
            # Check existence 
            html_file_path = os.path.join(okay_dir, "report.html")

            # If the 'report.html' exists in the designated directory
            if os.path.exists(html_file_path) == True:
                relpath = os.path.relpath(html_file_path)
                #print "KAMO report.html path: ", relpath
                self.html_reports.append(relpath)
            else:
                continue

        self.final_dires = []
        # Searching the maximum number of run
        for cluster_dir in max_cluster_dirs:
            # print "FINAL_PROCESSING=%s" % cluster_dir
            dp=DirectoryProc.DirectoryProc(cluster_dir)
            third_layers = dp.findDirs()

            max_run = 0
            for sec_dir in third_layers:
                # print "PROCESSING=", sec_dir
                if sec_dir.rfind("run") != -1:
                    try:
                        run_no = int(sec_dir.replace("run_",""))
                        if run_no > max_run:
                            max_run = run_no
                            join_dir = os.path.join(cluster_dir, sec_dir)
                            # print "JOJOJOJO=", join_dir
                    except:
                        print "Something wrong in directory name: %s " % sec_dir

            self.final_dires.append(join_dir)

        return self.final_dires

    def findReflectionFilesIn(self, run_dirs, file_list = ["XSCALE.LP", "XSCALE.INP", "aniso.log", "pointless.log"], isIncludeMTZ=True):
        self.reflection_related_files = []
        saved_dir = ""
        xscale_list = []

        # in 'run_01, run_02, run_03' directories
        for each_run in run_dirs:
            #self.logger.debug("D=%s" % each_run)
            if saved_dir == each_run:
                if self.isDebug: print "IDENTICAL!!!!"
                continue
            else:
                #self.logger.debug("data_root : %s" % each_run)
                saved_dir = each_run

            for check_file in file_list:
                check_path = os.path.join(each_run, check_file)
                if os.path.exists(check_path) == True:
                    relpath = os.path.relpath(check_path)
                    self.logger.debug("OK: checking existence: %s" % check_path)
                    self.reflection_related_files.append(relpath)
                    if relpath.rfind("XSCALE.LP") != -1:
                        xscale_list.append(each_run)
            # MTZ file
            if isIncludeMTZ == True:
                mtz_path = os.path.join(each_run, "ccp4/xscale.mtz")
                mtz_relpath = os.path.relpath(mtz_path)
                self.reflection_related_files.append(mtz_relpath)

        return self.reflection_related_files

        #print "REFLECTION_RELATED=",self.reflection_related_files

    def getArchiveFileList(self):
        arc_files = []

        arc_files += self.html_reports
        arc_files += self.reflection_related_files

        return arc_files
        
    # This is the latest version at BL32XU 2020/02/18
    def makeReportMerge(self, prefix="archive", option = "NO"):
        if self.isPrep == False:
            self.prep()

        # preparing everything
        okay_dirs = self.getListOfFinishedMergeDirs()
        # Final directories (Final scaling at the highest resolution)
        # 'cluster_####' directory with the largest number of datasets
        # 'run_??' : the largest number of 'run': Max=3
        final_dirs = self.findFinalResultsDir(okay_dirs)
        # Find required files in these directories
        tt.findReflectionFilesIn(final_dirs, file_list = ["XSCALE.LP", "XSCALE.INP", "aniso.log", "pointless.log"])
        filelist = self.getArchiveFileList()

        return filelist

    def makeMessage(self, filelist):
        if len(filelist) != 0:
            mail_comments = "\nY=Y=Y=Y=Y=Y=Y=Y E-MAIL REPORT BODY Y=Y=Y=Y=Y=Y=Y=Y=Y=Y\n"

            mail_comments += "Datasets were merged by using KAMO automerge.\n"
            mail_comments += "Please refer merging statistics by browsing following html.\n"
            for html_report in self.html_reports:
                mail_comments += "%s\n" % os.path.relpath(html_report)

            mail_comments += "\nX=X=X=X=X=X=X= An attached archive files =X=X=X=X=X=X=X=X=X\n"
            mail_comments += "= This archive file includes followings.\n"
            if isIncludeMTZ == True:
                mail_comments += "Reflection files are indluded.\n"
            else:
                mail_comments += "Reflection files are not included.\n"

            for targetdir in self.final_dires:
                mail_comments += "%s\n" % os.path.relpath(targetdir)

            mail_comments += "\nZ=Z=Z=Z=Z=Z=Z=Z=Z=Z Explanations Z=Z=Z=Z=Z=Z=Z=Z=Z=Z=Z=Z=Z=Z"
            mail_comments += "\n= _final/ is a final result of KAMO auto merge.\n"
            mail_comments += "= The resolution limit of the directory was determined automatically.\n"
            mail_comments += "= The resolution limit was defined by CC(1/2)~50% in XSCALE.LP in\n"
            mail_comments += "= 'cluster' with the 'largest' number of datasets.\n"
            mail_comments += "= We are sending only the cluster results with the largest number of merged datasets.\n"
            mail_comments += "= Please check 'other' clusters after you get your HDD backup.\n"
            mail_comments += "= You can overview all of results in 'html' file for each sample.\n"
            mail_comments += "= Hierarchical clustering: cell parameter based -> BLEND (merge_blend_*) \n"
            mail_comments += "= Hierarchical clustering: CC intensity based -> cc (merge_ccc_*) \n"
            mail_comments += "= If the directory name is 'merge_blend_3.0S_SAMPLENAME/'\n"
            mail_comments += "= a clustering is conducted in 'cell parameter based' method.'\n"
            mail_comments += "= Starting resolution limit for merging is 3.0A resolution.'\n"
            mail_comments += "= 'KAMO automerge' estimates resolution limit by CC(1/2) for each dataset.\n"
            mail_comments += "= If the resolution value is lower than 'starting resolution limit', KAMO \n"
            mail_comments += "= restarts XSCALE process after setting'lower resolution value' in XSCALE.INP.\n"
            mail_comments += "= In this manner, resolution limit is defined by the program.\n"
            mail_comments += "= The string '_final' in the directory path has a meaning of 'final resolution limit'.\n"
            mail_comments += "= SAMPLENAME should be identical to the name defined in ZOOPREP.csv file.\n\n"
            mail_comments += "= Please contact us if you have any questions. \n"
            mail_comments += "= Kunio Hirata (Corresponding developer of ZOO system): kunio.hirata@riken.jp \n"

        else:
            return "Nothing"

        return mail_comments

    def makeArchiveFileMerge(self):
        if len(all_list) != 0:
            # Making archive commend
            da = MyDate.MyDate()
            dstr = da.getNowMyFormat(option="other")

            tgz_file = "%s_%s.tgz " % (dstr, prefix)
            print tgz_file

            command = "tar cvfz %s" % tgz_file
            for good_file in all_list:
                if self.isDebug: print "GOOD",good_file
                command += "%s " % good_file

            while(1):
                if os.path.isfile(tgz_file) == False:
                    print "waiting for %s" % tgz_file
                    break
                    time.sleep(1)
                else:
                    os.path.getsize(tgz_file)
                    break

            os.system(command)
        else:
            print "No good merged results."

if __name__ == "__main__":
    input_dir = sys.argv[1]
    unko = Unko(input_dir)
    prefix = sys.argv[2]

    #def makeReportMerge(self, prefix="archive", option = "NO"):
    #filelist = tt.makeReportMerge("archive_test", option = "NO")
    #mail_comment = tt.makeMessage(filelist)
    #print mail_comment

    # Test for large wedge datasets
    #filelist = unko.makeArchiveLargeWedge()
    #print "makeArchiveLargeWedge:large_wedge=",filelist


    # print "Non-mergeing directories"
    # file_list = unko.makeReportLargeWedge(mtz_prep=False)
    #print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    #for fname in file_list:
        #print "FNAME=%s" % fname
    #print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    #print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

    print "Merging directories"
    # Test for merging directories
    archive_index = 0
    # Merging directories with '_final' results
    okay_dirs = unko.getListOfFinishedMergeDirs()
    cluster_dirs = unko.findClusterDirsIn(okay_dirs)
    run_dirs = unko.findRunDirsIn(cluster_dirs)
    file_list = unko.findReflectionFilesIn(run_dirs, file_list = ["xscale.hkl"], isIncludeMTZ=False)

    for f in file_list:
        print "F=",f

   # # Find directories with final merging calculations
   #  final_dirs = unko.findFinalResultsDir(okay_dirs)
   #  unko.findReflectionFile(final_dirs, file_list=["XSCALE.LP", "XSCALE.INP", "aniso.log", "pointless.log"])
   #  files_from_merge = unko.getArchiveFileList()
   #
   #  for each_file in files_from_merge:
   #      print "FINAL:",each_file
