#!/usr/bin/env python
# coding: utf-8

import numpy as np
import cv2
from matplotlib import pyplot as plt

from PIL import Image, ImageDraw


# mm dimensions
original_points=[0,0]
original_point = np.array(original_points)
original_point

next_point=np.array([1,1])

# widths definitions (vector)
left_margin = np.array([10.0, 0.0]) # [mm]
wcenter_margin = np.array([5.0, 0.0]) # [mm]
hcenter_margin = np.array([0.0, 5.0]) # [mm]
top_margin = np.array([0, 10.0]) # [mm]
between_margin = np.array([0, 1.0]) # [mm]

# Hole vector
width_vector = np.array([15.0, 0.0])
height_vector = np.array([0.0, 1.5])

# Well number
vector = next_point - original_point

# definition hole 
def calc_hole_corner2(number):
    # Horizontal vector calculation
    if 1<= number <=10:
        left_edge_offset = left_margin
    elif 11<=number <=20:
        left_edge_offset = left_margin + width_vector + wcenter_margin

    # Vertical vector calculation
    if 1<=number<=5:
        height_vec = -top_margin - (number-1)*(height_vector + between_margin)
    elif 11<=number<=15:
        height_vec = -top_margin - (number-11)*(height_vector + between_margin)
    elif 6<=number<=10:
        height_vec = -top_margin - 5 * height_vector - 4* between_margin - hcenter_margin - (number-6)*(height_vector + between_margin)
    elif 16<=number<=20:
        height_vec = -top_margin - 5 * height_vector - 4* between_margin - hcenter_margin - (number-16)*(height_vector + between_margin)
    
    # Corner calculation
    left_upper = original_point + left_edge_offset + height_vec
    left_lower = left_upper - height_vector
    right_upper = left_upper + width_vector
    right_lower = right_upper - height_vector
    
    # Draw shikaku
    img = np.full((600, 600, 3), 128, dtype=np.uint8)
        
    print(left_upper, left_lower, right_upper, right_lower)
    
    plt.plot(left_upper[0], left_upper[1], 'o')
    plt.plot(right_upper[0], right_upper[1], 'o')
    plt.plot(left_lower[0], left_lower[1], 'o')
    plt.plot(right_upper[0], right_lower[1], 'o')
    
    return left_upper, left_lower, right_upper, right_lower
    
#     plt.plot(left_upper[0], left_upper[1])    
#     cv2.rectangle(img, left_upper, right_lower, (0, 255, 0), thickness=-1)
#     cv2.imwrite("test.png", img)

# Calculation of hole corner
for i in range(1,21,1):
    corner=calc_hole_corner2(i)
