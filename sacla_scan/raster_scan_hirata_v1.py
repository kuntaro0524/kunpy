# -*- coding: utf-8 -*-
from math import *
import numpy as np
import ecpy
import sys
import matplotlib.pyplot as plt

######################
# Modify here.
########################
# Origin of XZ coordinates 
x_origin = 35200
z_origin = 0 # where the focal point of the video is fitted to the top of the holder

# Target scan hole
isHole1=True
isHole2=True
isHole3=True
isHole4=True

######################
# Setting parameters #
######################
# motor settings #
axisX = "bl3_st5_pm014"
axisZ = "bl3_st5_pm098"
#speed_X = 1200
speed_Z = 4000
coef_X = 0.5
coef_Z = 0.05

# separation for each shot, um #
sepX = 25.
sepZ = 25.
# horizontal scan speed (30 Hz operation)
speed_X = int(round(sepX * 30 / coef_X))

mX = ecpy.PulseMotor(axisX)
mZ = ecpy.PulseMotor(axisZ)

mX.SetSpeed(speed_X)
mZ.SetSpeed(speed_Z)

print("""Motor settings
    motorX: {}, {} pps, {} um/pulse
    motorZ: {}, {} pps, {} um/pulse""".format(axisX, speed_X, coef_X, axisZ, speed_Z, coef_Z))

print("""\nIf the motor settings are incorrect,
    please reactivate this module after correcting the source file.""")

########################
# Scan single square. #
#######################
def ScanSingleSquare(xstart, xend, zstart, zend, zstep):
    # pulse length for horizontal scan
    hori_move_rel_pulse = xend - xstart
 
    # Move to start points
    mX.Move(xstart)
    print("moving to the start position for hole # ({},{})".format(xstart, zstart))
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

    n_line = 0
    for z_work in range(zstart, zend, zstep):
        mZ.Move(z_work)
        mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

        # Horizontal movement
        # Kisuu: (+X) direction
        # Gusuu: (-X) direction
        if n_line % 2 == 0:
            direction_sign = 1
        else:
            direction_sign = -1

        mX.MoveRelativePosition(int(direction_sign * hori_move_rel_pulse))
        mX.WaitForSettingEnd(sleep_time=0.2, wait_time=240)

        n_line += 1

    return True

def moveAxis(xstart, xend, zstart, zend, zstep):
    # Move to start points
    print("moving to the start position for hole # ({},{})".format(xstart, zstart))
    mX.Move(xstart)
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)
    mZ.Move(zstart)
    mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

    print("moving to the end position for hole # ({},{})".format(xend, zend))
    mX.Move(xend)
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)
    mZ.Move(zend)
    mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

def moveTest(okay):
    zstep = 500

    # hole1 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole2 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole3 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole4 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    moveAxis(xstart, xend, zstart, zend, zstep)

def ScanSingleSquareMaster(isOkay):
    zstep = 500

    # hole1 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    # hole2 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    # hole3 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    # hole4 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

if __name__=='__main__':
    okay = True
    #moveTest(okay)
    ScanSingleSquareMaster(okay)

