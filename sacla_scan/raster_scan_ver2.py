# -*- coding: utf-8 -*-

from math import *
import numpy as np
import ecpy
import sys
import matplotlib.pyplot as plt


######################
# Setting parameters #
######################
# motor settings #
axisX = "bl3_st5_pm014"
axisZ = "bl3_st5_pm098"
#speed_X = 1200
speed_Z = 4000
coef_X = 0.5
coef_Z = 0.05

# separation for each shot, um #
sepX = 50.
sepZ = 50.
speed_X = int(round(sepX * 30 / coef_X))

mX = ecpy.PulseMotor(axisX)
mZ = ecpy.PulseMotor(axisZ)

mX.SetSpeed(speed_X)
mZ.SetSpeed(speed_Z)

print("""Motor settings
    motorX: {}, {} pps, {} um/pulse
    motorZ: {}, {} pps, {} um/pulse""".format(axisX, speed_X, coef_X, axisZ, speed_Z, coef_Z))

print("""\nIf the motor settings are incorrect,
    please reactivate this module after correcting the source file.""")


# location of the first hole, pulse #
iniX = -41610
iniZ = -505000

# distance between holes, um #
spaceX = 7000.
spaceZ = 7000.

# scan range for each hole, p/m um #
rangeX = 5500./2.
rangeZ = 4000./2.

# scan range for long X scans, um #
rangeXL = 35000 + rangeX * 2

# number of holes #
numX = 6
numZ = 5
numHole = numX * numZ

################################
# Functions for controling Run #
################################
# Start Run
def RunStart(shotN):
    run = ecpy.Run()
    run.Initialize()
    run.EnableShutter("Bl3Xfel")
    
    run.SetShotNumber(shotN)
    run.SetShutterPattern("Bl3Xfel","repeat",500,shotN-500,0)
    
    run.Start()

# Stop Run
def RunStop():
    run = ecpy.Run()
    run.End()
    run.Finalize()

##################################
# Definition of hole coordinates #
##################################
listX = np.zeros(numHole)
startX = np.zeros(numHole)
listZ = np.zeros(numHole)
startZ = np.zeros(numHole)
endZ = np.zeros(numHole)
startXL = np.zeros(numZ)

for i in range(numHole):
    listX[i] = int(iniX + int(i/numZ) * spaceX / coef_X)
    if int(i/numZ) % 2 == 0:
        if i % 2 == 0:
            startX[i] = int(listX[i] - rangeX / coef_X)
        else:
            startX[i] = int(listX[i] + rangeX / coef_X)
        listZ[i] = int(iniZ + (i%numZ) * spaceZ / coef_Z)
        startZ[i] = int(listZ[i] - rangeZ / coef_Z)
        endZ[i] = int(listZ[i] + rangeZ / coef_Z)
    else:
        if i % 2 == 0:
            startX[i] = int(listX[i] + rangeX / coef_X)
        else:
            startX[i] = int(listX[i] - rangeX / coef_X)
        listZ[i] = int(iniZ + (numZ - 1 - (i%numZ)) * spaceZ / coef_Z)
        startZ[i] = int(listZ[i] + rangeZ / coef_Z)
        endZ[i] = int(listZ[i] - rangeZ / coef_Z)

for i in range(numZ):
    if i % 2 == 0:
        startXL[i] = startX[i]
    else:
        startXL[i] = startXL[0] + rangeXL / coef_X


##############
# Scan modes #
##############
def ScanSingle(num, sZ=9999):
    """
        This method is used to scan for a paticular hole.
          'num': hole number, int
          'sZ': start Z position, pulse (int), optional"""
    
    if num >= numHole:
        print("Hole number is invalid.")
        raise
    else:
        print("Hole number: {}".format(num))
    try:
        numLine = round((sZ - startZ[num])/(sepZ/coef_Z))
        if numLine%2==0:
            sX = startX[num]
        else:
            sX = listX[num] - int(np.sign(startX[num] - listX[num]) * rangeX / coef_X)
        if sZ == 9999:
            sZ = startZ[num]
        elif sZ > startZ[num] and sZ > endZ[num]:
            print("start Z position is invalid")
            raise
        elif sZ < startZ[num] and sZ < endZ[num]:
            print("start Z position is invalid")
            return
        else:
            sZ = numLine * int(sepZ/coef_Z) + startZ[num]

        mX.Move(int(sX))
        mZ.Move(int(sZ))
        print("moving to the start position for hole # {}, ({},{})".format(num,sX,sZ))
        mX.WaitForSettingEnd(sleep_time=0.2,wait_time=120)
        mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=120)

        while mZ.GetPosition() != endZ[num]:
            mX.MoveRelativePosition(int(np.sign(listX[num] - mX.GetPosition()) * rangeX*2/coef_X))
            mX.WaitForSettingEnd(sleep_time=0.2, wait_time=120)

            mZ.MoveRelativePosition(int(np.sign(endZ[num] - mZ.GetPosition()) * sepZ/coef_Z))
            mZ.WaitForSettingEnd(sleep_time=0.2, wait_time=120)

        mX.MoveRelativePosition(int(np.sign(listX[num] - mX.GetPosition()) * rangeX*2/coef_X))
        mX.WaitForSettingEnd(sleep_time=0.2, wait_time=120)
        print("Finished for hole # {}".format(num))

    except:
        print("KeyboardInterrupt detected")
        #mX.Stop()
        #mZ.Stop()
        mX.WaitForSettingEnd(sleep_time=0.2, wait_time=60)
        mZ.WaitForSettingEnd(sleep_time=0.2, wait_time=60)
        print("Stopped")
        raise


def SampleScan(sn=0,en=0,sZ=9999):
    """
        This method is used to scan sample holes over specified range.
          'sn': start hole number, int, optional
          'en': end hole number, int, optional
          'sZ': start Z position for the first hole, pulse (int), optional"""

    if en == 0:
        en = numHole - 1
    else:
        pass
    try:
        for i in range(sn, en+1):
            if i == sn and sZ != 9999:
                ScanSingle(i, sZ=sZ)
            else:
                ScanSingle(i)
    except:
        pass
    finally:
        print("Finished")


def LongXScan(sZ=9999):
    """
        This method is used to scan sample with long X travel range.
          'sZ': start Z position, pulse (int), optional
        
        If you do not enter any values, the scan starts from the current position"""
    
    if sZ == 9999:
        sZ = mZ.GetPosition() + int(sepZ/coef_Z)
    else:
        pass

    columZ = np.where(listZ[:numZ] + rangeZ / coef_Z > sZ)[0][0]
    #print(columZ)
    if sZ < startZ[columZ]:
        sZ = startZ[columZ]
        sX = startX[0]
    else:
        numLine = round((sZ - startZ[columZ])/(sepZ/coef_Z))
        sZ = numLine * int(sepZ/coef_Z) + startZ[columZ]
        if numLine % 2 == 0:
            sX = startXL[columZ]
        else:
            sX = startXL[columZ] + np.sign(listX[columZ] - startXL[columZ]) * rangeXL / coef_X

    try:
        mX.Move(int(sX))
        mZ.Move(int(sZ))
        print("moving to the start position for hole # {}, ({},{})".format(columZ,sX,sZ))
        mX.WaitForSettingEnd(sleep_time=0.5,wait_time=120)
        mZ.WaitForSettingEnd(sleep_time=0.5,wait_time=120)
        
        print('start raster scan')
        i = columZ
        while i < numZ:
            while mZ.GetPosition() != endZ[i]:
                mX.MoveRelativePosition(int(np.sign(listX[i] - mX.GetPosition()) * rangeXL/coef_X))
                mX.WaitForSettingEnd(sleep_time=0.5, wait_time=120)
            
                mZ.MoveRelativePosition(int(sepZ/coef_Z))
                # Modified by Hirata. sleep_time is changed to 0.5 from 0.2
                # Error could be occurred in 'Z' translation.
                mZ.WaitForSettingEnd(sleep_time=0.5, wait_time=120)
        
                mX.MoveRelativePosition(int(np.sign(listX[i] - mX.GetPosition()) * rangeXL/coef_X))
                mX.WaitForSettingEnd(sleep_time=0.5, wait_time=120)

                # Following 2 lines were added: No Z movement at the left edge of a scan line.
                mZ.MoveRelativePosition(int(sepZ/coef_Z))
                mZ.WaitForSettingEnd(sleep_time=0.5, wait_time=120)

                i += 1

        print("All scan finished")

    except:
        print("KeyboadInterrupt detected")
        mX.WaitForSettingEnd(sleep_time=0.2, wait_time=60)
        mZ.WaitForSettingEnd(sleep_time=0.2, wait_time=60)
        print("Paused")


