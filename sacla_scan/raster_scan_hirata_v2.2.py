# -*- coding: utf-8 -*-
from math import *
import numpy as np
import ecpy
import sys
import matplotlib.pyplot as plt

# Authority setting : this is the most important modification 2020/12/11
ecpy.SetAuthority("staff")

# Log file setting
import logging, logging.config
from logging import getLogger, Formatter, StreamHandler, FileHandler
logname="sacla_eh5_scan_20210206.log"

logging.config.fileConfig('logging.conf', defaults={'logfile_name': logname})
#logging.basicConfig(logname)
logger = getLogger("SACLA_EH5")

######################
# Modify here.
########################
# mm dimensions
original_points=[0,0]
original_point = np.array(original_points)

# widths definitions (vector)
left_margin = np.array([8.8, 0.0]) # [mm]

# Horizontal margin at the center of holder
hori_center_margin = np.array([2.0, 0.0]) # [mm]

# Vertical margin at the center of holder
vert_center_margin = np.array([0.0, 2.0]) # [mm]

# Top margin from the holder edge to the 'roof' line on the 1st hole.
top_margin = np.array([0, 6.8]) # [mm]
between_margin = np.array([0, 1.0]) # [mm]

# Hole vector
width_vector = np.array([15.0, 0.0])
height_vector = np.array([0.0, 2.0])

# Calculation of the translation vector
# sense parameters: 
# same direction: Holder size +vector and pulse + vector => 1.0
# different direction: => -1.0
sense_x = -1.0
sense_z = 1.0

# Origin of XZ coordinates
#x_origin = 34168
# 2020/12/11 
#x_origin = -34700
# Line 2 Z value (should be obtained on the first day)
z_line2_upper = -396910 # pulse
# 9.8mm from line 2 top to the origin 0.05um/pulse
z_margin_from_line2_upper_to_origin = 9.8 * 1000.0 / 0.05 

x_origin = -34673 # 2020/12/12/ 18:11

# position of the top of the sample holder. (Z axis would not reach to this position acutually)
z_origin = z_line2_upper - z_margin_from_line2_upper_to_origin 

######################
# Setting parameters #
######################
# motor settings #
axisX = "bl3_st5_pm014"
axisZ = "bl3_st5_pm098"
speed_Z = 4000
coef_X = 0.5
coef_Z = 0.05

# KH convetion ratio
# 1 pulse 0.5 um -> 10 pulse 5um -> 2000 pulse 1mm
# shiki: 1/0.0005 = 2000 pulse/mm
x_mm2pulse = 1.0/(coef_X/1000.0)
z_mm2pulse = 1.0/(coef_Z/1000.0)

# separation for each shot, um #
sepX = 25.
sepZ = 25.
# horizontal scan speed (30 Hz operation)
speed_X = int(round(sepX * 30 / coef_X))
logger.info(speed_X)

debug = True

# Motor initilization
mX = ecpy.PulseMotor(axisX)
mZ = ecpy.PulseMotor(axisZ)

mX.SetSpeed(speed_X)
mZ.SetSpeed(speed_Z)

logger.info("""Motor settings
    motorX: {}, {} pps, {} um/pulse
    motorZ: {}, {} pps, {} um/pulse""".format(axisX, speed_X, coef_X, axisZ, speed_Z, coef_Z))

logger.info("""\nIf the motor settings are incorrect,
    please reactivate this module after correcting the source file.""")

def moveToCorner(corner_code_pulse):
    logger.info("Move to %s" % corner_code_pulse)
    mX.Move(int(corner_code_pulse[0]))
    mX.WaitForSettingEnd(sleep_time=0.2, wait_time=240)
    mZ.Move(int(corner_code_pulse[1]))
    mZ.WaitForSettingEnd(sleep_time=0.2, wait_time=240)
    logger.info("MOVE finished.")

########################
# Scan single square. #
#######################
# 2020/12/11 Large modification for flexible measurements
def ScanSingleSquare(corner_codes, isSimulate=False):
    # Resolve the corner coordinates
    left_upper, right_upper, left_lower, right_lower = corner_codes

    logger.info("LEFT_UPPER %s\n"% left_upper)
    logger.info("RIGHT_UPPER %s\n"% right_upper)
    logger.info("LEFT_LOWER %s\n"% left_lower)
    logger.info("RIGHT_LOWER %s\n"% right_lower)

    # pulse length for horizontal scan
    hori_move_rel_pulse = (right_upper - left_upper)[0]
    # horizontal direction
    logger.info("Move to the initial edge:%s\n"% hori_move_rel_pulse)

    # X, Z start and end positions
    # Move to start points
    if isSimulate==False:
        moveToCorner(left_upper)
    logger.info("moving to the start position for hole # ({}".format(left_upper))

    # Z parameters calculation
    zstart = int(left_upper[1])
    zend = int(left_lower[1])
    zstep = int(sepZ / coef_Z)

    logger.info("Z scan setting (start:%8s - end:%8s step:%8s)\n" % (zstart, zend, zstep))

    n_line = 0
    for z_work in range(zstart, zend+1, zstep):
        logger.info("Z moves to %s" % z_work)
        if isSimulate == False:
            mZ.Move(z_work)
            mZ.WaitForSettingEnd(sleep_time=0.2, wait_time=240)
        logger.info("Z finishes to move to %s\n" % z_work)

        # Horizontal movement
        # Kisuu: (+X) direction
        # Gusuu: (-X) direction
        if n_line % 2 == 0:
            direction_sign = 1
            logger.info("@@@ Going way....")
        else:
            direction_sign = -1
            logger.info("@@@ Backing way....")

        # Scan to horizontal direction
        if isSimulate == False:
            mX.MoveRelativePosition(int(direction_sign * hori_move_rel_pulse))
            mX.WaitForSettingEnd(sleep_time=0.2, wait_time=240)
        logger.info("LINE: zstart=%s %05d Goal=%10s" % (z_work, n_line, hori_move_rel_pulse * direction_sign))

        n_line += 1

    return True

# definition hole
# 2020/12/11 version holder
def calc_hole_corner(number):
    # Horizontal vector calculation
    if 1<= number <=10:
        left_edge_offset = left_margin
    elif 11<=number <=20:
        left_edge_offset = left_margin + width_vector + hori_center_margin

    # Vertical vector calculation
    if 1<=number<=5:
        height_vec = -top_margin - (number-1)*(height_vector + between_margin)
    elif 11<=number<=15:
        height_vec = -top_margin - (number-11)*(height_vector + between_margin)
    elif 6<=number<=10:
        height_vec = -top_margin - 5 * height_vector - 4* between_margin - vert_center_margin - (number-6)*(height_vector + between_margin)
    elif 16<=number<=20:
        height_vec = -top_margin - 5 * height_vector - 4* between_margin - vert_center_margin - (number-16)*(height_vector + between_margin)
    
    # Corner calculation
    left_upper = original_point + left_edge_offset + height_vec
    left_lower = left_upper - height_vector
    right_upper = left_upper + width_vector
    right_lower = right_upper - height_vector

    if debug: logger.info("LU:%s LL:%s RU:%s RL:%s" % (left_upper, left_lower, right_upper, right_lower))
    # Draw shikaku
    if debug==True:
        img = np.full((600, 600, 3), 128, dtype=np.uint8)
        logger.info(left_upper, left_lower, right_upper, right_lower)
        plt.plot(left_upper[0], left_upper[1], 'o')
        plt.plot(right_upper[0], right_upper[1], 'o')
        plt.plot(left_lower[0], left_lower[1], 'o')
        plt.plot(right_upper[0], right_lower[1], 'o')

    # img.write("test.png")

    return left_upper, right_upper, left_lower, right_lower
    
def calc_translation_vector_from_origin_mm(holder_num):
    corner_codes = calc_hole_corner(holder_num)
    if debug: logger.info("BEFORE:", corner_codes)
    new_codes = []
    for corner_code in corner_codes:
        xcompo = -sense_x*corner_code[0]
        zcompo = -sense_z*corner_code[1]
        new_codes.append([xcompo,zcompo])
        #logfile.write(">> Holder No. %03d: (X, Z)=(%10.5f, %10.5f)\n" % (holder_num, xcompo, zcompo))
        logger.info(">> Holder No. %03d: (X, Z)=(%10.5f, %10.5f)\n" % (holder_num, xcompo, zcompo))
    logger.info(new_codes)
    return new_codes

def calc_trans_vec_from_origin_pulse(holder_num):
    corner_codes_mm=calc_translation_vector_from_origin_mm(holder_num)
    pulse_codes=[]
    for corner_code_mm in corner_codes_mm:
        if debug:logger.info("CORNER=", corner_code_mm)
        xcompo=corner_code_mm[0] * x_mm2pulse + x_origin
        zcompo=corner_code_mm[1] * z_mm2pulse + z_origin
        vector = np.array([xcompo, zcompo])
        pulse_codes.append(vector)
    return pulse_codes

def scanMaster(isSimulate=False):
    #logfile.write("from now scan master starts scan.\n")
    logger.info("from now scan master starts scan.\n")
    #holder_sequence = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    #holder_sequence = [3,4,5,6,7,8,9,12,13,14,15,16,17,18,19]
    #holder_sequence = [2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19]
    holder_sequence = [2]
    logger.info("Sequence=%s\n" % holder_sequence)
    for i in holder_sequence:
        # Calculate 'translation vector' of pulse motor axes.
        # The directions are opposite against 'holder design' axis directions
        corner_codes = calc_trans_vec_from_origin_pulse(i)
        logger.info(">>>>> Processing hole No.%d ... <<<<<\n" % i)
        ScanSingleSquare(corner_codes, isSimulate=isSimulate)

def moveToHoleStartPoint(hole_number, isSimulate=False):
    logger.info("Hole number=%s" % hole_number)

    # Calculate 'translation vector' of pulse motor axes.
    # The directions are opposite against 'holder design' axis directions
    corner_codes = calc_trans_vec_from_origin_pulse(hole_number)

    # Left upper corner
    left_upper = corner_codes[0]
    moveToCorner(left_upper)

def moveAxis(xstart, xend, zstart, zend, zstep):
    # Move to start points
    logger.info("moving to the start position for hole # ({},{})".format(xstart, zstart))
    mX.Move(xstart)
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)
    mZ.Move(zstart)
    mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

    logger.info("moving to the end position for hole # ({},{})".format(xend, zend))
    mX.Move(xend)
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)
    mZ.Move(zend)
    mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

def moveTest(okay):
    zstep = 500

    # hole1 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole2 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole3 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole4 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    moveAxis(xstart, xend, zstart, zend, zstep)

def ScanSingleSquareMaster8(isOkay):
    zstep = 500

    if isHole1 == True:
        # hole1 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300
        zstart = -450000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole2 == True:
        # hole2 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = -450000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole3 == True:
        # hole3 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300 - 44000
        zstart = -294000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole4 == True:
        # hole4 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300
        zstart = -294000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole5 == True:
        # hole5 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300
        zstart = -138000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole6 == True:
        # hole6 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = -138000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole7 == True:
        # hole7 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = 18000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole8 == True:
        # hole8 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300
        zstart = 18000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

def testMove8(isOkay):
    zstep = 500

    # hole1 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -450000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole2 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -450000
    xend = xstart + (-30000)
    zend = -390000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole3 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500 - 44000
    zstart = -294000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole4 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500
    zstart = -294000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole5 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -138000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole6 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -138000
    xend = xstart + (-30000)
    zend = zstart  + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole7 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = 18000
    xend = xstart + (-30000)
    zend = zstart  + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole8 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500
    zstart = 18000
    xend = xstart + (-30000)
    zend = zstart  + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

if __name__=='__main__':
    okay = True
    scanMaster(isSimulate=True)
    #scanMaster(isSimulate=False)
