#!/usr/bin/env python
# coding: utf-8
import numpy as np
import cv2
from matplotlib import pyplot as plt

from PIL import Image, ImageDraw


# mm dimensions
original_points=[0,0]
original_point = np.array(original_points)
original_point

next_point=np.array([1,1])

debug=False

# widths definitions (vector)
left_margin = np.array([10.0, 0.0]) # [mm]
wcenter_margin = np.array([5.0, 0.0]) # [mm]
hcenter_margin = np.array([0.0, 5.0]) # [mm]
top_margin = np.array([0, 10.0]) # [mm]
between_margin = np.array([0, 1.0]) # [mm]

# Hole vector
width_vector = np.array([15.0, 0.0])
height_vector = np.array([0.0, 1.5])

# Well number
vector = next_point - original_point

# definition hole 
def calc_hole_corner(number):
    # Horizontal vector calculation
    if 1<= number <=10:
        left_edge_offset = left_margin
    elif 11<=number <=20:
        left_edge_offset = left_margin + width_vector + center_margin

    # Vertical vector calculation
    if 1<=number<=5:
        height_vec = -top_margin - (number-1)*(height_vector + between_margin)
    elif 11<=number<=15:
        height_vec = -top_margin - (number-11)*(height_vector + between_margin)
    elif 6<=number<=10:
        height_vec = -top_margin - 5 * height_vector - 4* between_margin - hcenter_margin - (number-6)*(height_vector + between_margin)
    elif 16<=number<=20:
        height_vec = -top_margin - 5 * height_vector - 4* between_margin - hcenter_margin - (number-16)*(height_vector + between_margin)
    
    # Corner calculation
    left_upper = original_point + left_edge_offset + height_vec
    left_lower = left_upper - height_vector
    right_upper = left_upper + width_vector
    right_lower = right_upper - height_vector
    
    # Draw shikaku
    img = np.full((600, 600, 3), 128, dtype=np.uint8)
        
#     print(left_upper, left_lower, right_upper, right_lower)
    if debug==True:
        plt.plot(left_upper[0], left_upper[1], 'o')
        plt.plot(right_upper[0], right_upper[1], 'o')
        plt.plot(left_lower[0], left_lower[1], 'o')
        plt.plot(right_upper[0], right_lower[1], 'o')
    
    return left_upper, left_lower, right_upper, right_lower
    
#     plt.plot(left_upper[0], left_upper[1])    
#     cv2.rectangle(img, left_upper, right_lower, (0, 255, 0), thickness=-1)
#     cv2.imwrite("test.png", img)

# Calculation of hole corner
for i in range(1,21,1):
    corner=calc_hole_corner(i)
    
# Calculation of the translation vector
# sense parameters: 
# same direction: Holder size +vector and pulse + vector => 1.0
# different direction: => -1.0
sense_x = 1.0
sense_z = 1.0

def calc_translation_vector_from_origin_mm(holder_num):
    corner_codes = calc_hole_corner(holder_num)
    if debug: print("BEFORE:", corner_codes)
    new_codes = []
    for corner_code in corner_codes:
        xcompo = -sense_x*corner_code[0]
        zcompo = -sense_z*corner_code[1]
        new_codes.append([xcompo,zcompo])
    return new_codes

# convetion ratio
# 1 pulse 0.5 um -> 10 pulse 5um -> 2000 pulse 1mm
# shiki: 1/0.0005 = 2000 pulse/mm
x_mm2pulse = 2000.0
z_mm2pulse = 1000.0

def calc_trans_vec_from_origin_pulse(holder_num):
    corner_codes_mm=calc_translation_vector_from_origin_mm(holder_num)
    pulse_codes=[]
    for corner_code_mm in corner_codes_mm:
#         print("CORNER=", corner_code_mm)
        xcompo=corner_code_mm[0] * x_mm2pulse
        zcompo=corner_code_mm[1] * z_mm2pulse
        pulse_codes.append([xcompo, zcompo])
    return pulse_codes

holder_sequence = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

for i in holder_sequence:
    vecs = calc_trans_vec_from_origin_pulse(i)
    print(vecs)
    # Draw shikaku
    img = np.full((600, 600, 3), 128, dtype=np.uint8)
     
    for vec in vecs:
        plt.plot(vec[0], vec[1], 'o')
