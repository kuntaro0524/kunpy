# -*- coding: utf-8 -*-
from math import *
import numpy as np
import ecpy
import sys
import matplotlib.pyplot as plt
import datetime

######################
# Modify here.
########################
# Origin of XZ coordinates 
#x_origin = 34168
# 2020/07/22 12:33 holder 01
#x_origin = 41500 
# 2020/07/23 01:56 holder 03
#x_origin = 34664 
# 2020/07/23 08:46 holder 04
#x_origin = 14134 
# 2020/07/23 10:13 holder 04
# x2 -> pulse preset to 40000 at the holder origin
#x_origin = 40000
# 2020/07/23 12:14 holder 05
# x2 -> pulse preset to 40000 at the holder origin(35800pls)
#x_origin = 39500
# 2020/07/23 18:26 holder 06 (2 holes)
#x_origin = 42000
# 2020/07/23 20:23 holder 07
#x_origin = 42000
# 2020/07/24  2:32 holder 08
#x_origin = 41500
# 2020/07/24 18:00 holder 12
x_origin = 42500
z_origin = 0 # where the focal point of the video is fitted to the top of the holder

# Target scan hole
isHole1=False
isHole2=True
isHole3=True
isHole4=True
isHole5=True
isHole6=True
isHole7=True
isHole8=True

######################
# Setting parameters #
######################
# motor settings #
axisX = "bl3_st5_pm014"
axisZ = "bl3_st5_pm098"
#speed_X = 1200
speed_Z = 4000
coef_X = 0.5
coef_Z = 0.05

# separation for each shot, um #
sepX = 10.
sepZ = 25.
# horizontal scan speed (30 Hz operation)
speed_X = int(round(sepX * 30 / coef_X))

mX = ecpy.PulseMotor(axisX)
mZ = ecpy.PulseMotor(axisZ)

mX.SetSpeed(speed_X)
mZ.SetSpeed(speed_Z)

print("""Motor settings
    motorX: {}, {} pps, {} um/pulse
    motorZ: {}, {} pps, {} um/pulse""".format(axisX, speed_X, coef_X, axisZ, speed_Z, coef_Z))

print("""\nIf the motor settings are incorrect,
    please reactivate this module after correcting the source file.""")

########################
# Scan single square. #
#######################
def ScanSingleSquare(xstart, xend, zstart, zend, zstep):
    # pulse length for horizontal scan
    hori_move_rel_pulse = xend - xstart
 
    # Moving to the start position
    # changing speed of X motor for raster scan
    # (750 um/sec for travelling to the start position)
    speed_X = int(round(750.0 / coef_X))
    mX.SetSpeed(speed_X)
    mX.Move(xstart)
    print("moving to the start position for hole # ({},{})".format(xstart, zstart))
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

    # changing speed of X motor for raster scan
    # horizontal scan speed (30 Hz operation)
    speed_X = int(round(sepX * 30 / coef_X))
    mX.SetSpeed(speed_X)

    n_line = 0
    for z_work in range(zstart, zend, zstep):
        mZ.Move(z_work)
        mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

        # Horizontal movement
        # Kisuu: (+X) direction
        # Gusuu: (-X) direction
        if n_line % 2 == 0:
            direction_sign = 1
        else:
            direction_sign = -1

        mX.MoveRelativePosition(int(direction_sign * hori_move_rel_pulse))
        mX.WaitForSettingEnd(sleep_time=0.2, wait_time=240)

        n_line += 1

    # Finishing log string -> console.
    datestr = datetime.datetime.now()
    print("{} Line scan has been finished.".format(datestr))

    return True

# Code for test 'step_um' effects
# Center 
def ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um):
    # pulse length for horizontal scan
    hori_move_rel_pulse = xend - xstart

    # Moving to the start position
    # changing speed of X motor for raster scan
    # (750 um/sec for travelling to the start position)
    speed_X = int(round(750.0 / coef_X))
    mX.SetSpeed(speed_X)

    # Move to start points
    mX.Move(xstart)
    print("moving to the start position for hole # ({},{})".format(xstart, zstart))
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

    # changing speed of X motor for raster scan
    # horizontal scan speed (30 Hz operation)
    speed_X = int(round(step_um * 30 / coef_X))
    mX.SetSpeed(speed_X)

    # Z start & end calculation
    zstep = int(step_um / coef_Z)
    center_z = int((zstart + zend) / 2.0)
    center_scan_um = center_scan_mm * 1000.0 #[um]
    wing_z = int(center_scan_um / step_um / 2.0) #[lines]
    wing_z_pulse = wing_z * zstep #[pulse]
    z_test_start = center_z - wing_z_pulse
    z_test_end = center_z + wing_z_pulse

    n_line = 0
    for z_work in range(z_test_start, z_test_end, zstep):
        mZ.Move(z_work)
        mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

        # Horizontal movement
        # Kisuu: (+X) direction
        # Gusuu: (-X) direction
        if n_line % 2 == 0:
            direction_sign = 1
        else:
            direction_sign = -1

        mX.MoveRelativePosition(int(direction_sign * hori_move_rel_pulse))
        mX.WaitForSettingEnd(sleep_time=0.2, wait_time=240)

        n_line += 1

    datestr = datetime.datetime.now()
    print("{} Line scan has been finished.".format(datestr))

    return True

def moveAxis(xstart, xend, zstart, zend, zstep):
    # Move to start points
    print("moving to the start position for hole # ({},{})".format(xstart, zstart))
    mX.Move(xstart)
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)
    mZ.Move(zstart)
    mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

    print("moving to the end position for hole # ({},{})".format(xend, zend))
    mX.Move(xend)
    mX.WaitForSettingEnd(sleep_time=0.2,wait_time=240)
    mZ.Move(zend)
    mZ.WaitForSettingEnd(sleep_time=0.2,wait_time=240)

def moveTest(okay):
    zstep = 500

    # hole1 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole2 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole3 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole4 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    moveAxis(xstart, xend, zstart, zend, zstep)

def ScanSingleSquareMaster4(isOkay):
    zstep = 500

    # hole1 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    # hole2 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -294000
    xend = xstart + (-30000)
    zend = -234000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    # hole3 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    # hole4 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = 28000
    xend = xstart + (-30000)
    zend = 88000
    ScanSingleSquare(xstart, xend, zstart, zend, zstep)

def ScanSingleSquareMaster8(isOkay):
    zstep = int(sepZ / coef_Z)

    if isHole1 == True:
        # hole1 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300
        zstart = -450000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole2 == True:
        # hole2 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = -450000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole3 == True:
        # hole3 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300 - 44000
        zstart = -294000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole4 == True:
        # hole4 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300
        zstart = -294000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole5 == True:
        # hole5 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300
        zstart = -138000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole6 == True:
        # hole6 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = -138000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole7 == True:
        # hole7 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = 18000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

    if isHole8 == True:
        # hole8 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300
        zstart = 18000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanSingleSquare(xstart, xend, zstart, zend, zstep)

# The code for checking experimental efficiency

def ScanTest():
    isHole5 = False
    isHole6 = False
    isHole7 = False
    isHole8 = False

    # 20 lines at around Z center will be evaluated.
    n_line = 10

    if isHole1 == True:
        # step for horizontal axis
        step_um = 10.0
        center_scan_mm = step_um * n_line / 1000.0
        # hole1 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300
        zstart = -450000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)

    if isHole2 == True:
        # step for horizontal axis
        step_um = 25.0
        center_scan_mm = step_um * n_line / 1000.0
        # hole2 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = -450000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)
 
    if isHole3 == True:
        # step for horizontal axis
        step_um = 10.0
        center_scan_mm = step_um * n_line / 1000.0
        # hole3 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300 - 44000
        zstart = -294000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)

    if isHole4 == True:
        # step for horizontal axis
        step_um = 25.0
        center_scan_mm = step_um * n_line / 1000.0
        # hole4 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300
        zstart = -294000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)

    if isHole5 == True:
        # hole5 : [(xstart1, zstart1), (xend1, zend1)]
        xstart = x_origin - 6300
        zstart = -138000
        xend = xstart + (-30000)
        zend = zstart + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)

    if isHole6 == True:
        # hole6 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = -138000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)

    if isHole7 == True:
        # hole7 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300 - 44000
        zstart = 18000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)

    if isHole8 == True:
        # hole8 : [(xstart2, zstart2), (xend2, zend2)]
        xstart = x_origin - 6300
        zstart = 18000
        xend = xstart + (-30000)
        zend = zstart  + 60000
        ScanTestOnlyCenter(xstart, xend, zstart, zend, center_scan_mm, step_um)

def testMove8(isOkay):
    zstep = 500

    # hole1 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -450000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole2 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -450000
    xend = xstart + (-30000)
    zend = -390000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole3 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500 - 44000
    zstart = -294000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole4 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500
    zstart = -294000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole5 : [(xstart1, zstart1), (xend1, zend1)]
    xstart = x_origin - 2500
    zstart = -138000
    xend = xstart + (-30000)
    zend = zstart + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole6 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = -138000
    xend = xstart + (-30000)
    zend = zstart  + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole7 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500 - 44000
    zstart = 18000
    xend = xstart + (-30000)
    zend = zstart  + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

    # hole8 : [(xstart2, zstart2), (xend2, zend2)]
    xstart = x_origin - 2500
    zstart = 18000
    xend = xstart + (-30000)
    zend = zstart  + 60000
    moveAxis(xstart, xend, zstart, zend, zstep)

if __name__=='__main__':
    okay = True
    #testMove8(okay)
    #ScanSingleSquareMaster(okay)
    #ScanSingleSquareMaster8(okay)
    #ScanTest()
    ScanSingleSquareMaster8(okay)
