import os, sys, math, numpy, scipy, glob
import ReadSchBSS
import ConvertHD5
from libtbx import easy_mp

beamline = "BL32XU"

# Class for summing up diffraction images on HD5 files.
class SumUpImages:
    def __init__(self, proc_path, out_path):
        # proc_path: BSS image directory
        self.proc_path = os.path.abspath(proc_path)
        self.dlb = ReadSchBSS.ReadSchBSS(self.proc_path)

        if beamline == "BL32XU":
            self.master_nframes = 100

        self.master_conds = []

        # out_path: image output directory
        self.out_path = os.path.abspath(out_path)

    def storeConds(self, total_osc=1.0, condition="middle"):
        # searching all of schedule files in this directory
        sch_files = glob.glob("%s/*sch" % self.proc_path)

        if len(sch_files) == 0:
            print "No data"
            
        proc_list = []
        for sch_file in sch_files:
            sch_lines=open(sch_file,"r").readlines()
            #prefix,n_multi,startphi,endphi,osc_width,nframes,cry_codes = self.dlb.getDataInfo(sch_file)
            prefix,mode,n_multi,startphi,endphi,osc_width,nframes,cry_codes = self.dlb.getDataInfo(sch_file)

            nconv = int(total_osc / osc_width)
            print "CONDS=",prefix, "NDS=",n_multi, startphi, endphi, osc_width, nframes, "NCONV=", nconv
            master_file=os.path.join(self.proc_path,"%s_master.h5"%prefix)

            if os.path.exists(master_file) == True:
                print "To be processed=", master_file

                if mode == "MULTIPLE":
                    for nth_data in range(0, n_multi):
                        center_n=int(nframes/2.0)
                        start_num_of_this_data=nframes*nth_data+1
                        center_of_rotation=start_num_of_this_data+center_n
        
                        conv_startnum=center_of_rotation-int(nconv/2)
                        conv_endnum  =center_of_rotation+int(nconv/2)
                        num_list = []

                        for imgnum in range(conv_startnum, conv_endnum):
                            num_list.append(imgnum)

                        phi_start_obj = startphi + conv_startnum * osc_width
                        osc_new = osc_width * float(len(num_list))
                        print "PHI_START=", phi_start_obj
                        print "image_num_list=",num_list
                        print "osc_width=",osc_new
        
                        out_name="%s/%s_check_%03dth_%05d-%05d.cbf"%(self.out_path,prefix,nth_data,conv_startnum,conv_endnum)
                        proc_list.append((master_file, num_list, phi_start_obj, osc_new, out_name))

                elif mode == "HELICAL":
                    for nth_data in range(0, n_multi):
                        center_n=int(nframes/2.0)
                        start_num_of_this_data=nframes*nth_data+1
                        center_of_rotation=start_num_of_this_data+center_n
        
                        conv_startnum=center_of_rotation-int(nconv/2)
                        conv_endnum  =center_of_rotation+int(nconv/2)
                        num_list = []

                        for imgnum in range(conv_startnum, conv_endnum):
                            num_list.append(imgnum)

                        phi_start_obj = startphi + conv_startnum * osc_width
                        osc_new = osc_width * float(len(num_list))
                        print "PHI_START=", phi_start_obj
                        print "image_num_list=",num_list
                        print "osc_width=",osc_new
        
                        out_name="%s/%s_check_%03dth_%05d-%05d.cbf"%(self.out_path,prefix,nth_data,conv_startnum,conv_endnum)
                        proc_list.append((master_file, num_list, phi_start_obj, osc_new, out_name))
    
                else:
                    print "Strange!!"
            else: 
                print "Schedule files do not exist."

    def procMultiDS(self):
        sch_files = glob.glob("%s/*sch" % self.proc_path)

        for sch_file in sch_files:
            sch_lines=open(sch_file,"r").readlines()
            #prefix,n_multi,startphi,endphi,osc_width,nframes,nconv=self.dlb.getDataInfo(sch_file)
            prefix,n_multi,startphi,endphi,osc_width,nframes,cry_codes = self.dlb.getDataInfo(sch_file)
    
            master_file=os.path.join(self.proc_path,"%s_master.h5"%prefix)
        
            proc_list=[]
            jpgfile_list=[]
    
            print "N_MULTI=",n_multi
            for nth_data in range(0,n_multi):
                    center_n=int(nframes/2.0)
                    start_num_of_this_data=nframes*nth_data+1
                    center_of_rotation=start_num_of_this_data+center_n
    
                    conv_startnum=center_of_rotation-int(nconv/2)
                    conv_endnum  =center_of_rotation+int(nconv/2)
    
                    filename="%s/multi_check_%03dth_%05d-%05d.cbf"%(self.out_path,nth_data,conv_startnum,conv_endnum)
                    proc_list.append((master_file,conv_startnum,conv_endnum,filename))
    
            print proc_list
            print "making %5d files"%len(proc_list)
            #jpgfile_list.append(easy_mp.pool_map(fixed_func=lambda n: ConvertHD5.ConvertHD5(n).process(), args=proc_list, processes=20))

        easy_mp.pool_map(fixed_func=lambda n: ConvertHD5.ConvertHD5(n).process(), args=proc_list, processes=8)
        print jpgfile_list

    def procHelical(self,sch_file,sch_path):
        sch_lines=open(sch_file,"r").readlines()
        #prefix,n_multi,startphi,endphi,osc_width,nframes,nconv=self.dlb.getDataInfo(sch_file)
        prefix,n_multi,startphi,endphi,osc_width,nframes,cry_codes = self.dlb.getDataInfo(sch_file)
        # in helical data collection: each 'cry*sch' makes one dataset
        n_multi=1
        master_file=os.path.join(sch_path,"%s_master.h5"%prefix)

        proc_list=[]
        jpgfile_list=[]

        print "N_MULTI=",n_multi
        for nth_data in range(0,n_multi):
            center_n=int(nframes/2.0)
            start_num_of_this_data=nframes*nth_data+1
            center_of_rotation=start_num_of_this_data+center_n

            conv_startnum=center_of_rotation-int(nconv/2)
            conv_endnum  =center_of_rotation+int(nconv/2)

            filename="%s/%s_%05d-%05d.cbf"%(self.out_path,prefix,conv_startnum,conv_endnum)
            proc_list.append((master_file,conv_startnum,conv_endnum,filename))
            print proc_list

        print "making %5d files"%len(proc_list)
        jpgfile_list.append(easy_mp.pool_map(fixed_func=lambda n: ConvertHD5.ConvertHD5(n).process(), args=proc_list, processes=16))

    def procNormal(self, master_file, startnum, endnum, num_of_merge):
        n_frames = endnum - startnum + 1
        n_block = int(n_frames / num_of_merge)
    
        proc_list = []
        # In master file, this makes blocks for merging
        for i in range(0, n_block):
            bl_start = i * num_of_merge + 1
            bl_end = bl_start + num_of_merge - 1
            block_id = i + 1
            filename = "%s/merged_%05d.cbf" % (self.out_path,block_id)
            proc_list.append((master_file, bl_start, bl_end, filename))
    
        print "making %05d files" % len(proc_list)
        #easy_mp.pool_map(fixed_func=lambda n: run(n), args=proc_list, processes=8)
        easy_mp.pool_map(fixed_func=lambda n: ConvertHD5.ConvertHD5(n).process(), args=proc_list, processes=8)

#proc_path="/isilon/users/target/target/AutoUsers/200122/kuntaro/CPS4467-12/data00/"
#proc_path="/isilon/users/target/target/AutoUsers/200213/Astellas/CPS3951-15/data00/"
proc_path="/isilon/users/target/target/AutoUsers/200213/Astellas/CPS3951-15/data00/"
#sch_path="/isilon/users/target/target/AutoUsers/200213/Astellas/CPS3951-15/data00/"
#proc_path="/isilon/users/target/target/AutoUsers/200213/tsukazaki/BMM0001-05/data00/"
proc_path="/isilon/users/target/target/AutoUsers/200213/Astellas/CPS3951-15/data00/"

proc_path = sys.argv[1]
si = SumUpImages(proc_path, "./")
#si.procMultiDS()
#si.storeConds()

si.procNormal("/isilon/users/admin41/admin41/AutoUsers/200218/Yao/RaoSH005-06/data00/cry00_master.h5",1,3600,10)
