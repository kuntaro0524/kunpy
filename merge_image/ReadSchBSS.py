import os,sys,math,numpy,glob
sys.path.append("/isilon/BL32XU/BLsoft/PPPP")
from libtbx import easy_mp
import ConvertHD5
import DirectoryProc

# One schedule file processing
class ReadSchBSS:
    def __init__(self,datadir):
        self.datadir=datadir

    def getConditions(self,schfile):
        sch_lines=open(schfile,"r").readlines()
        ndata = 0
        for line in sch_lines:
            #print line
            if line.rfind("Advanced gonio coordinates")!=-1:
                ndata+=1
            if line.rfind("Scan Condition")!=-1:
                cols=line.split()
                startphi=float(cols[2])
                endphi=float(cols[3])
                osc_width=float(cols[4])
                nframes=int((endphi-startphi)/osc_width)
                # 1deg summation
                nconv=int(1.0/osc_width)

        return startphi,endphi,osc_width,nframes

    def procAll(self):
        sch_files="*sch"
        sch_list=glob.glob(self.datadir+"/*sch")

        cols=""
        if len(sch_list) == 0:
            print "No schedule file in %s" % self.datadir
        for schfile in sch_list:
            startphi,endphi,osc_width,nframes=self.getConditions(schfile)
            print schfile, cols,startphi,endphi,osc_width,nframes

    def getScheudlePaths(self):
        dp=DirectoryProc.DirectoryProc(self.datadir)
        #findTargetFileInTargetDirectories(self,string_in_dire,string_in_filename,exclude_str=""):

        return sch_files,sch_paths

    def getDataInfo(self,sch_file):
        sch_lines=open(sch_file,"r").readlines()
        n_multi=0
        for line in sch_lines:
            #print "LIME=",line
            # Advanced mode: 3 # 0: none, 1: vector centering, 2: multiple centering, 3: multi-crystals

            if line.rfind("Advanced mode:") != -1:
                mode_number = int(line.split(":")[1].split()[0])
                if mode_number == 1:
                    self.mode = "HELICAL"
                if mode_number == 2:
                    self.mode = "MULTI_CENTER"
                if mode_number == 3:
                    self.mode = "MULTIPLE"
                if mode_number == 0:
                    self.mode = "NONE"
                print self.mode

        # For general information
        # Helical data collection -> cry00.sch, cry01.sch, cry02.sch....
        # Each crystal generates its own cry??.sch file
        for line in sch_lines:
            if line.rfind("Scan Condition")!=-1:
                cols=line.split()
                startphi=float(cols[2])
                endphi=float(cols[3])
                osc_width=float(cols[4])
                nframes=int((endphi-startphi)/osc_width)
            if line.rfind("Sample Name:")!=-1:
                prefix=line.replace("Sample Name:","").strip()

        cry_codes = []

        for line in sch_lines:
            if self.mode == "MULTIPLE":
                if line.rfind("Advanced gonio coordinates")!=-1:
                    cols = line.split()
                    xtmp = float(cols[4])
                    ytmp = float(cols[5])
                    ztmp = float(cols[6])
                    cry_codes.append((xtmp, ytmp, ztmp))
                    n_multi+=1

            if self.mode == "HELICAL":
                if line.rfind("Advanced gonio coordinates")!=-1:
                    cols = line.split()
                    cry_index = int(cols[3].replace(":",""))
                    xtmp = float(cols[4])
                    ytmp = float(cols[5])
                    ztmp = float(cols[6])
                    cry_codes.append((xtmp, ytmp, ztmp))

                    if cry_index == 2:
                        n_multi += 1

        #print "######",prefix,n_multi,startphi,endphi,osc_width,nframes, cry_codes
        return prefix,self.mode,n_multi,startphi,endphi,osc_width,nframes,cry_codes

if __name__ == "__main__":
    ddd=ReadSchBSS("./")
    ddd.getDataInfo(sys.argv[1])
    #ddd.convImages()
    #ddd.procMultiDS()
    #sch_files,sch_paths=ddd.getScheudlePaths()

    """
    for sch_file,sch_path in zip(sch_files,sch_paths):
        print "PROCESSING",sch_file
        if sch_file.rfind("multi")!=-1:
            ddd.procMultiDS(sch_file,sch_path)
        elif sch_file.rfind("cry")!=-1:
            ddd.procHelical(sch_file,sch_path)
    """
