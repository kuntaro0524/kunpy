import h5py,glob
from yamtbx.dataproc import eiger
from yamtbx.dataproc import cbf
from yamtbx.dataproc.XIO.plugins import eiger_hdf5_interpreter
from libtbx import easy_mp
from PIL import Image
import PIL.ImageOps 
import numpy,os
import glob

class ManCBF:
    def __init__(self):
        self.isPrep = False
        self.nproc = 16

    def setNproc(self, nproc):
        self.nproc = nproc

    def sumCBF(self, file1, file2):
        #body1 = cbf.load_cbf_as_numpy(file1)
        #body2 = cbf.load_cbf_as_numpy(file2)
        body1 , size1, size2 = cbf.load_minicbf_as_numpy(file1)
        body2 , size1, size2 = cbf.load_minicbf_as_numpy(file2)

        head1 = cbf.get_pilatus_header(file1)
        #print type(head1), head1

        body3 = body1 + body2

        cbf.save_numpy_data_as_cbf(body3, size1, size2, "TEST", "test.cbf", head1)

    # Sumup images to oscillation direction
    def sumUpOsc(self, param_list):
        cbf_prefix, startnum, endnum, out_img = param_list
        data = None
        sel = None

        #print "Processing %05d - %05d : %s" % (startnum, endnum, out_img)
        # Loop of summing up images
        for imgnum in range(startnum, endnum+1):
            filename = "%s_%06d.cbf" % (cbf_prefix, imgnum)
            tmp_body, size1, size2 = cbf.load_minicbf_as_numpy(filename)
            if data is None:
                data = tmp_body
            else:
                data += tmp_body

        # Check all pixel-masks
        if sel is None:
            sel = tmp_body < 0
        else:
            sel |= tmp_body < 0

        # set -10 to masked pixels
        data[sel] = -10

        h = cbf.get_pilatus_header(filename)

        cbf.save_numpy_data_as_cbf(data.flatten(),
                                   size1,
                                   size2,
                                   title=out_img,
                                   cbfout=out_img,
                                   pilatus_header=h)

        return True

    def getHeaderWithRotationInfo(self, imgfile, phi_start, osc_width):
        h = cbf.get_pilatus_header(imgfile)
        #print h
        cols = h.split("\n")

        new_header = ""
        for i, col in enumerate(cols):
            #print i, col
            if col.rfind("Start_angle") != -1:
                strings = col.split()
                strings[2] = " %8.4f " % phi_start
                col = "# "
                for string in strings[1:]:
                    col += string
            elif col.rfind("Angle_increment") != -1:
                strings = col.split()
                strings[2] = " %8.4f " % osc_width
                col = "# "
                for string in strings[1:]:
                    col += (string + " ")

            new_header += (col + "\n")

        return new_header

    def modPhiInfo(self, cbfin, cbfout, phi_start, osc_width):
        # Read a header
        h = cbf.get_pilatus_header(cbfin)
        # print(h)
        # Read a cbf body from cbfin
        cbfbody, size1, size2 = cbf.load_minicbf_as_numpy(cbfin)

        cols = h.split("\n")
        new_header = ""
        for i, col in enumerate(cols):
            if col.rfind("Start_angle") != -1:
                strings = col.split()
                strings[2] = " %8.4f " % phi_start
                col = "# "
                for string in strings[1:]:
                    col += string
            elif col.rfind("Angle_increment") != -1:
                strings = col.split()
                strings[2] = " %8.4f " % osc_width
                col = "# "
                for string in strings[1:]:
                    col += (string + " ")

            new_header += (col + "\n")
        # Making a new image
        cbf.save_numpy_data_as_cbf(cbfbody.flatten(),
                                   size1=size1,
                                   size2=size2,
                                   title=cbfout,
                                   cbfout=cbfout,
                                   pilatus_header=new_header)
        return True

    # Multiple dose slicing data
    # lys01_master.h5
    # data_000001.h5 1-100
    # data_000002.h5 101-200
    # data_000003.h5 201-300
    # data_000004.h5 301-400...

    def sumUpList(self, param_list):
        sumfile_list, out_img, start_phi, osc_width = param_list
        data = None
        sel = None

        nok = 0
        #print "SUMMUUP:", sumfile_list
        for sumfile in sumfile_list:
            print "Summing up %s" % sumfile
            tmp, size1, size2 = cbf.load_minicbf_as_numpy(sumfile)

            if data is None:
                data = tmp
            else:
                data += tmp

        if sel is None:
            sel = tmp < 0
        else:
            sel |= tmp < 0

        data[sel] = -10

        new_header = self.getHeaderWithRotationInfo(sumfile, start_phi, osc_width)

        cbf.save_numpy_data_as_cbf(data.flatten(),
                                   size1=size1,
                                   size2=size2,
                                   title=out_img,
                                   cbfout=out_img,
                                   pilatus_header=new_header)

    #    mh5.sumDoseSlicing(master_file, n_sum_time, nimg_per_data, outprefix)
    def sumDoseSlicing(self, masterfile, n_sum_time, nimg_per_data, outprefix, dphi=0.1):
        data = None
        sel = None

        # Loop of summing up images
        proc_params = []
        start_phi = 0.0
        for original_imgnum in range(1, nimg_per_data+1):
            sumnum_list = []
            # n_sum_time : the number of summation time
            for junk_index in range(0, n_sum_time):
                sum_up_index = original_imgnum + nimg_per_data * (junk_index)
                sumnum_list.append(sum_up_index)

            # sumup list
            params = masterfile, sumnum_list, original_imgnum, outprefix, start_phi, dphi
            #params = masterfile, sumnum_list, block_id, outprefix, start_phi, dphi * nsum_rotation

            proc_params.append(params)
            start_phi += dphi

        easy_mp.pool_map(fixed_func=lambda n: self.sumUpList(n), args=proc_params, processes=self.nproc)

        return True

    #    mh5.sumDoseSlicing(master_file, n_sum_time, nimg_per_data, outprefix)
    def sumDoseSlicingPhiSlicing(self, cbf_prefix, n_sum_time, nsum_rotation, nimg_per_data, outprefix, dphi=0.1):
        data = None
        sel = None
        # making 'image number' list for summation
        # This block makes a information for summing up along 'rotation axis'.
        # 0.1 deg x 10 = 1.0 deg (1,2,3,4,5,6,7,8,9,10)
        sum_info_rot = []
        n_block = int(nimg_per_data / nsum_rotation)
        for i in range(0, n_block):
            bl_start = i * nsum_rotation + 1
            bl_end = bl_start + nsum_rotation - 1
            block_id = i + 1
            sum_info_rot.append((cbf_prefix, bl_start, bl_end, block_id))

        # This block makes a information for summing up along 'dose axis'
        #print "SUM_INFO_ROT=", sum_info_rot
        #print "NSUM_TIME=", n_sum_time
        # sum_info_rot
        # n_block -> 100 frames 0.1 deg
        # n_block -> 10 blocks for 10 summation, 5 blocks for 20 summation
        # Loop of summing up images
        proc_params = []
        # Loop for each 'block'
        start_phi = 0.0
        for cbf_prefix, bl_start, bl_end, block_id in sum_info_rot:
            sumfile_list = []
            # start angle of the rotation
            start_phi += dphi * nsum_rotation
            # Making summation list for 'dose' axis
            for original_imgnum in range(bl_start,bl_end+1):
                # n_sum_time : the number of summation time
                # Ex) 1st dataset = 1 MGy (No. 1-100)
                #     2nd dataset = 2 MGy (No. 101-200)
                #     3rd dataset = 3 MGy (No. 201-300)
                # n_sum_time = 2 -> Synthesize 2 MGy data
                # n_sum_time = 3 -> Synthesize 3 MGy data
                for junk_index in range(0, n_sum_time):
                    sum_up_index = original_imgnum + nimg_per_data
                    merge_cbf = "%s_%03d_%06d.cbf" % (cbf_prefix, junk_index+1, original_imgnum)
                    print merge_cbf
                    sumfile_list.append(merge_cbf)
            # sumup list
            #cbf_prefix, sumnum_list, out_img, start_phi, osc_width = param_list
            outimage = "%s_%06d.cbf" % (outprefix, block_id)
            new_osc_width = dphi * nsum_rotation
            params = sumfile_list, outimage, start_phi, new_osc_width
            proc_params.append(params)

       ##for p in proc_params:
        #    print "PROC_PARAMS=", p

        easy_mp.pool_map(fixed_func=lambda n: self.sumUpList(n), args=proc_params, processes=self.nproc)

        return True

if __name__ == "__main__":
    import sys

    file1 = sys.argv[1]

    mcbf = ManCBF()

    cbfin=sys.argv[1]
    cbfout=sys.argv[2]
    phi_start=float(sys.argv[3])
    osc_width=float(sys.argv[4])

    mcbf.modPhiInfo(cbfin, cbfout, phi_start, osc_width)
