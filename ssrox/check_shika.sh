#!/bin/bash

root=/isilon/users/target/target/AutoUsers/201019/abe/

for slog in `find $root -name diffscan.log`
do
 reld=`dirname $slog | sed -e "s,$root/,,"`
 echo "RELD=",$reld
 for prefix in `grep FILE_NAME $slog | sed -e 's,.*/,,; s/_?.*//'`
 do
  test -e $root/hirata_proc/01.CheckSHIKA/$reld/hits_${prefix}.lst && continue # skip already processed
  test -e $reld/_spotfinder/summary.dat || continue

  pin=`echo $reld | sed -e "s,/.*,,"`
  sample=`awk '/'$pin'/{print $3}' ./sample.txt`
  echo "FOUND: $reld/${prefix} $sample"
  grep "^${prefix}_ " $reld/_spotfinder/summary.dat | awk '/n_spots/ {a+=1; if($5>=5){h+=1}}END{print "  >=10:", h, 100*h/a}'
 done
done
