import os, sys, optparse
import SSROXmaster

if __name__ == "__main__":

    parser = optparse.OptionParser()

    parser.add_option(
        "-z", "--zoodb",
        default="None",
        help="ZOO .db file for extracting sample/measurement information.",
        type="string",
        dest="zoodb"
    )

    parser.add_option(
        "-p", "--nproc",
        default=12,
        help="Number of CPUs for crystfel processing.",
        type="int",
        dest="nproc"
    )

    parser.add_option(
        "-s", "--nspots",
        default=5,
        help="Number of diffraction spots/image for data processing",
        type="int",
        dest="nspots"
    )

    parser.add_option(
        "-g", "--geom",
        default=None,
        help="geometry file for processing.",
        type="string",
        dest="geomfile"
    )

    parser.add_option(
        "-m", "--measurement_root",
        default=None,
        help="A root directory of the measurements.",
        type="string",
        dest="meas_root"
    )

    parser.add_option(
        "-c", "--pdbconf",
        default=None,
        help="Configure file of PDB files",
        type="string",
        dest="pdbconf"
    )

    parser.add_option(
        "--nw", "--nwing",
        default=2,
        help="wing is the number of conditions add to the left/right and upper/lower wings",
        type="int",
        dest="nwing"
    )

    parser.add_option(
        "--stp", "--step",
        default=3,
        help="pixel length between each grid.",
        type="int",
        dest="step"
    )

    parser.add_option(
        "-f", "--nframes",
        default=-1,
        help="maximum number of frames to be processed.",
        type="int",
        dest="nframes"
    )

    parser.add_option(
        "--smpl", "--sample",
        default=None,
        help="Sample name in ZOO data collection sheet.",
        type="string",
        dest="samplename"
    )


    (opts, args) = parser.parse_args()

    print(opts.nwing, opts.step)

    ssroxmaster = SSROXmaster.SSROXmaster(opts.zoodb, opts.meas_root, opts.geomfile, opts.pdbconf)
    # ssroxmaster = SSROXmaster.SSROXmaster(zoodb_file, meas_rootdir, geomfile_path, pdbconf_path)

    # beam position calculation
    count=0
    for i in range(-opts.nwing, opts.nwing+1):
        for j in range(-opts.nwing, opts.nwing + 1):
            dx = i * opts.step
            dy = j * opts.step
            print(dx, dy)
            count+=1

    # Min/Max X
    min = -opts.nwing * opts.step
    max = opts.nwing * opts.step

    print("Counter=", count)
    print("Min/Max = ", min, max)
    ssroxmaster.refineBeam(opts.nspots, opts.samplename, min_dx=min, max_dx=max, min_dy=min, max_dy=max, step=opts.step,nproc=opts.nproc, nframes=opts.nframes)
