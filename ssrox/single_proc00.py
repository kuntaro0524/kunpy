#!/usr/bin/env python
# coding: utf-8
import sys, os
import pandas as pd
import sqlite3
import logging
import logging.config
import SSROXpinMaster as SSRM
import PDBconfig

# Initialization of the log file
logname = "./ssrox_proc.log"
each_env = "/isilon/users/target/target/Staff/rooms/"
#logging.config.fileConfig('%s/kunpy/ssrox/logging.conf' % each_env, defaults={'logfile_name': logname})
logging.config.fileConfig('/isilon/users/target/target/Staff/rooms/kuntaro/ssrox/logging.conf', defaults={'logfile_name': logname})
logger = logging.getLogger('SSROX')
os.chmod(logname, 0666)

# Parsing arguments
if len(sys.argv) != 8:
    print(len(sys.argv))
    print("PROGRAM requires X arguments")
    print("PUCKID PINID N_MOUNT(normally 0) SAMPLE_NAME MEAS_ROOTDIR GEOMFILE PDBCONFIG")
    sys.exit()
puckid = sys.argv[1]
pinid = int(sys.argv[2])
n_mount = int(sys.argv[3])
sample_name = sys.argv[4]

# root directory should not be extracted from the value in the zoodb
# because the path should be replaced at beamline and laboratories soon.
meas_root = os.path.abspath(sys.argv[5])
if os.path.exists(meas_root) == False:
    logger.info("No such directories.")
    sys.exit()

# Data processing directory
proc_root = os.path.abspath(".")
# Geometry file path
geom_orig = sys.argv[6]
# PDB file config
pdbconf_file = sys.argv[7]

# PDB config file
try:
    pdbconf = PDBconfig.PDBconfig(pdbconf_file)
except Exception as e:
    print(e.args)
    sys.exit()

# ofile=open("sample_prep.txt","w")
ppinfo = "%s-%02d" % (puckid, pinid)

scan_dir = "%s/%s/scan%02d/ssrox/" % (meas_root, ppinfo, n_mount)
logger.info("data directory = %s %s" % (sample_name, scan_dir))

# Extract PDB file name from configure file
try:
    pdbfile = pdbconf.getPDB(sample_name)
except Exception as e:
    print(e.args)
    pdbfile = ""

# Processing
proc_dir = os.path.join(proc_root, ppinfo)
if os.path.exists(proc_dir) == False:
    os.makedirs(proc_dir)
print(pdbfile)
ssrox_master = SSRM.SSROXpinMaster(scan_dir, proc_dir, geom_orig, pdbfile)
try:
    ssrox_master.run(2, nproc=128, beam_dx=0, beam_dy=0, redo_flag=False)
except Exception as e:
    logger.error(e.args)