import h5py
import sys

if len(sys.argv) != 3 and len(sys.argv) != 4:
    print "Usage: %s master.h5 mask.h5 [dials_mask.pickle]" % sys.argv
    sys.exit(-1)

f = h5py.File(sys.argv[1], "r")
mask = f["/entry/instrument/detector/detectorSpecific/pixel_mask"].value
f.close()

if (len(sys.argv) == 4):
    import numpy as np
    import cPickle as pickle

    m = pickle.load(open(sys.argv[3]))[0].as_numpy_array()
    mask[m == False] = 1

# Add more masks (horizontal lines in the middle of ASICs)
# these coordinates are for EIGER 16M

#mask[255:261,:] = 1
#mask[806:812,:] = 1
#mask[1357:1363,:] = 1
#mask[1908:1914,:] = 1
#mask[2459:2465,:] = 1
#mask[3010:3016,:] = 1
#mask[3561:3566,:] = 1
#mask[4112:4118,:] = 1

# clearly dead (EIGER 9M at BL32XU)
mask[1596,1561] = 1
mask[1595,1561] = 1
mask[1591,1551] = 1

out = h5py.File(sys.argv[2])
out["pixel_mask"] = mask
out.close()

