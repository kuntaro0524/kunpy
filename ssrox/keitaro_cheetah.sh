#!/bin/bash
prefix="ssrox"
for i in `grep "^${prefix}_ " ./summary.dat | awk '/n_spots/ && $5>=10{print $6}' | sed -e 's,.*_0*\([0-9][0-9]*\)\.img,\1,'`
do
 i=$((i-1))
 n=$((i/100+1))
 j=$((i%100))
 printf "$root/$data/${prefix}_data_%.6d.h5 //%d\n" $n $j >> hits_${prefix}.lst
done

