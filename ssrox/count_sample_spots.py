#!/usr/bin/env python
# coding: utf-8
import sys
import pandas as pd
import sqlite3
import SSROXpinMaster as SSRM
import os, logging
import logging.config

# Initialization of the log file
logname = "./ssrox_proc.log"
logging.config.fileConfig('/isilon/users/target/target/Staff/rooms/kuntaro/ssrox/logging.conf', defaults={'logfile_name': logname})
logger = logging.getLogger('SSROX')
os.chmod(logname, 0666)

file_sqlite3=sys.argv[1]
conn = sqlite3.connect(file_sqlite3)
cursor=conn.cursor()

df=pd.read_sql_query('SELECT * FROM ESA', conn)
df.columns

# root directory should not be extracted from the value in the zoodb
# because the path should be replaced at beamline and laboratories soon.
root_dir=sys.argv[2]

proc_dir=os.path.abspath(".")

# ofile=open("sample_prep.txt","w")
for sample_name, newdf in df.groupby('sample_name'):
    puckids=newdf['puckid']
    pinids=newdf['pinid']
    n_mounts=newdf['n_mount']

    # For each sample consists of several pins
    n_hits_sample = 0
    for puckid, pinid,n_mount in zip(puckids, pinids, n_mounts):
        ppinfo="%s-%02d" % (puckid, pinid)

        scan_dir = "%s/%s/scan%02d/ssrox/" % (root_dir, ppinfo, n_mount)

        # Processing
        ssrox_master=SSRM.SSROXpinMaster(scan_dir, puckid, pinid, proc_dir, geom_file="dummay", pdbfile="dummy")
        hits_file_list=ssrox_master.prepProcessing(nspot_thresh=10)
        n_hits_sample+=len(hits_file_list)
        print(puckid, pinid, n_hits_sample)

    print(sample_name, n_hits_sample)

    #def __init__(self, scan_dir, puck, pin, proc_dir, geom_file, pdbfile):
