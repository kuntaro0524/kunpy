import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt

def returnPlotData(df):
    bin_indices = pd.cut(df['dstar2'], 100, precision=0, labels=False)
    bin_labels = pd.cut(df['dstar2'], 100, precision=0, labels=False)
    print(bin_indices)
    df['pppp'] = bin_indices

    color_r = 0.02
    color_b = 1.0
    Ide = []
    ds2 = []
    err = []
    for n_bin, gdf in df.groupby('pppp'):
        mean_I = gdf['Idecay_mean'].mean()
        mean_ds2 = gdf['dstar2'].mean()
        variant = gdf['Idecay_mean'].std()

        Ide.append(mean_I)
        ds2.append(mean_ds2)
        err.append(variant)

    Ide_a = np.array(Ide)
    ds2_a = np.array(ds2)
    err_a = np.array(err)

    return Ide_a, ds2_a, err_a

# Plot preparation
fig, axis = plt.subplots(nrows=1, ncols=1, sharey=True, figsize=(18.0, 12.0))
axis.set_yscale('log')

# print(len(axes))
plt.rcParams['lines.markersize'] = 0.5

df1 = pd.read_csv("./10.csv")
df2 = pd.read_csv("./50.csv")
df3 = pd.read_csv("./100.csv")
df4 = pd.read_csv("./200.csv")
df5 = pd.read_csv("./dose10.0_1000.csv")

color_index=0
color_array=['red', 'blue', 'pink', 'black', 'green']
labels=['10-merged', '50-merged', '100-merged', '200-merged', '1000-merged']
line_styles=['--', '-.', ':','-', '-']

for dataframe in [df1, df2, df3, df4, df5]:
    Ia, ds2a, err = returnPlotData(dataframe)
    # plt.errorbar(ds2a, Ia, yerr=err, capsize=5, markersize=5, fmt='o', ecolor='black', color='blue')
    axis.plot(ds2a, Ia, color=color_array[color_index],label=labels[color_index], linestyle=line_styles[color_index])
    color_index+=1

plt.legend(loc="upper left", fontsize=14)
plt.show()
