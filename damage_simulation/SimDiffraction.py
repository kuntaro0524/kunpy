import sys
import random
import copy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

class SimDiffraction():
    def __init__(self, refl_csv):
        self.ref_df = pd.read_csv(refl_csv)
        # print(self.ref_df)
        self.plot=False
        self.isInit=False

    def init(self):
        self.max_angle = self.ref_df['angle'].max()
        print(self.max_angle)

    def calcDecayFactor(self, frame_index, total_dose):
        dose_per_frame = total_dose / float(self.n_frames)
        dose_for_this_frame = dose_per_frame * float(file_index)
        decay_factor = 0.5 * dose_for_this_frame

        return decay_factor

    def applyRelativeB(self, frame_index, relB):
        chosen_df = self.ref_df[self.ref_df['angle']==float(frame_index)]
        cdf=copy.deepcopy(chosen_df)
        cdf['Idecay']=chosen_df['I'] * np.exp(-relB * chosen_df['dstar2'])

        if self.plot:
            fig, axes=plt.subplots(1,2,figsize=(14,10), sharey=True)
            cdf.plot.scatter('dstar2', 'Idecay', ax=axes.flatten()[0])
            chosen_df.plot.scatter('dstar2', 'I', ax=axes.flatten()[1])
            plt.show()

        return cdf

    def plotDataFrame(self, df, figname, column="I"):
        fig, axes = plt.subplots(1, 1, figsize=(14, 10), sharey=True)
        df.plot.scatter('dstar2', column, ax=axes)

        # plt.show()
        plt.savefig(figname)

    # Add 'collection sequence' for applying relative B-factor
    # all_seq = [1,2,3,4,5,6,7,8,9,10] as a numpy.array
    def addCollectionSequence(self, all_seq, chosen_df):
        for collection_sequence, angle in enumerate(all_seq):
            # print(collection_sequence, angle)
            chosen_df.loc[chosen_df['angle'] == angle, 'collect_sequence'] = collection_sequence + 1
        return chosen_df

    def makeCrystalData(self, start_angle, end_angle):
        # End angle is out of the maximum frames in DF
        if start_angle == 45.0:
            end_angle = end_angle-start_angle
            start_angle = 1
            sel1 = self.ref_df['angle'] >= start_angle
            sel2 = self.ref_df['angle'] <= end_angle
            selall=sel1&sel2
            chosen_df = self.ref_df[selall]
            # Data collection sequence
            all_seq=np.arange(start_angle, end_angle+1)
            chosen_df=self.addCollectionSequence(all_seq, chosen_df)
            return chosen_df

        elif end_angle >= 45.0:
            end01 = 45
            hamidashi = end_angle - 45
            start02 = 1
            end02 = start02 + hamidashi
            # The initial data frame
            df01 = self.ref_df[self.ref_df['angle']>=start_angle]
            # print("DF01=",len(df01))
            # The second data frame
            sel1 = self.ref_df['angle']>=start02
            sel2 = self.ref_df['angle']<end02
            selall=sel1&sel2

            df02 = self.ref_df[selall]
            # connect df01 and df02
            chosen_df=pd.concat([df01, df02])

            # Data collection sequence is important to apply relative B-factor
            # This block appends 'collection sequence' to the data frame.
            first_seq=np.arange(start_angle, end01+1)
            second_seq=np.arange(start02, end02)
            all_seq=np.concatenate([first_seq, second_seq])
            # Data collection sequence
            chosen_df=self.addCollectionSequence(all_seq, chosen_df)

            return chosen_df

        else:
            sel1 = self.ref_df['angle'] >= start_angle
            sel2 = self.ref_df['angle'] <= end_angle
            selall=sel1&sel2
            chosen_df = self.ref_df[selall]
            # Data collection sequence
            all_seq=np.arange(start_angle, end_angle+1)
            chosen_df=self.addCollectionSequence(all_seq, chosen_df)

            return chosen_df

    def makeDecayedCrystalData(self, start_angle, end_angle, total_dose):
        # fig, axes = plt.subplots(1, 1, figsize=(14, 10), sharey=True)
        # fig, axes = plt.subplots()

        crystal_df = self.makeCrystalData(start_angle, end_angle)
        # print(crystal_df['collect_sequence'])
        # print(crystal_df.describe())
        # crystal_df.plot.scatter('dstar2', "I", color='red',ax=axes)

        n_frame = end_angle - start_angle + 1
        dose_per_frame = total_dose / n_frame
        # print("Dose/frame", dose_per_frame)
        # print(n_frame)
        relB = 0.0

        n_add=0
        for index, gdf in crystal_df.groupby('angle'):
            # print("Relative B-factor %8.2f is applied to this frame" % relB)
            # print(gdf.describe())
            # print("Relative B-factor ", relB)
            gdf['relativeB']=gdf['collect_sequence']*dose_per_frame
            gdf['Idecay'] = gdf['I'] * np.exp(-gdf['relativeB'] * gdf['dstar2'])

            if n_add==0:
                newdf = gdf
            else:
                newdf = pd.concat([newdf, gdf])
            n_add+=1
        # print(len(newdf))
        # self.plotDataFrame(newdf, "decay.png", column="Idecay")
        # newdf.plot.scatter('dstar2', 'Idecay', ax=axes)
        plt.show()

        # self.dumpDF(newdf)
        return newdf

    def dumpDF(self, df):
        for index, row in df.iterrows():
            print(row[['angle', 'collect_sequence', 'relativeB']])

    def generateRandomCrystals(self, n_merge, total_dose):
        for i in range(0, n_merge):
            start_deg = int(random.uniform(1, 45))
            end_deg=start_deg + 9
            print(start_deg, end_deg)
            crystal1=self.makeDecayedCrystalData(start_deg,end_deg,total_dose)

            if i==0:
                damage_df=crystal1
            else:
                damage_df = pd.concat([damage_df, crystal1])

        return damage_df

    def mergeCrystals(self, n_merge, total_dose):
        # Generating damaged structure factors
        print("start end")
        damaged_crystals = self.generateRandomCrystals(n_merge, total_dose)

        store_info=[]

        check_index=0
        # Grouping by using 'reflection index'
        for r_index, gdf in damaged_crystals.groupby('r_index'):
            # average value of dstar2
            # dstar2 is identical always but I cannot make a good code to achieve it.
            dstar2=gdf['dstar2'].mean()
            # averaging B-factor
            mean_relativeB = gdf['relativeB'].mean()
            std_relativeB=gdf['relativeB'].std()

            tmpdict={
                "Idecay_mean": gdf['Idecay'].mean(),
                "Idiff_mean": (np.fabs(gdf['Idecay']-gdf['I'])).mean(),
                "r_index": r_index,
                "dstar2": dstar2,
                "redun": len(gdf),
                "meanRelB": mean_relativeB,
                "stdRelB": std_relativeB
            }
            store_info.append(tmpdict)

        final_df=pd.DataFrame(store_info)

        return final_df

    def applyDecayToEachData(self, total_dose):
        iii=[]
        for i_nframe in range(0, self.n_frames):
            decay_factor = self.calcDecayFactor(i_nframe, total_dose)

        frame_data=np.array(iii)
        print(len(frame_data))
        print(frame_data)

        plt.plot(frame_data)
        plt.show()

if __name__ == "__main__":
    sim=SimDiffraction(sys.argv[1])

    sim.init()
    # sim.applyRelativeB(1, 5)
    #sim.applyDecayToEachData(10.0)
    # df=sim.makeCrystalData(45,55)
    # print(df.describe())

    # n_merge=int(sys.argv[2])
    # total_dose=float(sys.argv[3])

    # sim.mergeCrystals(n_merge,total_dose)
    # total_dose=5
    # d_10=sim.mergeCrystals(10, total_dose)
    # d_10.to_csv("dose%0.1f_010.csv"%total_dose)
    # d_50=sim.mergeCrystals(50, total_dose)
    # d_50.to_csv("dose%0.1f_050.csv"%total_dose)
    # d_100 = sim.mergeCrystals(100, total_dose)
    # d_100.to_csv("dose%0.1f_100.csv" % total_dose)
    # d_200 = sim.mergeCrystals(200, total_dose)
    # d_200.to_csv("dose%0.1f_200.csv"%total_dose)
    #
    # fig, axes = plt.subplots(1, 1, figsize=(14, 10), sharey=True)
    # df.plot.scatter('dstar2', 'Idecay_mean', ax=axes)
    # plt.savefig(figname)
    #
    # # total_dose=10
    # # sim.makeDecayedCrystalData(43,52,total_dose)

    total_dose=float(sys.argv[2])
    ndata=int(sys.argv[3])

    d_10=sim.mergeCrystals(ndata, total_dose)
    d_10.to_csv("dose%0.1f_%04d.csv"%(total_dose, ndata))
