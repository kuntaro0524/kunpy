#!/bin/bash

dose_list="1 2 5 10 20 50 100"
nds_list="10 100 150 200 500 1000 10000"

ori_refl="/isilon/users/target/target/Staff/rooms/kuntaro/damage_simulation/test.csv"

for dose in $dose_list; do

for nds in $nds_list; do
echo $dose $nds

prefix=d${dose}_n${nds}

qsub -pe par 1 -j y -cwd -S /bin/bash -V -N $prefix << +
yamtbx.python gen_dose_nframe_simdata.py $ori_refl $nds $dose
+

done

done
