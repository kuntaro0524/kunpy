#!/bin/bash

dose_list="1 10 20 40"
nds_list="25 100 150 200 500"

ori_refl="./test.csv"

for dose in $dose_list; do

for nds in $nds_list; do
echo $dose $nds

prefix=d${dose}_n${nds}

yamtbx.python gen_dose_nframe_simdata.py $ori_refl $nds $dose

done
done
