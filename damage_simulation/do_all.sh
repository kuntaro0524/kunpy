#!/bin/bash
yamtbx.python gen_dose_nframe_simdata.py test.csv  25 5
yamtbx.python gen_dose_nframe_simdata.py test.csv  50 5
yamtbx.python gen_dose_nframe_simdata.py test.csv  75 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 100 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 125 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 150 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 175 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 200 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 250 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 300 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 500 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 1000 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 5000 5
yamtbx.python gen_dose_nframe_simdata.py test.csv 10000 5

