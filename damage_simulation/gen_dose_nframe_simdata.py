import sys
import random
import copy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import SimDiffraction

if __name__ == "__main__":


    # Genuine structure factors in this simulation
    correct_f = sys.argv[1]

    sim=SimDiffraction.SimDiffraction(correct_f)
    sim.init()

    nmerge = int(sys.argv[2])
    dose = float(sys.argv[3])

    df=sim.mergeCrystals(nmerge, dose)
    df.to_csv("dose%.1f_%04d.csv"%(dose, nmerge))
