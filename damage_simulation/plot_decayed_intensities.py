import sys
import random
import copy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import SimDiffraction

if __name__ == "__main__":
    # Genuine structure factors in this simulation
    correct_f ="/isilon/users/target/target/Staff/rooms/kuntaro/damage_simulation/test.csv"

    sim=SimDiffraction.SimDiffraction(correct_f)
    sim.init()

    n_merge = int(sys.argv[1])
    dose = float(sys.argv[2])
    dataframe=sim.generateRandomCrystals(n_merge, dose)
    dataframe.to_csv("dose%.1f_%04d.csv"%(dose, n_merge))


