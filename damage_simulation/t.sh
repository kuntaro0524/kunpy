#!/bin/bash

dose_list="1"
nds_list="10 100"

for dose in $dose_list; do

for nds in $nds_list; do
echo $dose $nds

prefix=dose${dose}_N${nds}

qsub -pe par 1 -j y -cwd -S /bin/bash -V -N $prefix << +
yamtbx.python gen_dose_nframe_simdata.py $nds $dose
+

done

done
