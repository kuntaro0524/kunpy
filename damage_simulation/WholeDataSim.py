import sys
import random
import copy
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

class WholeDataSim():
    def __init__(self, start_asym, end_asym):
        self.start_asym=0.0
        self.end_asym=45.0
        self.width=1
        
        self.n_max_refl=100
        # D range
        self.dmin=1.4
        self.dmax=25

        # D^3 range
        self.ds3_min=np.power(1/self.dmax,3)
        self.ds3_max=np.power(1/self.dmin,3)

        # original B-factor
        self.b_orig = 25.0

        # reflection for each frame
        self.reflections=[]

        self.n_frames=int((self.end_asym-self.start_asym)/self.width)

        # reflection index
        self.r_index=0

    def getExppart(self, B, d):
        ds = 1/2.0/d
        kakko=-B*(ds*ds)

        return np.exp(kakko)

    def genReflectionOnEachFrame(self, frame_index):
        # intensity length
        intensity_dict={}
        intensity_list=[]

        for i in range(0, self.n_max_refl):
            ds3 = random.uniform(self.ds3_min, self.ds3_max)
            d = 1.0/np.cbrt(ds3)

            tmp_intensity = 1000.0*random.random()
            exp_part = self.getExppart(self.b_orig, d)
            b_intensity = tmp_intensity * exp_part

            intensity_dict={
                'r_index': self.r_index,
                'angle': frame_index+1,
                'd': d,
                'dstar2': 1/d/d,
                'I': b_intensity,
                'I_norm': tmp_intensity
            }
            intensity_list.append(intensity_dict)
            self.r_index+=1

        # Convert the list to data frame
        df = pd.DataFrame(intensity_list)

        #print(df)
        #df.plot.scatter(x='dstar2', y='I')
        #plt.show()

        print(df.describe())

        return df

    def genData(self):
        # Storing 'non-decayed' intensities in each frame
        for i in range(0, self.n_frames):
            frame_refl_df = self.genReflectionOnEachFrame(i)
            if i==0:
                all_df = frame_refl_df
            else:
                all_df = pd.concat([all_df, frame_refl_df])

        all_df.plot.scatter(x='dstar2', y='I', logy=True)
        plt.show()

        return all_df

    def get(self, start_angle, end_angle):
        diff_angle = end_angle - start_angle
        while(start_angle < 45.0) :
            start_angle += 45.0
        while(start_angle > 45.0):
            start_angle -= 45.0

        end_angle = start_angle+diff_angle
        if end_angle > 45.0:
            end_angle = end_angle - 45.0
            start_angle = start_angle - 45.0

        print(start_angle, end_angle)

        rtn_array=[]

        for array in self.intensity_array[int(start_angle):int(end_angle)]:
            print(array)

if __name__ == "__main__":
    asym_size=float(sys.argv[1])
    wds=WholeDataSim(0,asym_size)
    aaa=wds.genData()
    aaa.to_csv("sim_refl_asym_%.1f.csv"%asym_size, index=False)
