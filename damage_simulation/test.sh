#!/bin/bash

dose_list="1 10 40"
nds_list="25 50 75 100 125 150 175 200 250 300"

ori_refl="./test.csv"

for dose in $dose_list; do

for nds in $nds_list; do
echo $dose $nds

prefix=d${dose}_n${nds}

echo $ori_refl $nds $dose

yamtbx.python gen_dose_nframe_simdata.py $ori_refl $nds $dose

done
done
