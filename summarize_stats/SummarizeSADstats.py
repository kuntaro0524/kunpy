import pandas as pd
import sys, os
sys.path.append("../Libs/")
import AnaXSCALE
import logging

# This class is designed to each merging directory
class SummarizeSADstats():

    # root_dir: 00.SAD/ directories from ODEN
    def __init__(self, root_dir):
        self.root_dir = root_dir
        self.isPrepPf = False
        self.logger = logging.getLogger("ODEN").getChild("SADstats")

    def setDose(self, dose_value):
        self.dose = dose_value

    def checkExistence(self, shelx_project_name = "lys"):
        # is bad flag
        isBad = False

        self.logger.info("ls %s" % self.root_dir)
        # xscale.hkl existence
        xscale_path = os.path.join(self.root_dir, "../xscale.hkl")
        if os.path.exists(xscale_path) == False:
            self.logger.error("xscale.hkl cannot be found.")
            self.isExistHKLfile = False
            isBad = True
        # PDB file for lys.pdb
        # PDB file from SHELXE existense
        pdbname = "%s.pdb" % shelx_project_name
        pdb_path = os.path.join(self.root_dir, pdbname)
        if os.path.exists(pdb_path) == False:
            self.logger.error("Built-PDB: %s does not exist" % pdb_path)
            self.isExistShelxModel = False
            self.isMapCCavailable = False
            isBad = True
        # Existence of evaluating phase directory
        phase_dir = os.path.join(self.root_dir, "../02.CheckPhase/")
        # directory check
        if os.path.exists(phase_dir) == False:
            self.isCheckedPhase = False
            self.logger.error("02.CheckPhase directory not found.")
            isBad = True

        # directory is okay
        else:
            # MTZ file check
            mtz_phase_path = os.path.join(phase_dir, "lys.mtz")
            if os.path.exists(mtz_phase_path) == False:
                self.logger.error("MTZ_ERROR %s" % mtz_phase_path)
                self.isExistPhaseMTZ = False
                isBad = True

        # MAP cc calculation
        # refinement model existence
        # Existence of evaluating phase directory
        mr_dir = os.path.join(self.root_dir, "../01.MR/")
        # directory check
        if os.path.exists(mr_dir) == False:
            self.isMRdir = False
            self.logger.error("MR directory not found.")
            isBad = True
        else:
            # refined model file check
            pdbmodel_path = os.path.join(mr_dir, "lys_001.pdb")
            if os.path.exists(mtz_phase_path) == False:
                self.logger.error("REFINE_ERROR %s" % pdbmodel_path)
                self.isExistRefinedPDB = False
                isBad = True

        if isBad == False:
            return 1
        else:
            return 0

    def read_phenix_merging_stats(self, filename='merging_stats.log', return_dataFrame=False):
        log_path = os.path.join(self.root_dir, filename)

        lines = open(log_path,"r").readlines()
        start_flag=False

        stats_lines = []
        for line in lines:
            if line == "\n":
                continue
            if line.rfind(" d_max  d_min   #obs  #uniq   mult.  %comp       <I>  <I/sI>    r_mrg   r_meas    r_pim   cc1/2   cc_ano")!=-1:
                start_flag = True
                continue
            if line.rfind("References:") !=-1:
                break
            if start_flag == True:
                stats_lines.append(line)

        if start_flag == False:
            raise Exception("data statistics table does not exist")

        def get_params(line):
            cols=line.split()
            d_max=float(cols[0])
            d_min=float(cols[1])
            mult=float(cols[4])
            comp=float(cols[5])
            isigi=float(cols[7])
            r_mrg=float(cols[8])
            r_mes=float(cols[9])
            r_pim=float(cols[10])
            cc_half=float(cols[11])
            cc_ano=float(cols[12])

            return d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano

        def csv_string(params, shelltype):
            d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano = params
            param_names = ["dmax", "dmin", "redun", "compl", "isigi", "r_mrg", "r_mes", "r_pim", "cc_half", "cc_ano"]

            param_list = []
            name_list = []
            for param, param_name in zip(params, param_names):
                param_list.append(param)
                name_list.append("%s_%s"% (shelltype, param_name))
            return name_list, param_list

        param_list = []
        names_list = []
        for index,line in enumerate(stats_lines):
            # d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano
            if index==0:
                params = get_params(line)
                names, params = csv_string(params, "phenix_inner")
                param_list += params
                names_list += names
            if index==len(stats_lines)-1:
                params = get_params(line)
                names, params = csv_string(params, "phenix_overall")
                params = get_params(line)
                param_list += params
                names_list += names
            if index==len(stats_lines)-2:
                params = get_params(line)
                names, params = csv_string(params, "phenix_outer")
                param_list += params
                names_list += names

        if return_dataFrame==False:
            return names_list, param_list
        else:
            # Data frame return
            dict = {}
            for name, param in zip(names_list, param_list):
                dict[name] = param
            df = pd.DataFrame(dict.values(), index=dict.keys()).T
            return df

    def analyzeRefinement(self, relative_path="../01.MR/"):
        refine_logpath = os.path.join(self.root_dir, relative_path, "lys_001.log")

        # check existence
        if os.path.exists(refine_logpath) == False:
            self.logger.info("Refinement log does not exist")
            freer = -9999.9999
            rfac = -9999.9999
        else:
            lines = open(refine_logpath, "r").readlines()
            last_line = lines[-1].replace(",","")
            cols = last_line.split()

            freer = float(cols[6])
            rfac = float(cols[3])

        # insert CC values to the pandas dataframe.
        if self.isPrepPf == False: self.analyzeXSCALELP()

        self.stats_df['model_freeR'] = freer
        self.stats_df['model_rfact'] = rfac

        return freer, rfac

    def analyzeXSCALELP(self, relative_path="../"):
        # XSCALE path
        xscale_path = os.path.join(self.root_dir, relative_path, "XSCALE.LP")

        if os.path.exists(xscale_path) == False:
            raise Exception("XSCALE.LP does not exist at the root directory.")
        # XSCALE.LP will be converted to CSV file.
        ac = AnaXSCALE.AnaXSCALE(xscale_path)
        # File name of analyzed xscale.lp
        out_csv = os.path.join(self.root_dir, "xscale.csv")
        ac.makeStatsCSV(out_csv)

        #XSCALE.CSV should be read
        self.stats_df = pd.read_csv(out_csv)
        self.stats_df.dropna()

        # prep flag is true
        self.isPrepPf = True
        return self.stats_df

    def analyzeMapCC(self, check_phase=False):
        # File convertion from mapcc.dat file
        if check_phase == True:
            mapccfile = os.path.join(self.root_dir, "../02.CheckPhase/mapcc.dat")
        else:
            mapccfile = os.path.join(self.root_dir, "mapcc.dat")

        mapcc=None
        region_mapcc=None
        if os.path.exists(mapccfile) == False:
            self.logger.info("MapCC calculation was not conducted. check_phase=%s" % check_phase)
            mapcc = -99999.99999
            region_mapcc = -99999.99999
        else:
            lines = open(mapccfile, "r").readlines()
            for line in lines:
                if line.rfind("Correlation")!=-1:
                    cols = line.split()
                    region_mapcc = float(cols[5])
                    mapcc = float(cols[7])
                    break

        # insert CC values to the pandas dataframe.
        if self.isPrepPf == False: self.analyzeXSCALELP()

        if check_phase == True:
            self.stats_df['mapcc_checkphase'] = mapcc
            self.stats_df['region_cc_checkphase'] = region_mapcc
        else:
            self.stats_df['mapcc'] = mapcc
            self.stats_df['region_cc'] = region_mapcc

        return mapcc, region_mapcc

    def setDoseAndPath(self):
        # insert wilson B-factor to the pandas dataframe.
        if self.isPrepPf == False: self.analyzeXSCALELP()

        self.stats_df['dose'] = self.dose
        self.stats_df['proc_path'] = self.root_dir

    def analyzeIsoB(self):
        # File convertion from mapcc.dat file
        isobfile = os.path.join(self.root_dir, "isob.dat")
        wilson_b = None
        if os.path.exists(isobfile) == False:
            self.logger.info("Isotropic B-factor file does not exist.%s" % self.root_dir)
            wilson_b = -9999.9999
        else:
            lines = open(isobfile, "r").readlines()
            for line in lines:
                if line.rfind("Least")!=-1:
                    cols = line.split()
                    wilson_b = float(cols[7])
                    break

        # insert wilson B-factor to the pandas dataframe.
        if self.isPrepPf == False: self.analyzeXSCALELP()

        self.stats_df['wilsonb'] = wilson_b

    def analyzeSHELXE(self, isMerge=True, target_file = "contrast.dat"):
        #File path
        filepath = os.path.join(self.root_dir, target_file)
        # file existence check
        if os.path.exists(filepath) == False:
            raise Exception("contrast.dat does not exist.")

        # Read the CSV file
        datadf=pd.read_csv(filepath)

        # Check initialization
        if self.isPrepPf == False: self.analyzeXSCALELP()
        # join the shelxe pandas data frame to the entire panda data frame.
        if isMerge == True:
            self.stats_df = self.stats_df.join(datadf)

        # print(self.stats_df)

    def analyzePhenixMergingStatistics(self):
        try:
            # Reading a log file from phenix.merging_stats
            name_list, param_list = self.read_phenix_merging_stats()
        except Exception as e:
            print("analyzePhenixMergingStatitics. Trouble to read 'merging stats")
            return False

        # PHENIX merging statistics data
        phenix_csv = os.path.join(self.root_dir, "phenix_stats.csv")
        po = open(phenix_csv, "w")

        for index, colname in enumerate(name_list):
            if index == len(name_list)-1:
                po.write("%s" % colname)
            else:
                po.write("%s," % colname)

        po.write("\n")

        for index, param in enumerate(param_list):
            if index == len(param_list)-1:
                po.write("%s" % param)
            else:
                po.write("%s," % param)
        po.close()

        phenix_stats_info = pd.read_csv(phenix_csv)
        self.stats_df = self.stats_df.join(phenix_stats_info)
        self.stats_df.dropna()

        return True

    def analyzePhase(self):
        mapcc, regionmap_cc = self.analyzeMapCC(check_phase=True)

        return True

    def run(self, output_csv):
        out_path = os.path.join(self.root_dir, output_csv)
        # Essential
        self.analyzeXSCALELP()
        # Essential
        self.setDoseAndPath()
        # Additional
        self.analyzeIsoB()

        # Essential
        try:
            self.analyzeMapCC()
        except Exception as e:
            self.logger.error("Program terminated: check" % self.root_dir)
            sys.exit()
        # Essential
        try:
            self.analyzePhenixMergingStatistics()
        except Exception as e:
            self.logger.error("Program terminated: check" % self.root_dir)
            sys.exit()
        # Refinement
        self.analyzeRefinement()
        self.analyzeSHELXE()
        self.analyzePhase()

        self.stats_df.to_csv(out_path)

    def run2(self, output_csv):
        out_path = os.path.join(self.root_dir, output_csv)
        # Essential
        self.analyzeXSCALELP()
        # Essential
        self.setDoseAndPath()
        # Additional
        self.analyzeIsoB()

        # Essential
        try:
            self.analyzeMapCC()
        except Exception as e:
            self.logger.error("Program terminated: check" % self.root_dir)
            sys.exit()
        # Essential
        try:
            self.analyzePhenixMergingStatistics()
        except Exception as e:
            self.logger.error("Program terminated: check" % self.root_dir)
            sys.exit()
        # Refinement
        self.analyzeRefinement()
        self.analyzeSHELXE()
        self.analyzePhase()

        self.stats_df.to_csv(out_path)

if __name__ == "__main__":
    sus = SummarizeSADstats(sys.argv[1])
    sus.checkExistence("lys")
    # sus.run("stats_summary.csv")
    # sus.analyzeSHELXE()
