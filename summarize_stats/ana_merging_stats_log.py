import os,sys,math

lines = open("merging_stats.log","r").readlines()

start_flag=False

stats_lines = []
for line in lines:
    if line == "\n":
        continue
    if line.rfind(" d_max  d_min   #obs  #uniq   mult.  %comp       <I>  <I/sI>    r_mrg   r_meas    r_pim   cc1/2   cc_ano")!=-1:
        start_flag = True
        continue
    if line.rfind("References:") !=-1:
        break
    if start_flag == True:
        stats_lines.append(line)

if start_flag == False:
    print("data statistics table does not exist")

def get_params(line):
    cols=line.split()
    d_max=float(cols[0])
    d_min=float(cols[1])
    mult=float(cols[4])
    comp=float(cols[5])
    isigi=float(cols[7])
    r_mrg=float(cols[8])
    r_mes=float(cols[9])
    r_pim=float(cols[10])
    cc_half=float(cols[11])
    cc_ano=float(cols[12])

    return d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano

for index,line in enumerate(stats_lines):
    # d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano
    if index==0:
        params = get_params(line)
        print("inner shell", params)
    if index==len(stats_lines)-1:
        print "LINE=",line
        params = get_params(line)
        print("overall ", params)
    if index==len(stats_lines)-2:
        print "LINE=",line
        params = get_params(line)
        print("outer shell ", params)

##
## d_max  d_min   #obs  #uniq   mult.  %comp       <I>  <I/sI>    r_mrg   r_meas    r_pim   cc1/2   cc_ano
##  38.66   2.65 579209   3686   157.14  99.89  100510.1   101.7    0.076    0.076    0.006   1.000   0.566

