import pandas as pd
import sys
import os

run1 = pd.read_csv(sys.argv[1])
run2 = pd.read_csv(sys.argv[2])
run3 = pd.read_csv(sys.argv[3])

# Merging all of data frames
run_all=pd.concat([run1,run2,run3])
