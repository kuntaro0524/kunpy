import os, sys
import pandas as pd
import copy

class PhenixMergingStats():

    def __init__(self, phenix_log):
        self.phenix_log = phenix_log
        self.isdebug = False

    def readLog(self):
        self.lines = open(self.phenix_log, "r").readlines()

        extracted_txts=[]
        table_flag = False
        table_index=0
        self.shells_info=[]

        for line in self.lines:
            if "Statistics by resolution bin:" in line:
                table_flag = True
                continue
            if table_flag == True and "References" in line:
                break
            table_dic = {}
            if table_flag:
                cols=line.split()
                if(len(cols) == 13):
                    if table_index == 0:
                        label_indices=cols
                    else:
                        for col_index, col in enumerate(cols):
                            table_dic[label_indices[col_index]] = float(col)
                        self.shells_info.append(table_dic)

                    table_index += 1

        # inner/outer shells
        inner_shell = self.shells_info[0]
        indf = pd.DataFrame(inner_shell.values(), index=inner_shell.keys()).T
        # print(indf)
        # outer shells
        outer_shell = self.shells_info[-2]
        # print(outer_shell)
        outf = pd.DataFrame(outer_shell.values(), index=outer_shell.keys()).T
        # print(outf)
        # Over all stats
        all_shell = self.shells_info[-1]
        # print(all_shell)
        alldf = pd.DataFrame(all_shell.values(), index=all_shell.keys()).T
        # print(alldf)
        # Merged data frame
        merged_df = pd.concat([indf, outf, alldf])
        #print(merged_df)

        # DataFrame includes 'inner_shell' 'outer_shell' 'over_all' statistics
        return merged_df

    """
    d_max
    d_min
    # obs      1789930.000
    # uniq       30733.000
    mult.
    % comp
    < I > 144547.100
    < I / sI > 12.600
    r_mrg
    r_meas
    r_pim
    cc1 / 2
    cc_ano
    """

    def makeDataSeriesLog(self):
        merged_df = self.readLog()

        index=0
        mydict = {}
        for column_name, ser in merged_df.iterrows():
            #print("TYPE", type(ser))
            if index==0:
                c1=ser.add_prefix('p_')
                c2=c1.add_suffix('_in')
                df = copy.deepcopy(c2)

            if index==1:
                print("processing outer shell")
                c1=ser.add_prefix('p_')
                c2=c1.add_suffix('_out')
                df = pd.concat([df, c2])

            if index == 2:
                print("processing overall shell")
                c1 = ser.add_prefix('p_')
                c2 = c1.add_suffix('_all')
                df = pd.concat([df, c2])

            index+=1

        if self.isdebug:
            print("========== makeDataSeriesLog ============")
            print(df.shape)
            print(df)
            print("========== makeDataSeriesLog ============")

        return df.T

        # print(final_series)

if __name__ == "__main__":
    pms = PhenixMergingStats(sys.argv[1])
    # pms.readLog()
    pms.makeDataSeriesLog()
