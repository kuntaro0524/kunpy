import sys, glob, os
sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/Libs/")
import AnaXSCALE
import PhenixMergingStats
import pandas as pd

if __name__ == "__main__":
    # logging check
    import logging, logging.config
    logname = "summarize_xscale_phenix_stats.log"
    configfile = "/isilon/users/target/target/Staff/rooms/kuntaro/Libs/logging.conf"
    logging.config.fileConfig(configfile, defaults={'logfile_name': logname})
    logger=logging.getLogger("BL32XU")

    # data processing path
    proc_path = sys.argv[1]

    lpfile=os.path.join(proc_path,"XSCALE.LP")
    mgfile=os.path.join(proc_path,"merging_stats.log")

    logger.info("%s\n%s\n " % (lpfile, mgfile))

    if os.path.exists(lpfile)==False:
        logger.info("%s does not exist" % lpfile)
        sys.exit()
    if os.path.exists(mgfile)==False:
        logger.info("%s does not exist" % mgfile)
        sys.exit()

    logger.info("AnaXSCALE starts")
    ac = AnaXSCALE.AnaXSCALE(lpfile)
    s1 = ac.makeStatsSeries()
    logger.info("AnaXSCALE %5d" % len(s1))

    logger.info("Merging stats.")
    pms = PhenixMergingStats.PhenixMergingStats(mgfile)
    s2 = pms.makeDataSeriesLog()
    logger.info("Merging %5d" % len(s2))

    logger.info("Merge two lines")
    s1["logname"] = lpfile
    s12 = pd.concat([s1,s2])
    logger.info("Merged data length= %5d" % len(s12))
    logger.info("Merged dataframe (XSCALE+phenix.merging...%s" % type(s12))

    d1 = pd.DataFrame([s12])
    # CSV file name
    csv_name = os.path.join(proc_path, "xscale_pms.csv")
    d1.to_csv(csv_name, index=False)
