import pandas as pd
import sys, os
from Libs import AnaXSCALE

###########################

def read_phenix_merging_stats(filename='merging_stats.log'):
    lines = open(filename,"r").readlines()
    start_flag=False

    stats_lines = []
    for line in lines:
        if line == "\n":
            continue
        if line.rfind(" d_max  d_min   #obs  #uniq   mult.  %comp       <I>  <I/sI>    r_mrg   r_meas    r_pim   cc1/2   cc_ano")!=-1:
            start_flag = True
            continue
        if line.rfind("References:") !=-1:
            break
        if start_flag == True:
            stats_lines.append(line)

    if start_flag == False:
        print("data statistics table does not exist")

    def get_params(line):
        cols=line.split()
        d_max=float(cols[0])
        d_min=float(cols[1])
        mult=float(cols[4])
        comp=float(cols[5])
        isigi=float(cols[7])
        r_mrg=float(cols[8])
        r_mes=float(cols[9])
        r_pim=float(cols[10])
        cc_half=float(cols[11])
        cc_ano=float(cols[12])

        return d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano

    def csv_string(params, shelltype):
        d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano = params
        param_names = ["dmax", "dmin", "redun", "compl", "isigi", "r_mrg", "r_mes", "r_pim", "cc_half", "cc_ano"]

        param_list = []
        name_list = []
        for param, param_name in zip(params, param_names):
            param_list.append(param)
            name_list.append("%s_%s"% (shelltype, param_name))
        return name_list, param_list

    param_list = []
    names_list = []
    for index,line in enumerate(stats_lines):
        # d_max, d_min, mult, comp, isigi, r_mrg, r_mes, r_pim, cc_half, cc_ano
        if index==0:
            params = get_params(line)
            names, params = csv_string(params, "phenix_inner")
            param_list += params
            names_list += names
        if index==len(stats_lines)-1:
            params = get_params(line)
            names, params = csv_string(params, "phenix_overall")
            params = get_params(line)
            param_list += params
            names_list += names
        if index==len(stats_lines)-2:
            params = get_params(line)
            names, params = csv_string(params, "phenix_outer")
            param_list += params
            names_list += names

    # print("Params=", param_list)
    # print("Names=", names_list)

    return names_list, param_list

# XSCALE.LP will be converted to CSV file.
ac = AnaXSCALE.AnaXSCALE(sys.argv[1])
ac.makeStatsCSV(sys.argv[2])

#XSCALE.CSV should be read
run1 = pd.read_csv(sys.argv[2])
run1.dropna()

# File convertion from mapcc.dat file
mapccfile = "./mapcc.dat"
mapcc=None
region_mapcc=None
if os.path.exists(mapccfile) == False:
    print("mapcc.dat was not found.")
else:
    lines = open(mapccfile, "r").readlines()
    for line in lines:
        if line.rfind("Correlation")!=-1:
            cols = line.split()
            region_cc = float(cols[5])
            mapcc = float(cols[7])
            break

# insert CC values to the pandas dataframe.
run1['mapcc'] = mapcc
run1['region_cc'] = region_cc

# File convertion from mapcc.dat file
isobfile = "./isob.dat"
wilson_b = None
if os.path.exists(isobfile) == False:
    print("isob.dat file was not detected.")
else:
    lines = open(isobfile, "r").readlines()
    for line in lines:
        if line.rfind("Least")!=-1:
            cols = line.split()
            wilson_b = float(cols[7])
            break

run1['wilsonb'] = wilson_b
print(run1.head())

# Reading a log file from phenix.merging_stats
name_list, param_list = read_phenix_merging_stats()

# PHENIX merging statistics data
phenix_csv = "phenix_stats.csv"
po = open(phenix_csv, "w")

for colname in name_list:
    po.write("%s," % colname)
po.write("\n")
for param in param_list:
    po.write("%s," % param)
po.close()

phenix_stats_info = pd.read_csv(phenix_csv)
print(run1, phenix_stats_info)
new_df=run1.join(phenix_stats_info)

print(new_df)

new_df.to_csv("qqqq.csv")