import sys, glob, os
sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/Libs/")
import AnaXSCALE
import PhenixMergingStats
import pandas as pd

if __name__ == "__main__":

    # logging check
    import logging, logging.config
    logname = "summarize_xscale_phenix_stats.log"
    configfile = "/isilon/users/target/target/Staff/rooms/kuntaro/Libs/logging.conf"
    logging.config.fileConfig(configfile, defaults={'logfile_name': logname})
    logger=logging.getLogger("BL32XU")

    xscalelp_list = glob.glob("*/XSCALE.LP")

    proc_dirs=[]
    for x in xscalelp_list:
        procdir=x.replace("XSCALE.LP","")
        proc_dirs.append(procdir)

    file_index=0
    for proc_dir in proc_dirs:
        lpfile=os.path.join(proc_dir,"XSCALE.LP")
        mgfile=os.path.join(proc_dir,"merging_stats.log")

        logger.info("%s\n%s\n " % (lpfile, mgfile))

        if os.path.exists(lpfile)==False:
            logger.info("%s does not exist" % lpfile)
            continue
        if os.path.exists(mgfile)==False:
            logger.info("%s does not exist" % mgfile)
            continue

        logger.info("AnaXSCALE starts")
        ac = AnaXSCALE.AnaXSCALE(lpfile)
        s1 = ac.makeStatsSeries()
        logger.info("AnaXSCALE %5d" % len(s1))

        logger.info("Merging stats.")
        pms = PhenixMergingStats.PhenixMergingStats(mgfile)
        s2 = pms.makeDataSeriesLog()
        logger.info("Merging %5d" % len(s2))

        logger.info("Merge two lines")
        s1["logname"] = lpfile
        s12 = pd.concat([s1,s2])
        logger.info("Merged data length= %5d" % len(s12))

        logger.info("Merged dataframe (XSCALE+phenix.merging...%s" % type(s12))
        d1 = pd.DataFrame([s12])

        if file_index==0:
            logger.info("Initial data frame")
            df=d1
        else:
            logger.info("Connecting data frames")
            df = pd.concat([df, d1])
        file_index += 1

    print(df)
    df.to_csv("xscale_pms_summary.csv",index=False)
