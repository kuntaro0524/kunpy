import sys, glob, os
sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/Libs/")
import AnaXSCALE
import PhenixMergingStats
import pandas as pd

if __name__ == "__main__":

    # logging check
    import logging, logging.config
    logname = "summarize_xscale_phenix_stats.log"
    configfile = "/isilon/users/target/target/Staff/rooms/kuntaro/Libs/logging.conf"
    logging.config.fileConfig(configfile, defaults={'logfile_name': logname})
    logger=logging.getLogger("BL32XU")

    xscalelp_list = glob.glob("*/XSCALE.LP")

    proc_dirs=[]
    for x in xscalelp_list:
        procdir=x.replace("XSCALE.LP","")
        proc_dirs.append(procdir)

    # For merging data frames
    file_index=0
    # Loop for each directory
    for proc_dir in proc_dirs:
        # Generating expected files
        lpfile=os.path.join(proc_dir,"XSCALE.LP")
        mgfile=os.path.join(proc_dir,"merging_stats.log")

        logger.info("%s\n%s\n " % (lpfile, mgfile))

        if os.path.exists(lpfile)==False:
            logger.info("%s does not exist" % lpfile)
            continue
        if os.path.exists(mgfile)==False:
            logger.info("%s does not exist" % mgfile)
            continue

        logger.info("AnaXSCALE starts")
        ac = AnaXSCALE.AnaXSCALE(lpfile)
        s1 = ac.makeStatsSeries()
        logger.info("AnaXSCALE %5d" % len(s1))

        logger.info("Merging stats.")
        pms = PhenixMergingStats.PhenixMergingStats(mgfile)
        s2 = pms.makeDataSeriesLog()
        logger.info("Merging %5d" % len(s2))

        logger.info("Merge two lines")
        s1["logname"] = lpfile
        s12 = pd.concat([s1,s2])
        logger.info("Merged data length= %5d" % len(s12))

        logger.info("Merged dataframe (XSCALE+phenix.merging...%s" % type(s12))
        d1 = pd.DataFrame([s12])

        if file_index==0:
            logger.info("Initial data frame")
            df=d1
        else:
            logger.info("Connecting data frames")
            df = pd.concat([df, d1])
        file_index += 1

    print(df)

    # Normally used function
    sel01 = df['p_<I/sI>_out']>1.0
    normal_df = df[sel01]

    if len(df)==0:
        logger.info("No datasets with larger <I/sigI> at outer shell than 1.0")
    else:
        normal_df=normal_df.sort_values('overall_redun', ascending=False)
        normal_df.to_csv("redundant_data.csv")

    # outer shell <I/sigI> > 1.0
    # Rmeas & Rmerge should not be "minus" values
    sel01 = df['p_<I/sI>_out']>1.0
    sel02 = df['p_r_meas_out']>0.0
    sel03 = df['p_r_mrg_out']>0.0
    clean_df = df[sel01&sel02&sel03]

    if len(df)==0:
        logger.info("No datasets with 'clean' protocol")
    else:
        clean_df = clean_df.sort_values('overall_redun', ascending=False)
        clean_df.to_csv("clean_publication_data.csv")

    # 75 percentile 'overall_redun'
    redun_thresh = df['overall_redun'].quantile(0.75)
    sel01 = df['overall_redun']>redun_thresh

    # 25 percentile 'p_r_pim_out'
    rpim_thresh = df['p_r_pim_out'].quantile(0.25)
    sel02 = df['p_r_pim_out'] < rpim_thresh
    reduced_redun_df = df[sel01 & sel02]

    if len(df)==0:
        logger.info("No datasets with 'percentile'(Rpim, overall_redun) protocol")
    else:
        reduced_redun_df = reduced_redun_df.sort_values('overall_redun', ascending=False)
        reduced_redun_df.to_csv("cosmetic_data.csv")
