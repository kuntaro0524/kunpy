import glob,sys
import pandas as pd
import SummarizeSADstats 

if __name__ == "__main__":
    print(sys.argv)

    sus = SummarizeSADstats.SummarizeSADstats(sys.argv[1])
    df=sus.read_phenix_merging_stats(return_dataFrame=True)
    print(df)
