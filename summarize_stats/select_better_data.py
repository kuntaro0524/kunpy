#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import sys
from matplotlib import pyplot as plt
import numpy as np
#get_ipython().run_line_magic('matplotlib', 'inline')

df = pd.read_csv(sys.argv[1])

# ### <I/sigI> in phenix.merging_statistics 
df['overall_redun'].quantile([0.0, 0.25, 0.5, 0.75, 1.00])

# Statistical threshold of 'redundancy'
half_quantile = df['overall_redun'].quantile(0.75)

# <I/sigI> should be larger than 1.0
sel01 = df['p_<I/sI>_out'] >= 1.0
sel02 = df['overall_redun'] > half_quantile
d01=df[sel01&sel02]

# selected 
d01=d01.sort_values(['p_<I/sI>_out', 'p_r_pim_out'], ascending=[False,True])
d01.to_csv("half_quantile.csv")

#d01.plot.scatter(x='p_<I/sI>_out', y='overall_redun')

if len(d01) > 0:
    print(d01['logname'])

half_cut_rpim=d01['p_r_pim_out'].quantile(0.75)
print("Half cut Rpim= %8.5f" % half_cut_rpim)
sel03 = df['p_r_pim_out'] > half_cut_rpim

d02=d01[sel03]

if len(d02) > 0:
    print(d02['logname'])
else:
    print("length is none!")
    sys.exit()

# This is the most frequent processing
sel01 = df['p_<I/sI>_out'] > 1.0
d02 = df[sel01]
d02=d02.sort_values('overall_redun', ascending=False)
d02['logname']
d02.to_csv("sorted.csv")
