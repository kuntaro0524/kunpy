# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt, patches
 
# サンプル円データを生成
def generate_circle_points(a, b, r, dx):
    # 原点(0, 0)の円を用意
    x = np.arange(-r, r, dx)
    yp = np.sqrt(r ** 2 - x ** 2)
    ym = -yp
    xi = np.concatenate([x, x])
    yi = np.concatenate([yp, ym])
 
    # a, bでオフセットする
    xi += a
    yi += b
 
    return xi, yi
 
# 円の方程式でカーブフィットする関数
def circle_fitting(xi, yi):
    M = np.array([[np.sum(xi ** 2), np.sum(xi * yi), np.sum(xi)],
                  [np.sum(xi * yi), np.sum(yi ** 2), np.sum(yi)],
                  [np.sum(xi), np.sum(yi), 1*len(xi)]])
    Y = np.array([[-np.sum(xi ** 3 + xi * yi ** 2)],
                  [-np.sum(xi ** 2 * yi + yi ** 3)],
                  [-np.sum(xi ** 2 + yi ** 2)]])
 
    M_inv = np.linalg.inv(M)
    X = np.dot(M_inv, Y)
    a = - X[0] / 2
    b = - X[1] / 2
    r = np.sqrt((a ** 2) + (b ** 2) - X[2])
    return a, b, r
 
# サンプルデータ作成
xi, yi = generate_circle_points(0, 0, 1, 0.1)
noise = np.random.normal(loc=0, scale=0.05, size=len(xi))
xi += noise
yi += noise


x_csv=np.array([1334,1072,1536,2027,1887])
y_csv=np.array([1113,1375,2119,1816,1187])
 
# サンプルデータに対してカーブフィット
a, b, r = circle_fitting(x_csv, y_csv)
print('a=', a, 'b=', b, 'r=', r)
 
# ここからグラフ描画
# フォントの種類とサイズを設定する。
plt.rcParams['font.size'] = 14
plt.rcParams['font.family'] = 'Times New Roman'
 
# 目盛を内側にする。
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
 
# グラフの上下左右に目盛線を付ける。
fig = plt.figure(figsize=(5, 5))
ax1 = fig.add_subplot(111)
ax1.yaxis.set_ticks_position('both')
ax1.xaxis.set_ticks_position('both')
 
# 軸のラベルを設定する。
ax1.set_xlabel('x')
ax1.set_ylabel('y')
 
# データプロットの準備。
ax1.scatter(x_csv, y_csv, label='dataset')
circle = patches.Circle( (a, b), r, facecolor='None', linestyle='--', edgecolor="red", label="circle fitting")
 
ax1.add_patch(circle)
ax1.legend()
 
# グラフを表示する。
fig.tight_layout()
plt.show()
plt.close()
