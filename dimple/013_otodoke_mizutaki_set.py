# 013_otodoke_mizutaki_set.py
# This script will pick up some dimple output and mizutaki.gif
# Use this script in zoo root directory

# Log
# 2020/06/XX ver 0.00 test
# 2020/06/18 ver 0.01 modified to pick up merged dataset

import os
import sys
import glob

html_large_wedge = 'mizutaki_party.html'
html_merged = 'mizutaki_party_merged.html'

html_simple_large_wedge = 'mizutaki_view.html'
html_simple_merged = 'mizutaki_view_merged.html'

arc_name = 'mizutaki_set_200618.tar.gz'

# if archive merged dataset, set True
add_merged = False 

current_dir = os.getcwd()
arc_lst_file = '%s/arc.lst' % current_dir

arc_lst = []


########### Large wedge #################
dimple_lst_large_wedge = glob.glob('./_kamoproc/*/*/*/dimple')
mizutaki_lst_large_wedge = glob.glob('./_kamoproc/*/*/*/dimple/mizutaki/*/mizutaki.gif')
mizutaki_html_large_wedge = glob.glob('./_kamoproc/*/*/*/dimple/mizutaki/mizutaki_*.html')

# add dimple files (final.pdb, final.mtz, dimple.log)
for path in dimple_lst_large_wedge:
    dimple_path = path
    final_pdb_path = os.path.join(dimple_path, 'final.pdb')
    arc_lst.append(final_pdb_path)
    final_mtz_path = os.path.join(dimple_path, 'final.mtz')
    arc_lst.append(final_mtz_path)
    dimple_log_path = os.path.join(dimple_path, 'dimple.log')
    arc_lst.append(dimple_log_path)

# add mizutaki.gif in each data directory
for path in mizutaki_lst_large_wedge:
    arc_lst.append(path)

# add mizutaki.html in each data directory
for path in mizutaki_html_large_wedge:
    arc_lst.append(path)



########### Merged #################
if add_merged == True:
    dimple_lst_merged = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple')
    mizutaki_lst_merged = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple/mizutaki/*/mizutaki.gif')
    mizutaki_html_merged = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple/mizutaki/mizutaki_*.html')


    # add dimple files (final.pdb, final.mtz, dimple.log)
    for path in dimple_lst_merged:
        dimple_path = path 
        final_pdb_path = os.path.join(dimple_path, 'final.pdb')
        arc_lst.append(final_pdb_path)
        final_mtz_path = os.path.join(dimple_path, 'final.mtz')
        arc_lst.append(final_mtz_path)
        dimple_log_path = os.path.join(dimple_path, 'dimple.log')
        arc_lst.append(dimple_log_path)

    # add mizutaki.gif in each data directory
    for path in mizutaki_lst_merged:
        arc_lst.append(path)

    # add mizutaki.html in each data directory
    for path in mizutaki_html_merged:
        arc_lst.append(path)


    arc_lst.append(html_merged)
    arc_lst.append(html_simple_merged)


arc_lst.append(html_large_wedge)
arc_lst.append(html_simple_large_wedge)
            

ofile = open(arc_lst_file, 'w')
for arc_target in arc_lst:
    ofile.write("%s\n" % arc_target)
ofile.close()

# Execute archive
command = "tar cvfz %s --files-from=%s" % (arc_name, arc_lst_file)
print "COMMAND=%s" % command
os.system(command)
