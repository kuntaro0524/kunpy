# 018_enjoy_mizutaki_party_merged.py
# 
# This script will prepare two types of html.
# first, in each dimple directory, an html file
# with all GIF anime around ROI is prepared in each mizutaki directroy
# second, summarized html (only puck_pin ID and link are listed) is 
# prepared in zoo root directory

# Log
# 2020/06/18 ver 0.00 make mizutaki_party.html for merged dataset
# 2020/06/18 ver 0.01 prep functions for each step

# TODO: prepare options to process data in large wedge merged dataset(#1 2020/06/18)

import glob
import os

# dimple dir for large wedge: './_kamoproc/(puck-pin-id)/(data)/(collect-mode-data)/dimple'
# dimple dir for large_wedge_merge: './_kamoproc/merge_*/*_final/cluster_*/run_*/dimple'
# dimple dir for multi_merge: './_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple'

current_dir = os.getcwd()

# get dimple_dir() will get the list of dimple directory
# if

flag_merge = False

# html header information (common)
header_info_common = '''
<!DOCTYPE html>
 <html>
 <head>
 <meta charset="UTF-8">

 <style>
 .dataset_table {
     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
     width: 100%%;
     border-collapse: collapse;
 }

 .dataset_table td, .dataset_table th {
     font-size: 1em;
     border: 1px solid #98bf21;
     text-align: center;
     padding: 3px 7px 2px 7px;
 }

 .dataset_table th {
     font-size: 1.1em;
     text-align: center;
     padding-top: 5px;
     padding-bottom: 4px;
     background-color: #A7C942;
     color: #ffffff;
 }

 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
 }
 .img {
     width: 300px;
     height: 300px;
     object-fit: cover;
 }
 
</style>
'''

def get_dimple_dir_list():
    """
    make dimple list (for large wedge single data and merged data)
    :return: dimple_dir_lst, dimple_dir_lst_merged
    """

    lst_large = glob.glob('./_kamoproc/*/*/*/dimple')
    lst_large = sorted(lst_large)

    lst_multi_merge = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple')
    if len(lst_multi_merge) != 0:
        lst_multi_merge = sorted(lst_multi_merge)
    
    return lst_large, lst_multi_merge


def prep_mizutaki_html(lst, merged=False):
    """
    prepare mizutaki.html in each mizutaki directory
    :param lst: dimple directory list to make mizutaki.html in each mizutaki directory 
    :param merged: True if prep mizutaki.html for merged data 
    :return: 
    """
    
    current_dir = os.getcwd()
    
    # prep mizutaki_XXX.html in each mizutaki direcotry for merged dataset
    if merged:
        head_merged = """
        <h4> Results </h4>
<table class="dataset_table">
<tr>
<th> Merge_name </th>
<th> ROI  </th>
<th> GIF anime </th>
</tr>
        """
    
        for dimple_dir in lst:
            mizutaki_dir = os.path.join(dimple_dir, 'mizutaki')
            merged_name = dimple_dir.split('/')[2:6]
            
            html_str = header_info_common
            html_str += head_merged
            if os.path.exists(mizutaki_dir):
                os.chdir(mizutaki_dir)
                mizutaki_lst = glob.glob('*/mizutaki.gif')
                mizutaki_lst = sorted(mizutaki_lst)
                
                html_out = 'mizutaki_' + merged_name + '.html'
                fo = open(html_out, 'w')
                
                tmp_dir = os.getcwd()
                for i in mizutaki_lst:
                    tmp_path = os.path.join(tmp_dir, i)
                    ROI = tmp_path.split('/')[-2]
                    mizutaki_rel_path = os.path.relpath(tmp_path, tmp_dir)
                    gif_anime = '<a href="' + mizutaki_rel_path + '"><img src="' + mizutaki_rel_path + '"></a/>'
                    html_str += """
<tr><td>%s</td><td>%s</td><td>%s</td></tr>
                    """ % (merged_name, ROI, gif_anime)

                fo.write(html_str)
                fo.close()

                os.chdir(current_dir)

    # prep mizutaki_XXX.html in each mizutaki directory (for large wedge dataset)
    else:
        head_normal = """
                <h4> Results </h4>
<table class="dataset_table">
<tr>
<th> Puck_Pin_ID </th>
<th> ROI  </th>
<th> GIF anime </th>
</tr>
"""        
        for dimple_dir in lst:
            mizutaki_dir = os.path.join(dimple_dir, 'mizutaki')
            puck_pin_ID = dimple_dir.split('/')[2]

            html_str = header_info_common
            html_str += head_normal
            if os.path.exists(mizutaki_dir):
                os.chdir(mizutaki_dir)
                mizutaki_lst = glob.glob('*/mizutaki.gif')
                mizutaki_lst = sorted(mizutaki_lst)

                html_out = 'mizutaki_' + puck_pin_ID + '.html'
                fo = open(html_out, 'w')

                tmp_dir = os.getcwd()
                for i in mizutaki_lst:
                    tmp_path = os.path.join(tmp_dir, i)
                    ROI = tmp_path.split('/')[-2]
                    mizutaki_rel_path = os.path.relpath(tmp_path, tmp_dir)
                    gif_anime = '<a href="' + mizutaki_rel_path + '"><img src="' + mizutaki_rel_path + '"></a/>'
                    html_str += """
        <tr><td>%s</td><td>%s</td><td>%s</td></tr>
                            """ % (puck_pin_ID, ROI, gif_anime)

                fo.write(html_str)
                fo.close()

                os.chdir(current_dir)
        

# make mizutaki_party.html (summarized html file)
def prep_mizutaki_party_html(merged=False):
    """
    prep mizutaki_party.html
    :param merged: True if prepare mizutaki_party_merged.html (for merged dataset)
    :return: 
    """
    current_dir = os.getcwd()
    if merged:
        fs = open('mizutaki_party_merged.html', 'w')
        html_str = header_info_common
        html_str += """
        <h4> Results </h4>

        <table class="dataset_table">
        <tr>
        <th> Merge_name </th>
        <th> Link </th>
        </tr>
        """

        mizutaki_html_lst = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple/mizutaki/mizutaki_*.html')
        mizutaki_html_lst = sorted(mizutaki_html_lst)
        for path in mizutaki_html_lst:
            merged_name = '-'.join(path.split('/')[2:6])
            html_link = '<a href="' + path + '">check</a>'
            html_str += """
        <tr><td>%s</td><td>%s</td></tr>
        """ % (merged_name, html_link)

        fs.write(html_str)

        fs.close()
        
    else:
        fs = open('mizutaki_party.html', 'w')
        html_str = header_info_common
        html_str += """
        <h4> Results </h4>

        <table class="dataset_table">
        <tr>
        <th> Puck_Pin_ID </th>
        <th> Link </th>
        </tr>
        """

        mizutaki_html_lst = glob.glob('./_kamoproc/*/*/*/dimple/mizutaki/mizutaki_*.html')
        mizutaki_html_lst = sorted(mizutaki_html_lst)
        for path in mizutaki_html_lst:
            puck_pin_ID = path.split('/')[2]
            html_link = '<a href="' + path + '">check</a>'
            html_str += """
        <tr><td>%s</td><td>%s</td></tr>
        """ % (puck_pin_ID, html_link)

        fs.write(html_str)
        fs.close()

# prep_mizutaki_party_html()

# first, run get_dimple_dir_list()
dimple_dir_lst, dimple_dir_lst_merged = get_dimple_dir_list()
# second, prep_mizutaki_html()
prep_mizutaki_html(lst=dimple_dir_lst)
#prep_mizutaki_html(lst=dimple_dir_lst_merged, merged=True)
# third, prep_mizutaki_party_html()
prep_mizutaki_party_html(merged=False)
#prep_mizutaki_party_html(merged=True)
