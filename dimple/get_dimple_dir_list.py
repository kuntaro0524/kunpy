import os
import sys
import glob


def get_dimple_dir_list():
    '''get dimple dir list
    use this function under zoo root directory
    '''

    dimple_dir_lst = []

    lst_large = glob.glob('./_kamoproc/*/*/*/dimple')
    lst_large = sorted(lst_large)
    dimple_dir_lst.append(lst_large)

    lst_large_merge = glob.glob('./_kamoproc/merge_*/*_final/cluster_*/run_*/dimple')
    if len(lst_large_merge) != 0:
        lst_large_merge = sorted(lst_large_merge)
        dimple_dir_lst.append(lst_large_merge)

    lst_multi_merge = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple')
    if len(lst_multi_merge) != 0:
        lst_multi_merge = sorted(lst_multi_merge)
        dimple_dir_lst.append(lst_multi_merge)

    return dimple_dir_lst, flag_merge


# get_dimple_dir_list()