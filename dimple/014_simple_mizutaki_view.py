# 014 simple_mizutaki_view.py
# 2020/0616 v0.00  test 


import os, sys, math, logging, csv, glob
import Libs.MyException as MyException
import Libs.Xds2Mtz as Xds2Mtz
import Libs.ResolutionFromXscaleHKL as ResolutionFromXscaleHKL
import Libs.ComRefine as ComRefine
import logging.config
import Libs.KstrHTML as KstrHTML

beamline = "BL32XU"

# working directory should be zoo_root directory
# search 'mizutaki.gif' from '_kamoproc' and '_kamo_30deg'
# change search path when the directory structure is changed
# glob.glob will return the list of path for 'mizutaki.gif' 

current_dir = os.getcwd()

# normally in mizutaki.gif stored in '_kamoproc/dataXX/cryXXX/dimple/mizutaki/XXX/'
mizutaki_lst = glob.glob('./_kamoproc/*/*/*/dimple/mizutaki/*/mizutaki.gif')
mizutaki_lst = sorted(mizutaki_lst)
# glob serch path should be modified for merged dataset
#mizutaki_lst_merged = glob.glob('_kamo_30deg/*/*/*/dimple/mizutaki/*/mizutaki.gif')

html_name = 'mizutaki_view.html'

fo = open(html_name, 'w')

header_info = """
<!DOCTYPE html>
 <html>
 <head>
 <meta charset="UTF-8">

 <style>
 .dataset_table {
     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
     width: 100%%;
     border-collapse: collapse;
 }

 .dataset_table td, .dataset_table th {
     font-size: 1em;
     border: 1px solid #98bf21;
     text-align: center;
     padding: 3px 7px 2px 7px;
 }

 .dataset_table th {
     font-size: 1.1em;
     text-align: center;
     padding-top: 5px;
     padding-bottom: 4px;
     background-color: #A7C942;
     color: #ffffff;
 }

 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
 }
 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
 }
</style>
<h4> Results </h4>

<table class="dataset_table">
<tr>
<th> puck_pin </th>
<th> ROI  </th>
<th> GIF anime </th>
</tr>
"""

html_str = header_info

for path in mizutaki_lst:
    #print path
    puck_pin = path.split('/')[2]
    #print puck_pin
    ROI = path.split('/')[-2]
    mizutaki_rel_path = os.path.relpath(path, ".")
    gif_anime = '<a href="' + mizutaki_rel_path + '"><img src="' + mizutaki_rel_path + '"></a>'
    html_str += """
<tr><td>%s</td><td>%s</td><td>%s</td></tr> 
""" % (puck_pin, ROI, gif_anime)

fo.write(html_str)

