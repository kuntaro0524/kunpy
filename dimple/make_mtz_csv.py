import os, sys, math, numpy, scipy, glob
import Unko
import MyDate
import XDSReporter
import DirectoryProc

# Merge directory
# _kamo directories
dp = DirectoryProc.DirectoryProc(".")
merge_related_files = []
arc_files = []
kamodir = sys.argv[1]

archive_index = 0
abs_kamo = os.path.abspath(os.path.join("./", kamodir))
unko = Unko.Unko(abs_kamo)
# Merging directories with '_final' results
okay_dirs = unko.getListOfGoodMergeDirs()

xscale_file_list = []
for ok_d in okay_dirs:
    # Find directories with final merging calculations
    dp = DirectoryProc.DirectoryProc(ok_d)
    xscale_list, path_list = dp.findTarget("xscale.hkl")
    xscale_file_list += xscale_list

for xscale_file in xscale_file_list:
    if xscale_file.rfind("run_03") != -1:
        cols = xscale_file.split("/")
        for col in cols:
            if col.startswith("merge") == True:
                sample_name = col.split("S")[1][1:]
                print "DUMMY,%s,%s" % (sample_name, xscale_file)
