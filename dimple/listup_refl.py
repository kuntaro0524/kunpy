import sys,os
import glob,numpy
import time
import logging
from Libs import Unko
from Libs import ESA
from Libs import DBinfo

def find_puckpin(puck_pin_name):
    puckid, pinid = puck_pin_name.split("-")
    print puckid, pinid

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Usage: python listup_refl.py ZOODBFILE OUT_CSVFILE"
        sys.exit()
    # For _kamproc directory
    zoo_db = sys.argv[1]
    csv_file = sys.argv[2]

    # OUTFILE
    outcsv = open(csv_file,"w")

    # ZOODB file is read
    esa = ESA.ESA(zoo_db)
    conds = esa.getDict()

    # For _kamoproc directory
    unko = Unko.Unko("_kamoproc")

    file_list = unko.makeReportLargeWedge(mtz_prep=False)
    for fname in file_list:
        if fname.rfind("XDS_ASCII.HKL")!=-1:
            cols = fname.split('/')
            puck_pin_name = cols[1]
        else:
            continue

        for cond in conds:
            dbinfo = DBinfo.DBinfo(cond)
            dbinfo.prepParams()
            if puck_pin_name == dbinfo.puck_pin:
                abs_path = os.path.join(os.getcwd(), fname)
                outcsv.write("%s,%s\n" %(dbinfo.sample_name, abs_path))

    # For _kamo_30deg directory
    unko = Unko.Unko("_kamo_30deg")

    # Merging directories with '_final' results
    okay_dirs = unko.getListOfFinishedMergeDirs()
    cluster_dirs = unko.findClusterDirsIn(okay_dirs)
    run_dirs = unko.findRunDirsIn(cluster_dirs)
    file_list = unko.findReflectionFilesIn(run_dirs, file_list = ["xscale.hkl"], isIncludeMTZ=False)

    for f in file_list:
        cols = f.split('/')
        for col in cols:
            if col.rfind("merge") != -1:
                sample_name = col.split("S_")[1]
                abs_path = os.path.join(os.getcwd(), f)
                outcsv.write("%s,%s\n" %(sample_name, abs_path))