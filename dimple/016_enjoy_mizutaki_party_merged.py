# 016_enjoy_mizutaki_party_merged.py
# 
# This script will prepare two types of html.
# first, in each dimple directory, an html file
# with all GIF anime around ROI is prepared in each mizutaki directroy
# second, summarized html (only puck_pin ID and link are listed) is 
# prepared in zoo root directory

# Log
# 2020/06/18 ver 0.00 make mizutaki_party.html for merged dataset
# 2020/06/18 ver 0.01

import glob
import os

# dimple dir for large wedge: './_kamoproc/(puck-pin-id)/(data)/(collect-mode-data)/dimple'
# dimple dir for large_wedge_merge: './_kamoproc/merge_*/*_final/cluster_*/run_*/dimple'
# dimple dir for multi_merge: './_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple'

current_dir = os.getcwd()

# get dimple_dir() will get the list of dimple directory
# if

flag_merge = False

# html header information (common)
header_info_common = '''
<!DOCTYPE html>
 <html>
 <head>
 <meta charset="UTF-8">

 <style>
 .dataset_table {
     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
     width: 100%%;
     border-collapse: collapse;
 }

 .dataset_table td, .dataset_table th {
     font-size: 1em;
     border: 1px solid #98bf21;
     text-align: center;
     padding: 3px 7px 2px 7px;
 }

 .dataset_table th {
     font-size: 1.1em;
     text-align: center;
     padding-top: 5px;
     padding-bottom: 4px;
     background-color: #A7C942;
     color: #ffffff;
 }

 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
 }
 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;

     background-color: #EAF2D3;
 }
</style>
'''


def get_dimple_dir_list(mode='large_wedge'):
    """get dimple dir list
    use this function under zoo root directory
    """

    dimple_dir_lst = []

    if mode == 'large_wedge':
        lst_large = glob.glob('./_kamoproc/*/*/*/dimple')
        lst_large = sorted(lst_large)
        dimple_dir_lst.append(lst_large)
    elif mode == 'large_wedge_merge':
        lst_large_merge = glob.glob('./_kamoproc/merge_*/*_final/cluster_*/run_*/dimple')
        if len(lst_large_merge) != 0:
            lst_large_merge = sorted(lst_large_merge)
            dimple_dir_lst.append(lst_large_merge)

    elif mode == 'multi_merge':
        lst_multi_merge = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple')
        if len(lst_multi_merge) != 0:
            lst_multi_merge = sorted(lst_multi_merge)
            dimple_dir_lst.append(lst_multi_merge)

    return dimple_dir_lst, flag_merge


# get_dimple_dir_list()


def prep_mizutaki_html(dimple_dir_lst, flag_merge=False):
    html_str = header_info_common

    dimple_dir_lst, flag_merge = get_dimple_dir_list()
    # prep mizutaki_XXX.html in each mizutaki direcotry
    if flag_merge:
        for i in dimple_dir_lst:
             = os.path.join(current_dir, i.lstrip('./'))
            merged_name = '-'.join(i.split('/')[2:6])
    else:
        for i in dimple_dir_lst:

    mizutaki_dir = os.path.join(dimple_dir, 'mizutaki')
    if os.path.exists(mizutaki_dir):
        
        os.chdir(mizutaki_dir)
        mizutaki_lst = glob.glob('*/mizutaki.gif')
        mizutaki_lst = sorted(mizutaki_lst)
        html_out = 'mizutaki_' + sample_name + '.html'
        fo = open(html_out, 'w')
        
        header_info = header_info_common
        header_info += """
<h4> Results </h4>

<table class="dataset_table">
<tr>
<th> merge_name </th>
<th> ROI  </th>
<th> GIF anime </th>
</tr>
"""
        html_str = header_info


        tmp_dir = os.getcwd()
        for i in mizutaki_lst:
            tmp_path = os.path.join(tmp_dir, i)
            ROI = tmp_path.split('/')[-2]
            mizutaki_rel_path = os.path.relpath(tmp_path, tmp_dir)
            gif_anime = '<a href="' + mizutaki_rel_path + '"><img src="' + mizutaki_rel_path + '"></a>'
            html_str += """
<tr><td>%s</td><td>%s</td><td>%s</td></tr>
""" % (merged_name, ROI, gif_anime)
        fo.write(html_str)
        fo.close()

        os.chdir(current_dir)


# make mizutaki_party.html (summarized html file)
os.chdir(current_dir)

fs = open('mizutaki_party_merged.html', 'w')
header_info_summary_html = header_info_common
header_info_summary_html += """
<h4> Results </h4>

<table class="dataset_table">
<tr>
<th> merge_name </th>
<th> link </th>
</tr>
"""

html_str_summary_html = header_info_summary_html

mizutaki_html_lst = glob.glob('./_kamo_30deg/merge_*/*_final/cluster_*/run_*/dimple/mizutaki/mizutaki_*.html')
mizutaki_html_lst = sorted(mizutaki_html_lst)
for path in mizutaki_html_lst:
    merged_name = '-'.join(path.split('/')[2:6])
    html_link = '<a href="' + path + '">check</a>'
    html_str += """
<tr><td>%s</td><td>%s</td></tr>
""" % (merged_name, html_link)

fs.write(html_str)

fs.close()
