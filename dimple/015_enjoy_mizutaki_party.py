# 015_enjoy_mizutaki_party.py
# 2020/06/16 ver 0.00 test
# This script will prepare two types of html.
# first, in each dimple directory, an html file
# with all GIF anime around ROI is prepared in each mizutaki directroy
# second, summarized html (only puck_pin ID and link are listed) is 
# prepared in zoo root directory


import glob
import os


current_dir = os.getcwd()
puck_pin_lst = glob.glob('./_kamoproc/*/*/*/dimple')

for i in puck_pin_lst:
    dimple_dir = os.path.join(current_dir, i.lstrip('./'))
    puck_pin_ID = i.split('/')[-4] 

    mizutaki_dir = os.path.join(dimple_dir, 'mizutaki')
    if os.path.exists(mizutaki_dir):
        
        os.chdir(mizutaki_dir)
        mizutaki_lst = glob.glob('*/mizutaki.gif')
        mizutaki_lst = sorted(mizutaki_lst)
        html_out = 'mizutaki_' + puck_pin_ID + '.html'
        fo = open(html_out, 'w')
        
        header_info = """
<!DOCTYPE html>
 <html>
 <head>
 <meta charset="UTF-8">

 <style>
 .dataset_table {
     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
     width: 100%%;
     border-collapse: collapse;
 }

 .dataset_table td, .dataset_table th {
     font-size: 1em;
     border: 1px solid #98bf21;
     text-align: center;
     padding: 3px 7px 2px 7px;
 }

 .dataset_table th {
     font-size: 1.1em;
     text-align: center;
     padding-top: 5px;
     padding-bottom: 4px;
     background-color: #A7C942;
     color: #ffffff;
 }

 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
 }
 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
        
     background-color: #EAF2D3;
 }
</style>
<h4> Results </h4>

<table class="dataset_table">
<tr>
<th> puck_pin </th>
<th> ROI  </th>
<th> GIF anime </th>
</tr>
"""
        html_str = header_info


        tmp_dir = os.getcwd()
        for i in mizutaki_lst:
            tmp_path = os.path.join(tmp_dir, i)
            ROI = tmp_path.split('/')[-2]
            mizutaki_rel_path = os.path.relpath(tmp_path, tmp_dir)
            gif_anime = '<a href="' + mizutaki_rel_path + '"><img src="' + mizutaki_rel_path + '"></a>'
            html_str += """
<tr><td>%s</td><td>%s</td><td>%s</td></tr>
""" % (puck_pin_ID, ROI, gif_anime)
        fo.write(html_str)
        fo.close()


        os.chdir(current_dir)


# make mizutaki_party.html
os.chdir(current_dir)

fs = open('mizutaki_party.html', 'w')
header_info_sum = """
<!DOCTYPE html>
 <html>
 <head>
 <meta charset="UTF-8">

 <style>
 .dataset_table {
     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
     width: 100%%;
     border-collapse: collapse;
 }

 .dataset_table td, .dataset_table th {
     font-size: 1em;
     border: 1px solid #98bf21;
     text-align: center;
     padding: 3px 7px 2px 7px;
 }

 .dataset_table th {
     font-size: 1.1em;
     text-align: center;
     padding-top: 5px;
     padding-bottom: 4px;
     background-color: #A7C942;
     color: #ffffff;
 }

 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
 }
 .dataset_table tr.alt td {
     color: #000000;
     background-color: #EAF2D3;
 }
</style>
<h4> Results </h4>

<table class="dataset_table">
<tr>
<th> puck_pin </th>
<th> link </th>
</tr>
"""

html_str_sum = header_info_sum

mizutaki_html_lst = glob.glob('./_kamoproc/*/*/*/dimple/mizutaki/mizutaki_*.html')
mizutaki_html_lst = sorted(mizutaki_html_lst)
for path in mizutaki_html_lst:
    puck_pin = path.split('/')[2]
    html_link = '<a href="' + path + '">check</a>'
    html_str_sum += """
<tr><td>%s</td><td>%s</td></tr>
""" % (puck_pin, html_link)

fs.write(html_str_sum)

fs.close()
