import Toilet, sys, optparse

if __name__ == "__main__":
    parser = optparse.OptionParser()

    report_prefix = sys.argv[1]

    #parser.add_option( "--unmerged_hkl", action='store_true',
    parser.add_option("-u","--unmerged_hkl", action='store_true',
        help = "option to choose mode to archive 'unmerged hkl' files instead of MTZ file",
        default=False)

    # Archive with crystal information(Captured images and SHIKA report html)
    parser.add_option("-c","--with_crystal_info", action='store_true',
        help = "option to choose whether crystal information is included or not.",
        default=False)

    # Beamline
    parser.add_option(
        "-b","--beamline",
        choices=["BL32XU","BL45XU","BL41XU"],
        help="type of input method",
        #dest="beamline_name", required=True)
        dest="beamline_name")

    (options, args) = parser.parse_args()

    beamline = options.beamline_name
    print beamline
    print(options.with_crystal_info)

    if len(sys.argv) < 3:
        print "Usage: toilet.py REPORT_PREFIX OPTION"
        sys.exit()

    toilet = Toilet.Toilet(".", report_prefix, beamline)
    if options.unmerged_hkl == False:
        print "MTZ files will be archived..."
        toilet.makeAllArchive(mtz_option=True, inc_cry_info=options.with_crystal_info)

    elif options.unmerged_hkl == True:
        print "Unmerged HKL files will be archived..."
        toilet.makeAllArchive(mtz_option=False, inc_cry_info=options.with_crystal_info)
