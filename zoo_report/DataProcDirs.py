import sys,os

sys.path.append("/isilon/kunpy/Libs/")

import MyException
import glob,numpy
import DirectoryProc

# DEBUG flag
isIncludeMTZ = False

class DataProcDirs():
    def __init__(self, root_dir):
        self.root_dir = os.path.abspath(root_dir)
        self.isPrep = False
        self.isDebug = False
        # Dictionary for directories (type and directory name)
        self.dir_dict = {}
        # Checking 'decision.log'
        self.log_string = []

    def prep(self):
        # store all of directories in the designated path.
        dp = DirectoryProc.DirectoryProc(self.root_dir)
        dirs = dp.findDirs()

        if self.isDebug: print "Directory list in root_dir=", dirs, "in %s" % self.root_dir

        self.merge_dirs = []
        # Merge-related directoryies
        for d in dirs:
            if self.isDebug: print "DDDDDDDDDDDD=", d
            if d.startswith("merge") == True:
                abs_path_merge = os.path.join(self.root_dir, d)
                # abs_path_merge = os.path.abspath(d)
                self.merge_dirs.append(abs_path_merge)

        self.dir_dict["merge"] = self.merge_dirs
        print self.dir_dict["merge"]

        # Finding data processing directory
        self.found_procd = []

        # For each data processing directory  (both large and small wedge will be detected.)
        # _kamoproc/CPS1416-01/ : pin_dir = "CPS1416-01"
        print ("ProcDirs:prep(): data processing directory analysis")
        for pin_dir in dirs:
            abs_pin_dir = os.path.join(self.root_dir, pin_dir)
            pp = DirectoryProc.DirectoryProc(abs_pin_dir)
            # Directory search in the directory here
            qq = pp.findDirs()
            # Check root_dir/path1/path2/:(path2 = q)
            # /isilon/users/target/target/AutoUsers/200218/ariyoshi/_kamoproc/CPS1416-01
            # qq = ["data00", "data01"..]
            # Thus 'q' is succeeded data processing directory
            for q in qq:
                max_index = -9999
                # Data directory exists
                if q.rfind("data") != -1:
                    cols = q.split('/')
                    index_str = cols[-1].replace("data","")
                    #print int(index_str)
                    max_index = int(index_str)
                    #print max_index
                if max_index != -9999:
                    ddir_abs = os.path.abspath(abs_pin_dir)
                    #print ddir_abs
                    found_d = "%s/%s/" % (ddir_abs, q)
                    #print "EEEEE",found_d
                    self.found_procd.append(found_d)
                else:
                    if self.isDebug == True:
                        print "No data processed: %s" % q

        # Directories for 'each' data processing
        self.dir_dict["each_procd"] = self.found_procd

        # Here we should group found directories
        # TYPE: 'multi' 'helical' 'single'
        self.multi_dirs = []
        self.largewedge_dirs = []

        for procd_abs in self.found_procd:
            ddd = DirectoryProc.DirectoryProc(procd_abs)
            # directories in "...data00/KOZO0001-09-multi_1101-1200" -> multiple small wedge datasets
            # otherwise: directory can be regarded as 'large wedge' type.
            procd_dirs = ddd.findDirs()
            
            for procd in procd_dirs:
                if procd.rfind("multi") != -1:
                    final_path = os.path.join(procd_abs, procd)
                    self.multi_dirs.append(final_path)
                else:
                    final_path = os.path.join(procd_abs, procd)
                    self.largewedge_dirs.append(final_path)

        self.dir_dict["small_wedge"] = self.multi_dirs
        self.dir_dict["large_wedge"] = self.largewedge_dirs

        if self.isDebug: print "Analysing data structures has been finished: ",self.dir_dict

        if self.isDebug:
            for proc_type, dir in self.dir_dict.items():
                print "#########################################"
                print proc_type, dir
                print "#########################################"

        self.isPrep = True
        return self.found_procd

    def getTypeDirs(self, designated_key):
        if self.isPrep == False: self.prep()

        if designated_key not in self.dir_dict:
            raise MyException.NoKeysInDict("No designated key in the existing dict.")
        return self.dir_dict[designated_key]

    def listupProcessDirs(self, type_of_data):
        if self.isPrep == False: self.prep()
        proc_dirs = self.getTypeDirs(type_of_data)

        return proc_dirs

    # This is for each path directory
    # Directory path is analyzed to get 'PUCK_PIN' directory name
    def find_pin_datasets(self, proc_dir):
        cols = proc_dir.split('/')
        loop_index = 0
        # Searching "kamo" string in cols.
        for col in cols:
            if col.rfind("kamo") != -1:
                data_dir = os.path.join(cols[loop_index+1], cols[loop_index+2], cols[loop_index+3])
                return cols[loop_index+1], data_dir
            else:
                loop_index += 1

        MyException.NoKAMOpathInString("Designated columns do not include '_kamo*' path")

if __name__ == "__main__":
    arc_files = []
    jw = DataProcDirs(sys.argv[1])
    jw.isDebug = False

    #small_wedge_okay = jw.listupGoodProcessDirs("small_wedge")
    #ng_dirs = jw.listupBadProcessDirs("small_wedge")
    #print small_wedge_okay
    #print "NG=", ng_dirs

    ppp=jw.listupProcessDirs("large_wedge")
    #print "All Large_wedge=", ppp, len(ppp)
    print "Largewedge=",ppp, len(ppp)

    ppp=jw.listupProcessDirs("small_wedge")
    print "All small wedge=",ppp, len(ppp)

