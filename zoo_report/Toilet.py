import os, sys, math, numpy, scipy, glob
import Libs.Unko as Unko
import Libs.ZOOreporter as ZOOreporter
import Libs.MyDate as MyDate
import Libs.XDSReporter as XDSReporter
import Libs.DirectoryProc as DirectoryProc

class Toilet:
    def __init__(self, zoo_path, report_prefix, beamline):
        self.zoo_path = zoo_path
        self.prefix = report_prefix
        self.beamline = beamline
        self.arc_files = []

    def makeHTML(self, inc_cry_info=False):
        # Finding all zoo.db files
        zoo_files = glob.glob("%s/*.db" % self.zoo_path)

        # Size check and go
        if len(zoo_files) == 0:
            print "No zoo.db files"
            return 

        html_files = []
        index = 0
        for zoo_file in zoo_files:
            zoo_size = os.path.getsize(zoo_file)
            prefix_html = "%s_%02d" % (self.prefix, index)
            if zoo_size < 1000:
                print "Too small: Skipped %s" % zoo_size
            else:
                print "ZOO_FILE=%s" % zoo_file
                zrp = ZOOreporter.ZOOreporter(zoo_file, self.beamline)
                html_files.append(zrp.makeHTML(prefix_html))
                if inc_cry_info:
                    # SHIKA report & crystal captured imagees
                    crystal_info_paths=zrp.getCrystalInfoPaths()
                    self.arc_files += crystal_info_paths
                    print(self.arc_files)
            index += 1

        return html_files

    def makeReflectionArchive(self, option, mtz_option=False):
        # Default directory to be checked
        kamodir = "%s/_kamoproc/" % self.zoo_path
        kamo_abs_path = os.path.abspath(kamodir)

        # searching zoo.db files
        unko = Unko.Unko(kamo_abs_path)

        if option == "kamo_large":
            filelist = unko.makeReportLargeWedge(mtz_prep=mtz_option)
            #filelist = unko.makeArchiveLargeWedge()
            return filelist

        else:
            print "Option is unknown."

    # 2020/02/28 K.Hirata
    # Roughly completed in 'large wedge data collection'
    def makeAllArchive(self, mtz_option=True, inc_cry_info=False):
        # Archive file prefix
        da = MyDate.MyDate()
        dstr = da.getNowMyFormat(option="min")
        arc_file = "%s_%s.tgz" % (dstr, self.prefix)

        # HTML file about data collection in ZOO
        html_files = self.makeHTML(inc_cry_info)
        for html_file in html_files:
            html_file_rel = os.path.relpath(html_file, "./")
            self.arc_files.append(html_file_rel)

        # Large wedge reflection files -> .tgz file
        reflection_files = self.makeReflectionArchive("kamo_large", mtz_option)
        print "REFLECTION_FILES=", reflection_files
        self.arc_files += reflection_files

        # XDSreporter for large wedge
        tmp_kamo = "%s/_kamoproc/" % self.zoo_path
        abs_kamo = os.path.abspath(tmp_kamo)

        if os.path.exists(abs_kamo) != False:
            xdsr = XDSReporter.XDSReporter(abs_kamo)
            xdsr.makeHTML()
            xds_report_html = os.path.relpath("%s/correct.html" % abs_kamo, "./")
            xds_report_src = os.path.relpath("%s/contents/" % abs_kamo, "./")

            # File append to the archive file list
            self.arc_files.append(xds_report_html)
            self.arc_files.append(xds_report_src)

        # Merge directory
        # _kamo directories
        dp = DirectoryProc.DirectoryProc(self.zoo_path)
        dires = dp.findDirs()
        merge_related_files = []

        for each_dir in dires:
            archive_index = 0
            if each_dir == "_kamoproc":
                abs_kamo = os.path.abspath(os.path.join(self.zoo_path, each_dir))
                unko = Unko.Unko(abs_kamo)
                # Merging directories with '_final' results
                okay_dirs = unko.getListOfFinishedMergeDirs()

                # Find directories with final merging calculations
                final_dirs = unko.findFinalResultsDir(okay_dirs)
                # MTZ is required or not
                if mtz_option == True:
                    unko.findReflectionFilesIn(final_dirs, file_list = ["XSCALE.LP", "XSCALE.INP", "aniso.log", "pointless.log"])
                # MTZ file will not be listed for 'archive' candidates but xscale.hkl
                else: 
                    unko.findReflectionFilesIn(final_dirs, file_list = ["xscale.hkl","XSCALE.LP", "XSCALE.INP", "aniso.log", "pointless.log"],isIncludeMTZ=False)
                files_from_merge = unko.getArchiveFileList()
                archive_index += 1

                self.arc_files += files_from_merge

            if each_dir.rfind("_kamo_") != -1:
                abs_kamo = os.path.abspath(os.path.join(self.zoo_path, each_dir))
                unko = Unko.Unko(abs_kamo)
                # Merging directories with '_final' results
                okay_dirs = unko.getListOfFinishedMergeDirs()
                # Find directories with final merging calculations
                final_dirs = unko.findFinalResultsDir(okay_dirs)
                # MTZ is required or not
                if mtz_option == True:
                    unko.findReflectionFilesIn(final_dirs, file_list = ["XSCALE.LP", "XSCALE.INP", "aniso.log", "pointless.log"])
                # MTZ file will not be listed for 'archive' candidates but xscale.hkl
                else: 
                    unko.findReflectionFilesIn(final_dirs, file_list = ["xscale.hkl","XSCALE.LP", "XSCALE.INP", "aniso.log", "pointless.log"],isIncludeMTZ=False)
                files_from_merge = unko.getArchiveFileList()
                archive_index += 1
                self.arc_files += files_from_merge

        # Making archive file list
        arc_list_file = "%s/arc.lst" % self.zoo_path
        ofile = open(arc_list_file, "w")

        for arc_target in self.arc_files:
            ofile.write("%s\n" % arc_target)

        ofile.close()

        # Executing the command
        command = "tar cvfz %s --files-from=%s/arc.lst" % (arc_file, self.zoo_path)
        print "COMMAND=%s" % command
        os.system(command)

if __name__ == "__main__":

    zoo_path = sys.argv[1]
    report_prefix = sys.argv[2]
    beamline=sys.argv[3]

    print zoo_path, report_prefix
    print "NUMBER OF ARGUMENTS=", len(sys.argv)
    if len(sys.argv) != 4:
        print "Usage: toilet.py ZOO_PATH REPORT_PREFIX"
        sys.exit()

    toilet = Toilet(zoo_path, report_prefix,beamline)
    toilet.makeAllArchive(mtz_option=False)
