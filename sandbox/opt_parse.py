import optparse, os, sys

parser = optparse.OptionParser()

parser.add_option("--bl", "--beamline", dest="beamline_name", help="The name of your beamline", metavar="FILE")
parser.add_option("--z1", "--zoofile1", dest="filename1", help="ZOO csv/db File No.1.", metavar="FILE")
parser.add_option("--t1", "--time1", dest="timelimit1", help="data collection time in minuts for file1.", metavar="FILE")
parser.add_option("--z2", "--zoofile2", dest="filename2", help="ZOO csv/db File No.2.", metavar="FILE")
parser.add_option("--t2", "--time2", dest="timelimit2", help="data collection time in minuts for file2.", metavar="FILE")
parser.add_option("--z3", "--zoofile3", dest="filename3", help="ZOO csv/db File No.3.", metavar="FILE")
parser.add_option("--t3", "--time3", dest="timelimit3", help="data collection time in minuts for file3.", metavar="FILE")
parser.add_option("--z4", "--zoofile4", dest="filename4", help="ZOO csv/db File No.4.", metavar="FILE")
parser.add_option("--t4", "--time4", dest="timelimit4", help="data collection time in minuts for file4.", metavar="FILE")
parser.add_option("--q", "--quiet", action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")


#

#print parser.parse_args()

(options, args) = parser.parse_args()

# Beamline name
bl_name = options.beamline_name

if options.filename1 is None:
    print "Nothing will be done"
    sys.exit(1)

# Time and Zoo input files
if options.filename1 is not None:
    zoo_input1 = options.filename1
    if options.timelimit1 is None:
        print "Timelimit1 is not defined."
        timelimit1 = 9999999999 # [mins]
    else:
        timelimit1 = float(options.timelimit1)

print zoo_input1, timelimit1
