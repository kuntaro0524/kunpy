import optparse, os, sys

parser = optparse.OptionParser()

parser.add_option(
    "-b","--beamline",
    choices=["BL32XU","BL45XU","BL41XU"],
    help="type of input method",
    dest="beamline_name")

parser.add_option(
    "--k1","--kamodir1",
    default="_kamoproc",
    help="Input KAMO data processing directory path with option (see --o1, --kamo_mode1)",
    type="string",
    dest="kamodir1"
    )

parser.add_option(
    "--o1","--kamo_mode1",
    default="multi",
    help = "option to choose mode to merge in the destination dicretory",
    type="string",
    dest="option1"
    )
parser.add_option(
    "--k2","--kamodir2",
    default="_kamo_30deg",
    type="string",
    dest="kamodir2"
    )

parser.add_option(
    "--o2","--kamo_mode2",
    default="all",
    type="string",
    dest="option2"
    )

parser.add_option(
    "--only-one",
    action = 'store_true',
    default=False,
    dest="single_flag"
    )


(options, args) = parser.parse_args()


# print "Options = " , options

# Beamline name
bl_name = options.beamline_name

# The largest definition

if options.single_flag == False:
    print "Normal data processing"
    print options.kamodir1, options.option1, options.kamodir2, options.option2, options.beamline_name

else:
    print "Abnormal data processing"
    print options.kamodir1, options.option1, options.beamline_name
