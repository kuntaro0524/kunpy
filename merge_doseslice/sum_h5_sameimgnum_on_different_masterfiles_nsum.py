import h5py,glob, sys
from yamtbx.dataproc import eiger
from yamtbx.dataproc import cbf
from yamtbx.dataproc.XIO.plugins import eiger_hdf5_interpreter
from libtbx import easy_mp
from PIL import Image
import PIL.ImageOps 
import numpy,os

from libtbx import easy_mp

sys.path.append("/isilon/BL32XU/BLsoft/PPPP/Libs")
import MergeH5

if __name__ == "__main__":

    master_files = glob.glob("%s/*master.h5" % ".")
    master_files.sort()

    print(master_files)

    startnum=1
    endnum=3600
    rot_start=0.0
    osc_width=0.1
    outprefix="PPP"

    mh5 = MergeH5.MergeH5()
    mh5.setNproc(8)

    n_masterfiles=len(master_files)

    n_sum=int(sys.argv[1])
    n_proc=int(sys.argv[2])

    patt=n_sum
    print("############### PATTERN %d ################"%patt)
    print(n_masterfiles)
    n_sum = patt
    n_rep = int(n_masterfiles/n_sum)
    print(n_sum, n_rep)
    outdir="sum_%03d"%patt

    if os.path.exists(outdir) == False:
        os.makedirs(outdir)
    
    for n_sum_repeat in range(0, n_rep):
        outprefix="data%03d" % n_sum_repeat
        proc_list = []
        start_master_index = n_sum_repeat * patt
        end_master_index = start_master_index + patt
        print("SUMM:",start_master_index, end_master_index)
        proc_master_files = master_files[start_master_index: end_master_index]
        print(proc_master_files)

        for imgnum in range(startnum, endnum+1):
            param_list = []
            master_info_list = []
    
            for master_file in proc_master_files:
                master_info_list.append((master_file, imgnum, imgnum))
    
            start_phi = rot_start + float(imgnum) * osc_width
            param_list = master_info_list, outdir, outprefix, imgnum, start_phi, osc_width
            print "Process list:", param_list
            proc_list.append(param_list)

        easy_mp.pool_map(fixed_func=lambda n: mh5.sumUpSimpleList(n), args=proc_list, processes=n_proc)
