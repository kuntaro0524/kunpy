import os, sys, math, glob, numpy
import GatherExpInfo
import CheckProcStatus
import DataProcDirs
import MyException

if __name__ ==  "__main__":
    proc_path = sys.argv[1]
    kamo_path = sys.argv[2]
    beamline = sys.argv[3]

    print "#####################################################"
    gei = GatherExpInfo.GatherExpInfo(proc_path, beamline)
    gei.getDataDirListFromZOODB(kamo_path)
    print "#####################################################"

    dpd = DataProcDirs.DataProcDirs(kamo_path)
    large_wedge_dirs = dpd.listupProcessDirs("large_wedge")

    # number of directories for large wedge data processing
    n_large_wedge = len(large_wedge_dirs)
    n_good = 0

    """
    while(1):
        for large_wedge_dir in large_wedge_dirs:
            decision_log = os.path.join(large_wedge_dir,"decision.log")
            #print os.path.exists(decision_log)

            if os.path.exists(decision_log) == False:
                raise (MyException.NoDecisionLogInProcessDir)

            lines = open(decision_log, "r").readlines()
            error_message = "Normal"

            for line in lines:
                if line.rfind("error:") != -1:
                    error_message = line.replace("error:", "").strip()
                if line.rfind("failed") != -1:
                    error_message = line.strip()
                if line.rfind("finished") != -1:
                    finish_flag = True
                    n_good += 1

            print decision_log, error_message, finish_flag

        print "n_good=", n_good
        if n_good == n_large_wedge:
            break
    """
