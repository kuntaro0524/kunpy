import os,sys, numpy, glob, time
import NikuDango

if __name__ == "__main__":

    import optparse, os, sys

    parser = optparse.OptionParser()

    # Options
    parser.add_option(
        "-b","--beamline",
        choices=["BL32XU","BL45XU","BL41XU"],
        help="type of input method",
        dest="beamline_name")

    parser.add_option(
        "--k1","--kamodir1",
        default="_kamoproc",
        help="Input KAMO data processing directory path with option (see --o1, --kamo_mode1)",
        type="string",
        dest="kamodir1"
        )

    parser.add_option(
        "--o1","--kamo_mode1",
        default="multi",
        help = "option to choose mode to merge in the destination dicretory",
        type="string",
        dest="option1"
        )
    parser.add_option(
        "--k2","--kamodir2",
        default="_kamo_30deg",
        type="string",
        dest="kamodir2"
        )

    parser.add_option(
        "--o2","--kamo_mode2",
        default="all",
        help="option to choose mode to merge in the destination dicretory(for the second path)",
        type="string",
        dest="option2"
        )

    parser.add_option(
        "--only-one",
        action = 'store_true',
        default=False,
        help="case to run make_niku_dango.py in order to (re-)process a designated KAMO directory with option --k1 and --o1",
        dest="single_flag"
        )

    (options, args) = parser.parse_args()

    # Entire path
    zoo_root = "."
    beamline = options.beamline_name

    # The largest definition
    # single data processing flag
    # single data processing flag

    if options.single_flag == False:
        print "Normal data processing"
        print options.kamodir1, options.option1, options.kamodir2, options.option2, options.beamline_name

        dirlist = (options.kamodir1, options.kamodir2)
        optlist = (options.option1, options.option2)

        print "DLIST=", dirlist
        print "optlist=", optlist

        nikudango = NikuDango.NikuDango(zoo_root, beamline)
        nikudango.setConds(dirlist, optlist)
        nikudango.makemake()

    else:
        print "Single data processing"
        print options.kamodir1, options.option1, options.beamline_name
        nikudango = NikuDango.NikuDango(zoo_root, beamline)
        nikudango.setConds(options.kamodir1, options.option1)
        nikudango.makemake()

    all_shells = []

