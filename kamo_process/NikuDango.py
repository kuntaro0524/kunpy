import os,sys, numpy, glob, time
import Libs.GatherExpInfo as GatherExpInfo
import Libs.DBinfo as DBinfo
import Libs.DirectoryProc as DirectoryProc
import Libs.Date as Date
import Libs.MyException as MyException
import logging

class NikuDango:
    def __init__(self, zoo_root_path, beamline):
        self.zoo_root_path = os.path.abspath(zoo_root_path)
        self.isPrep = False
        self.beamline = beamline
        self.debug = False

        # Logging setting
        d = Date.Date()
        time_str = d.getNowMyFormat(option="date")
        logname = "./nikudango_%s.log" % (time_str)
        logging.config.fileConfig('/isilon/kunpy/Libs/logging.conf', defaults={'logfile_name': logname})
        self.logger = logging.getLogger('NIKUDANGO')

        # Default processing directories and modes
        self.kamo_options = [('_kamoproc','multi'), ('_kamo_30deg','all')]
        #self.kamo_options = [('_kamo_10deg','all')]

    def checkModeContamination(self,conds_list):
        for cond in conds_list:
            print cond

    def calcHalfCorner(self, cond):
        wavelength = cond['wavelength']
        dist_ds = cond['dist_ds']

        if self.beamline.lower() == "bl32xu":
            min_dim = 233.0
        elif self.beamline.lower() == "bl45xu":
            min_dim = 422.0
        elif self.beamline.lower() == "bl41xu":
            min_dim = 311.2

        # half corner resolution
        half_corner_radius = numpy.sqrt(5.0) / 2.0 * (min_dim / 2.0)
        theta_re = 0.5 * numpy.arctan(half_corner_radius / dist_ds)

        dmin_re = wavelength / 2.0 / numpy.sin(theta_re)
        return dmin_re

    def checkExpMode(self, mode, acceptable_modes):
        for ok_mode in acceptable_modes:
            if mode == ok_mode:
                return True
            else:
                continue
        return False

    # Parallel processing with KAMO buttagiri mode
    # check existence of data processing directories
    def waitForDataProcDirs(self, kamo_root, conds, acceptable_modes):
        # Count number of pins to be processed
        n_pins = len(conds)

        self.collected_dirs = []
        # wait for generation of 'data proc' directories
        for cond in conds:
            # BSS log file path
            dbinfo = DBinfo.DBinfo(cond)
            dbinfo.prepParams()
            data_dir, dc_dir, logfiles = self.makeCheckDirNamesFromDBinfo(dbinfo)
            # Mode check
            isOkay = self.checkExpMode(dbinfo.mode, acceptable_modes)
            if isOkay == True:
                self.logger.info("%s is included." % dc_dir)
            else:
                self.logger.info("%s is not included." % dc_dir)
                continue
            # Data collection was not conducted 
            if len(logfiles) == 0:
                self.logger.info("Data was not collected: %s" % data_dir)
                continue
            # Synthesized KAMO data processing directory
            each_data_dir = os.path.join(kamo_root, data_dir)
            self.logger.info("%s is checking" % each_data_dir)
            # Loop for waiting for a generation of the directory
            # Waiting for one by one directory
            while(1):
                n_finished = 0
                if os.path.exists(each_data_dir) == False:
                    self.logger.info("%s does not exist" % each_data_dir)
                    percent_done = float(n_finished)/float(n_pins) * 100.0
                    self.logger.info("All pins: %5d, already prepared: %5d (%5.2f percent)" % (n_pins, n_finished, percent_done))
                    time.sleep(10.0)
                else:
                    self.logger.info("%s is done" % each_data_dir)
                    n_finished += 1
                    break

        self.logger.info("check existence finished.")

    def waitForDataProcLogs(self, kamo_root, conds, acceptable_modes):
        # Count number of pins to be processed
        n_pins = len(conds)
        # Making a list for checking directory existence
        n_finished = 0
        # Reset the directory list for data processing
        self.dirs_for_procs = []
        self.dirs_to_be_checked = []

        # wait for generation of 'data proc' directories
        for cond in conds:
            # BSS log file path
            dbinfo = DBinfo.DBinfo(cond)
            dbinfo.prepParams()
            # Data processing directories for each sample pin.
            data_dir, dc_dir, logfiles = self.makeCheckDirNamesFromDBinfo(dbinfo)
            # Data collection was not conducted 
            if len(logfiles) == 0:
                self.logger.info("Data was not collected: %s" % data_dir)
                continue
            # Synthesized KAMO data processing directory
            kamo_each_dir = os.path.join(kamo_root, data_dir)
            self.logger.info("KAMO each directory : %s is checking" % kamo_each_dir)
            # Loop for waiting for a generation of the directory
            # Waiting for one by one directory

            # Mode check
            isOkay = self.checkExpMode(dbinfo.mode, acceptable_modes)

            if isOkay == True:
                self.logger.info("%s is included in this merge judged by mode." % dc_dir)
            else:
                self.logger.info("%s is not included in this merge." % dc_dir)
                continue

            # 2020/12/07 debug for BL45XU duplicated CSV files for kamo auto merge.
            # EIGER case: log files are only two in data collection directory.
            # PILATUS case: Same number of log files exist with collected crystals.
            # Log files are analyzed in data collection directories
            for logfile in logfiles:
                # .log suffix: 2 patterns in data collection directory
                # EIGER download log and bss data collection log.
                # EIGER download log file should be skipped.
                if logfile.rfind("eiger") != -1:
                    continue
                else:
                    # Extract sample_name from zoo.db
                    sample_name = dbinfo.sample_name
                    # absolute path for each data processing directory
                    kamo_each_dir = os.path.join(kamo_root, data_dir)
                    # To analyze the directory
                    dp = DirectoryProc.DirectoryProc(kamo_each_dir)
                    self.logger.info("Finding directories in %s" % kamo_each_dir)
                    # directories contained in the path
                    first_layers = dp.findDirs()
                    # Information required to prepare 'automatic merge script'
                    # Half corner resolution
                    half_corner_resol = self.calcHalfCorner(cond)
                    # meas_name
                    meas_name = dbinfo.meas_name
                    # Adding the path for writing CSV for automatic data merging.
                    # 2020/12/07 during debugging by K. Hirata
                    # For PILATUS beamlines, redundant definitions should be reduced !?
                    self.dirs_for_procs.append((kamo_each_dir, half_corner_resol, sample_name, meas_name, dbinfo.mode))
                    # To make a list for checking 'progress in KAMO data processing'
                    for layer in first_layers:
                        each_proc_dir = os.path.join(kamo_each_dir, layer)
                        # here path for data processing is stored into the checking list.
                        self.logger.info("Adding directory paths in detailed.: %s" % layer)
                        self.dirs_to_be_checked.append(each_proc_dir)

        # Number of datasets to be checked
        # what will be checked: whether data processing is finished or not.
        num_of_data = len(self.dirs_to_be_checked)
        self.logger.info("%5d datasets will be checked from now" % num_of_data)

        # Debugging
        for data in self.dirs_to_be_checked:
            self.logger.debug("Check finished or not PATH=%s " % data)

        # Checking KAMO decision log files.
        while (1):
            n_finished = 0
            for check_dir in self.dirs_to_be_checked:
                self.logger.debug("Checking %s" % check_dir)
                if os.path.exists(check_dir) == True:
                    try:
                        if self.isKAMOprocFinished(check_dir) == True:
                            n_finished += 1
                            percent_done = float(n_finished) / float(num_of_data) * 100.0
                            self.logger.info("All pins: %5d, already prepared: %5d (%5.2f percent)" % (num_of_data, n_finished, percent_done))
                    except:
                        self.logger.info("Log files do not appear in %s" % check_dir)
                        percent_done = float(n_finished)/float(num_of_data) * 100.0
                        self.logger.info("All pins: %5d, already prepared: %5d (%5.2f percent)" % (num_of_data, n_finished, percent_done))
                else:
                    self.logger.info("Skipping to check now: %s" % check_dir)
            if n_finished == num_of_data:
                break
            else:
                self.logger.info("waiting for 30 sec...")
                time.sleep(30.0)
        self.logger.info("All of procs have been finished!!")
    
    def makeCheckDirNamesFromDBinfo(self, dbinfo):
        data_dir = "%s/data%02d" % (dbinfo.puck_pin, dbinfo.n_mount)
        dc_dir = os.path.join(dbinfo.root_dir, data_dir)
        logfiles = glob.glob("%s/*.log" % (dc_dir))
        self.logger.info("DC_DIR: %s" % dc_dir)

        return data_dir, dc_dir, logfiles

    # 'merge_mode' : types of data merging
    # 'multi': merging multiple small wedge datasets
    # 'all' : merging all datasets with same 'sample_name' tags
    def getDataDirListFromZOODB(self, kamo_rootdir, merge_mode="multi"):
        # KAMO absolute path
        kamo_abs = os.path.abspath(kamo_rootdir)
        self.logger.info("data processing in %s" % kamo_abs)
        self.logger.info("Processing mode=%s" % merge_mode)

        # checking list
        if merge_mode == 'all':
            acceptable_modes = ['multi','single','helical', 'mixed']
        elif merge_mode == 'multi':
            acceptable_modes = ['multi']

        gei = GatherExpInfo.GatherExpInfo(self.zoo_root_path, self.beamline)
        # read zoo.db files and prepare 'gei.conds'; a list of zoo conditions
        gei.prep()

        # check if data processing directories are ready
        self.waitForDataProcDirs(kamo_abs, gei.conds, acceptable_modes)

        # Wait for data processing logs
        self.waitForDataProcLogs(kamo_abs, gei.conds, acceptable_modes)

        # Exclude merging 'multiple' only in 'all' scheme
        if merge_mode == "all":
            self.excludeMergeMultiSmallWedgeOnlyData()

        return self.dirs_for_procs

    def excludeMergeMultiSmallWedgeOnlyData(self):
        selected_list = []
        sorted_list = sorted(self.dirs_for_procs, key=lambda x:x[2])

        for d, resol, sample_name, meas_name, mode in sorted_list:
            self.logger.info("Excluding %s: %s" % (d, sample_name))

        # Preparation of resolution sorting
        saved_samplename = sorted_list[0][2]
        self.logger.info("The first component's sample_name =%s"% saved_samplename)
        merge_groups = []
        same_groups = []
        for each_dataset in sorted_list:
            procd, resol, sample_name, meas_name, mode = each_dataset
            self.logger.debug("????????%s????????:####%s####" % (sample_name, saved_samplename))
            # Append the first dataset to a merging group
            if len(merge_groups) == 0:
                self.logger.info("The first dataset is %s" % procd)
                merge_groups.append(each_dataset)
            # The same resolution value is gathered to merge together
            elif sample_name == saved_samplename:
                merge_groups.append(each_dataset)
                self.logger.info("Processing include %s with %s" % (procd, sample_name))
            else:
                # Append a current list of datasets to the entire group list
                same_groups.append(merge_groups)
                # Updating resolution limit for the next line
                saved_samplename = sample_name
                # A list of the group should be reset.
                merge_groups = []
                # And append the first dataset to the next group with the resolution
                self.logger.info("The first dataset is %s" % procd)
                merge_groups.append(each_dataset)
        same_groups.append(merge_groups)

        # If the same group only contains datasets collected with 'multi' mode,
        # the datasets should be neglected in 'all' scheme.
        self.dirs_for_procs = []
        for list_of_each_samplename in same_groups:
            self.logger.info("EACH SAMPLE LIST")
            self.logger.info(list_of_each_samplename)
            # The first experimental mode
            flag_change = False
            flag_include_non_multi = False
            saved_mode = list_of_each_samplename[0][4]

            for a_pin_list in list_of_each_samplename:
                datadir, resol, sample_name, meas_name, mode = a_pin_list
                self.logger.info("MMMMMMODDEEE==%s:%s"%(mode, datadir))

                if mode != saved_mode:
                    flag_change = True
                    self.logger.info("%s should be included." % datadir)
                    break
                if mode != "multi":
                    self.logger.info("Mode is not 'multiple small wedge' scheme.")
                    flag_include_non_multi = True
                    break

            for a_pin_list in list_of_each_samplename:
                datadir, resol, sample_name, meas_name, mode = a_pin_list
                if flag_change == True or flag_include_non_multi == True:
                    self.logger.info("### INCLUDED %s" % datadir)
                    self.dirs_for_procs.append(a_pin_list)
                else:
                    self.logger.info("@@@ REJECTED %s" % datadir)

        return self.dirs_for_procs

    def isKAMOprocFinished(self, kamo_proc_dir):
        check_file = "%s/decision.log" % kamo_proc_dir
        self.logger.debug("KAMO log:%s is being analyzed." % check_file)

        if os.path.exists(check_file) == False:
            self.logger.debug("KAMO decision log does not exist.")
            raise (MyException.NoDecisionLogInProcessDir)
        lines = open(check_file, "r").readlines()
        error_message = "Normal"
        for line in lines:
            if line.rfind("error:") != -1:
                error_message = line.replace("error:", "").strip()
            if line.rfind("failed") != -1:
                error_message = line.strip()
            if line.rfind("finished") != -1:
                self.logger.debug("KAMO done.")
                return True
        return False

    # Grouping datasets with same resolution limits
    def makeGroups(self):
        self.logger.debug("Length of dirs_for_procs=%5d" % len(self.dirs_for_procs))
        # Sort by half corner resolution
        sorted_list = sorted(self.dirs_for_procs, key=lambda x:x[1])
        # Preparation of resolution sorting
        saved_resol = sorted_list[0][1]
        merge_groups = []
        same_groups = []
        for each_dataset in sorted_list:
            procd, resol, sample_name, meas_name, mode = each_dataset
            # Append the first dataset to a merging group
            if len(merge_groups) == 0:
                self.logger.info("The first dataset is %s" % procd)
                merge_groups.append(each_dataset)
            # The same resolution value is gathered to merge together
            elif resol == saved_resol:
                merge_groups.append(each_dataset)
                self.logger.info("Processing include %s at %8.2f A resolution" % (procd, resol))
            else:
                # Append a current list of datasets to the entire group list
                same_groups.append(set(merge_groups))
                # Updating resolution limit for the next line
                saved_resol = resol
                # A list of the group should be reset.
                merge_groups = []
                # And append the first dataset to the next group with the resolution 
                self.logger.info("The first dataset is %s" % procd)
                merge_groups.append(each_dataset)
        same_groups.append(set(merge_groups))

        return same_groups

    # KAMO automatic data merging requires 2 files for each.
    # 1) CSV file cotaining reflection file list for each group of same resolution limit.
    # 2) .sh file for running kamo.automerge with 'defined resolution limit'
    # isLargeWedge: defining ''
    def prepMergeFiles(self, kamodir, merge_prefix,script_dir="merge_inputs",isLargeWedge=False):
        # Absolute directory of "KAMO" data processing
        abs_kamodir = os.path.abspath(kamodir)
        # Script files are stored into 'script_dir'
        scripts_dir = os.path.join(self.zoo_root_path,script_dir)
        self.logger.info("All scripts for KAMO auto-merging will be made in %s" % scripts_dir)
        # A list of scripts to be run 
        script_files = []
        # Making a directory 
        if os.path.exists(scripts_dir) == False:
            os.makedirs(scripts_dir)
        else:
            self.logger.info("preparation OK.")
        
        # Making groups with 'same' resolution limits
        same_groups = self.makeGroups()
        csv_file_list = []
        # All of shell scripts
        all_script_list = []
        for file_index, same_group in enumerate(same_groups):
            csv_name = "%s_merge_%03d.csv" % (merge_prefix, file_index)
            # CSV file is output to the 'scripts_dir'
            csv_out = os.path.join(scripts_dir, csv_name)
            csv_file_list.append(csv_out)
            csv_file = open(csv_out, "w")
            csv_file.write("topdir,name,anomalous\n")
            for procd, resol, sample_name, meas_name, mode in same_group:
                if meas_name == 'phasing':
                    meas_name = 'yes'
                else:
                    meas_name = 'no'
                csv_file.write("%s,%s,%s\n" % (procd, sample_name,meas_name))

            csv_file.close()
            # CC estimation resolution limits
            cc_dmin = resol + 1.5
            # degrees per batch
            if isLargeWedge == True:
                degrees_per_batch = 5.0
            else:
                degrees_per_batch = 1.0
            # Making a dictionary to make a script
            dict_proc = dict(half_corner_res=resol, cc_dmin=cc_dmin, csv_file=csv_out, \
                                        workdir=abs_kamodir, degrees_per_batch=degrees_per_batch)
            script_strs = self.getScriptString(dict_proc)
            # Automerge script file
            sc_name = "%s_merge_%03d.sh" % (merge_prefix, file_index)
            script_file = os.path.join(scripts_dir, sc_name)
            # Storing script names into the return variant
            script_files.append(script_file)
            asfile = open(script_file, "w")
            asfile.write(script_strs)
            asfile.close()

        # Making merging scripts
        if len(script_files) == 0:
            return ""

        all_run_path = self.getMergingScripts(script_files)

        # Merging scripts
        return all_run_path

    def setConds(self, kamoroot_list, mode_list):
        self.kamo_options = []
        for kamoroot,mode in zip(kamoroot_list, mode_list):
            self.kamo_options.append((kamoroot, mode))
            self.logger.info("Appending %s with %s" % (kamoroot, mode))

    # The total control function to be called
    def makemake(self):
        # 'merge_mode' is used to select datasets
        # 'multi': merging multiple small wedge datasets
        # 'helical' : merging helical datasets only
        # 'single' : merging single datasets only
        # 'all' : merging all datasets having same 'sample_name' tags.

        for kamopath, merge_mode in self.kamo_options:
            self.logger.info("PATH=%s, Option=%s" %(kamopath, merge_mode))
            # Check an existence of the directory
            if os.path.exists(kamopath) == False:
                self.logger.info("%s does not exist..." % kamopath)
                continue
            dlist = self.getDataDirListFromZOODB(kamopath, merge_mode)
            if len(dlist) == 0:
                self.logger.info("No datasets for %s in %s" % (merge_mode, kamopath))
                continue
            self.prepMergeFiles(kamopath, "giri_%s" % merge_mode)

    def getMergingScripts(self, script_files):
        # Merging scripts
        self.all_run_path = os.path.join(self.zoo_root_path, "all_run.sh")

        # Check if the file exists
        header_flag = False
        if os.path.exists(self.all_run_path):
            # Check if the header exists
            all_run_file = open(self.all_run_path,"r")
            for line in all_run_file.readlines():
                if line.rfind("#!/bin/sh") != -1:
                    header_flag = True
                    break
            all_run_file.close()
        # Header line is written when it is not on the current file
        all_run_file = open(self.all_run_path,"aw")
        if header_flag == False:
            all_run_file.write("#!/bin/sh\n")
        for script_file in script_files:
            all_run_file.write("sh %s\n" % script_file)
        all_run_file.close()
        return self.all_run_path

    def getScriptString(self, dict_proc):
        automerge_script_str = """
#!/bin/sh
# 2 dmin values should be evaluated.
# Merging process will be applied to datasets with better thresholds
# of completeness/redundancy.
# When a "wrongly" shorter dmin(higher resolution) is set here, 
# completeness value becomes very low for bad datasets and this 
# process does not reach to merging/scaling stages.
dmin_start_1=%(half_corner_res).2f

# dmin for CC calculation
# this should be set for each dmin values
# Very simple calculation (modify 'cc_margin' if you need)
# cc_dmin = dmin_start + cc_margin
cc_dmin=%(cc_dmin).2f

# Minimum normal/anomalous completeness
# Normal completeness
min_cmpl=10
# Anomalous completeness
min_acmpl=10

# Minimum redundancy (2021/10/11 mat)
min_redun = 1.0

# Minimum anomalous redundancy
min_aredun=1.0

# Maximum number of cluster
max_clusters=10

# Memory disk
use_ramdisk=true

# Number of cores
nproc=8

kamo.auto_multi_merge \
  filtering.choice=cell filtering.cell_iqr_scale=2.5 \
  csv="%(csv_file)s" \
  workdir=%(workdir)s/ \
  prefix=merge_blend_${dmin_start_1}S_ \
  cell_method=reindex \
  reject_method=framecc+lpstats \
  rejection.lpstats.stats=em.b+bfactor \
  merge.max_clusters=${max_clusters} \
  merge.d_min_start=$dmin_start_1 \
  merge.clustering=blend \
  merge.blend.min_cmpl=$min_cmpl \
  merge.blend.min_acmpl=$min_acmpl \
  merge.blend.min_redun=$min_redun \
  merge.blend.min_aredun=$min_aredun \
  xscale.degrees_per_batch=%(degrees_per_batch).1f \
  xscale.reference=bmin \
  batch.engine=sge \
  merge.batch.engine=sge \
  merge.batch.par_run=merging \
  merge.nproc=$nproc \
  merge.batch.nproc_each=$nproc \
  xscale.use_tmpdir_if_available=${use_ramdisk} \
  batch.sge_pe_name=par &

kamo.auto_multi_merge \
  filtering.choice=cell filtering.cell_iqr_scale=2.5 \
  csv="%(csv_file)s" \
  workdir=%(workdir)s/ \
  prefix=merge_ccc_${dmin_start_1}S_ \
  cell_method=reindex \
  reject_method=framecc+lpstats \
  rejection.lpstats.stats=em.b+bfactor \
  merge.max_clusters=${max_clusters} \
  xscale.reference=bmin \
  merge.d_min_start=$dmin_start_1 \
  merge.clustering=cc \
  merge.cc_clustering.d_min=${cc_dmin} \
  merge.cc_clustering.min_cmpl=$min_cmpl \
  merge.cc_clustering.min_acmpl=$min_acmpl \
  merge.cc_clustering.min_redun=$min_redun \
  merge.cc_clustering.min_aredun=$min_aredun \
  xscale.degrees_per_batch=%(degrees_per_batch).1f \
  batch.engine=sge \
  merge.batch.engine=sge \
  merge.batch.par_run=merging \
  merge.nproc=$nproc \
  merge.batch.nproc_each=$nproc \
  xscale.use_tmpdir_if_available=${use_ramdisk} \
  batch.sge_pe_name=par &""" % dict_proc

        return automerge_script_str

if __name__ == "__main__":

    zoo_root = sys.argv[1]
    beamline = sys.argv[2]

    nikudango = NikuDango(zoo_root, beamline)
    nikudango.setConds("_kamo_15deg","all")
    nikudango.makemake()
