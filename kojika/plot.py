import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

np.random.seed(0)

sns.set_theme()

#uniform_data = np.random.rand(10,12)
#ax = sns.heatmap(uniform_data)

# Flight
flights = sns.load_dataset("flights")
flights = flights.pivot("month","year","passengers")

ax = sns.heatmap(flights)
plt.show(block=True)
