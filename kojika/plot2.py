import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

np.random.seed(0)

sns.set_theme()

arr_2d = np.arange(-8, 8).reshape((4, 4))

ax = sns.heatmap(arr_2d)
plt.show(block=True)


