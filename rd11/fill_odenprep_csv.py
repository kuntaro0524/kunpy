import pandas as pd
import sys

df = pd.read_csv(sys.argv[1])

df['prefix']="ssad"
df['space_group']="P43212"
df['anom_atom']="S"
df['n_anom']=8
df['solv_cnt']=0.4
df['build_cycle']=1
df['n_dm']=20

df.to_csv("oden_prep.csv", index=False)
# prefix,hkl_file,cell_params,space_group,anom_atom,n_anom,n_try,phase_dmax,solv_cnt,n_dm,build_cycle,dmin

