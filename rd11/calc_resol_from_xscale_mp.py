import sys,os,subprocess
sys.path.append("/isilon/users/target/target/Staff/rooms/kuntaro/Libs")
import glob,numpy
import DirectoryProc
import AnaCORRECT

from yamtbx.dataproc import eiger
from yamtbx.dataproc import cbf
from yamtbx.dataproc.XIO.plugins import eiger_hdf5_interpreter
from libtbx import easy_mp

import ResolutionFromXscaleHKL

def exec_cmd(cmd):
  return subprocess.Popen(
      cmd, stdout=subprocess.PIPE,
      shell=True).communicate()[0]

# Finding xscale.mtz
reflection_file = sys.argv[1]
dp=DirectoryProc.DirectoryProc(os.environ["PWD"])
xscalehkl_list,path_list=dp.findTarget(reflection_file)

# xscale_hkl : an absolute path of "xscale.hkl"
def calc_resolution_limit(xscale_hkl):
    rfx=ResolutionFromXscaleHKL.ResolutionFromXscaleHKL(xscale_hkl)
    resol = rfx.get_resolution()
    return xscale_hkl,resol

list_answers = easy_mp.pool_map(fixed_func=lambda n: calc_resolution_limit(n), args=xscalehkl_list, processes=16)

print list_answers

for compo in list_answers:
    if compo == None:
        continue
    print "COMPO=",compo

    filename, resolution = compo
    #logf.write("%20s %5.2f\n" % (filename, resolution))

#logf.close()
