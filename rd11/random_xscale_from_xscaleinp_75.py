import random,sys,os

# input file is the XSCALE.INP of the all merging datasets
lines = open(sys.argv[1], "r").readlines()

# NBATCH get flag
isNbatch = False
isGetImageDirectory = False

input_files = []
store_lines = []
# B-factor reference file should be fixed for all scaling
b_reference_file = ""
# Read XSCALE.INP and store XDS_ASCII.HKL for merging
# into 'input_files' as a list
for line in lines:
    if line.rfind("INPUT_FILE") != -1:
        # When the Input file is 'b-factor reference' file
        if line.rfind("*")!=-1:
            b_reference_file = line.replace("*","").split("=")[1].lstrip()
        else:
            pppp=line.split("=")
            relative_refl_path = pppp[1].lstrip()
            abs_refl_path = os.path.abspath(relative_refl_path)
            input_files.append(abs_refl_path)
    # if 'NBATCH' definition is required.
    elif line.rfind("NBATCH=") != -1:
        if isNbatch == False:
            nbatch = line.split("=")[1]
            isNbatch = True
        else:
            continue
    # Storing essential input commands for the next XSCALE
    else:
        store_lines.append(line)

# making XDS input file
def make_xscale_out(dir_name, b_reference_file, proc_subsets, force_to_run = True, genMTZ=True, skipIfExist=True):
    # Check if the processing directory already exists or not
    if os.path.exists(dir_name) == False:
        os.makedirs(dir_name)
    else:
    # Check if the processing is already done or not
        xscale_path=os.path.join(dir_name, "xscale.hkl")
        if skipIfExist==True and os.path.exists(xscale_path)==True:
            print("Already processed")
            convertOnly(dir_name)
            return True
        else:
            pass

    # relative_refl_path = os.path.relpath()
    filename = os.path.join(dir_name, "XSCALE.INP")
    xdsinp = open(filename,"w")

    for line in store_lines:
        xdsinp.write("%s" % line)

    # B-factor reference file
    relpath = os.path.relpath(b_reference_file, dir_name)
    xdsinp.write("INPUT_FILE=*%s" % relpath)

    # Residual files
    for proc_set in proc_subsets:
        relpath = os.path.relpath(proc_set, dir_name)
        xdsinp.write("INPUT_FILE=%s" % relpath)
        xdsinp.write("NBATCH=%s" % nbatch)

    xdsinp.close()

    # If 'XSCALE.LP' exists XSCALE is skipped
    if force_to_run == False:
        # checking if 'XSCALE.LP' exists or not
        xscalelp_path = os.path.join(dir_name, "XSCALE.LP")
        if os.path.exists(xscalelp_path):
            print("Skipping the XSCALE")
            return True
    else:
        # Making a com file
        abs_dir = os.path.abspath(dir_name)
        comfile = os.path.join(abs_dir, "xds_auto.sh")
        ofile = open(comfile,"w")
        # ofile.write("#!/bin/csh\n\n")
        ofile.write("#$ -wd %s\n" % abs_dir)
        ofile.write("#$ -o %s\n"%abs_dir)
        ofile.write("#$ -e %s\n\n"%abs_dir)
        ofile.write("yamtbx.run_xscale\n")
        ofile.write("xds2mtz.py xscale.hkl\n")
        ofile.write("\rm -rf \*.cbf\n")

    # MTZ file generation
        ofile.close()
        os.system("chmod a+x %s" % comfile)
        os.system("qsub %s" % comfile)

def convertOnly(dir_name):
    # Making a com file
    abs_dir = os.path.abspath(dir_name)
    comfile = os.path.join(abs_dir, "f2mtz.sh")
    ofile = open(comfile,"w")
    ofile.write("#$ -wd %s\n" % abs_dir)
    ofile.write("#$ -o %s\n"%abs_dir)
    ofile.write("#$ -e %s\n\n"%abs_dir)
    ofile.write("xds2mtz.py xscale.hkl\n")

    # MTZ file generation
    ofile.close()
    os.system("chmod a+x %s" % comfile)
    os.system("qsub %s" % comfile)

# number of datasets in run_03 of the maximum number: cluster
# 199 1MGy
# 293 2MGy
# 251 5MGy
# 306 10MGy
# 279 20MGy

# number of merged datsets for each scaling
n_merge_list = [75]
# number of repetition for each scaling
n_sets = 10

for n_merge in n_merge_list:
    for i in range(0,n_sets):
        # Directory name
        dir_name = "merge%03d_index%02d" % (n_merge, i)
        # Reduce 1 for 'B-factor reference file'
        n_select = n_merge - 1
        proc_subsets = random.sample(input_files, n_select)
        make_xscale_out(dir_name,b_reference_file,proc_subsets,skipIfExist=True)
