run /isilon/BL32XU/BLsoft/PPPP/24.NABE/loadmtz.py
bg_color white
set ignore_case_chain
set mesh_width, 0.5
set ray_trace_fog, 1
set depth_cue, 1
set fog_start, 0.65
set ray_shadows, 0
set ray_opaque_background, 1

load /isilon/BL32XU/BLsoft/PPPP/24.NABE/Kohda/Models/190202_group2_all_refine_125.pdb, refined

#as stick
#as ribbon
#util.color_chains("elem C")
#set ribbon_color, red
util.chainbow("refined")

#/isilon/BL32XU/BLsoft/PPPP/24.NABE/Kohda/Models/190202_group2_all_refine_125.pdb, refined
print run_loadmtz("/isilon/BL32XU/BLsoft/PPPP/24.NABE/Kohda/Models/refine_001.mtz", "all", labels="FOFCWT,PHFOFCWT", isolevel=2.0)

python
print "DEBUG1"
print cmd.get_names()
print "DEBUG1"

while 1:
 if any(map(lambda x: "msh_" in x, cmd.get_names())):
  print "msh OK!"
  break
 print "waiting.."
 time.sleep(1)

python end

#color grey50, msh*
#color density, msh*
color green, msh*
hide everything, (org or refined)
show stick, (org or refined)

python
for c in "d":
 print cmd.set_view((\
     0.650046349,   -0.383833706,    0.655822873,\
    -0.708196044,    0.006858584,    0.705973148,\
    -0.275473922,   -0.923372805,   -0.267374724,\
    -0.000000000,    0.000000000,  -43.132709503,\
   -10.293999672,  -17.613000870,  -44.488998413,\
    40.435009003,   45.830417633,  -20.000000000 ))

 print cmd.clip("slab", 15)
 print cmd.png("test.5s_%s.png"%c, ray=1)

python end

print cmd.get_names()
quit

