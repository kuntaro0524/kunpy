run /oys/xtal/yamtbx/pymol_plugins/loadmtz.py
bg_color white
set ignore_case_chain
set mesh_width, 1
set ray_trace_fog, 0
set depth_cue, 0
set ray_shadows, 0

load /isilon/users/target/target/AutoUsers/190122/Toma/_kamoproc/merge_cc_pattern2_ligand2/cc_3.00A_final/cluster_0041/run_03/ccp4/TEST/refine_001.pdb, refined
as sticks
util.color_chains("elem C")

print run_loadmtz("refine_001.mtz", "all", labels="FOFCWT,PHFOFCWT", isolevel=2.5)

python
while 1:
 if any(map(lambda x: "msh_" in x, cmd.get_names())):
  print "msh OK!"
  break
 print "waiting.."
 time.sleep(1)

python end

#color grey50, msh*
color green, msh*
hide everything, (org or refined) and not resn LEU resi 778

python
for c in "efgh":
 print cmd.align("org and chain %s"%c, "refined and chain %s"%c)
 print cmd.zoom("chain %s and resn LEU resi 778"%c)
 print cmd.orient("chain %s and resn LEU resi 778"%c)
 print cmd.clip("slab", 5)
 print cmd.png("FCS_2.5s_%s.png"%c, ray=1)

python end

print cmd.get_names()
quit

