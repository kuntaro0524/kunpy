import subprocess
import pandas as pd
import os
import glob

#print subprocess
#output = subprocess.call(['qstat'],shell=True)
output = subprocess.check_output(['qstat'],shell=True)

# column 1: ID, 5: run status

lines = output.split("\n")
icount = 0

proc_dirs=[]
dics=[]
for line in lines:
    cols = line.split()

    #print(len(cols))
    if len(cols) == 9:
        job_id = int(cols[0])
        script = cols[2]
        command = "qstat -j %s" % job_id
        output2 = subprocess.check_output(command, shell = True)
        lines=output2.split('\n')
        for line in lines:
            if line.rfind("cwd")!=-1:
                cwd=line.split()[1]
                matome={'job_id':job_id, 'script': script, 'cwd': cwd}
                dics.append(matome)
        icount += 1

df = pd.DataFrame(dics)

# Refinement
sel_refine = df['script'].str.contains("refine")
refine_df=df[sel_refine]

def terminate_id(id_num):
    command = "qdel %d" % id_num
    output2 = subprocess.check_output(command, shell = True)
    print(output2)

for index,ed in refine_df.iterrows():
    proc_dir=ed['cwd']
    print(proc_dir)
    # PDB file generation
    pdb_path=os.path.join(proc_dir, "mr_001.pdb")
    if os.path.exists(pdb_path):
        print("PDB is okay: %s"%pdb_path)
        terminate_id(ed['job_id'])

# SHELX
sel_refine = df['script'].str.contains("shelx")
refine_df=df[sel_refine]

def check_shelxe_ends(logfile):
    lines=open(logfile,"r").readlines()
    for line in lines:
        if line.rfind("SHELXE finished")!=-1:
            return True
    return False

for index,ed in refine_df.iterrows():
    proc_dir=ed['cwd']
    shelxe_logs = glob.glob("%s/shelxe*.log"%proc_dir)
    ok_count=0
    for shelxe_log in shelxe_logs:
        if check_shelxe_ends(shelxe_log)==True:
            ok_count+=1
        else:
            print("%s : not done" % shelxe_log)
            continue
    if ok_count==2:
        print("%s is okay to be terminated." % proc_dir)
        terminate_id(ed['job_id'])
