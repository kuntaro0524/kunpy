from doctest import master
import h5py,glob
from yamtbx.dataproc import eiger
from yamtbx.dataproc import cbf
from yamtbx.dataproc.XIO.plugins import eiger_hdf5_interpreter
from libtbx import easy_mp
from PIL import Image
import PIL.ImageOps 
import numpy,os
import glob

class ManH5:
    def __init__(self):
        self.isPrep = False
        self.nproc = 180

    def setNproc(self, nproc):
        self.nproc = nproc

    def readHeader(self, masterfile):
        h = eiger_hdf5_interpreter.Interpreter().getRawHeadDict(masterfile)
        #h5 = h5py.File(masterfile, "r")
        return h

    def miyasuiCBF(self, masterfile, imgindex, thresh=1.0):
        sel = None
        tmp = eiger.extract_data(masterfile, imgindex, apply_pixel_mask=False)
        data = tmp

        mean_value = tmp.mean()
        sel = tmp < mean_value * thresh

        data[sel] = 0

        cbfname="out.cbf"
        self.writeCBF(masterfile, cbfname, data)

    def writeCBF(self, masterfile, cbfname, bodydata): 
        h = eiger_hdf5_interpreter.Interpreter().getRawHeadDict(masterfile)
        h5 = h5py.File(masterfile, "r")

        h["Detector"] = "PILATUS3 6M"
        h["SerialNumber"] = "60-0125"
        h["ExposurePeriod"] = h5["/entry/instrument/detector/frame_time"].value
        cbf.save_numpy_data_as_cbf(bodydata.flatten(),
                                   size1=bodydata.shape[1],
                                   size2=bodydata.shape[0],
                                   title="Extracted image",
                                   cbfout=cbfname,
                                   pilatus_header="""
# Detector: %(Detector)s, S/N %(SerialNumber)s
# Pixel_size %(PixelX)e m x %(PixelY)e m
# %(SensorMaterial)s sensor, thickness %(SensorThickness).3e m
# Exposure_time %(ExposureTime).6f s
# Exposure_period %(ExposurePeriod).6f s
# Count_cutoff %(Overload)d counts
# Wavelength %(Wavelength).6f A
# Detector_distance %(Distance).3e m
# Beam_xy (%(BeamX).1f, %(BeamY).1f) pixels
# Start_angle %(PhiStart).6f deg.
# Angle_increment %(PhiWidth).6f deg.
        """ % h)


    def sumUpList2(self, param_list):
        masterfile, sumnum_list, start_phi, osc_phi, out_image = param_list
        data = None
        sel = None

        for imgindex in sumnum_list:
            print("Reading %s %05d" % (masterfile, imgindex))
            tmp = eiger.extract_data(masterfile, imgindex, apply_pixel_mask=False)

            if data is None:
                data = tmp
            else:
                data += tmp

        if sel is None:
            sel = tmp < 0
        else:
            sel |= tmp < 0

        data[sel] = -10

        h = eiger_hdf5_interpreter.Interpreter().getRawHeadDict(masterfile)
        h5 = h5py.File(masterfile, "r")

        h['PhiStart'] = start_phi
        h['PhiWidth'] = osc_phi

        h["Detector"] = "PILATUS3 6M"
        h["SerialNumber"] = "60-0125"
        h["ExposurePeriod"] = h5["/entry/instrument/detector/frame_time"].value
        cbf.save_numpy_data_as_cbf(data.flatten(),
                                   size1=data.shape[1],
                                   size2=data.shape[0],
                                   title=out_image,
                                   cbfout=out_image,
                                   pilatus_header="""
# Detector: %(Detector)s, S/N %(SerialNumber)s
# Pixel_size %(PixelX)e m x %(PixelY)e m
# %(SensorMaterial)s sensor, thickness %(SensorThickness).3e m
# Exposure_time %(ExposureTime).6f s
# Exposure_period %(ExposurePeriod).6f s
# Count_cutoff %(Overload)d counts
# Wavelength %(Wavelength).6f A
# Detector_distance %(Distance).3e m
# Beam_xy (%(BeamX).1f, %(BeamY).1f) pixels
# Start_angle %(PhiStart).6f deg.
# Angle_increment %(PhiWidth).6f deg.
        """ % h)

    # Check illegal pixels on a master file
    def countBadpix(self, master_file, imgnum):
        data = None
        sel = None

        tmp = eiger.extract_data(master_file, imgnum, apply_pixel_mask=False)
        dim=tmp.shape[0]*tmp.shape[1]

        if data is None:
            data = tmp
        else:
            data += tmp

        if sel is None:
            sel = tmp < 0
        else:
            sel |= tmp < 0

        selnum=numpy.count_nonzero(tmp <= -3)
        # bad percentage
        bad_per=float(selnum)/float(dim)*100.0
        if bad_per > 7.0:
            print("file=%s %5d : %5.2f perc." %(master_file, imgnum, float(selnum)/float(dim)*100.0))

    # Simple summation of pixel values
    def countTotalPixels(self, master_file, imgnum):
        #print("Processing",master_file,imgnum)

        tmp = eiger.extract_data(master_file, imgnum, apply_pixel_mask=False)
        #dim=tmp.shape[0]*tmp.shape[1]
        # For debugging
        isDebug=False
        if isDebug:
            for x in range(0,tmp.shape[0]):
                for y in range(0, tmp.shape[1]):
                    if tmp[x,y] < 0:
                        print("%5d %5d %10d"%(x,y,tmp[x,y]))

        # selection on 'valid' pixels
        sel = tmp > 0
        selected_pixs = tmp[sel]

        total_score=numpy.sum(selected_pixs)

        return total_score

    def countTotalMP(self, param_list):
        master_file, imgnum = param_list
        count=self.countTotalPixels(master_file, imgnum)
        return(imgnum, count)

    def countInH5(self, masterfile, nimages):
        procparams = []
        # n_sum_time : the number of summation time
        for junk_index in range(1, nimages+1):
            procparams.append((masterfile, junk_index))

        # Debugging
        isDebug=False
        if isDebug:
            for p in procparams:
                masterfile, iindex=p
                score=self.countTotalPixels(masterfile, iindex)

        results = easy_mp.pool_map(fixed_func=lambda n: self.countTotalMP(n), args=procparams, processes=self.nproc)

        # PREFIX
        prefix = masterfile.replace(".h5","")

        ofile=open("%s.dat"%prefix,"w")
        for result in results:
            imgnum, score = result
            ofile.write("%5d %10d\n"%(imgnum, score))

        return results

    def generateSingle(self, masterfile, imgidx, out_image):
        tmp = eiger.extract_data(masterfile, imgidx, apply_pixel_mask=False)
        # Data body and select boolean list
        data = None
        sel = None

        if data is None:
            data = tmp
        else:
            data += tmp

        if sel is None:
            sel = tmp < 0
        else:
            sel |= tmp < 0

        data[sel] = -10

        h = eiger_hdf5_interpreter.Interpreter().getRawHeadDict(masterfile)
        h5 = h5py.File(masterfile, "r")

        h['PhiStart'] = 0.0
        h['PhiWidth'] = 0.25
<<<<<<< HEAD
        print("parametrs = " , h)
=======
        #print "parametrs = " , h
>>>>>>> origin/master

        h["Detector"] = "PILATUS3 6M"
        h["SerialNumber"] = "60-0125"
        h["ExposurePeriod"] = h5["/entry/instrument/detector/frame_time"].value
        cbf.save_numpy_data_as_cbf(data.flatten(),
                                   size1=data.shape[1],
                                   size2=data.shape[0],
                                   title=out_image,
                                   cbfout=out_image,
                                   pilatus_header="""
# Detector: %(Detector)s, S/N %(SerialNumber)s
# Pixel_size %(PixelX)e m x %(PixelY)e m
# %(SensorMaterial)s sensor, thickness %(SensorThickness).3e m
# Exposure_time %(ExposureTime).6f s
# Exposure_period %(ExposurePeriod).6f s
# Count_cutoff %(Overload)d counts
# Wavelength %(Wavelength).6f A
# Detector_distance %(Distance).3e m
# Beam_xy (%(BeamX).1f, %(BeamY).1f) pixels
# Start_angle %(PhiStart).6f deg.
# Angle_increment %(PhiWidth).6f deg.
        """ % h)

if __name__ == "__main__":
    import sys
    print("Starting!")
    mh5=ManH5()
    print("Starting medium")
    mydic=mh5.readHeader(sys.argv[1])
    print(mydic)
    nimages =mydic['Nimages']
    print(mydic['Nimages'],mydic['Ntrigger'],mydic['Nimages_each'])

    #print(mh5.countTotalPixels(sys.argv[1],100))
    #mh5.countInH5(sys.argv[1], nimages)
    #mh5.generateSingle(sys.argv[1], 100, "out.cbf")

    image_num = int(sys.argv[2])
    thresh=float(sys.argv[3])
    mh5.miyasuiCBF(sys.argv[1], image_num, thresh)

    """
    print("Checking %s from now..." % sys.argv[1])
    for i in range(1,3601):
        master_file=sys.argv[1]
        mh5.countBadpix(master_file, i)
    """
