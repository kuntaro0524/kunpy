import os,sys
import numpy as np
import DiffscanLog
import ManH5

# Usage
# arguments
# argv[1] : scan master file.
# argv[2] : diffscan_path where 'diffscan.log' exists.

if __name__ == "__main__":
    mh5=ManH5.ManH5()
    h5file = sys.argv[1]

    mydic=mh5.readHeader(h5file)
    nimages =mydic['Nimages']
    results = mh5.countInH5(sys.argv[1], nimages)

    diffscan_path = sys.argv[2]

    cols=np.zeros(nimages+1)
    for result in results:
        imgnum, score = result
        cols[imgnum]=score

    dl = DiffscanLog.DiffscanLog(diffscan_path)
    dl.prep()
    # NDarray (V x H) map from diffscan.log
    # The 1st scan result will be analyzed
    dlmap = dl.getNumpyArray(scan_index=0)
    nv, nh = dl.getScanDimensions()
    print("Vertical  =", nv)
    print("Horizontal=", nh)
    # in [mm] steps
    v_step, h_step = dl.getScanSteps()
    print(v_step, h_step)
    
    nimages_all = nv * nh
    
    # Checking the heat map
    prefix=h5file.replace(".h5","")
    ofile=open("%s_heatmap.csv"%prefix,"w")
    new_list = []
    for v in range(0, nv):
        for h in range(0, nh):
            # diffscan.log map
            scanindex, imgnum, x, y, z = dlmap[v, h]
            score = cols[int(imgnum)]
            ofile.write("%s,%d,%8.4f,%8.4f,%8.4f,%4d,%4d,%5d\n"%(int(scanindex),int(imgnum),x,y,z,v,h,score))
