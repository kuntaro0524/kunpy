import os, sys
#sys.path.append("/Users/kuntaro/kundev/kunpy/Libs/")
import Libs.DirectoryProc as DP

print("Usage: args[1]=path_to_copy args[2]=hdd_limit_size [TB]")
path_to_copy=sys.argv[1]
# convert TB to GB
hdd_limit_size = float(sys.argv[2]) * 1000.0

def get_size_dir(path='.', skip_dirs=False):
    total_size = 0
    file_list = []
    dir_list = []
    for dir_path in os.listdir(path):
        full_path = os.path.join(path, dir_path)
        if os.path.isfile(full_path):
            total_size += os.path.getsize(full_path)
        elif os.path.isdir(full_path) and skip_dirs==False:
            dir_size = get_size_dir(full_path)
            # print("%s %s" % (full_path, dir_size))
            total_size += dir_size
    return total_size

# size_of_dir=get_size_dir('.')
# gbu = size_of_dir/1E9
# print("Data = %5.1f GB" % gbu)
dp = DP.DirectoryProc("./")
dirlist=dp.getDirList()
print(type(dirlist))

total_size = 0.0 # unit (GB)
size_list = []
sorted_list = dirlist.sort()
for d in dirlist:
    size_dir=get_size_dir(d)/1E9 # unit (GB)
    # print(d, size_dir)
    size_list.append(size_dir)
    total_size+=size_dir

# print(len(size_list), len(dirlist))
size_dir = get_size_dir("./", skip_dirs=True) / 1E9
total_size+=size_dir
# print("Current directory=", size_dir)
dirlist.append("./")
size_list.append(size_dir)
print("TOTAL=",total_size)

n_disk = 0
curr_size = 0.0 # GB
hdd_list = []
each_hdd = []
for dname, dsize in zip(dirlist, size_list):
    curr_size += dsize
    if curr_size > hdd_limit_size:
        print("Change HDD")
        hdd_list.append(each_hdd)
        each_hdd=[]
        n_disk += 1
        curr_size = dsize
    each_hdd.append((dname,dsize))
    print(dsize, dname, curr_size)

# When the size of data did not exceed the HDD volume
if curr_size < hdd_limit_size:
    hdd_list.append(each_hdd)

ofile = open("hdd_backup.txt", "a")
rfile = open("rsync.sh", "w")
print("Making RSYNC command")
print(hdd_list)
index=0
for each_hdd_list in hdd_list:
    #print(each_hdd_list)
    command="rsync -auv "
    ofile.write("HDD-%03d\n"%index)
    for (fname, dsize) in each_hdd_list:
        command+="%s "%fname
        ofile.write("%s %s\n"%(fname,dsize))
    command+= " %s" % path_to_copy
    rfile.write("%s\n"%command)
    index+=1

rfile.close()
ofile.close()

# print(hdd_list)
# get_size_dir()
# print("%5.1f GB"% (get_dir_size_old('./')))
# # 56130856
