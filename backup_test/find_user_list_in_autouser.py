import os

import Libs.DirectoryProc as D
ddd=D.DirectoryProc("./")

dlist = ddd.getDirList()

user_list = []
for d in dlist:
    ppp = D.DirectoryProc(d)
    new_list = ppp.getDirList()
    for nd in new_list:
        relpath = os.path.relpath(nd, d)
        if relpath.rfind("CPS")!=-1:
            continue
        user_list.append(relpath)

print(set(user_list))
