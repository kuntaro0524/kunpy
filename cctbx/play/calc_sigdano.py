"""
To check if peak of DANO map is due to model bias or not.
Pass output mtz file of phenix.refine to this script.
And then run FFT.
Can you still see peak of anomalous scatterers?
"""

import sys
import iotbx.mtz
from cctbx.array_family import flex
import numpy
from iotbx import file_reader

def run(hklin):
    hkl_in = file_reader.any_file(hklin, force_type="hkl")
    hkl_in.check_file_type("hkl")
    anom_data = []

    for array in hkl_in.file_server.miller_arrays :
       if (not array.anomalous_flag()) : continue
       if (array.is_xray_amplitude_array()) or (array.is_xray_intensity_array()):
           anom_data.append(array)

    #arrays = iotbx.any_file(hklin).as_miller_arrays()
    i_obs = filter(lambda x:x.is_xray_intensity_array() and x.anomalous_flag(), anom_data)[0]
    print "  I-obs:", i_obs.info()

    # Original DANO
    f_obs = i_obs.french_wilson()
    dano = f_obs.anomalous_differences()
    #print "DANO=", dano.info()
    dano = dano.customized_copy(data=dano.data())

    #dano = dano_model

    dano_max = flex.max(flex.abs(dano.data()))
    print "MAX dano=", dano_max

    sigma_dano = flex.sum(flex.pow2(dano.data()-flex.mean(dano.data()))) / dano.size()
    print "SIGMA of DANO:", sigma_dano

    #for hkl,I,sigI in dano:
    #    print(hkl,I,sigI)

    #random = dano.customized_copy(data=flex.double(numpy.random.normal(size=dano.size(), loc=0, scale=sigma_dano)), sigmas=None)

if __name__ == "__main__":
    run(sys.argv[1])
