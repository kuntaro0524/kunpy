"""
Usage:
 phenix.python dano_vs_d.py your.sca 20
"""

import iotbx.file_reader
from cctbx.array_family import flex
import numpy as np
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))

def run(hklin, n_bins=40):
    for array in iotbx.file_reader.any_file(hklin).file_server.miller_arrays:
        # skip if not anomalous intensity data
        if not (array.is_xray_intensity_array() and array.anomalous_flag()):
            print
            "skipping", array.info()
            continue

        # take anomalous differences
        dano = array.anomalous_differences()

        # HKL, anomalous_signal, sigma_anomalous
        aa=[]
        ia=[]
        sa=[]
        for dano_data in dano:
            at, it, st=dano_data
            # print(at,it,st)
            aa.append(at)
            ia.append(it)
            sa.append(st)

        # Convert list to ndarray
        hkl_a=np.array(aa)
        intensty_a=np.array(ia)
        sigma_a=np.array(sa)
        # Number of data
        ndata = len(hkl_a)
        plotdata=intensty_a/float(ndata)

        # Drawing 'histogram'
        bin_heights, bin_borders, _ = plt.hist(plotdata, bins='auto', label='histogram')
        bin_centers = bin_borders[:-1] + np.diff(bin_borders) / 2
        # Fitting the
        popt, _ = curve_fit(gaussian, bin_centers, bin_heights, p0=[0., 1000, 1.])
        mu, amplitude, sigma = popt
        print(hklin, mu,amplitude,np.fabs(sigma))
        

        # Plotting the fitted curve of sigma function
        x_interval_for_fit = np.linspace(bin_borders[0], bin_borders[-1], 10000)
        plt.plot(x_interval_for_fit, gaussian(x_interval_for_fit, *popt), label='sigma=%5.3f'%sigma)
        plt.legend()

        plt.savefig("test.png")

# run()

if __name__ == "__main__":
    import sys

    hklin = sys.argv[1]
    # n_bins = int(sys.argv[2])
    run(hklin)
