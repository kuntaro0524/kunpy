import sys
import iotbx.mtz
from cctbx.array_family import flex
import numpy
from iotbx import file_reader

def run(hklin):
    hkl_in = file_reader.any_file(hklin, force_type="hkl")
    hkl_in.check_file_type("hkl")
    anom_data = []

    for array in hkl_in.file_server.miller_arrays :
       #print("anomalous=", array.anomalous_flag())
       if (not array.anomalous_flag()) : continue
       if (array.is_xray_amplitude_array()) or (array.is_xray_intensity_array()):
           anom_data.append(array)

    i_obs = filter(lambda x:x.is_xray_intensity_array() and x.anomalous_flag(), anom_data)[0]
    #print "  I-obs:", i_obs.info()

    get_I_arrays=lambda x: filter(lambda y: y.is_xray_intensity_array(), x)

    # Original DANO
    f_obs = i_obs.french_wilson()
    dano = f_obs.anomalous_differences()
    #print "DANO=", dano.info()
    dano = dano.customized_copy(data=dano.data())

    #dano = dano_model

    dano_max = flex.max(flex.abs(dano.data()))
    #print "MAX dano=", dano_max

    # Really random DANO
    sigma_dano = flex.sum(flex.pow2(dano.data()-flex.mean(dano.data()))) / dano.size()
    return dano_max, sigma_dano

if __name__ == "__main__":
    dano_max, sigma_dano=run(sys.argv[1])
    print("%s , max=%8.4f, sigdano=%8.4f" % (sys.argv[1],dano_max,sigma_dano))
