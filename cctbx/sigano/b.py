from __future__ import absolute_import, division, print_function
import wxtbx.plots
from libtbx.utils import Sorry
import wx
import os
import sys

# DEBUGGING
import inspect

def display_file_info(file_name, obs_type="amplitudes", parent=None,
    n_bins=40, out=sys.stdout):
  from iotbx import file_reader
  hkl_in = file_reader.any_file(file_name, force_type="hkl")
  hkl_in.check_file_type("hkl")
  anom_data = []
  for array in hkl_in.file_server.miller_arrays :
    if (not array.anomalous_flag()) : continue
    if (array.is_xray_amplitude_array()) or (array.is_xray_intensity_array()):
      anom_data.append(array)
  if (len(anom_data) == 0):
    raise Sorry("No anomalous data arrays found.")
  x_label = "D_anom(F) / <F>"
  if (obs_type == "intensities"):
    x_label = "D_anom(I) / <I>"
  index=0
  for array in anom_data :
    print("Test Index=%d" % index,type(array))
    array_name = os.path.basename(file_name)+":"+array.info().label_string()
    print(inspect.getargspec(array.bijvoet_ratios))
    #array.show_array()
    index+=1

display_file_info(sys.argv[1])
