#!/usr/bin/env python
# coding: utf-8

import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
from scipy.stats import norm
import scipy
from scipy.optimize import curve_fit


files=sys.argv[1:]

print(files)

# Gaussian function for fitting
def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))

for infile in files:
    df1=pd.read_csv(infile,delim_whitespace=True, header=None)
    fig = plt.figure(dpi=400,figsize=(4,3))
    ax = fig.add_subplot(1,1,1)
    # Bins
    bin_heights, bin_borders, _ = plt.hist(df1[3], bins='auto', label=infile)

    bin_centers = bin_borders[:-1] + np.diff(bin_borders) / 2
    popt, _ = curve_fit(gaussian, bin_centers, bin_heights, p0=[1.,1000, 100.])

    print("%s popt=%s" % (infile, popt))

    x_interval_for_fit = np.linspace(bin_borders[0], bin_borders[-1], 10000)
    plt.plot(x_interval_for_fit, gaussian(x_interval_for_fit, *popt), label='fit')
    plt.legend()
    plt.savefig("%s.png"%infile)

