"""
Usage:
 phenix.python dano_vs_d.py your.sca 20
"""

import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import curve_fit
import iotbx.file_reader
from cctbx.array_family import flex
import numpy as np


def func(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))

def run(hklin, n_bins):
    for array in iotbx.file_reader.any_file(hklin).file_server.miller_arrays:
        # skip if not anomalous intensity data
        if not (array.is_xray_intensity_array() and array.anomalous_flag()):
            print "skipping", array.info()
            continue

        # We assume that data is already merged
        assert array.is_unique_set_under_symmetry()

        # take anomalous differences
        dano = array.anomalous_differences()
        dano_sn = dano.data()/dano.sigmas()

        # histogram
        hist_1, bins = np.histogram(dano_sn, 100, range(-10,10))
        bins=bins[:-1]

        param_ini=[80,100,5]
        popt, pcov = curve_fit(func, bins, hist_1, p0=param_ini)

        fig, ax = plt.subplots()
        ax.plot(bins, fitting, 'k')

        plt.show()

# run()

if __name__ == "__main__":
    import sys
    hklin = sys.argv[1]
    n_bins = int(sys.argv[2])
    run(hklin, n_bins)
