  
"""
Usage:
 phenix.python dano_vs_d.py your.sca 20
"""

import iotbx.file_reader
from cctbx.array_family import flex

def run(hklin, n_bins):
    for array in iotbx.file_reader.any_file(hklin).file_server.miller_arrays:
        # skip if not anomalous intensity data
        if not (array.is_xray_intensity_array() and array.anomalous_flag()):
            print "skipping", array.info()
            continue

        # We assume that data is already merged
        assert array.is_unique_set_under_symmetry()

        # take anomalous differences
        dano = array.anomalous_differences()

        #print("Array info")
        #print(array.info(),array.info().label_string())
        #print("INFO")
        #print(dano.data())

        dano_sn = dano.data()/dano.sigmas()

        for d,s in zip(dano.data(),dano.sigmas()):
            print(d,s)

        #sigma_dano = flex.sum(flex.pow2(dano.data()-flex.mean(dano.data()))) / dano.size()
        #print(sigma_dano)

# run()

if __name__ == "__main__":
    import sys
    hklin = sys.argv[1]
    n_bins = int(sys.argv[2])
    run(hklin, n_bins)
